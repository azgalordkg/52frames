<?php

namespace App;

use Carbon\Carbon;
use App\Services\Helpers;
use App\Services\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AlbumController;

class Album extends Model
{
    protected $fillable = [
        'imported_from_row_id',
        'imported_info',
        'top1_photo_id',
        'top2_photo_id',
        'top3_photo_id',
        'top4_photo_id',
        'top5_photo_id',
        'top6_photo_id',
        'patreon_photo_id',
        'flier_photo_id',
        'publicly_visible',
        'live',
        'year',
        'week_number',
        'start_date',
        'end_date',
        'theme_title',
        'shorturl',
        'blurb',
        'short_blurb',
        'extra_credit_title',
        'extra_credit_body',
        'tips_body',
        'tags',
        'upload_custom_msg',
    ];

    protected $hidden = [
        'imported_from_row_id',
        'cover_photo_id',
        'cover_photo_data',
        'sample_photo_id',
        'sample_photo_data',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'cover_photo_data' => 'array',
        'sample_photo_data' => 'array',
        'imported_info' => 'array'
    ];

    public function getIsCurrentAlbumAttribute()
    {
        $timezone = env('APP_TIMEZONE');
        $albumStartDate = Carbon::parse($this->start_date, $timezone);
        return $albumStartDate->timestamp == Album::getThisWeekChallengeStartDay()->timestamp;
    }

    public function getCoverPhotoAttribute(): ?array
    {
        $photoId = $this->live && $this->top1_photo_id ? $this->top1_photo_id : $this->cover_photo_id;
        if ($photoId) {
            $photoQuery = Photo::whereId($photoId);
            if ($this->live) return [
                'using_temp_cover_photo' => false,
                'photo' => $photoQuery->with(['owner'])->first()->toArray()
            ];
            else return [
                'using_temp_cover_photo' => true,
                'extra_data' => $this->cover_photo_data ?: [],
                'photo' => $photoQuery->first()->toArray()
            ];
        }
        return null;
    }

    public function getTempCoverPhotoAttribute(): ?array
    {
        if ($this->cover_photo_id) {
            $photo = Photo::whereId($this->cover_photo_id)->first();
            $otherColumns = Helpers::filterByKeysModel($photo, [
                'filename'
            ]);
            return [
                'extra_data' => $this->cover_photo_data ?: [],
                'photo' => array_merge($photo->toArray(), $otherColumns)
            ];
        }
        return null;
    }

    public function getSamplePhotoAttribute(): ?array
    {
        if ($this->sample_photo_id) {
            $photo = Photo::whereId($this->sample_photo_id)->first();
            $otherColumns = Helpers::filterByKeysModel($photo, [
                'filename'
            ]);
            return [
                'extra_data' => $this->sample_photo_data ?: [],
                'photo' => array_merge($photo->toArray(), $otherColumns)
            ];
        }
        return null;
    }

    public static function insertRules()
    {
        return [
            'year' => 'required|integer',
            'week_number' => 'required|integer',
            'start_date' => 'required|date_format:Y-m-d|before:end_date',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'theme_title' => 'required|string',
            'shorturl' => [
                'required',
                'string',
                'min:3',
                'regex:/^[a-z0-9][-a-z.0-9_~]+$/'
            ],
            'blurb' => 'required|string',
            'short_blurb' => 'nullable|string',
            'temp_cover_photo' => 'array',
            'temp_cover_photo.file' => 'required|mimes:jpeg,png|max:5120',
            'temp_cover_photo.owner_name' => 'required|string',
            'temp_cover_photo.album_theme' => 'required|string',
            'temp_cover_photo.album_week_number' => 'required|integer',
            'temp_cover_photo.url' => 'nullable|url',
            'sample_photo' => 'array',
            'sample_photo.file' => 'required|mimes:jpeg,png|max:5120',
            'sample_photo.owner_name' => 'required|string',
            'sample_photo.album_theme' => 'required|string',
            'sample_photo.album_week_number' => 'required|integer',
            'sample_photo.url' => 'required|url',
            'extra_credit_title' => 'required_with:extra_credit_body|nullable|string',
            'extra_credit_body' => 'required_with:extra_credit_title|nullable|string',
            'tips_body' => 'nullable|string',
            'tags' => 'array|max:10',
            'tags.*' => [
                'max:30',
                'regex:/^#?[a-zA-z0-9]+$/',
            ]
        ];
    }

    public static function csvImportRules()
    {
        $rules = self::insertRules();

        return Helpers::filterByKeysArray($rules, [
            'year',
            'week_number',
            'start_date',
            'end_date',
            'theme_title',
            'shorturl',
            'extra_credit_title'
        ]);
    }

    public static function getSummary(Album $album): array
    {
        $showTheseColumns = [
            'id',
            'publicly_visible',
            'live',
            'year',
            'week_number',
            'start_date',
            'end_date',
            'theme_title',
            'shorturl',
            'extra_credit_title',
            'upload_custom_msg',
        ];
        $output = [];
        foreach ($showTheseColumns as $column) {
            $output = array_merge($output, [
                $column => $album->$column
            ]);
        }
        return $output;
    }

    /**
     * Gets the current week's album.
     * @return array|null
     */
    public static function currentWeek(): ?array
    {
        $appTz = env('APP_TIMEZONE');
        $today = Carbon::now($appTz);
        $thisWeekChallengeStartDay = self::getThisWeekChallengeStartDay();
        $album = Album::where('start_date', '<=', $today)
            ->where('start_date', '>=', $thisWeekChallengeStartDay)
            ->first();
        return $album ? self::getSummary($album) : null;
    }

    public static function getThisWeekChallengeStartDay(): Carbon
    {
        $appTz = env('APP_TIMEZONE');
        $today = Carbon::now($appTz);
        $albumStartDayNumberFromSunday = env('APP_ALBUM_STARTS_ON_WHICH_WEEK_DAY_NUM', Carbon::MONDAY);
        Carbon::setWeekStartsAt((int) $albumStartDayNumberFromSunday);
        $startOfWeek = $today->startOfWeek();
        return $startOfWeek;
    }

    /**
     * Gets the current week's album.
     * @param Request $request
     * @return array|null
     */
    public static function getRecent(Request $request): ?array
    {
        $albumController = new AlbumController();
        $recentAlbumsResponse = $albumController->getRecentAlbums($request);
        if (!is_array($recentAlbumsResponse)) return Transport::returnNow($recentAlbumsResponse);
        return Helpers::filterByKeysArray($recentAlbumsResponse, [
            'album_current_week',
            'upcoming_albums',
            'previous_albums',
            'photos_by_framers_you_follow'
        ]);
    }

    public static function getPhotoIdsNotForDisplay(Album $album): array
    {
        $output = [];
        $columns = [
            'cover_photo_id',
            'sample_photo_id'
        ];
        foreach ($columns as $column) {
            if ($album->$column) $output[] = $album->$column;
        }
        return $output;
    }

    public static function getNumberOfPhotos(Album $album): int
    {
        return $album->photos()->whereNotIn('id', self::getPhotoIdsNotForDisplay($album))->count();
    }

    public static function generateWeekShortPublicUrl(Album $album)
    {
        $weekNumber = $album->week_number;
        $shorturl = $album->shorturl;
        return "week-$weekNumber-$shorturl";
    }

    public static function generatePublicUrl(Album $album)
    {
        $year = $album->year;
        $weekShortUrl = self::generateWeekShortPublicUrl($album);
        return "/albums/$year/$weekShortUrl";
    }

    public static function generateChallengePagePublicUrl(Album $album)
    {
        $albumUrl = self::generatePublicUrl($album);
        return "$albumUrl/challenge";
    }

    /**
     * @param Album $album
     * @return Album|null|static
     */
    public static function getNextAlbum(Album $album)
    {
        $nextAlbumStartDate = Carbon::parse($album->start_date)->addWeek();
        return Album::where('start_date', $nextAlbumStartDate)->first();
    }

    public static function getUpcomingAlbums(int $howMany = null): array
    {
        $albums = [];

        $maxUpcomingAlbumsCount = is_null($howMany) ? env('APP_UPCOMING_ALBUMS_COUNT', 1) : $howMany;
        $thisWeekChallengeStartDay = self::getThisWeekChallengeStartDay();
        $nextWeekStartDay = $thisWeekChallengeStartDay->addWeek();

        $upcomingAlbums = Album::where('start_date', '>=', $nextWeekStartDay)
            ->orderBy('start_date', 'asc')
            ->limit($maxUpcomingAlbumsCount)
            ->get();

        foreach ($upcomingAlbums as $album) {
            $coverPhoto = Helpers::filterByKeysModel($album, [
                'cover_photo'
            ]);
            $albumArr = array_merge(self::getSummary($album), $coverPhoto, [
                'start_date_utc' => self::getStartDateUtc($album)
            ]);
            array_push($albums, $albumArr);
        }

        return $albums;
    }

    public static function getPreviousAlbums(int $howMany = null, bool $withWinners = false): array
    {
        $albums = [];

        $maxUpcomingAlbumsCount = is_null($howMany) ? env('APP_UPCOMING_ALBUMS_COUNT', 1) : $howMany;
        $thisWeekChallengeStartDay = self::getThisWeekChallengeStartDay();

        $previousAlbums = Album::where('start_date', '<', $thisWeekChallengeStartDay)
            ->orderBy('start_date', 'desc')
            ->limit($maxUpcomingAlbumsCount)
            ->get();

        $user = Auth::guard('api')->user();

        foreach ($previousAlbums as $album) {
            $userUploaded = $user ? Photo::whereAlbumId($album->id)->whereUserId($user->id)->count() > 0 : false;
            if ($withWinners) {
                $otherColumns = [
                    'winners' => array_values(Helpers::getTopPhotos($album, 3))
                ];
            } else {
                $otherColumns = Helpers::filterByKeysModel($album, [
                    'cover_photo'
                ]);
            }
            $albumArr = array_merge(self::getSummary($album), $otherColumns, [
                'user_uploaded' => $userUploaded
            ]);
            array_push($albums, $albumArr);
        }

        return $albums;
    }

    public static function getStartDateUtc(Album $album)
    {
        $appTimezone = env('APP_TIMEZONE');
        return Carbon::parse($album->start_date, $appTimezone)->toW3cString();
    }

    public static function getEndDateUtc(Album $album)
    {
        $appTimezone = env('APP_TIMEZONE');
        return Carbon::parse($album->end_date, $appTimezone)->addDay()->toW3cString();
    }

    public static function randomFramersYouFollow(User $user = null): array
    {
        if (!$user) return [];

        $influencers = Follower::whereFollowerUserId($user->id)
            ->inRandomOrder()
            ->get();

        return $influencers->toArray();
    }

    public static function getPhotosOfFramersById(int $albumId, array $framersIds, int $maxPhotosCount)
    {
        $result = [];
        foreach ($framersIds as $framerId) {
            $photo = Photo::whereUserId($framerId)
                ->whereAlbumId($albumId)
                ->with('owner')
                ->first();
            if ($photo) {
                $result[] = $photo->toArray();
                if (count($result) >= $maxPhotosCount) break;
            }
        }
        return $result;
    }

    /**
     * @param Album $album
     * @return array
     */
    public static function getNewPhotographersIds(Album $album): array
    {
        $ids = [];
        $photos = $album->photos;
        /** @var Photo $photo */
        foreach ($photos as $photo) {
            $ownerId = $photo->owner->id;
            if (true === User::isNewbie($ownerId)) {
                $ids[] = $ownerId;
            }
        }

        return $ids;
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function sorting()
    {
        $this->hasMany(AlbumSort::class, 'album_id', 'id');
    }

    public function migratedFromCsvRow()
    {
        return $this->belongsTo(CsvUploadedRow::class, 'imported_from_row_id', 'id');
    }

}
