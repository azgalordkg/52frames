<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use Closure;
use Illuminate\Http\Request;

class FilterExtraCredit
{
    public const FILTER_EXTRA_CREDIT = 'extra_credit';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        if ($request->filter_by === self::FILTER_EXTRA_CREDIT)
        {
            $photosQuery->where('photos.qualifies_extra_credit', true);
        }

        return $photosQuery;
    }
}
