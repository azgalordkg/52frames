<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use App\Album;
use Closure;
use Illuminate\Http\Request;

class FilterNewbies
{
    public const FILTER_NEWBIES = 'new';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $album = Album::find($request->album_id);

        if ($request->filter_by === self::FILTER_NEWBIES && null !== $album)
        {
            $newPhotographersIds = Album::getNewPhotographersIds($album);
            $photosQuery->whereIn('photos.user_id', $newPhotographersIds);
        }

        return $photosQuery;
    }
}
