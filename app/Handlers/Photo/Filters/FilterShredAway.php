<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use App\Album;
use Closure;
use Illuminate\Http\Request;

class FilterShredAway
{
    public const FILTER_SHRED_AWAY= 'shred_away';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        if ($request->filter_by === self::FILTER_SHRED_AWAY)
        {
            $photosQuery->where('photos.critique_level', '=', 'shred_away');
        }

        return $photosQuery;
    }
}
