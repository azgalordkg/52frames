<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilterMyPhoto
{
    public const FILTER_MY_PHOTO = 'my_photo';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $user = Auth::guard('api')->user();

        if ($request->filter_by === self::FILTER_MY_PHOTO && null !== $user)
        {
            $photosQuery->where('photos.user_id', $user->id);
        }

        return $photosQuery;
    }
}
