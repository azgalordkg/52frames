<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use App\Album;
use Closure;
use Illuminate\Http\Request;

class FilterStaffPicks
{
    public const FILTER_STAFF_PICKS = 'staff_picks';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $album = Album::find($request->album_id);

        if ($request->filter_by === self::FILTER_STAFF_PICKS && null !== $album)
        {
            $photosQuery->whereIn('photos.id', [
                $album->top1_photo_id,
                $album->top2_photo_id,
                $album->top3_photo_id,
                $album->top4_photo_id,
                $album->top5_photo_id,
                $album->top6_photo_id,
            ]);
        }

        return $photosQuery;
    }
}
