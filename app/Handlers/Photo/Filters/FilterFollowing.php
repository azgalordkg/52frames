<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Filters;

use App\Follower;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilterFollowing
{
    public const FILTER_FOLLOWING = 'following';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $user = Auth::guard('api')->user();

        if ($request->filter_by === self::FILTER_FOLLOWING && null !== $user)
        {
            $following = Follower::whereFollowerUserId($user->id)->get();
            $followingUserIds = array_column($following->toArray(), 'following_user_id');
            $photosQuery->whereIn('photos.user_id', $followingUserIds);
        }

        return $photosQuery;
    }
}
