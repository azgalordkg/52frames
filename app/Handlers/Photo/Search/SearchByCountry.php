<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Search;

use Closure;
use Illuminate\Http\Request;

class SearchByCountry
{
    public const SEARCH_BY_COUNTRY = 'country';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $country = $request->search_query;

        if ($request->search_by === self::SEARCH_BY_COUNTRY && null !== $country)
        {
            $photosQuery->where('photos.location', 'like', '%' . $request->search_query . '%');
        }

        return $photosQuery;
    }
}
