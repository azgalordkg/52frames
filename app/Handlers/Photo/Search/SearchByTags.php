<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Search;

use Closure;
use Illuminate\Http\Request;

class SearchByTags
{
    public const SEARCH_BY_TAGS = 'tags';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $search_query = $request->search_query;

        if ($request->search_by === self::SEARCH_BY_TAGS && null !== $search_query)
        {
            $tags = explode(",", trim($request->search_query));
            $tags = array_unique($tags);
            $tags = array_filter($tags);
            if (false === empty($tags)) {
                $photosQuery->where(function ($query) use ($tags) {
                    foreach ($tags as $tag) {
                        $query->orWhere('photos.tags', 'like', '%' . $tag . '%');
                    }
                });
            }
        }

        return $photosQuery;
    }
}
