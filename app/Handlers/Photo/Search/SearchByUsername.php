<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Search;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchByUsername
{
    public const SEARCH_BY_USERNAME = 'username';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $username = $request->search_query;

        if ($request->search_by === self::SEARCH_BY_USERNAME && null !== $username)
        {
            $photosQuery
                ->join('users', 'users.id', '=', 'photos.user_id')
                ->where(function ($query) use ($username) {
                    $query->where('users.firstname', 'like', '%' . $username . '%')
                        ->orWhere('users.lastname', 'like', '%' . $username . '%')
                        ->orWhere(DB::raw("CONCAT(`firstname`, ' ', `lastname`)"), 'like', '%' . $username . '%')
                        ->orWhere(DB::raw("CONCAT(`lastname`, ' ', `firstname`)"), 'like', '%' . $username . '%')
                        ->orWhere('users.handle', 'like', '%' . $username . '%');
                });
        }

        return $photosQuery;
    }
}
