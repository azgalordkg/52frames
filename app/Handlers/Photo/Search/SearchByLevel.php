<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Search;

use Closure;
use Illuminate\Http\Request;

class SearchByLevel
{
    public const SEARCH_BY_LEVEL = 'level';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $level = $request->search_query;

        if ($request->search_by === self::SEARCH_BY_LEVEL && null !== $level)
        {
            $photosQuery
                ->join('users', 'users.id', '=', 'photos.user_id')
                ->join('user_profiles', 'user_profiles.user_id', '=', 'users.id')
                ->where(function ($query) use ($level) {
                    $query->where('user_profiles.photography_level', '=', $level);
                });
        }

        return $photosQuery;
    }
}
