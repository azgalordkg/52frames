<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Sorts;

use App\Album;
use App\AlbumSort;
use App\Photo;
use App\Services\Helpers;
use App\Traits\HelpsAlbum;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SortRandom
{
    use HelpsAlbum;

    public const SORT_NEW_RANDOM = 'new_random';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $user = Auth::guard('api')->user();
        $album = Album::find($request->album_id);
        if (null === $album) {
            return $photosQuery;
        }

        if ($request->sort_by === self::SORT_NEW_RANDOM)
        {
            $top3Ids = array_column(Helpers::getTopPhotos($album, 3), 'id');
            $fliersIds = array_column($this->getFlierPhotos($album), 'id');
            $staticSort = array_values(array_unique(array_merge($top3Ids, $fliersIds)));

            $allOthers = Photo::whereAlbumId($album->id)
                ->whereNotIn('id', $staticSort)
                ->select('photos.id')
                ->get()->toArray();
            $randomizedIds = array_column($allOthers, 'id');
            shuffle($randomizedIds);

            $allIds = array_values(array_merge($staticSort, $randomizedIds));

            $sortArr = null;
            if ($user) {
                $userId = $user->id;
                $sort = AlbumSort::whereAlbumId($album->id)->whereUserId($userId)->first();
                $sort->photo_ids = json_encode($allIds);
                $sort->save();
            }

            $photosQuery->orderByRaw("FIELD(photos.id," . implode(',', $allIds) . ")");
        } elseif (false === isset($request->sort_by) || null === $request->sort_by) {
            $sortArr = $this->getOrGenerateUserSortForAlbumId($album, @$user->id);
            $photosQuery->orderByRaw("FIELD(photos.id," . implode(',', $sortArr) . ")");
        }

        return $photosQuery;
    }
}
