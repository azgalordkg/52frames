<?php declare(strict_types = 1);

namespace App\Handlers\Photo\Sorts;

use Closure;
use Illuminate\Http\Request;

class SortByNumber
{
    public const SORT_BY_LOVES = 'loves';
    public const SORT_BY_VIEWS = 'views';
    public const SORT_BY_COMMENTS = 'comments';

    public const SORT_DIR_DESC = 'desc';
    public const SORT_DIR_ASC = 'asc';

    public function handle(Request $request, Closure $next)
    {
        $photosQuery = $next($request);

        $sortDirection = self::SORT_DIR_DESC;
        if ($request->sort_dir === self::SORT_DIR_ASC) {
            $sortDirection = self::SORT_DIR_ASC;
        }

        $sortBy = $request->sort_by;
        if (
            self::SORT_BY_LOVES    === $sortBy ||
            self::SORT_BY_VIEWS    === $sortBy ||
            self::SORT_BY_COMMENTS === $sortBy
        ) {
            $photosQuery->orderBy('photos.num_' . $sortBy, $sortDirection);
        }

        return $photosQuery;
    }
}
