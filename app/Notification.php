<?php

namespace App;

class Notification extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'sent',
        'keyword',
        'for_user_id',
        'source_model_id',
        'source_model_type',
        'by_user_id',
        'related_model_id',
        'related_model_type',
        'involved_model_id',
        'involved_model_type',
        'target_action_json',
    ];

    public function forUser()
    {
        return $this->belongsTo(User::class, 'for_user_id', 'id');
    }

    public function byUser()
    {
        return $this->belongsTo(User::class, 'by_user_id', 'id');
    }

    public function sourceModel()
    {
        return $this->morphTo();
    }

    public function relatedModel()
    {
        return $this->morphTo();
    }

    public function involvedModel()
    {
        return $this->morphTo();
    }

    public static function getDailyEmailNotificationKeywords()
    {
        return [
            env('NOTIF_HOOK_PHOTO_LOVE'),
            env('NOTIF_HOOK_PHOTO_COMMENT'),
            env('NOTIF_HOOK_COMMENT_REPLY'),
            env('NOTIF_HOOK_PROFILE_FOLLOW')
        ];
    }

    public static function mapKeywordToAlbum(Notification $notification): ?Album
    {
        switch (@$notification->keyword) {
            case env('NOTIF_HOOK_PHOTO_LOVE'):
            case env('NOTIF_HOOK_PHOTO_COMMENT'):
                return $notification->sourceModel->album;
            case env('NOTIF_HOOK_COMMENT_REPLY'):
                return $notification->involvedModel->album;
            default:
                return null;
        }
    }

    public static function saveAction(string $keyword, int $forUserId, Model $reactedToModel, Model $reactionModel, Model $highLevelModel = null, array $targetAction = null): ?Notification
    {
        $reactionModelId = $reactionModel->user_id ?: $reactionModel->id;
        if ($forUserId == $reactionModelId) return null;
        $data = [
            'keyword' => $keyword,
            'for_user_id' => $forUserId,
            'source_model_id' => $reactedToModel->id,
            'source_model_type' => get_class($reactedToModel),
            'by_user_id' => $reactionModelId,
            'related_model_id' => $reactionModel->id,
            'related_model_type' => get_class($reactionModel),
        ];
        if ($highLevelModel) {
            $data['involved_model_id'] = $highLevelModel->id;
            $data['involved_model_type'] = get_class($highLevelModel);
        }
        if ($targetAction) $data['target_action_json'] = json_encode($targetAction);
        return Notification::create($data);
    }

}






























