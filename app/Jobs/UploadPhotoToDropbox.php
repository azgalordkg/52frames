<?php

namespace App\Jobs;

use App\User;
use App\Photo;
use App\Mail\UploadErrorEmail;
use App\Services\DropboxService;
use App\Traits\HelpsFileUploads;
use Illuminate\Support\Facades\Mail;

class UploadPhotoToDropbox extends JobQueue
{
    use HelpsFileUploads;

    protected $photo;

    /**
     * Create a new job instance.
     *
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!env('DROPBOX_TOKEN')) return;

        $photoPaths = $this->decodePhotoFilePaths($this->photo->original_res_filename);

        $file = fopen($photoPaths['localFilePath'], 'r');

        try {

            $dropboxClient = new DropboxService();
            $dropboxClient->upload($photoPaths['filePath'], $file);

            @fclose($file);

            $this->photo->uploaded_to_dropbox = true;
            $this->photo->save();

        } catch (\Exception $exception) {
            @fclose($file);
            throwException($exception);
        }

        $this->shouldDeleteLocalCopiesIfAllPhotosAreUploaded($this->photo->album);

    }

}
