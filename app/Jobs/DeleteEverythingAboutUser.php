<?php

namespace App\Jobs;

use App\Mail\AccountDeletedEmail;
use App\Photo;
use App\Traits\HelpsFileUploads;
use App\User;
use Illuminate\Support\Facades\Mail;

class DeleteEverythingAboutUser extends JobQueue
{
    use HelpsFileUploads;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = User::flagDeleting($user);
    }

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        $this->deleteAllSubmittedPhotos();

        $this->sendEmailAccountDeleted();

        $this->user->delete();
    }

    protected function deleteAllSubmittedPhotos()
    {
        $photos = Photo::whereUserId($this->user->id)->get();
        foreach ($photos as $photo) {
            $this->removeFromEverywhereExistingPhoto($photo);
        }
    }

    protected function sendEmailAccountDeleted()
    {
        $template = new AccountDeletedEmail($this->user);
        Mail::to($this->user)->send($template);
    }
}