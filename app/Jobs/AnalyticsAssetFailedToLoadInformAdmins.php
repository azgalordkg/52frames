<?php

namespace App\Jobs;

use App\Role;
use App\User;
use App\UserRole;
use App\AnalyticsTriggerTerm;
use Illuminate\Support\Facades\Mail;
use App\Mail\AnalyticsStaticAssetDidntLoad;

class AnalyticsAssetFailedToLoadInformAdmins extends JobQueue
{
    protected $triggerTerm;
    protected $inputs;

    /**
     * Create a new job instance.
     *
     * @param AnalyticsTriggerTerm $triggerTerm
     * @param array $inputs
     */
    public function __construct(AnalyticsTriggerTerm $triggerTerm, array $inputs)
    {
        $this->triggerTerm = $triggerTerm;
        $this->inputs = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipients = $this->getSuperAdmins();
        $this->sendEmails($recipients);
    }

    protected function getSuperAdmins()
    {
        $userAdminIds = $this->getUsersWithSuperAdminRoles();
        return User::whereIn('id', $userAdminIds)
            ->where('lastname', '!=', '')
            ->whereNotNull('lastname')
            ->get();
    }

    protected function getUsersWithSuperAdminRoles()
    {
        $usersWithSuperAdminRoles = Role::whereKeyword('super_admin')->first()
            ->usersRoles()->whereEnabled(true)
            ->get()->toArray();
        return array_column($usersWithSuperAdminRoles, 'user_id');
    }

    protected function sendEmails($recipients)
    {
        foreach ($recipients as $recipient) {
            $template = new AnalyticsStaticAssetDidntLoad($this->triggerTerm, $this->inputs);
            Mail::to($recipient)->send($template);
        }
    }

}
