<?php

namespace App\Jobs;

use App\User;
use App\Album;
use App\Photo;
use App\Services\MailchimpService;

class UpdateMailchimpUsersDidntUpload extends JobQueue
{
    protected $thisWeekAlbum;

    /**
     * Create a new job instance.
     *
     * @param Album $thisWeekAlbum
     */
    public function __construct(Album $thisWeekAlbum)
    {
        $this->thisWeekAlbum = $thisWeekAlbum;
    }

    /**
     * Gets a list of Users who didn't submit to the Album.
     * @param int $albumId
     * @return array
     */
    protected function getUsersWhoDidntSubmitToAlbum(int $albumId)
    {
        $users = [];
        if ($albumId > 0) {
            $photos = Photo::whereAlbumId($albumId)->get();
            $usersIdSubmitted = array_column($photos->toArray(), 'user_id');
            $usersWhoDidntUpload = User::whereNotIn('id', $usersIdSubmitted)->get();
            $users = $usersWhoDidntUpload->toArray();
        }
        return $users;
    }

    /**
     * Gets the Album previous to the one in the input.
     * @param Album $thisWeekAlbum
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    protected function getLastWeekAlbum(Album $thisWeekAlbum)
    {
        if ($thisWeekAlbum->week_number > 1) $lastWeekAlbum = Album::whereWeekNumber($thisWeekAlbum->week_number - 1)->first();
        else $lastWeekAlbum = Album::where('year', $thisWeekAlbum->year - 1)->orderBy('week_number', 'desc')->first();
        return $lastWeekAlbum;
    }

    /**
     * Execute the job.
     *
     * @param MailchimpService $mailchimpService
     * @return void
     */
    public function handle(MailchimpService $mailchimpService)
    {
        $thisWeekAlbumId = $this->thisWeekAlbum->id;
        $usersWhoDidntUploadThisWeekThisWeek = $this->getUsersWhoDidntSubmitToAlbum($thisWeekAlbumId);
        $userIdsThisWeek = array_column($usersWhoDidntUploadThisWeekThisWeek, 'id');

        $lastWeekAlbum = $this->getLastWeekAlbum($this->thisWeekAlbum);
        $lastWeekAlbumId = @$lastWeekAlbum->id ?: 0;

        $usersWhoDidntUploadThisWeekLastWeek = $this->getUsersWhoDidntSubmitToAlbum($lastWeekAlbumId);
        $userIdsLastWeek = array_column($usersWhoDidntUploadThisWeekLastWeek, 'id');

        $targetUsers = [];
        foreach ($userIdsThisWeek as $userId) {
            if (in_array($userId, $userIdsLastWeek)) {
                if (Photo::whereUserId($userId)->count()) {
                    $targetUsers[] = User::whereId($userId)->first();
                }
            }
        }

        foreach ($targetUsers as $user) {
            $mailchimpService->updateUserMergeFields($user->email, [
                'STATUSCOM' => 'INACTIVE'
            ]);
        }
    }

}