<?php

namespace App\Jobs;

use App\Services\MailchimpHelper;
use App\User;
use App\Services\MailchimpService;

class UpdateMailchimpPhotoUploadStatus extends JobQueue
{
    protected $user;

    protected $isFirstUpload;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param bool $isFirstUpload
     */
    public function __construct(User $user, bool $isFirstUpload)
    {
        $this->user = $user;
        $this->isFirstUpload = $isFirstUpload;
    }

    /**
     * Execute the job.
     *
     * @param MailchimpService $mailchimpService
     * @return void
     */
    public function handle(MailchimpService $mailchimpService)
    {
        if (!MailchimpHelper::shouldProcess()) return;

        if ($this->isFirstUpload) {
            $mailchimpService->updateUserMergeFields($this->user->email, [
                'STATUSCOM' => 'first-submission-affirmative'
            ]);
        } else {
            $mailchimpService->updateUserMergeFields($this->user->email, [
                'STATUSCOM' => 'ACTIVE'
            ]);
        }
    }
}