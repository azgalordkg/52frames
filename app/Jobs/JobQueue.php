<?php

namespace App\Jobs;

use App\Mail\GenericEmailAdminBackendError;
use App\Services\JobQueueException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class JobQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;

    /**
     * Below is an example of how to override the default backend error email template.
     *
     * @param \Exception $exception
     * @param array|null $extraInfo
     * @param callable|null $emailTemplate
     * @return mixed
     */
    public function failed(\Exception $exception, array $extraInfo = null, callable $emailTemplate = null)
    {
        return JobQueueException::sendFailedJobEmail($this, $exception, $extraInfo, $emailTemplate ?: function ($formattedErrorsArr) {
            return new GenericEmailAdminBackendError($formattedErrorsArr);
        });
    }
}