<?php

namespace App\Jobs;

use App\Photo;
use App\Mail\UploadErrorEmail;
use App\Services\JobQueueException;
use App\Traits\HelpsFileUploads;
use Illuminate\Support\Facades\Storage;

class UploadPhotoToS3 extends JobQueue
{
    use HelpsFileUploads;

    protected $photo;

    /**
     * Create a new job instance.
     *
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!env('AWS_S3_KEY')) return;

        $photosForUpload = [
            'thumbnail_filename',
            'hi_res_filename',
        ];


        foreach ($photosForUpload as $photoType) {

            $photoPaths = $this->decodePhotoFilePaths($this->photo->$photoType);

            $file = fopen($photoPaths['localFilePath'], 'r');

            try {

                $targetFilename = $photoPaths['filePath'];

                Storage::disk('s3')->put($targetFilename, $file, 'public');

                @fclose($file);
                sleep(5);

//                $this->photo->$photoType = $this->generateS3Url($targetFilename);
                $this->photo->$photoType = $this->generateCloudFrontUrl($targetFilename);

                $this->photo->uploaded_to_cdn = true;
                $this->photo->save();

            } catch (\Exception $exception) {
                @fclose($file);
                throwException($exception);
            }

        }

        $this->shouldDeleteLocalCopiesIfAllPhotosAreUploaded($this->photo->album);

    }

}
