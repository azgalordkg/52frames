<?php

namespace App\Jobs\Scheduled;

use App\Album;
use App\Jobs\JobQueue;
use App\Mail\DailyEmailNotificationError;
use App\User;
use App\Follower;
use App\Notification;
use App\Mail\DailyEmailNotification;
use Illuminate\Support\Facades\Mail;

class DailyEmailNotificationSender extends JobQueue
{
    protected $forUserId;

    public function __construct(int $forUserId)
    {
        $this->forUserId = $forUserId;
    }

    protected function markNotifsAsSent(array $result)
    {
        foreach ($result as $row) {
            $row->sent = true;
            $row->save();
        }
    }

    protected function getUpdates(): array
    {
        $output = [];
        $keywords = Notification::getDailyEmailNotificationKeywords();
        $notifications = Notification::with(['byUser', 'sourceModel', 'relatedModel', 'involvedModel'])
            ->orderBy('involved_model_type')
            ->orderBy('involved_model_id')
            ->orWhereArray('keyword', $keywords)
            ->whereForUserId($this->forUserId)
            ->whereSent(false)
            ->get();
        foreach ($notifications as $notification) {
            $album = Notification::mapKeywordToAlbum($notification);
            if ($album && !$album->live) continue;
            else $output[] = $notification;
        }
        return $output;
    }

    protected function groupPhotoLovesAndComments(array $keywordGroupings)
    {
        $keywords = [
            env('NOTIF_HOOK_PHOTO_LOVE'),
            env('NOTIF_HOOK_PHOTO_COMMENT')
        ];
        $photoGrouping = [];
        foreach ($keywordGroupings as $keyword => $grouping) {
            if (in_array($keyword, $keywords)) {
                foreach ($grouping as $index => $notif) {
                    if (!isset($photoGrouping[$notif['source_model']['id']][$notif['keyword']])) {
                        $photoGrouping[$notif['source_model']['id']]['photo'] = $notif['source_model'];
                        $album = Album::whereId($notif['source_model']['album_id'])->first();
                        $photoGrouping[$notif['source_model']['id']]['photo']['album'] = $album->toArray();
                        $photoGrouping[$notif['source_model']['id']][$notif['keyword']] = [];
                    }
                    $photoGrouping[$notif['source_model']['id']][$notif['keyword']][] = $notif;
                }
            }
        }
        return $photoGrouping;
    }

    protected function addAlbumDataToInvolvedModel(array $notifArray)
    {
        $commentReplyKw = env('NOTIF_HOOK_COMMENT_REPLY');
        if ($notifArray['keyword'] == $commentReplyKw && @$notifArray['involved_model']) {
            $album = Album::whereId($notifArray['involved_model']['album_id'])->first();
            if ($album) $notifArray['involved_model']['album'] = $album->toArray();
        }
        return $notifArray;
    }

    protected function addPhotographerDataToInvolvedModel(array $notifArray)
    {
        $commentReplyKw = env('NOTIF_HOOK_COMMENT_REPLY');
        if ($notifArray['keyword'] == $commentReplyKw && @$notifArray['involved_model']) {
            $photographer = User::whereId($notifArray['involved_model']['user_id'])->first();
            if ($photographer) {
                $photographer = User::getDisplayText($photographer);
                $notifArray['involved_model']['user'] = $photographer;
            }
        }
        return $notifArray;
    }

    protected function organizeNotifications(array $result): array
    {
        $keywordGroupings = [];
        foreach ($result as $row) {
            if (!isset($keywordGroupings[$row->keyword]))
                $keywordGroupings[$row->keyword] = [];
            $data = $row->toArray();
            $data['by_user'] = User::getDisplayText($row->byUser);
            $data = $this->addAlbumDataToInvolvedModel($data);
            $data = $this->addPhotographerDataToInvolvedModel($data);
            $keywordGroupings[$row->keyword][] = $data;
        }
        $forUser = User::with('signedManifesto')->whereId($this->forUserId)->first();
        $numFollowers = Follower::whereFollowingUserId($this->forUserId)->count();
        $photoRelated = $this->groupPhotoLovesAndComments($keywordGroupings);
        return [
            'base_url' => env('APP_URL'),
            'for_user' => User::getDisplayText($forUser),
            'total_followers' => $numFollowers,
            'total_updates' => count($result),
            'grouped' => $keywordGroupings,
            'photo_regroup' => $photoRelated
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipient = User::whereId($this->forUserId)->first();
        if ($recipient) {
            if (!$recipient->settings->notif_emails_daily_dont_receive) {
                $notifications = $this->getUpdates();
                $organizedNotifs = $this->organizeNotifications($notifications);
                if ($organizedNotifs['total_updates'] > 0) {
                    try {
                        $template = new DailyEmailNotification($organizedNotifs);
                        Mail::to($recipient)->send($template);
                        $this->markNotifsAsSent($notifications);
                    } catch (\Exception $exception) {
                        $this->failed($exception, [
                            'name' => $recipient->name,
                            'email' => $recipient->email,
                            'data' => $organizedNotifs
                        ]);
                    }
                }
            }
        }
    }

    public function failed(\Exception $exception, array $userInfo = null, callable $emailTemplate = null)
    {
        return parent::failed($exception, $userInfo, function ($details) {
            return new DailyEmailNotificationError($details);
        });
    }
}