<?php

namespace App\Jobs\Scheduled\Dropbox;

use App\Jobs\JobQueue;
use App\Services\DropboxService;
use App\Traits\HelpsFileUploads;
use App\WufooDumpCompressPhotosList;
use Exception;
use Illuminate\Http\UploadedFile;

class WufooPhotoCompressor extends JobQueue
{
    use HelpsFileUploads;

    public $timeout = 300;

    protected $entry;
    protected $dropboxClient;

    public function __construct(WufooDumpCompressPhotosList $entry)
    {
        $this->entry = $entry;
    }

    protected function getWufooLocalFolderName()
    {
        return env('DROPBOX_COMMITTEE_LOCAL_FOLDER_NAME', '');
    }

    protected function getDropboxClient()
    {
        if (!$this->dropboxClient) $this->dropboxClient = new DropboxService();
        return $this->dropboxClient;
    }

    protected function downloadFromDropbox(string $filename)
    {
        $photoSourceFolder = env('DROPBOX_COMMITTEE_PHOTO_SOURCE_FOLDER');
        $filePath = $photoSourceFolder . $filename;

        $dropboxClient = $this->getDropboxClient();
        return $dropboxClient->download($filePath);
    }

    protected function saveFileToWufooFolder($fileStreamResource, string $filename)
    {
        $wufooFolderName = $this->getWufooLocalFolderName();
        $wufooLocalFolder = $this->getUploadsFolder() . $wufooFolderName;
        $this->folderShouldExist($wufooLocalFolder);
        $saveToLocation = $wufooLocalFolder . $filename;
        file_put_contents($saveToLocation, $fileStreamResource);
        return $saveToLocation;
    }

    protected function resizeAndSaveImageToWufooResizedFolder(string $localFilePath, string $filename)
    {
        $uploadedFile = new UploadedFile($localFilePath, $filename, null, filesize($localFilePath));
        $filenameWithoutExtension = pathinfo($filename, PATHINFO_FILENAME);
        $targetResizedFileLocation = $this->getWufooLocalFolderName() . 'resized/' . $filenameWithoutExtension;
        return $this->resizePhotoAndCopyTo($uploadedFile, $targetResizedFileLocation);
    }

    protected function uploadToDropbox(string $localFilePath)
    {
        $dropboxDestinationFolder = env('DROPBOX_COMMITTEE_PHOTO_COMPRESSED_SAVE_LOCATION');
        $targetFilePath = $dropboxDestinationFolder . pathinfo($localFilePath, PATHINFO_BASENAME);
        $localFile = fopen($localFilePath, 'r');
        $dropboxClient = $this->getDropboxClient();
        $uploadResult = $dropboxClient->uploadUsingRelativePath($targetFilePath, $localFile);
        @fclose($localFile);
        return $uploadResult;
    }

    protected function dropboxAlreadyHasCompressedVersion(string $filename)
    {
        $dropboxClient = $this->getDropboxClient();
        $dropboxDestinationFolder = env('DROPBOX_COMMITTEE_PHOTO_COMPRESSED_SAVE_LOCATION');
        $filePath = $dropboxDestinationFolder . pathinfo($filename, PATHINFO_FILENAME) . '.jpg';
        try {
            return $dropboxClient->getMetadata($filePath);
        } catch (Exception $exception) {
            return false;
        }
    }

    protected function markProcessing()
    {
        $this->entry->processing = true;
        $this->entry->save();
    }

    protected function markCompleted()
    {
        try {
            $this->entry->delete();
        } catch (Exception $exception) {
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->markProcessing();

        $filename = $this->entry->filename;

        if ($this->dropboxAlreadyHasCompressedVersion($filename))

            $this->markCompleted();

        else {

            $fileStream = $this->downloadFromDropbox($filename);

            $filePathSavedTo = $this->saveFileToWufooFolder($fileStream, $filename);

            $resizeResult = $this->resizeAndSaveImageToWufooResizedFolder($filePathSavedTo, $filename);

            $uploadResult = $this->uploadToDropbox($resizeResult['localFilePath']);

            unlink($resizeResult['localFilePath']);
            unlink($filePathSavedTo);

            $this->markCompleted();
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        print_r($exception->getLine());
        echo "\n";
        print_r($exception->getMessage());
        echo "\n";
    }
}