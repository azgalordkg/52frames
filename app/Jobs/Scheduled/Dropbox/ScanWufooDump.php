<?php

namespace App\Jobs\Scheduled\Dropbox;

use App\Jobs\JobQueue;
use App\Services\DropboxService;
use App\WufooDumpCompressPhotosConfig;
use App\WufooDumpCompressPhotosList;

class ScanWufooDump extends JobQueue
{
    protected function getCursor()
    {
        return WufooDumpCompressPhotosConfig::where('key', 'cursor')->first();
    }

    protected function getCursorString()
    {
        $cursorConfig = $this->getCursor();
        return @$cursorConfig['value'] ?: '';
    }

    protected function setCursor(string $newValue)
    {
        $cursor = $this->getCursor();
        if (!$cursor) $cursor = WufooDumpCompressPhotosConfig::create([
            'key' => 'cursor'
        ]);
        $cursor->value = $newValue;
        $cursor->save();
        return $cursor;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $photoSourceFolder = env('DROPBOX_COMMITTEE_PHOTO_SOURCE_FOLDER');
        if (!$photoSourceFolder) {
            echo "no photo source folder specified";
            return;
        }

        $photoDestinationFolder = env('DROPBOX_COMMITTEE_PHOTO_COMPRESSED_SAVE_LOCATION');
        if (!$photoDestinationFolder) {
            echo "no photo destination folder specified";
            return;
        }

        $dropboxClient = new DropboxService();

        $cursor = $this->getCursorString();

        if ($cursor)
            $result = $dropboxClient->listFolderContinue($cursor);
        else
            $result = $dropboxClient->listFolder($photoSourceFolder);

        if (!is_array(@$result['entries'])) {
            echo "response of source folder is not in a proper format";
            return;
        }

        foreach ($result['entries'] as $entry) {
            $existing = WufooDumpCompressPhotosList::whereFilename($entry['name'])->first();
            if (!$existing) {
                $record = WufooDumpCompressPhotosList::create([
                    'filename' => $entry['name']
                ]);
                WufooPhotoCompressor::dispatch($record);
            }
        }

        if (@$result['cursor']) $this->setCursor($result['cursor']);

    }

    /**
     * The job failed to process.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        print_r($exception->getLine());
        echo "\n";
        print_r($exception->getMessage());
        echo "\n";
    }
}