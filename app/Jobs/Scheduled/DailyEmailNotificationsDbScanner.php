<?php

namespace App\Jobs\Scheduled;

use App\Jobs\JobQueue;
use App\Notification;
use Illuminate\Support\Facades\DB;

class DailyEmailNotificationsDbScanner extends JobQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $keywords = Notification::getDailyEmailNotificationKeywords();
        $notifs = Notification::groupBy('for_user_id')
            ->select('for_user_id', DB::raw('count(*) as updates'))
            ->orWhereArray('keyword', $keywords)
            ->whereSent(false)
            ->get();
        foreach ($notifs as $notif) {
            DailyEmailNotificationSender::dispatch($notif->for_user_id);
        }
    }
}