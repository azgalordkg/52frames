<?php

namespace App;

use App\Traits\ManipulatesQueryScope;
use App\Traits\HelpsFormatTimestamps;
use App\Http\Controllers\Analytics\Helpers\HelpsAnalytics;
use Illuminate\Database\Eloquent\Model as ModelBase;
use Illuminate\Database\Eloquent\Builder;

abstract class Model extends ModelBase
{
    use ManipulatesQueryScope;
    use HelpsFormatTimestamps;
    use HelpsAnalytics;

    /**
     * Scope a query to only include entities with given substring in column.
     *
     * @param Builder $query
     * @param string  $column
     * @param string  $substring
     * @return Builder
     */
    public function scopeSuggest($query, $column, $substring)
    {
        return $query->where($column, 'like', '%'.$substring.'%');
    }
}
