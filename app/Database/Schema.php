<?php

namespace App\Database;

use App\Database\Traits\CreatesSchemaShortcuts;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Schema as SchemaBase;
use App\Database\Traits\HelpsForeignKeysOnSchema;

class Schema extends SchemaBase
{
    use CreatesSchemaShortcuts, HelpsForeignKeysOnSchema;


    // Added a Blueprint resolver

    protected static function addBlueprintResolver(Builder $schemaBuilder)
    {
        $schemaBuilder->blueprintResolver(function ($table, $callback) use ($schemaBuilder) {
            return new Blueprint($table, $callback);
        });
    }


    // Installed Resolver

    protected static function getFacadeAccessor()
    {
        $schemaBuilder = parent::getFacadeAccessor();
        self::addBlueprintResolver($schemaBuilder);
        return $schemaBuilder;
    }

    public static function connection($name)
    {
        $schemaBuilder = parent::connection($name);
        self::addBlueprintResolver($schemaBuilder);
        return $schemaBuilder;
    }
}
