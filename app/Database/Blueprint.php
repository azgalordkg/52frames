<?php

namespace App\Database;

use App\Database\Traits\ReturnsProperFluentObjects;
use App\Database\Traits\HelpsForeignKeysOnBlueprint;
use Illuminate\Database\Schema\Blueprint as BlueprintBase;

class Blueprint extends BlueprintBase
{
    use ReturnsProperFluentObjects, HelpsForeignKeysOnBlueprint;

    /**
     * Exactly the same lines as on parent::addColumn(). We are just executing these lines here because we want to replace the instance of Fluent.
     *
     * @param string $type
     * @param string $name
     * @param array $parameters
     * @return Fluent|\Illuminate\Support\Fluent
     */
    public function addColumn($type, $name, array $parameters = []): Fluent
    {
        $this->columns[] = $column = new Fluent($this,
            array_merge(compact('type', 'name'), $parameters)
        );

        return $column;
    }

    /**
     * Exactly the same lines as on parent::createCommand(). We are just executing these lines here because we want to replace the instance of Fluent.
     *
     * @param string $type
     * @param string $name
     * @param array $parameters
     * @return Fluent|\Illuminate\Support\Fluent
     */
    protected function createCommand($name, array $parameters = []): Fluent
    {
        return new Fluent($this, array_merge(compact('name'), $parameters));
    }
}