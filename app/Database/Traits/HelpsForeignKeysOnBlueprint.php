<?php

namespace App\Database\Traits;

use App\Model;

trait HelpsForeignKeysOnBlueprint
{
    use CommonBlueprintFunctions;

    protected function errorNoTableModel(string $methodName)
    {
        throw new \Exception("Please call \$table->setTableModel() first before calling {$methodName}()");
    }

    public function makeForeignKey(string $fieldname, string $foreignModel, string $refCol = 'id', bool $deleteOnCascade = true)
    {
        $this->changeTableEngine();
        $foreignTableName = $this->getTableNameFromModelString($foreignModel);
        $foreignKey = $this->foreign($fieldname)->references($refCol)->on($foreignTableName);
        if ($deleteOnCascade) $foreignKey->onDelete('cascade');
    }

    public function makeForeignKeyWithPreClean(string $fieldname, string $foreignModel, string $refCol = 'id', bool $deleteOnCascade = true)
    {
        $localModel = $this->getTableModel();
        if (!$localModel) $this->errorNoTableModel(__FUNCTION__);

        $this->deleteInvalidFKRows($fieldname, $foreignModel, $refCol);
        $this->makeForeignKey($fieldname, $foreignModel, $refCol);
    }

    public function deleteInvalidFKRows(string $foreignKey, string $foreignModel, string $targetKey = 'id')
    {
        $localModel = $this->getTableModel();
        if (!$localModel) $this->errorNoTableModel(__FUNCTION__);

        $localRows = $localModel::all();
        $foreignRows = $foreignModel::all();
        if (!$localRows->count() || !$foreignRows->count()) return;

        $foreignIds = array_column($foreignRows->toArray(), $targetKey);
        $localModel::whereNotIn($foreignKey, $foreignIds)->delete();
    }

    /**
     * Sometimes gives an error when deleting, esp if the Index is not installed as a Relation within MySQL/db (anymore) while the index still exists on the actual table.
     *
     * @param string $key
     */
    public function dropForeignKey(string $key)
    {
        $indexName = $this->getIndexNameIfExists($this->table, 'foreign', [$key]);
        if ($indexName) $this->dropForeign($indexName);
    }
}