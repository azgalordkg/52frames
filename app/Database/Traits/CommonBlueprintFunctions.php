<?php

namespace App\Database\Traits;

use App\Model;
use App\Database\Schema;
use Doctrine\DBAL\Schema\Table;

trait CommonBlueprintFunctions
{
    protected $tableModel;

    public function setTableModel(string $modelString)
    {
        $this->tableModel = $modelString;
    }

    public function getTableModel()
    {
        return $this->tableModel;
    }

    public function changeTableEngine(string $engineName = 'InnoDB')
    {
        $this->engine = $engineName;
    }

    protected function getTableNameFromModelString(string $modelClass)
    {
        return (new $modelClass())->getTable();
    }

    protected function getTableDetails(string $tableName): Table
    {
        return Schema::getConnection()->getDoctrineSchemaManager()->listTableDetails($tableName);
    }

    protected function hasIndex(string $tableName, string $indexName): bool
    {
        $tableDetails = $this->getTableDetails($tableName);
        return !!$tableDetails->hasIndex($indexName);
    }

    protected function getIndexNameIfExists(string $tableName, string $type, array $columns)
    {
        $exists = $this->hasIndex($tableName, $columns[0]);
        if ($exists) return $columns[0];
        $indexName = $this->createIndexName($type, $columns);
        $exists = $this->hasIndex($tableName, $indexName);
        if ($exists) return $indexName;
    }
}