<?php

namespace App\Database\Traits;

use App\Database\Blueprint;

trait CreatesSchemaShortcuts
{
    public static function dropTableColumns(string $table, array $columns)
    {
        self::table($table, function (Blueprint $table) use ($columns) {
            foreach ($columns as $column) $table->dropColumn($column);
        });
    }
}