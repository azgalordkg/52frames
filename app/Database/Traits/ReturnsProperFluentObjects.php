<?php

namespace App\Database\Traits;

use App\Database\Fluent;
use App\Traits\HelpsProxyMethodCalls;

trait ReturnsProperFluentObjects
{
    use HelpsProxyMethodCalls;

    /**
     * These are Overrides, but only done so that we fix all auto-suggests when building a migration file
     *
     * @return Fluent
     */
    protected function execParentMethod(): ?Fluent
    {
        $this->showErrorReporting();
        return $this->proxifyMethodCall(function ($backtrace) {
            return (count($backtrace) < 2) ? null : $backtrace[2];
        }, function ($prevMethod) {
            $targetMethod = 'parent::' . $prevMethod['function'];
            return call_user_func_array($targetMethod, $prevMethod['args']);
        }, function () {
            return;
        });
    }

    public function increments($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function tinyIncrements($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function smallIncrements($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function mediumIncrements($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function bigIncrements($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function char($column, $length = null): Fluent
    {
        return $this->execParentMethod();
    }

    public function string($column, $length = null): Fluent
    {
        return $this->execParentMethod();
    }

    public function text($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function mediumText($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function longText($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function integer($column, $autoIncrement = false, $unsigned = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function tinyInteger($column, $autoIncrement = false, $unsigned = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function smallInteger($column, $autoIncrement = false, $unsigned = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function mediumInteger($column, $autoIncrement = false, $unsigned = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function bigInteger($column, $autoIncrement = false, $unsigned = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedInteger($column, $autoIncrement = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedTinyInteger($column, $autoIncrement = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedSmallInteger($column, $autoIncrement = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedMediumInteger($column, $autoIncrement = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedBigInteger($column, $autoIncrement = false): Fluent
    {
        return $this->execParentMethod();
    }

    public function float($column, $total = 8, $places = 2): Fluent
    {
        return $this->execParentMethod();
    }

    public function double($column, $total = null, $places = null): Fluent
    {
        return $this->execParentMethod();
    }

    public function decimal($column, $total = 8, $places = 2): Fluent
    {
        return $this->execParentMethod();
    }

    public function unsignedDecimal($column, $total = 8, $places = 2): Fluent
    {
        return $this->execParentMethod();
    }

    public function boolean($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function enum($column, array $allowed): Fluent
    {
        return $this->execParentMethod();
    }

    public function json($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function jsonb($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function date($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function dateTime($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function dateTimeTz($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function time($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function timeTz($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function timestamp($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function timestampTz($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function nullableTimestamps(): Fluent
    {
        $this->execParentMethod();
    }

    public function binary($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function uuid($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function ipAddress($column): Fluent
    {
        return $this->execParentMethod();
    }

    public function macAddress($column): Fluent
    {
        return $this->execParentMethod();
    }
}