<?php

namespace App\Database\Traits;

use App\Database\Blueprint;

trait HelpsForeignKeysOnSchema
{
    public static function dropForeignKeys(string $table, array $keys)
    {
        self::table($table, function (Blueprint $table) use ($keys) {
            foreach ($keys as $key) {
                $table->dropForeignKey($key);
            }
        });
    }

    public static function dropForeignKeysAndTable(string $table, array $keys)
    {
        self::dropForeignKeys($table, $keys);
        self::dropIfExists($table);
    }
}