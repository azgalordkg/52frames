<?php

namespace App\Database;

use App\Traits\HelpsProxyMethodCalls;
use Illuminate\Support\Fluent as FluentBase;

class Fluent extends FluentBase
{
    use HelpsProxyMethodCalls;

    protected $tableBlueprint;

    public function __construct(Blueprint $table, $attributes = [])
    {
        $this->tableBlueprint = $table;
        parent::__construct($attributes);
    }

    protected function passRequestToBlueprint(): Fluent
    {
        return $this->proxifyMethodCall(function ($backtrace) {
            return (!$this->type || !$this->name || count($backtrace) < 2) ? null : $backtrace[2];
        }, function ($prevMethod) {
            $targetMethod = [$this->tableBlueprint, $prevMethod['function']];
            $methodArguments = array_merge([$this->name], $prevMethod['args']);
            call_user_func_array($targetMethod, $methodArguments);
            return $this;
        }, function () {
            return $this;
        }, function () {
            error_reporting(E_ALL);
        });
    }

    public function makeForeignKey(string $foreignModel, string $refCol = 'id', bool $deleteOnCascade = true): Fluent
    {
        return $this->passRequestToBlueprint();
    }

    public function makeForeignKeyWithPreClean(string $foreignModel, string $refCol = 'id', bool $deleteOnCascade = true): Fluent
    {
        return $this->passRequestToBlueprint();
    }

    public function deleteInvalidFKRows(string $foreignModel, string $targetKey = 'id'): Fluent
    {
        return $this->passRequestToBlueprint();
    }
}