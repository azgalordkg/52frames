<?php

namespace App;

class ManifestoAnswer extends Model
{
    protected $hidden = [
        'commitment_understanding',
        'being_constant_understanding',
        'creative_process_understanding',
        'vulnerability_understanding',
        'non_perfection_understanding',
        'understanding_to_release_the_notion_of_perfection',
        'understanding_for_the_sake_of_consistency'
    ];

    protected $fillable = [
        'user_id',
        'commitment_understanding',
        'being_constant_understanding',
        'creative_process_understanding',
        'vulnerability_understanding',
        'non_perfection_understanding',
        'understanding_to_release_the_notion_of_perfection',
        'understanding_for_the_sake_of_consistency',
        'why',
    ];

    /**
     * Rules for adding a nickname.
     * @return array
     */
    public static function nicknameRules()
    {
        $signupRules = User::signupRules();
        return [
            'nickname' => $signupRules['handle'],
            'display_as_handle' => $signupRules['display_as_handle'],
        ];
    }

    public static function photographyLevels()
    {
        return [
            'beginner' => 'Beginner - I enjoy taking photos',
            'beginner_dslr' => 'Beginner DSLR - I shoot with a DSLR, I understand my different lenses and some camera features, but not much more',
            'intermediate' => 'Intermediate - I shoot with a DSLR and understand most of my settings, but still learning more.',
            'advanced' => 'Advanced - I understand my camera settings and will also occasionally shoot with external flashes and stobes.',
            'advanced_pro' => 'Advanced Professional - I understand everything about how to take a photo, as well as studio lighting and at times make money with my photography.'
        ];
    }

    public static function cameraTypes()
    {
        return [
            'dslr_entry' => 'DSLR - Entry Level',
            'dslr_pro' => 'DSLR - Professional Level',
            'mirrorless' => 'Mirrorless',
            'point_and_shoot' => 'Point and Shoot',
            'phone' => 'Phone',
            'other' => 'Other'
        ];
    }

    /**
     * Rules for creating the Manifesto of a User.
     * @param bool $dontUserBasic
     * @return array
     */
    public static function introRules(bool $dontUserBasic = false)
    {
        $rules = [
            'country' => 'required|integer|exists:countries,id',
            'city' => 'nullable|string',
            'dont_share_location' => 'boolean',
            'photography_level' => 'required|string|in:' . implode(',', array_keys(self::photographyLevels())),
            'camera_type' => 'required|string|in:' . implode(',', array_keys(self::cameraTypes())),
            'photowalks' => 'nullable|string',
            'twitter_handle' => 'nullable|string|regex:/^[@#-_.~a-zA-Z0-9]+$/',
            'instagram_handle' => 'nullable|string|regex:/^[@#-_.~a-zA-Z0-9]+$/',
        ];
        if (!$dontUserBasic) $rules = array_merge($rules, self::nicknameRules(), [
            'how_did_hear' => 'nullable|string'
        ]);
        return $rules;
    }

    /**
     * Rules for completing the Manifesto.
     * @return array
     */
    public static function checkboxesRules()
    {
        return [
            'commitment_understanding' => 'accepted',
            'being_constant_understanding' => 'accepted',
            'creative_process_understanding' => 'accepted',
            'vulnerability_understanding' => 'accepted',
            'non_perfection_understanding' => 'accepted',
            'understanding_to_release_the_notion_of_perfection' => 'accepted',
            'understanding_for_the_sake_of_consistency' => 'accepted'
        ];
    }

    /**
     * Rules for completing the "why" portion of the Manifesto.
     * @return array
     */
    public static function whyRules()
    {
        return [
            'why' => 'string',
        ];
    }

    /**
     * Rules for importing CSV files
     * @return array
     */
    public static function csvImportRules()
    {
        return array_merge(self::introRules(true), self::whyRules(), [
            'state' => 'nullable|integer|exists:states,id'
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
