<?php

namespace App;

class UserProfile extends Model
{
    protected $fillable = [
        'user_id',
        'current_streak',
        'photowalks_level',
        'photography_level',
        'camera_type',
        'shortbio',
    ];

    protected $hidden = [
        'profile_photo'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
