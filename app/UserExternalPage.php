<?php

namespace App;

class UserExternalPage extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'user_id',
        'provider',
        'handle',
        'url',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
