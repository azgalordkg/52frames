<?php

namespace App;

use App\Services\UserSettingsHelper;

class UserSetting extends UserSettingsHelper
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'id',
        'user_id'
    ];

    protected $fillable = [
        'user_id',
        'photo_censor_show_always',
        'notif_emails_daily_dont_receive',
    ];

    public static function getNotEditableColumns()
    {
        return [
            'id',
            'user_id',
            'created_at',
            'updated_at'
        ];
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function findOrCreateRow(User $user)
    {
        return UserSetting::firstOrCreate([
            'user_id' => $user->id
        ]);
    }

}
