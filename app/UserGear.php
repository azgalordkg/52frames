<?php

namespace App;

class UserGear extends Model
{
    protected $fillable = [
        'user_id',
        'type',
        'value',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
