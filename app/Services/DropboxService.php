<?php

namespace App\Services;

use Spatie\Dropbox\Client as DropboxClient;

class DropboxService extends DropboxClient
{
    protected $uploadFolder;

    public function __construct(string $token = '')
    {
        $this->uploadFolder = env('DROPBOX_UPLOAD_FOLDER');
        parent::__construct($token ?: env('DROPBOX_TOKEN', ''));
    }

    public function upload(string $path, $contents, $mode = 'add'): array
    {
        if (!$this->accessToken) return [];
        $finalPath = $this->uploadFolder . $path;
        return parent::upload($finalPath, $contents, $mode);
    }

    public function uploadUsingRelativePath(string $relativeFilePath, $contents, $mode = 'add'): array
    {
        if (!$this->accessToken) return [];
        return parent::upload($relativeFilePath, $contents, $mode);
    }

    public function delete(string $path): array
    {
        if (!$this->accessToken) return [];
        $finalPath = $this->uploadFolder . $path;
        return parent::delete($finalPath);
    }
}