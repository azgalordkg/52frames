<?php

namespace App\Services;

use App\Album;
use App\Photo;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Helpers
{
    public static function filterByKeysArray(array $source, array $keys): array
    {
        return array_filter($source, function ($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    public static function filterByKeysModel(Model $model, array $keys): array
    {
        $output = [];
        foreach ($keys as $key) {
            if (isset($model->$key)) $output = array_merge($output, [
                $key => $model->$key
            ]);
        }
        return $output;
    }

    /**
     * Gets the list of Top photos of an album.
     * @param Album $album
     * @param int|null $maxCount
     * @return array
     */
    public static function getTopPhotos(Album $album, int $maxCount = null): array
    {
        $topPhotos = [];
        $columnsMap = [
            'top1_photo_id' => 'top1_photo',
            'top2_photo_id' => 'top2_photo',
            'top3_photo_id' => 'top3_photo',
            'top4_photo_id' => 'top4_photo',
            'top5_photo_id' => 'top5_photo',
            'top6_photo_id' => 'top6_photo'
        ];
        if (is_int($maxCount) && $maxCount <= 0) return $topPhotos;
        foreach ($columnsMap as $columnName => $targetName) {
            if ($album->$columnName) {
                $photo = Photo::whereId($album->$columnName)->with('owner')->first();
                if ($photo) {
                    $photoData = array_merge($photo->toArray(), [
                        'name' => $photo->filename,
                        'filename' => $photo->filename
                    ]);
                    $topPhotos[$targetName] = $photoData;
                    if (is_int($maxCount) && count($topPhotos) >= $maxCount) break;
                }
            }
        }
        return $topPhotos;
    }

    /**
     * @return array
     */
    public static function getUploaderAccountsId(): array
    {
        $uploaderIds = [];
        $uploaderEmails = [
            env('APP_FLIER_UPLOADER_EMAIL'),
            env('APP_PATREON_UPLOADER_EMAIL'),
            env('APP_COVER_PHOTO_UPLOADER_EMAIL'),
            env('APP_SAMPLE_PHOTO_UPLOADER_EMAIL'),
        ];

        $users = User::whereIn('email', $uploaderEmails)->get(['id']);
        foreach ($users as $user) {
            $uploaderIds[] = $user->id;
        }

        return $uploaderIds;
    }
}