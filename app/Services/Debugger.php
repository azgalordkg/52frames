<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;

class Debugger
{
    protected static $configKeyMainHolder = 'debugHelper';
    protected static $configKeyCanDebug = 'userCanDebug';

    protected static $allowedCommands = [
        'debug',
        'start_measure',
        'stop_measure',
        'add_measure',
        'measure',
    ];

    public function __construct(User $user = null, bool $hasCookie = false)
    {
        $debugAllowedLevel = env('APP_ADMIN_POINTS_WHO_CAN_DEBUG', 0);
        $userMaxAdminLevel = $user ? User::getMaxAdminPoints($user->id) : 0;
        $userCanDebug = $debugAllowedLevel && $userMaxAdminLevel && $userMaxAdminLevel >= $debugAllowedLevel;
        $canHaveDebug = env('DEBUGBAR_ENABLED', false) && $userCanDebug || $hasCookie;
        $methodDiff = $canHaveDebug ? 'enable' : 'disable';
        $methodName = $methodDiff . 'Debug';
        $this->$methodName(
            Debugger::$configKeyMainHolder,
            Debugger::$configKeyCanDebug,
            @$user->id,
            @$userMaxAdminLevel
        );
        return $canHaveDebug;
    }

    public function enableDebug(string $configKeyMainHolder, string $configKeyCanDebug, int $userId = null, int $maxAdminLevel = null)
    {
        $data = [$configKeyCanDebug => true];
        if ($userId) $data['userId'] = $userId;
        if ($maxAdminLevel) $data['maxAdminLevel'] = $maxAdminLevel;
        config([$configKeyMainHolder => $data]);
        \Debugbar::enable();
    }

    public function disableDebug(string $configKeyMainHolder, string $configKeyCanDebug)
    {
        $data = [$configKeyCanDebug => false];
        config([$configKeyMainHolder => $data]);
        \Debugbar::disable();
    }

    public static function canUserDebug(Request $request, User $user = null): bool
    {
        $debugHelperConfig = config('debugHelper');
        if (!$debugHelperConfig) {
            new Debugger($user, self::checkIfHasCookies($request));
            return self::canUserDebug($request, $user);
        }
        return (bool)$debugHelperConfig['userCanDebug'];
    }

    public static function checkIfHasCookies(Request $request)
    {
        $debugTerm = 'debug_mode';
        $cookies = $request->cookies->all();
        foreach ($cookies as $cookie => $value) {
            if ($cookie == $debugTerm) return true;
        }
        Debugger::log('$cookies: ', $cookies);
        return false;
    }

    public static function measureTimeSince(string $message, float $startTime = LARAVEL_START, float $endTime = null)
    {
        $endTime = $endTime ?: microtime(true);
        return self::execThroughDebugHandler('add_measure', $message, $startTime, $endTime);
    }

    public static function logTimeMeasurement(string $label, float $startTime, float $endTime = null)
    {
        $endTime = $endTime ?: microtime(true);
        return self::execThroughDebugHandler('add_measure', $label, $startTime, $endTime);
    }

    public static function startMeasuringTime(string $label, $message = null)
    {
        return self::execThroughDebugHandler('start_measure', $label, $message);
    }

    public static function stopMeasuringTime(string $label)
    {
        return self::execThroughDebugHandler('stop_measure', $label);
    }

    public static function measureFunctionExecTime(string $label, callable $callback)
    {
        return self::execThroughDebugHandler('measure', $callback);
    }

    public static function log()
    {
//        debug($var1, $someString, $intValue, $object);
        return self::execThroughDebugHandler('debug', ...func_get_args());
    }

    protected static function execThroughDebugHandler($command, $message = null, $startTime = null, $endTime = null)
    {
        $commandAllowed = in_array($command, self::$allowedCommands);
        if ($command != 'debug') {
            $args = [$message, $startTime, $endTime];
        } else {
            $args = func_get_args();
            array_shift($args);
        }
        return $commandAllowed ? $command(...$args) : null;
    }

}