<?php

namespace App\Services;

use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LocalOAuthService
{
    protected $appBaseUrl;

    protected $oAuthTokenPath;

    public function __construct()
    {
        $this->appBaseUrl = rtrim(env('LOCAL_OAUTH_SERVER_URL'), '/') . '/';
        $this->oAuthTokenPath = ltrim(env('LOCAL_OAUTH_SERVER_PATH'), '/');
    }

    public function loginUsingEmail(Request $request)
    {
        try {

            $http = new \GuzzleHttp\Client;
            $options = [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_secret' => env('LOCAL_API_CLIENT_SECRET'),
                    'client_id' => env('LOCAL_API_CLIENT_ID'),
                    'username' => $request->get('email'),
                    'password' => $request->get('password'),
                    'scope' => '',
                ],
            ];

            Log::info('options: ' . json_encode($options));
            Log::info('uri: ' . $this->appBaseUrl . $this->oAuthTokenPath);

            if (env('APP_ENV') == 'local') $options['verify'] = false;
            $response = $http->post($this->appBaseUrl . $this->oAuthTokenPath, $options);

            Log::info('response: ' . (string)$response->getBody());

            return json_decode((string)$response->getBody(), true);

        } catch (ClientException $exception) {

            $error = json_decode($exception->getResponse()->getBody());
            $httpCode = $exception->getCode();

            Log::info('clientexception: ' . $exception->getResponse()->getBody());

            return response()->json($error, $httpCode);

        }

    }
}
