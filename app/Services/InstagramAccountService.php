<?php declare(strict_types = 1);

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;

class InstagramAccountService extends SocialAccountService
{
    /**
     * @return string
     */
    protected function providerName(): string
    {
        return 'instagram';
    }

    /**
     * These field names will be translated to their correct scope names when Instagram receives them.
     * @return array
     */
    protected function providerFieldsList(): array
    {
        return [
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
        ];
    }

    /**
     * @param ProviderUser $providerUser
     * @return array
     */
    public function mapUserModel(ProviderUser $providerUser): array
    {
        $fullName = $providerUser->getName();
        $fullNameParts = explode(' ', $fullName);
        $firstName = @$fullNameParts[0] ?: null;
        $lastName  = @$fullNameParts[1] ?: null;

        $nickname = $providerUser->getNickname();
        if (null === $firstName && null === $lastName) {
            $firstName = $nickname;
        }

        $email = $providerUser->getEmail();

        return [
            'firstname' => $firstName,
            'lastname'  => $lastName,
            'email'     => $email,
        ];
    }

    /**
     * @param ProviderUser $providerUser
     * @return array
     */
    public function mapSocialAccountModel(ProviderUser $providerUser): array
    {
        return [
            'provider_user_id' => $providerUser->getId(),
            'provider'         => $this->providerName(),
            'avatar_small'     => @$providerUser->avatar,
            'avatar_big'       => @$providerUser->avatar_original,
        ];
    }
}
