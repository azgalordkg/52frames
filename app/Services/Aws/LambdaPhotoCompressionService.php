<?php

namespace App\Services\Aws;

use App\Services\Debugger;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class LambdaPhotoCompressionService
{
    protected $photo;
    protected $origFileExt;
    protected $localFilePath;
    protected $targetRelativePathAndFilenameOnly;

    protected $fileOpened;
    protected $photoCompressionQueueStorage;
    protected $photoCompressionCompletedStorage;

    public function __construct(UploadedFile $photo, string $targetRelativePathAndFilenameOnly)
    {
        $this->photo = $photo;
        $this->localFilePath = $photo->getPathname();
        $this->origFileExt = strtolower($photo->getClientOriginalExtension());
        $this->targetRelativePathAndFilenameOnly = $targetRelativePathAndFilenameOnly;
        $this->photoCompressionQueueStorage = Storage::disk('photoQueueS3');
        $this->photoCompressionCompletedStorage = Storage::disk('s3');
    }

    public function pushToQueue()
    {
        $localBucket = $this->getLocalAwsBucketName();
        $photoQueue = $this->getPhotoQueueBucketName();
        if (!$photoQueue || !$localBucket) return null;

        $result = $this->sendToS3PhotoQueueBucket();

        return $result;
    }

    public function awaitMainFileExistInCompletedFolder()
    {
        $result = false;

        $startTime = microtime(true);

        Debugger::log('--> LambdaPhotoCompressionService > awaitMainFileExistInCompletedFolder: the new photo will be here: ', $this->buildBasicTargetFilePath('jpg'));

        while (true) {
            $result = $this->doesMainFileExistInCompletedFolder();

            $timeNow = microtime(true);
            $timeDiff = $timeNow - $startTime;
            $timeWithinLimits = $timeDiff <= env('AWS_S3_BUCKET_PHOTO_COMPRESSION_AWAIT_COMPLETED_SECONDS', 60);

            if ($result !== false || !$timeWithinLimits) break;
            usleep(100);
        }

        if (!$result) Debugger::log('--> LambdaPhotoCompressionService > awaitMainFileExistInCompletedFolder: waiting for photo to appear in S3: timed-out, aborting');

        return $result;
    }

    protected function doesMainFileExistInCompletedFolder()
    {
        try {
            $targetFilePath = $this->buildBasicTargetFilePath('jpg');
            return $this->photoCompressionCompletedStorage->size($targetFilePath);
        } catch (\Exception $exception) {
            return false;
        }
    }

    protected function sendToS3PhotoQueueBucket()
    {
        try {
            $targetFilePath = $this->buildQueueFilePath();
            $file = File::get($this->photo);
            return $this->photoCompressionQueueStorage->put($targetFilePath, $file);
        } catch (\Exception $exception) {
            Debugger::log('sendToS3PhotoQueueBucket > exception: ', $exception->getMessage());
            return $exception;
        }
    }

    protected function getLocalAwsBucketName(): ?string
    {
        return env('AWS_S3_BUCKET', null);
    }

    protected function getPhotoQueueBucketName(): ?string
    {
        return env('AWS_S3_BUCKET_PHOTO_COMPRESSION_QUEUE', null);
    }

    protected function getPhotoQueueBucketInitFolder(): ?string
    {
        return env('AWS_S3_BUCKET_PHOTO_COMPRESSION_INIT_FOLDER', null);
    }

    protected function buildQueueFilePath()
    {
        $completedBucketName = $this->getLocalAwsBucketName();
        $basicFilePath = $this->buildBasicTargetFilePath();
        return $this->getPhotoQueueBucketInitFolder() . "/$completedBucketName/$basicFilePath";
    }

    protected function buildBasicTargetFilePath(string $extension = null)
    {
        return implode("", [
            $this->targetRelativePathAndFilenameOnly,
            '.',
            $extension ?: $this->origFileExt
        ]);
    }
}