<?php

namespace App\Services;

use App\Model;
use App\User;
use App\UserSetting;

abstract class UserSettingsHelper extends Model
{
    public static function updateAndSave(array $keyValuePair, User $user): UserSetting
    {
        $userSetting = $user->settings;
        $changed = false;
        foreach ($keyValuePair as $columnName => $newValue) {
            if (isset($userSetting->$columnName)) {
                $userSetting->$columnName = $newValue;
                $changed = true;
            }
        }
        if ($changed) $userSetting->save();
        return $userSetting;
    }
}