<?php

namespace App\Services;

use Spatie\Newsletter\NewsletterFacade;

class MailchimpService
{

    public $mailchimpApi;

    /**
     * MailchimpService constructor.
     * Documentation: http://developer.mailchimp.com/documentation/mailchimp/reference/overview/
     * Github: https://github.com/drewm/mailchimp-api
     * Adding an Email address to a Workflow Email:
     * http://developer.mailchimp.com/documentation/mailchimp/reference/automations/emails/queue/#create-post_automations_workflow_id_emails_workflow_email_id_queue
     *
     * @param NewsletterFacade $newsletter
     */
    public function __construct(NewsletterFacade $newsletter)
    {
        $this->mailchimpApi = $newsletter::getApi();
    }

    public function success()
    {
        return $this->mailchimpApi->success();
    }

    public function getLastError()
    {
        return $this->mailchimpApi->getLastError();
    }

    public function updateUserMergeFields($email, array $mergeFields, $listId = '')
    {
        if (!MailchimpHelper::shouldProcess()) return null;

        $list_id = $listId ?: env('MAILCHIMP_LIST_ID');
        $subscriber_hash = $this->mailchimpApi->subscriberHash($email);

        return $this->mailchimpApi->patch("lists/$list_id/members/$subscriber_hash", [
            'merge_fields' => $mergeFields
        ]);
    }
}