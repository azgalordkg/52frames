<?php

namespace App\Services;

use App\SocialAccount;
use App\User;
use Exception;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as ProviderUser;

abstract class SocialAccountService
{
    protected $providerUser;

    /**
     * @return string
     */
    abstract protected function providerName(): string;

    /**
     * @param ProviderUser $providerUser
     * @return array
     */
    abstract protected function mapUserModel(ProviderUser $providerUser): array;

    /**
     * @param ProviderUser $providerUser
     * @return array
     */
    abstract protected function mapSocialAccountModel(ProviderUser $providerUser): array;

    protected $providerScopesTermName = 'scopes';

    protected function providerScopesList(): array
    {
        return [
//            'public_profile',
//            'email',
        ];
    }

    protected $providerFieldsTermName = 'fields';

    protected function providerFieldsList(): array
    {
        return [
//            'id',
//            'name',
//            'first_name',
//            'last_name',
//            'email',
        ];
    }

    protected function detectIfScopeOrFields(): ?string
    {
        if (count($list = $this->providerScopesList())) return $this->providerScopesTermName;
        if (count($list = $this->providerFieldsList())) return $this->providerFieldsTermName;
        throw new \Exception('Please override providerScopesList() or providerFieldsList()');
    }

    protected function detectProviderScopesOrFieldnamesList(): array
    {
        $targetMethod = 'provider' . ucfirst($this->detectIfScopeOrFields()) . 'List';
        return $this->$targetMethod();
    }

    protected function getProviderDriver()
    {
        return Socialite::driver($this->providerName());
    }

    protected function getProviderDriverWithFields()
    {
        $instance = $this->getProviderDriver();
        $methodName = $this->detectIfScopeOrFields();
        $inputArray = $this->detectProviderScopesOrFieldnamesList();
        if ($methodName && method_exists($instance, $methodName))
            return $instance->$methodName($inputArray);
        return $instance;
    }

    public function redirectUrl()
    {
        return $this->getProviderDriverWithFields()->redirect();
    }

    public function getProviderUser()
    {
        return $this->providerUser = $this->getProviderDriverWithFields()->user();
    }

    public function getProviderUserByToken($accessToken)
    {
        return $this->providerUser = $this->getProviderDriverWithFields()->userFromToken($accessToken);
    }

    public function getSocialAccountByProviderId(ProviderUser $providerUser = null)
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve first parameter (ProviderUser), please specify.');

        return SocialAccount::whereProvider($this->providerName())
            ->whereProviderUserId($providerUser->getId())
            ->first();
    }

    public function createSocialAccountAndRelateToUser(User $user, ProviderUser $providerUser = null)
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve first parameter (User), please specify.');

        $formattedProviderUser = $this->mapSocialAccountModel($providerUser);

        $socialAccount = new SocialAccount();
        $socialAccount->fill($formattedProviderUser);
        $socialAccount->provider = $this->providerName();
        $socialAccount->user()->associate($user);
        $socialAccount->save();

        return $socialAccount;
    }

    protected function makeTokenIdentifier(ProviderUser $providerUser)
    {
        return implode(':', [
            $this->providerName(),
            $providerUser->getId()
        ]);
    }

    public function createOAuthToken(ProviderUser $providerUser = null)
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve First parameter, please specify.');

        $socialAccount = $this->getSocialAccountByProviderId($providerUser);
        if (!$socialAccount) throw new Exception('The SocialAccount of this User does not exist in the database yet.');

        return $socialAccount->user->createToken($this->makeTokenIdentifier($providerUser))->accessToken;
    }

    public function deleteSocialAccount(ProviderUser $providerUser = null)
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve First parameter, please specify.');

        $socialAccount = $this->getSocialAccountByProviderId($providerUser);
        if (!$socialAccount) throw new Exception('The SocialAccount of this User does not exist in the database yet.');

        SocialAccount::whereProvider($this->providerName())
            ->whereProviderUserId($providerUser->getId())
            ->whereUserId($socialAccount->user_id)
            ->delete();

        return true;
    }

    public function getOrCreateUser(ProviderUser $providerUser = null): User
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve First parameter, please specify.');

        $account = $this->getSocialAccountByProviderId($providerUser);
        if ($account) return $account->user;

        $user = User::whereEmail($providerUser->getEmail())->first();
        if (!$user) $user = User::create($this->mapUserModel($providerUser));
        $this->createSocialAccountAndRelateToUser($user);

        return $user;
    }

    public function authenticateUser(ProviderUser $providerUser = null)
    {
        $providerUser = $providerUser ?: $this->providerUser;
        if (!$providerUser) throw new Exception('Cannot resolve First parameter, please specify.');

        return auth()->login($providerUser->user);
    }

}