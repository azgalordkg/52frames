<?php

namespace App\Services;

use App\Jobs\JobQueue;
use App\User;
use Exception;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class JobQueueException extends Exception
{
    protected static $originJobName;

    protected static function getStackTrace(Exception $exception)
    {
        $traceArray = explode("\n", $exception->__toString());
        return array_map(function ($entry) {
            $entry = stripslashes($entry);
            $entry = htmlentities($entry);
            $entry = str_replace('/', '&#47;', $entry);
            return $entry;
        }, $traceArray);
    }

    protected static function formErrorMessage(Exception $exception, array $extraInfo = null)
    {
        $inputs = [
            'source' => self::$originJobName,
            'error' => [
                'message' => json_encode($exception->getMessage()),
                'code' => json_encode($exception->getCode()),
                'trace' => self::getStackTrace($exception)
            ]
        ];
        if ($extraInfo) $inputs['extra_info'] = $extraInfo;
        return $inputs;
    }

    protected static function failNotSetupProperly()
    {
        $jobName = self::$originJobName;
        echo "$jobName can't send failure email to because the failed() method is not setup properly. Please try again";
    }

    protected static function notValidTargetEmailAdd(string $targetEmail)
    {
        $jobName = self::$originJobName;
        echo "$jobName can't send failure email to $targetEmail because the user does not exist in the database.\n";
        echo "Try editing NOTIF_PROC_ISSUES_SENDTO_EMAIL in the environment variables.";
    }

    protected static function notValidMailable()
    {
        $jobName = self::$originJobName;
        $varName = '$emailTemplateGenerator';
        echo "$jobName --> $varName does not return a valid instance object of Illuminate\Mail\Mailable.\n";
    }

    public static function sendFailedJobEmail(JobQueue $failingJob, Exception $exception, array $extraInfo = null, callable $emailTemplateGenerator = null, User $sendTo = null)
    {
        self::$originJobName = get_class($failingJob);
        if (!$emailTemplateGenerator) return self::failNotSetupProperly();
        $targetEmail = env('NOTIF_PROC_ISSUES_SENDTO_EMAIL', '[no email address found in .env file]');
        $recipient = $sendTo ?: $targetEmail ? User::whereEmail($targetEmail)->first() : null;
        if (!$recipient) return self::notValidTargetEmailAdd($targetEmail);
        $inputs = self::formErrorMessage($exception, $extraInfo);
        $emailTemplate = $emailTemplateGenerator($inputs);
        if (!$emailTemplate || !is_a($emailTemplate, Mailable::class)) return self::notValidMailable();
        return Mail::to($recipient)->queue($emailTemplate);
    }

}