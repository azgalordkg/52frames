<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;

class FacebookAccountService extends SocialAccountService
{
    protected function providerName(): string
    {
        return 'facebook';
    }

    /*
     * we are not using this because the "public profile" doesn't have first_name and last_name, just a combined "name"
     *
    protected function providerScopesList(): array
    {
        return [
            'public_profile',
            'email',
//            'user_friends',
//            'user_birthday'
        ];
    }
    */

    /**
     * These field names will be translated to their correct scope names when Facebook receives them.
     * @return array
     */
    protected function providerFieldsList(): array
    {
        return [
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
        ];
    }

    public function mapUserModel(ProviderUser $providerUser): array
    {
        if (!($firstName = @$providerUser->user['first_name']) || !($lastName = @$providerUser->user['last_name']))
            throw new \Exception("Can't get First name or Last name!");
        return [
            'firstname' => $firstName,
            'lastname' => $lastName,
            'email' => $providerUser->getEmail(),
        ];
    }

    public function mapSocialAccountModel(ProviderUser $providerUser): array
    {
        return [
            'provider_user_id' => $providerUser->getId(),
            'provider' => $this->providerName(),
            'avatar_small' => @$providerUser->avatar,
            'avatar_big' => @$providerUser->avatar_original,
//            'profile_url' => @$providerUser->profileUrl,
//            'gender' => @$providerUser->user['gender'],
//            'locale' => @$providerUser->user['locale'],
//            'timezone' => @$providerUser->user['timezone']
        ];
    }
}