<?php

namespace App\Services;

use Illuminate\Http\JsonResponse;

class Transport
{
    public static function returnNow(JsonResponse $response): null
    {
        response($response, $response->status(), $response->headers->all())->send();
        return null;
    }
}