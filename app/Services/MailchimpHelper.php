<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

abstract class MailchimpHelper extends Controller
{
    abstract protected function mapRequestedMethods(Request $request): ?JsonResponse;

    public static function shouldProcess(): bool
    {
        return !env('MAILCHIMP_DONT_PROCESS_REQUEST');
    }

    protected function execRequestedMethod(Request $request): JsonResponse
    {
        if (MailchimpHelper::shouldProcess()) {
            $result = $this->mapRequestedMethods($request);
            if (!$result) $result = $this->requestedMethodNotFound();
            return $result;
        }
        return $this->ignoreRequest();
    }

    protected function requestedMethodNotFound(): JsonResponse
    {
        return response()->json([
            'validation_errors' => [
                'type' => ['Type of request not valid.']
            ]
        ], 400);
    }

    protected function ignoreRequest()
    {
        return response()->json([
            'ignored' => 'MailchimpHelper not processing request.'
        ], 202);
    }
}