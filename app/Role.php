<?php

namespace App;

class Role extends Model
{
    protected $fillable = [
        'dynamic',
        'access_level_points',
        'display_name',
        'keyword'
    ];

    public function usersRoles()
    {
        return $this->hasMany(UserRole::class, 'role_id', 'id');
    }

}
