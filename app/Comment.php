<?php

namespace App;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'photo_id',
        'album_id',
        'reply_to_comment_id',
        'message',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function replies()
    {
        return $this->hasMany(Comment::class, 'reply_to_comment_id', 'id');
    }

    public function parentComment()
    {
        return $this->belongsTo(Comment::class, 'reply_to_comment_id', 'id');
    }
}
