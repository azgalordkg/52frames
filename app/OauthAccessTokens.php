<?php

namespace App;

class OauthAccessTokens extends Model
{
    protected $fillable = [];

    protected $visible = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
