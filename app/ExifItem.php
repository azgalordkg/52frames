<?php

namespace App;

class ExifItem extends Model
{
    protected $fillable = [
        'parent_id',
        'group_name',
        'type',
        'name',
        'icon',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function parent()
    {
        return $this->belongsTo(ExifItem::class, 'parent_id', 'id');
    }

    public function members()
    {
        return $this->hasMany(ExifItem::class, 'parent_id', 'id');
    }
}
