<?php

namespace App;

class CsvUploadedRow extends Model
{
    protected $fillable = [
        'file_id',
        'cancelled',
        'migrated',
        'migrated_to_user_id',
        'album_id',
        'photo_id',
        'row_data',
        'validation_report'
    ];

    protected $hidden = [
        'cancelled'
    ];

    protected $casts = [
        'row_data' => 'array',
        'cancelled' => 'boolean',
        'migrated' => 'boolean',
        'validation_report' => 'array'
    ];

    public function scopeWithMigratedRows($query, array $columns = null)
    {
        $columns = $columns ?: ['user', 'album', 'photo.album', 'photo.owner'];
        return $query->with($columns);
    }

    public function file()
    {
        return $this->belongsTo(CsvUploadedFile::class, 'file_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'migrated_to_user_id', 'id');
    }

    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class, 'photo_id', 'id');
    }
}
