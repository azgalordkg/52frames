<?php

namespace App;

class UserProfilePhoto extends Photo
{
    public const TYPE_AVATAR = 'avatar';
    public const TYPE_BACKGROUND = 'background';

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id',
        'original_res_filename',
        'thumbnail_filename',
        'hi_res_filename',
        'imported_from_row_id',
        'imported_info',
    ];

    protected $hidden = [
        'user_id',
        'imported_from_row_id',
        'imported_info',
        'original_res_filename',
        'uploaded_to_dropbox',
        'uploaded_to_cdn',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'imported_info' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
