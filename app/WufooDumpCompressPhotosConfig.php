<?php

namespace App;

class WufooDumpCompressPhotosConfig extends Model
{
    protected $table = 'wufoo_dump_compress_photos_config';

    protected $fillable = [
        'key',
        'value'
    ];

}
