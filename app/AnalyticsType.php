<?php

namespace App;

class AnalyticsType extends Model
{
    protected $fillable = [
        'type'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public static function insertRules()
    {
        return [
            'type' => 'required|string'
        ];
    }

    public function triggerTerms()
    {
        return $this->hasMany(AnalyticsTriggerTerm::class, 'type_id', 'id');
    }
}
