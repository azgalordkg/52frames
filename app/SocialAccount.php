<?php

namespace App;

class SocialAccount extends Model
{
    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
        'avatar_small',
        'avatar_big',
        'profile_url',
        'gender',
        'locale',
        'timezone',
    ];

    protected $visible = [
        'id',
        'provider',
        'avatar_small',
        'avatar_big',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
