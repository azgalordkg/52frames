<?php

namespace App\Providers\Modified\Laravel\Socialite;

use Laravel\Socialite\Two\FacebookProvider as FacebookProviderBase;

class FacebookProvider extends FacebookProviderBase
{
    /**
     * The Graph API version for the request.
     *
     * @var string
     */
    protected $version = 'v3.0';
}