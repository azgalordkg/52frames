<?php

namespace App\Providers\Modified\Laravel\Socialite;

use Laravel\Socialite\SocialiteManager as SocialiteManagerBase;

class SocialiteManager extends SocialiteManagerBase
{
    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createFacebookDriver()
    {
        $config = $this->app['config']['services.facebook'];

        return $this->buildProvider(
            FacebookProvider::class, $config
        );
    }

    protected function createInstagramDriver()
    {
        $config = $this->app['config']['services.instagram'];

        return $this->buildProvider(
            InstagramProvider::class, $config
        );
    }
}