<?php

namespace App\Providers\Modified\Laravel\Socialite;

use Laravel\Socialite\Contracts\Factory;
use Laravel\Socialite\SocialiteServiceProvider as SocialiteServiceProviderBase;

class SocialiteServiceProvider extends SocialiteServiceProviderBase
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Factory::class, function ($app) {
            return new SocialiteManager($app);
        });
    }
}