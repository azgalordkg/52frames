<?php declare(strict_types = 1);

namespace App\Providers\Modified\Laravel\Socialite;

use SocialiteProviders\Instagram\Provider as BaseInstagramProvider;

class InstagramProvider extends BaseInstagramProvider
{
    const IDENTIFIER = 'instagram';
}
