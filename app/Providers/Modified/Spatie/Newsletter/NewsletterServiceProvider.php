<?php

namespace App\Providers\Modified\Spatie\Newsletter;

use DrewM\MailChimp\MailChimp;
use Spatie\Newsletter\NewsletterListCollection;
use Spatie\Newsletter\NewsletterServiceProvider as NewsletterServiceProviderBase;

class NewsletterServiceProvider extends NewsletterServiceProviderBase
{
    public function register()
    {
        $this->app->singleton(Newsletter::class, function () {
            $mailChimp = new Mailchimp(config('laravel-newsletter.apiKey'));

            $mailChimp->verify_ssl = config('laravel-newsletter.ssl', true);

            $configuredLists = NewsletterListCollection::createFromConfig(config('laravel-newsletter'));

            return new Newsletter($mailChimp, $configuredLists);
        });

        $this->app->alias(Newsletter::class, 'laravel-newsletter');
    }
}
