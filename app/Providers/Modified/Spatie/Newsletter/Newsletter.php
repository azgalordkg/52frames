<?php

namespace App\Providers\Modified\Spatie\Newsletter;

use App\Services\MailchimpHelper;
use Spatie\Newsletter\Newsletter as NewsletterBase;

class Newsletter extends NewsletterBase
{
    public function subscribe($email, $mergeFields = [], $listName = '', $options = [])
    {
        if (!MailchimpHelper::shouldProcess()) return false;

        return parent::subscribe($email, $mergeFields, $listName, $options);
    }
}