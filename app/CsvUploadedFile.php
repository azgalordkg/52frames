<?php

namespace App;

use App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules\PhotosCategory as PhotosCategoryValidationRules;
use App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules\AlbumsCategory as AlbumsCategoryValidationRules;
use App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules\UsersCategory as UsersCategoryValidationRules;

class CsvUploadedFile extends Model
{
    protected $fillable = [
        'user_id',
        'processing',
        'category',
        'filename',
        'header_columns',
        'header_columns_with_samples',
        'records_found',
        'columns_selected',
        'validation_summary'
    ];

    protected $hidden = [
        'processing'
    ];

    protected $casts = [
        'header_columns' => 'array',
        'header_columns_with_samples' => 'array',
        'columns_selected' => 'array',
        'validation_summary' => 'array',
    ];

    public static $categoryClassMapping = [
        'users' => UsersCategoryValidationRules::class,
        'albums' => AlbumsCategoryValidationRules::class,
        'photos' => PhotosCategoryValidationRules::class
    ];

    public static function getCategories()
    {
        return array_keys(self::$categoryClassMapping);
    }

    public function uploader()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function rows()
    {
        return $this->hasMany(CsvUploadedRow::class, 'file_id', 'id');
    }
}
