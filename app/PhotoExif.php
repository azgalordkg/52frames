<?php

namespace App;

class PhotoExif extends Model
{
    protected $fillable = [
        'photo_id',
        'exif_item_id',
        'value',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function exifQuestion()
    {
        return $this->belongsTo(ExifItem::class, 'exif_item_id', 'id');
    }

    public function exifMember()
    {
        return $this->belongsTo(ExifItem::class, 'value', 'id')
            ->where('parent_id', '>', 0);
    }

}
