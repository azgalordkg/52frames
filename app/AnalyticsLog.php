<?php

namespace App;

class AnalyticsLog extends Model
{
    protected $fillable = [
        'trigger_term_id',
        'user_id',
        'relative_page_path',
        'tracked_item_id',
        'extra_json'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'extra_json' => 'array'
    ];

    public static function insertRules()
    {
        return [
            'relative_page_path' => 'required|string',
            'tracked_item_id' => 'required|integer',
            'extra_json' => 'nullable|array'
        ];
    }

    public function ofUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function triggerTerm()
    {
        return $this->belongsTo(AnalyticsTriggerTerm::class, 'trigger_term_id', 'id');
    }

    public function analyticsTrackedItem()
    {
        return $this->belongsTo($this->triggerTerm->db_model, 'tracked_item_id', 'id');
    }
}
