<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Photo extends Model
{
    protected $fillable = [
        'imported_from_row_id',
        'imported_info',
        'num_views',
        'title',
        'caption',
        'is_graphic',
        'has_nudity',
        'model_consent',
        'qualifies_extra_credit',
        'critique_level',
        'screencast_critique',
        'location',
        'album_score',
    ];

    protected $hidden = [
        'imported_from_row_id',
        'original_res_filename',
        'uploaded_to_dropbox',
        'uploaded_to_cdn',
    ];

    protected $casts = [
        'imported_info' => 'array'
    ];

    public function getFilenameAttribute()
    {
        $filename = $this->original_res_filename;
        $dirName = dirname($filename) . '/';
        return str_replace($dirName, '', $filename);
    }

    public static function hasUserHearted(int $photoId): bool
    {
        $user = Auth::guard('api')->user();
        $userId = $user ? $user->id : 0;

        return !!Love::whereUserId($userId)->wherePhotoId($photoId)->count();
    }

    public static function howManyPhotosUploadedBefore(User $user): int
    {
        return Photo::whereUserId($user->id)->count();
    }

    public static function addExifData(Builder $builder): Builder
    {
        return $builder->with([
            'owner.signedManifesto',
            'exifAnswers.exifQuestion.parent',
            'exifAnswers.exifMember',
        ]);
    }

    public static function newEntryMaxKbLimit(): int
    {
        return 10240;
    }

    /**
     * Rules for adding a new Photo in the database.
     * @return array
     */
    public static function insertRules()
    {
        return [
            'album_id' => 'required|exists:albums,id',
            'title' => 'required|string|max:50',
            'caption' => 'required|string|max:800',
            'tags' => 'array|max:10',
            'tags.*' => [
                'max:30',
                'regex:/^#?[a-zA-z0-9]+$/',
            ],
            'photo' => 'required|mimes:jpeg,png|max:' . Photo::newEntryMaxKbLimit(),
            'screencast_critique' => 'boolean',
            'location' => 'nullable|string',
            'about_photo' => 'nullable|boolean',
            'about_photo_answers' => 'array',
            'about_photo_answers.*' => 'boolean',
            'camera_settings' => 'nullable|boolean',
            'camera_settings_answers' => 'array',
            'camera_settings_answers.*' => 'string|nullable',
            'critique_level' => 'required|string',
            'qualifies_extra_credit' => 'boolean',
            'is_graphic' => 'boolean',
            'has_nudity' => 'boolean',
            'model_consent' => 'required_if:has_nudity,1|boolean|in:1',
            'ownership_confirmation' => 'accepted',
            'album_score' => 'nullable|integer|min:0|max:5'
        ];
    }

    /**
     * Custom error messages.
     * @return array
     */
    public static function insertRulesErrorMessages()
    {
        return [
            'model_consent.required_if' => 'The model consent field is required.'
        ];
    }

    public static function csvImportRules()
    {
        $rules = self::insertRules();
        unset($rules['album_id']);
        unset($rules['photo']);
        unset($rules['tags']);
        unset($rules['tags.*']);
        return $rules;
    }

    public static function generateS3DownloadedPhotoLocalPath(string $s3FilePath): string
    {
        return storage_path("app/$s3FilePath");
    }

    public static function checkIfS3PhotoDownloadedToLocal(string $s3FilePath): bool
    {
        $filename = self::generateS3DownloadedPhotoLocalPath($s3FilePath);
        return file_exists($filename);
    }

    /**
     * Rules for updating an existing Photo in the database.
     * @return array
     */
    public static function updateRules()
    {
        $rules = Photo::insertRules();
        unset($rules['album_id']);
        return $rules;
    }

    /**
     * Validate Manual EXIF entries.
     * @param array $inputs
     * @return array
     */
    public static function validateManualExifInputs(array &$inputs)
    {
        $errors = [];
        foreach ($inputs as $key => $input) {
            switch ($key) {
                case 'about_photo_answers':
                    $parentRows = ExifItem::whereParentId(null)->whereGroupName('about_photo_questions')->get()->toArray();
                    $parentIds = array_column($parentRows, 'id');
                    foreach ($input as $exifItemId => $answer) {
                        $exifItem = ExifItem::whereIn('parent_id', $parentIds)->whereId($exifItemId)->first();
                        if (!$exifItem) {
                            if (!@$errors[$key]) $errors[$key] = [];
                            if (!@$errors[$key][$exifItemId]) $errors[$key][$exifItemId] = [];
                            $errors[$key][$exifItemId][] = 'Cannot find id in database.';
                        }
                        if ($answer !== 1 && $answer !== 0) {
                            if (!@$errors[$key]) $errors[$key] = [];
                            if (!@$errors[$key][$exifItemId]) $errors[$key][$exifItemId] = [];
                            $errors[$key][$exifItemId][] = 'Value must be a boolean.';
                        }
                    }
                    break;
                case 'camera_settings_answers':
                    foreach ($input as $exifItemId => $answer) {
                        $parentRow = ExifItem::whereParentId(null)->whereGroupName('camera_settings_questions')->whereId($exifItemId)->first();
                        if ($parentRow->type == 'array') {
                            $selectedId = ExifItem::whereParentId($parentRow->id)->whereId($answer)->first();
                            if (!$selectedId) {
                                if (!@$errors[$key]) $errors[$key] = [];
                                if (!@$errors[$key][$exifItemId]) $errors[$key][$exifItemId] = [];
                                $errors[$key][$exifItemId][] = 'Answer id cannot be found in database.';
                            }
                        } else if ($parentRow->type == 'text') {
                            if (!$answer) {
//                                if (!@$errors[$key]) $errors[$key] = [];
//                                if (!@$errors[$key][$exifItemId]) $errors[$key][$exifItemId] = [];
//                                $errors[$key][$exifItemId][] = 'Answer must not be blank';
                                unset($inputs[$key][$exifItemId]);
                            }
                        }
                    }
                    break;
            }
        }
        return $errors;
    }

    public function analyticsItemHandler(AnalyticsLog $newlyAddedRecord, \Closure $successCb = null, \Closure $failedCb = null)
    {
        // if you have a lot of analytics "item types" and you need a reference to break your code apart,
        // you can switch() on the keyword found on the $newlyAddedRecord->triggerTerm->term

        $this->lockForUpdate();
        $this->num_views++;
        $this->save();
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function exifAnswers()
    {
        return $this->hasMany(PhotoExif::class, 'photo_id', 'id');
    }

    public function loves()
    {
        return $this->hasMany(Love::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function migratedFromCsvRow()
    {
        return $this->belongsTo(CsvUploadedRow::class, 'imported_from_row_id', 'id');
    }

}
