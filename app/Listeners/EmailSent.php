<?php

namespace App\Listeners;

use App\Notification;
use Illuminate\Mail\Events\MessageSending;

class EmailSent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSending $event, Notification $notification = null)
    {

    }
}
