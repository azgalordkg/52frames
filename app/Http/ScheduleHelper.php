<?php

namespace App\Http;

use App\Jobs\Scheduled\DailyEmailNotificationsDbScanner;
use App\Jobs\Scheduled\Dropbox\ScanWufooDump;
use App\Traits\HelpsSchedules;
use Illuminate\Console\Scheduling\Schedule;

class ScheduleHelper
{
    use HelpsSchedules;

    public static function scheduleDailyEmailNotifications(Schedule $systemHook)
    {
        $shouldPerMinute = env('APP_SCHEDULER_DAILY_TO_PER_MINUTE');
        self::formToSchedule($systemHook, [
            'when' => $shouldPerMinute ? 'everyMinute' : 'daily',
            'noOverlapTerm' => env('NOTIF_DELIVERY_EMAILS_DAILY'),
            'callback' => function () {
                DailyEmailNotificationsDbScanner::dispatch();
            }
        ]);
    }

    public static function scheduleDropboxPhotoCompressFromWufooDump(Schedule $systemHook)
    {
        self::formToSchedule($systemHook, [
            'when' => 'everyMinute',
            'noOverlapTerm' => env('DROPBOX_COMMITTEE_PHOTO_OVERLAP_NAME'),
            'callback' => function () {
                ScanWufooDump::dispatch();
            }
        ]);
    }

}