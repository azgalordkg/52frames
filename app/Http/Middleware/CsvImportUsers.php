<?php

namespace App\Http\Middleware;

use Closure;

class CsvImportUsers
{
    protected $defaultRoles = [
        'csv_uploader',
        'csv_data_merge',
        'csv_data_manager'
    ];

    public function handle($request, Closure $next, ...$guards)
    {
        $guards = count($guards) ? $guards : $this->defaultRoles;

        $can = $request->user()->hasRolesOrHigher($guards);

        if (!$can) return response()->json([
            'not_enough_admin_points' => true
        ], 403);

        return $next($request);
    }
}
