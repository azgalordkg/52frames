<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\HelpsLogin;

class CommitteeMember
{
    use HelpsLogin;

    /**
     * Checks if the logged-in user is a Committee member.
     * Used by Laravel-File-Manager and TinyMCE.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->stopExpiredSession($request);

        $this->checkMaxAdminPoints($user, 200000);

        return $next($request);
    }
}
