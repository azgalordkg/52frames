<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Debugger;
use Illuminate\Support\Facades\Auth;

class CanUserHaveDebugMode
{
    /**
     * Give user a Debugger on the browser, if he has enough access.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::guard('web')->user();
        Debugger::canUserDebug($request, $user);
        return $next($request);
    }
}
