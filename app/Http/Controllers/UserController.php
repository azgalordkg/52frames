<?php

namespace App\Http\Controllers;

use App\Album;
use App\Jobs\DeleteEverythingAboutUser;
use App\Photo;
use App\Services\Helpers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private const SUGGEST_USERS_LIST_LIMIT = 10;

    public function __construct()
    {
        $this->middleware('auth:api')->except('suggestPhotographers');
    }

    /**
     * Gets all the relevant information about the logged-in User.
     * @param Request $request
     * @return array
     */
    public function apiMe(Request $request)
    {
        $user = $request->user();

        $userModel = User::whereId($user->id)->with([
            'signedManifesto',
            'settings'
        ])->first();

        $maxAdminPoints = User::getMaxAdminPoints($user->id);
        $previousPhotosCount = Photo::howManyPhotosUploadedBefore($user);

        return array_merge($userModel->toArray(), [
            'avatar' => $user->avatar,
            'max_admin_points' => $maxAdminPoints,
            'has_uploaded_before' => !!$previousPhotosCount,
            'albums_recent' => Album::getRecent($request)
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function suggestPhotographers(Request $request)
    {
        return $this->suggestUsers($request, true);
    }

    /**
     * @param Request $request
     * @param bool $withManifesto
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function suggestUsers(Request $request, bool $withManifesto = false)
    {
        $validation = Validator::make($request->all(), [
            'query' => 'required|string'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $result = [];
        $user = $request->user();
        $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

        $publicKeys  = ['id', 'name', 'handle'];
        $privateKeys = ['email'];
        $keys = $maxAdminPoints < 1000000 ? $publicKeys : array_merge($publicKeys, $privateKeys);

        $userInput = $request->get('query');
        $users = User::where(function ($query) use ($userInput) {
            $query->where('firstname', 'like', "%$userInput%")
                ->orWhere('lastname', 'like', "%$userInput%")
                ->orWhere(DB::raw("CONCAT(`firstname`, ' ', `lastname`)"), 'like', "%$userInput%")
                ->orWhere(DB::raw("CONCAT(`lastname`, ' ', `firstname`)"), 'like', "%$userInput%")
                ->orWhere('handle', 'like', "%$userInput%");
        })
            ->orderBy('firstname', 'asc')
            ->orderBy('lastname', 'asc')
            ->orderBy('handle', 'asc')
            ->limit(self::SUGGEST_USERS_LIST_LIMIT)->get();

        foreach ($users as $user) {
            if ($withManifesto && !$user->manifesto) continue;
            $userArr = Helpers::filterByKeysModel($user, $keys);
            $result[] = $userArr;
        }

        return $result;
    }

    /**
     * Deletes the currently logged-in User account.
     *
     * @param $id
     * @param Request $request
     * @return array
     */
    public function destroy($id, Request $request)
    {
        $targetUser = User::whereId($id)->first();
        if (!$targetUser) return response()->json([
            'error' => true,
            'no_such_user' => true
        ], 404);

        $loggedInUser = $request->user();
        if ($targetUser->id != $loggedInUser->id) {
            $maxAdminPoints = User::getMaxAdminPoints($loggedInUser->id);
            if ($maxAdminPoints < 1000000) {
                return response()->json([
                    'not_enough_admin_points' => true
                ], 403);
            }
        }

        DeleteEverythingAboutUser::dispatch($targetUser);

        return ['queued' => true, 'user_id' => (int)$id];
    }
}
