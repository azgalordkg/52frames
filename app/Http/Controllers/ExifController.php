<?php

namespace App\Http\Controllers;

use App\ExifItem;
use Illuminate\Http\Request;

class ExifController extends Controller
{
    /**
     * ExifController constructor.
     */
    public function __construct()
    {
        // TODO: uncomment it
//        $this->middleware('auth:api');
    }

    /**
     * Gets the list of EXIF Items from the database.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return ExifItem::whereParentId(null)->with('members')->get();
    }
}
