<?php

namespace App\Http\Controllers;

use App\Album;
use App\BypassAlbumUpload;
use App\Mail\CanUploadToAlbumAgain;
use App\Photo;
use App\Services\Helpers;
use App\Traits\HelpsFileUploads;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class BypassUploadController extends Controller
{
    use HelpsFileUploads;

    /**
     * Contains the middleware, etc.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function getListRules()
    {
        return [
            'album_id' => 'required|exists:albums,id'
        ];
    }

    protected function getInsertRules()
    {
        $listRules = $this->getListRules();
        return array_merge($listRules, [
            'user_id' => 'required|exists:users,id'
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), $this->getListRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = $request->user();

        $maxAdminPoints = User::getMaxAdminPoints($user->id);
        if ($maxAdminPoints < 1000000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $result = [];

        $allowedUsers = BypassAlbumUpload::whereAlbumId($request->album_id)->with('user')->get();
        foreach ($allowedUsers as $allowedUser) {
            $allowedUserArr = $allowedUser->toArray();
            $allowedUserAvatar = Helpers::filterByKeysModel($allowedUser->user, [
                'avatar',
                'name'
            ]);
            $result[] = array_merge_recursive($allowedUserArr, [
                'user' => $allowedUserAvatar
            ]);
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), $this->getInsertRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = $request->user();

        $maxAdminPoints = User::getMaxAdminPoints($user->id);
        if ($maxAdminPoints < 1000000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $photoIdRemoved = null;
        $exists = BypassAlbumUpload::whereAlbumId($request->album_id)->whereUserId($request->user_id)->count();
        if (!$exists) {

            $photo = Photo::whereAlbumId($request->album_id)->whereUserId($request->user_id)->first();
            if($photo) {
                $photoIdRemoved = $photo->id;
                $this->removeFromEverywhereExistingPhoto($photo);
            }

            BypassAlbumUpload::create([
                'album_id' => $request->album_id,
                'user_id' => $request->user_id
            ]);

            $album = Album::whereId($request->album_id)->first();
            $recipient = User::whereId($request->user_id)->first();
            Mail::to($recipient)->send(new CanUploadToAlbumAgain($album, $recipient));
        }

        $bypassUploadController2 = new BypassUploadController();
        $newList = $bypassUploadController2->index($request);

        return [
            'success' => true,
            'photo_id_removed' => $photoIdRemoved,
            'new_list' => $newList
        ];

    }

    public function destroy(Request $request)
    {
        $validation = Validator::make($request->all(), $this->getInsertRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = $request->user();

        $maxAdminPoints = User::getMaxAdminPoints($user->id);
        if ($maxAdminPoints < 1000000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        BypassAlbumUpload::whereAlbumId($request->album_id)->whereUserId($request->user_id)->delete();

        $bypassUploadController2 = new BypassUploadController();
        $newList = $bypassUploadController2->index($request);

        return [
            'success' => true,
            'new_list' => $newList
        ];
    }

}
