<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserRoleController extends Controller
{
    /**
     * UserRoleController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Adds a new Role to a User.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'role_id' => 'required|exists:roles,id'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $role = Role::whereId($request->role_id)->first();

        if (!$role->dynamic) {

            $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);

            if ($maxAdminPoints < 1000000) {
                return response()->json([
                    'not_enough_admin_points' => true,
                ], 403);
            }

            $nonDynamicRoles = Role::whereDynamic(false)->get()->toArray();
            $roleIds = array_column($nonDynamicRoles, 'id');
            UserRole::whereUserId($request->user_id)->whereIn('role_id', $roleIds)->delete();
        }

        $userRole = UserRole::whereUserId($request->user_id)->whereRoleId($request->role_id)->first();

        if (!$userRole) {
            UserRole::create([
                'user_id' => $request->user_id,
                'role_id' => $request->role_id
            ]);
        }

        return ['success' => true];

    }

    /**
     * Removes the Role from a User.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'dynamic' => 'required|boolean',
            'user_id' => 'required|exists:users,id',
            'role_id' => 'exists:roles,id',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $userRoles = UserRole::whereUserId($request->user_id);

        if (!$request->dynamic) {

            $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);

            if ($maxAdminPoints < 1000000) {
                return response()->json([
                    'not_enough_admin_points' => true
                ], 403);
            }

            $nonDynamicRoles = Role::whereDynamic(false)->get()->toArray();
            $roleIds = array_column($nonDynamicRoles, 'id');
            $userRoles->whereIn('role_id', $roleIds);

        } else if ($request->role_id) {

            $userRoles->whereRoleId($request->role_id);

        }

        if (!$userRoles->count()) {
            return ['no_such_role_before' => true];
        } else {
            $userRoles->delete();
            return ['success' => true];
        }

    }
}
