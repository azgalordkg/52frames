<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Traits\ManipulatesArray;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    use ManipulatesArray;

    /**
     * Gets the list of cities.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'query'      => 'string|nullable',
            'state_id'   => 'integer|exists:states,id',
            'country_id' => 'required|integer|exists:countries,id'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $cities = City::whereCountryId($request->country_id);

        if ($request->state_id) {
            $cities->whereStateId($request->state_id);
        }

        if ($request->query) {
            $cities->where('name', 'like', '%' . $request->get('query') . '%');
        }

        $citiesArray = $cities->orderBy('name')->get()->toArray();

        $country = Country::whereId($request->country_id)->first();
        if ($country->state_member_term) {
            $citiesWithoutStateId = $country->cities()->whereNull('state_id');

            if ($request->query) {
                $citiesWithoutStateId->where('name', 'like', '%' . $request->get('query') . '%');
            }

            $citiesWithoutState = $citiesWithoutStateId->orderBy('name')->get()->toArray();

            if (count($citiesWithoutState)) {
                $citiesArray = $this->deep_array_merge($citiesArray, $citiesWithoutState);
            }

        }
        return $citiesArray;
    }
}
