<?php

namespace App\Http\Controllers\Tools\MigrationTool;

use App\CsvUploadedFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HelpsCsvFileProcessing;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Tools\MigrationTool\Jobs\StartCsvValidationWorkers;

class ColumnSelectionController extends Controller
{
    use HelpsCsvFileProcessing;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('migration.roles: csv_uploader, csv_data_merger, csv_data_manager');
    }

    public function store(Request $request)
    {
        $inputs = $this->fixRequestInputs($request);
        $rules = $this->getUploadRules($request);
        $special = $this->specialValidation($request);
        $errors = $this->checkDuplicateColumnSelection($request);
        $validation = Validator::make($inputs, $rules);
        if ($validation->fails() || count($special) || count($errors)) {
            $errors = array_merge_recursive($special, $validation->errors()->toArray(), $errors);
            return response()->json([
                'validation_errors' => $this->removeDotFromValidationErrors($errors)
            ], 400);
        }

        $csvFile = CsvUploadedFile::whereId($request->csv_file_id)->first();

        if ($csvFile->processing) $this->cancelWorkers($csvFile);
        else $this->removePrevResults($csvFile);

        $csvFile->columns_selected = $inputs;
        $csvFile->validation_summary = $this->flushSummaryExceptMigrated($csvFile);
        $csvFile->save();

        StartCsvValidationWorkers::dispatch($csvFile);

        return ['success' => true];
    }

    protected function flushSummaryExceptMigrated(CsvUploadedFile $csvFile): ?array
    {
        $summary = $csvFile->validation_summary;
        if (!$summary) return null;

        $states = @$summary['states'];
        $migrated = @$states['migrated'];
        if (!@$migrated['count']) return null;

        $newStates = [];
        foreach ($states as $state => $value) {
            $newValue = ['count' => 0, 'rows' => []];
            if ($state == 'migrated') $newValue = $value;
            $newStates[$state] = $newValue;
        }

        $summary['states'] = $newStates;
        $summary['validated'] = $migrated['count'];

        return $summary;
    }

    protected function cancelWorkers(CsvUploadedFile $csvFile)
    {
        foreach ($csvFile->rows as $row) {
            if ($row->migrated) continue;
            $row->cancelled = true;
            $row->save();
        }
    }

    protected function removePrevResults(CsvUploadedFile $csvFile)
    {
        foreach ($csvFile->rows as $row) {
            if ($row->migrated) continue;
            $row->validation_report = null;
            $row->save();
        }
    }
}
