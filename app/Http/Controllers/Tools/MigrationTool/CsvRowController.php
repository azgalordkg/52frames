<?php

namespace App\Http\Controllers\Tools\MigrationTool;

use App\CsvUploadedRow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CsvRowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('migration.roles: csv_uploader, csv_data_merger, csv_data_manager');
    }

    protected function indexRules()
    {
        return [
            'file_id' => 'required|exists:csv_uploaded_files,id',
            'filter' => 'nullable|string|in:all,ok,warning,error,waiting,migrated'
        ];
    }

    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), $this->indexRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $rowsQuery = CsvUploadedRow::whereFileId($request->file_id)->withMigratedRows();

        switch ($request->filter) {
            case 'ok':
            case 'warning':
            case 'error':
                $rowsQuery->whereMigrated(false)->where('validation_report', 'like', '%"overall":"' . $request->filter . '"%');
                break;
            case 'migrated':
                $rowsQuery->whereMigrated(true);
                break;
            case 'waiting':
                $rowsQuery->whereValidationReport(null);
                break;
        }

        return $this->paginate($request, $rowsQuery, env('CSV_ROWS_PAGINATION_PER_PAGE'))->toArray();
    }
}
