<?php

namespace App\Http\Controllers\Tools\MigrationTool;

use App\CsvUploadedRow;
use App\CsvUploadedFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HelpsCsvFileProcessing;
use Illuminate\Support\Facades\Validator;

class CsvFileController extends Controller
{
    use HelpsCsvFileProcessing;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('migration.roles: csv_uploader, csv_data_merger, csv_data_manager');
    }

    /**
     * Rules when creating a new Album.
     * @return array
     */
    protected function indexRules()
    {
        return [
            'category' => 'required|in:' . implode(',', CsvUploadedFile::getCategories())
        ];
    }

    /**
     * Rules when creating a new Album.
     * @return array
     */
    protected function uploadRules()
    {
        return array_merge($this->indexRules(), [
            'file' => 'required',
            'extension' => 'required|in:csv'
        ]);
    }

    protected function uploadRulesMsgs()
    {
        $invalidFile = 'Please choose a valid CSV file.';
        return [
            'extension.required' => $invalidFile,
            'extension.in' => $invalidFile
        ];
    }

    protected function commonRelations(): array
    {
        return ['uploader'];
    }

    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), $this->indexRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $filesQuery = CsvUploadedFile::whereCategory($request->category)->with($this->commonRelations())->orderBy('created_at', 'desc');

        return $this->paginate($request, $filesQuery, env('CSV_IMPORT_PAGINATION_PER_PAGE'))->toArray();
    }

    public function show($id)
    {
        $validation = Validator::make(['id' => $id], ['id' => 'required|int|exists:csv_uploaded_files,id']);
        if ($validation->fails()) return response()->json([
            'validation_errors' => $validation->errors()->toArray()
        ], 400);
        return CsvUploadedFile::whereId($id)->with($this->commonRelations())->first();
    }

    public function store(Request $request)
    {
        ini_set('max_execution_time', 300); // 5minutes

        $inputs = $this->fixRequestInputs($request);
        $rules = $this->uploadRules();
        $csvFile = @$inputs['file'];
        if (!$csvFile) unset($rules['extension']);
        else $inputs['extension'] = $csvFile->getClientOriginalExtension();
        $validation = Validator::make($inputs, $rules, $this->uploadRulesMsgs());
        if ($validation->fails()) {
            $errors = $validation->errors()->toArray();
            if (@$errors['extension'] && !@$errors['file']) {
                $errors['file'] = $errors['extension'];
                unset($errors['extension']);
            }
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $csvData = $this->readCsvFile($csvFile);
        $headerColumns = $csvData['headers'];
        $csvRows = $csvData['rows'];

        if (!count($headerColumns) || (count($headerColumns) == 1 && !$headerColumns[0]) || !count($csvRows))
            return response()->json([
                'validation_errors' => [
                    'file' => ['Looks like the file is empty!']
                ]
            ], 400);

        $headersWithSamples = $this->addSamplesToHeaders($headerColumns, $csvRows);

        $csvUploadedFile = CsvUploadedFile::create([
            'user_id' => $request->user()->id,
            'category' => $request->category,
            'filename' => $csvFile->getClientOriginalName(),
            'header_columns_with_samples' => $headersWithSamples,
            'header_columns' => $headerColumns,
            'records_found' => count($csvRows)
        ]);

        foreach ($csvRows as $row) {
            CsvUploadedRow::create([
                'file_id' => $csvUploadedFile->id,
                'row_data' => $row
            ]);
        }

        return CsvUploadedFile::whereId($csvUploadedFile->id)->with($this->commonRelations())->first();
    }

    public function destroy($id, Request $request)
    {
        $this->minAdminPointsRequired(900100);

        $record = CsvUploadedFile::whereId($id)->first();
        if (!$record) $this->respond404();

        $record->delete();

        $this->respondSuccess();
    }
}
