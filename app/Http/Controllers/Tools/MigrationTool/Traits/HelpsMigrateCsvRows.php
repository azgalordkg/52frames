<?php

namespace App\Http\Controllers\Tools\MigrationTool\Traits;

use Illuminate\Http\Request;

trait HelpsMigrateCsvRows
{
    use HelpsMigrateUserCsvRow;
    use HelpsMigrateAlbumCsvRow;
    use HelpsMigratePhotoCsvRow;

    protected function checkIfShouldRemoveRowsArray(Request $request, array $validationSummary = null)
    {
        if ($request->no_rows && is_array($validationSummary)) {
            foreach (@$validationSummary['states'] as $state => $summary) {
                unset($validationSummary['states'][$state]['rows']);
            }
        }
        return $validationSummary;
    }
}
