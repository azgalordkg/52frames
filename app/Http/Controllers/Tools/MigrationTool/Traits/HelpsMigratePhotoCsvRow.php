<?php

namespace App\Http\Controllers\Tools\MigrationTool\Traits;

use App\User;
use App\Photo;
use App\CsvUploadedRow;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\PhotoController;

trait HelpsMigratePhotoCsvRow
{
    protected function migrateCsvPhotoRow(CsvUploadedRow $row): ?Photo
    {
        /*
        Array
        (
            [id] => 5336
            [file_id] => 3
            [migrated] =>
            [migrated_to_user_id] =>
            [album_id] =>
            [photo_id] =>
            [row_data] => Array
            (
                [0] => 555
                    [1] => Leslie
                    [2] => Clingan
                    [3] => mommyhon333@hotmail.com
                    [4] => Splash Pad
                    [5] => Sat straight up in bed one night early in the week and thought about using the splash pad at our outlet mall for this week's challenge.  Had hoped to capture some little feet dancing on the circular splash pads but it was too cool out.  Thinking now that maybe this simple picture was better anyway.  The water on this pad was struggling to gurgle forth which worked in my behalf, too!  I didn't get wet!!
                    [6] => splashpad, water, circles
                    [7] => Yes! Please consider my photo for a screencast critique!
                    [8] =>  Yup, I TOOK IT THIS WEEK and this photo is entirely my own creation.
                    [9] => 20180427_161702.jpg (https://52frames.wufoo.com/cabinet/bW13ajJnMWp0M3E3dg==/y209eic3vQk%3D/20180427_161702.jpg)
                    [10] => Yes! I'd like to tell you more!
                    [11] =>
                    [12] =>
                    [13] =>
                    [14] =>
                    [15] =>
                    [16] => Shot with a Phone
                    [17] =>
                    [18] =>
                    [19] =>
                    [20] =>
                    [21] =>
                    [22] =>
                    [23] =>
                    [24] =>
                    [25] =>
                    [26] =>
                    [27] =>
                    [28] =>
                    [29] =>
                    [30] =>
                    [31] =>
                    [32] =>
                    [33] =>
                    [34] =>
                    [35] =>
                    [36] =>
                    [37] =>
                    [38] => Outlet Shoppes of El Paso
                    [39] =>
                    [40] => 52F-CC Regular
                    [41] =>
                    [42] =>
                    [43] =>
                    [44] =>
                    [45] => first-submission-affirmative
                    [46] =>
                    [47] =>
                    [48] => 5
                    [49] =>
                    [50] => 2018-04-30
                    [51] => 2018-04-30 0:19:18
                    [52] => public
                    [53] =>
                    [54] =>
                    [55] => 24.92.124.178
                    [56] =>
                    [57] => 1
                    [58] =>
                    [59] =>
                    [60] => 6005
                )

            [validation_report] => Array
                (
                    [values] => Array
                        (
                            [album_id] => 17
                            [user_id] => 2097
                            [csv_imported_info] => Array
                                (
                                    [wufoo_entry_id] => 555
                                    [original_url_column_value] => 20180427_161702.jpg (https://52frames.wufoo.com/cabinet/bW13ajJnMWp0M3E3dg==/y209eic3vQk%3D/20180427_161702.jpg)
                                    [base_filename_from_orig_url] => 20180427_161702.jpg
                                    [s3_file_path] => 52f Wufoo Dump Master Archive-2018/17 - Circles/entry-555-20180427_161702.jpg
                                )

                            [title] => Splash Pad
                            [caption] => Sat straight up in bed one night early in the week and thought about using the splash pad at our outlet mall for this week's challenge.  Had hoped to capture some little feet dancing on the circular splash pads but it was too cool out.  Thinking now that maybe this simple picture was better anyway.  The water on this pad was struggling to gurgle forth which worked in my behalf, too!  I didn't get wet!!
                            [screencast_critique] => 1
                            [location] => Outlet Shoppes of El Paso
                            [tags] => Array
                                (
                                    [0] => splashpad
                                    [1] => water
                                    [2] => circles
                                )

                            [about_photo] => 1
                            [about_photo_answers] => Array
                                (
                                    [7] => 1
                                )

                            [camera_settings] =>
                            [camera_settings_answers] => Array
                                (
                                )

                            [critique_level] => 52f-cc_regular
                            [is_graphic] =>
                            [has_nudity] =>
                            [qualifies_extra_credit] =>
                            [ownership_confirmation] => 1
                        )

                    [metadata] => Array
                        (
                            [new_photo] => 1
                            [overall] => ok
                        )

                    [result] => Array
                        (
                        )

                )

            [created_at] => 2018-09-05T20:42:22+00:00
            [updated_at] => 2018-09-07T23:47:40+00:00

        )
        */

        $validationReport = $row->validation_report;
        $userInputs = @$validationReport['values'];
        $metadata = @$validationReport['metadata'];
        $keepInfo = @$userInputs['csv_imported_info'];

        if (!is_array($validationReport) || !is_array($userInputs) || !is_array($metadata) || !@$metadata['new_photo'])
            return null;

        $uploader = User::whereId($userInputs['user_id'])->first();
        if (!$uploader) return null;

        $existingPhoto = Photo::whereAlbumId($userInputs['album_id'])->whereUserId($userInputs['user_id'])->first();
        if ($existingPhoto) return $existingPhoto;

        $s3FilePath = @$keepInfo['s3_file_path'];
        $targetFilename = @$keepInfo['base_filename_from_orig_url'];
        $photoLocalFilePath = Photo::generateS3DownloadedPhotoLocalPath($s3FilePath);
        $photoLocalExists = Photo::checkIfS3PhotoDownloadedToLocal($s3FilePath);
        if (!$s3FilePath || !$photoLocalExists) return null;

        $photo = new Photo();

        $request = new Request($userInputs);

        $photoLocalCopyFromS3 = new UploadedFile(
            $photoLocalFilePath,
            $targetFilename,
            null,
            filesize($photoLocalFilePath)
        );

        $controller = new PhotoController();
        $controller->setUploader($uploader);
        $controller->setCsvPhotoLocalFile($photoLocalCopyFromS3);
        $result = $controller->store($request, $photo);

        if (!is_array($result) || !@$result['success']) return null;

        $photo->imported_from_row_id = $row->id;
        $photo->imported_info = $keepInfo;
        $photo->save();

        unlink($photoLocalFilePath);

        return $photo;
    }
}