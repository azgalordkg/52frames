<?php

namespace App\Http\Controllers\Tools\MigrationTool\Traits;

use App\Album;
use App\CsvUploadedRow;

trait HelpsMigrateAlbumCsvRow
{
    protected function getAlbumIfExistsUsingCsvInputs(array $inputs): ?Album
    {
        $startDate = @$inputs['start_date'] ?: 0;
        $endDate = @$inputs['end_date'] ?: 0;

        return Album::where('week_number', $inputs['week_number'])
            ->where('shorturl', $inputs['shorturl'])
            ->where('year', $inputs['year'])
            ->where('start_date', '>=', $startDate)
            ->where('start_date', '<=', $endDate)
            ->where('end_date', '>=', $startDate)
            ->where('end_date', '<=', $endDate)
            ->first();
    }

    protected function migrateCsvAlbumRow(CsvUploadedRow $row): ?Album
    {
        /*
        Array
        (
            [id] => 143
            [file_id] => 5
            [album_id] =>
            [photo_id] =>
            [migrated] =>
            [migrated_to_user_id] =>
            [row_data] => Array
                (
                    [0] => 1
                    [1] => 2018
                    [2] => Jan 01, 2018
                    [3] => Jan 07, 2018
                    [4] => Self Portrait
                    [5] => Imperfection
                    [6] => https://www.facebook.com/52frames/photos/?tab=album&album_id=1985796868112090
                    [7] => 1985796868112090
                    [8] => https://www.google.com/url?q=https://www.facebook.com/52frames/photos/?tab%3Dalbum%26album_id%3D1985892961435814&sa=D&ust=1529954864612000&usg=AFQjCNHj8KHngxUp2mo0fOegC17I3ZYoGg
                )

            [validation_report] => Array
                (
                    [values] => Array
                        (
                            [year] => 2018
                            [week_number] => 1
                            [start_date] => 2018-01-01
                            [end_date] => 2018-01-07
                            [theme_title] => Self Portrait
                            [shorturl] => self-portrait
                            [extra_credit_title] => Imperfection
                        )

                    [metadata] => Array
                        (
                            [new_album] => 1
                            [overall] => ok
                        )

                    [result] => Array
                        (
                        )

                )

            [created_at] => 2018-07-17T20:07:23+00:00
            [updated_at] => 2018-07-23T17:38:56+00:00

        )
         */

        $validationReport = $row->validation_report;
        $userInputs = @$validationReport['values'];
        $metadata = @$validationReport['metadata'];

        if (!is_array($validationReport) || !is_array($userInputs) || !is_array($metadata) || !@$metadata['new_album'])
            return null;

        $album = $this->getAlbumIfExistsUsingCsvInputs($userInputs);
        if ($album) return $album;

        $album = new Album($userInputs);
        $album->publicly_visible = 1;
        $album->imported_from_row_id = $row->id;
        $album->imported_info = [];
        $album->save();

        return $album;
    }
}