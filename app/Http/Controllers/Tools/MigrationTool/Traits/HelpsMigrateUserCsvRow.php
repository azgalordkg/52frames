<?php

namespace App\Http\Controllers\Tools\MigrationTool\Traits;

use App\User;
use App\CsvUploadedRow;
use App\Traits\HelpsLogin;
use Illuminate\Http\Request;
use App\Services\MailchimpService;
use Spatie\Newsletter\NewsletterFacade;
use App\Http\Controllers\ManifestoController;

trait HelpsMigrateUserCsvRow
{
    use HelpsLogin;

    protected $importingCsvUser = null;

    public function setImportingCsvUser(User $user)
    {
        $this->importingCsvUser = $user;
    }

    protected function getCsvUserIfExists(array $inputs): ?User
    {
        return User::whereEmail($inputs['email'])
            ->orWhere('imported_info', 'like', '%"' . $inputs['52f_id'] . '""%')
            ->first();
    }

    protected function migrateCsvUserAccount(CsvUploadedRow $row): ?User
    {
        /*
        Array
        (
            [id] => 5
            [file_id] => 3
            [album_id] =>
            [photo_id] =>
            [migrated] =>
            [migrated_to_user_id] =>
            [row_data] => Array
                (
                    [0] => Dena
                    [1] => Bailey
                    [2] => denabailey@gmail.com
                    [3] => I'm onboarding just to onboard. Can't commit to anything more than that... But I'm still happy to advise, etc.
                    [4] => 0108
                    [5] => Intermediate - I shoot with a DSLR and understand most of my settings, but still learning more.
                    [6] => DSLR - Entry Level
                    [7] => Israel
                    [8] =>
                    [9] => Efrat
                    [10] =>
                    [11] =>
                )

            [validation_report] => Array
                (
                    [values] => Array
                        (
                            [52f_id] => 0108
                            [firstname] => Dena
                            [lastname] => Bailey
                            [handle] =>
                            [email] => denabailey@gmail.com
                            [display_as_handle] =>
                            [how_did_hear] =>
                            [twitter_handle] =>
                            [instagram_handle] =>
                            [why] => I'm onboarding just to onboard. Can't commit to anything more than that... But I'm still happy to advise, etc.
                            [photography_level] => intermediate
                            [camera_type] => dslr_entry
                            [country] => 81
                            [country_name] => Israel
                            [state] =>
                            [state_name] =>
                            [state_member_term] => District
                            [city] => Efrat
                            [city_id] =>
                            [dont_share_location] =>
                            [photowalks] =>
                        )

                    [metadata] => Array
                        (
                            [merge_to_user_id] =>
                            [multiple_accounts_found] =>
                            [new_account] => 1
                            [overall] => warning
                        )

                    [result] => Array
                        (
                            [city] => Array
                                (
                                    [0] => Array
                                        (
                                            [warning] => 1
                                            [message] => This City is not found in the database. Please make sure the City name in this row is not another term (check spelling, etc) for an already existing one. If you are sure that this is a new City, you can decide to Migrate this row now. The City name here will then be added into the database as a new record. You may also choose to re-validate all these rows afterwards, to see the updated Error Messages, and perhaps see that new City name being reused by the other rows, by then.
                                        )

                                )

                        )

                )

            [created_at] => 2018-06-25T22:44:23+00:00
            [updated_at] => 2018-07-10T02:49:11+00:00
        )
        */

        $validationReport = $row->validation_report;
        $userInputs = @$validationReport['values'];
        $metadata = @$validationReport['metadata'];

        if (!is_array($validationReport) || !is_array($userInputs) || !is_array($metadata) || !@$metadata['new_account'])
            return null;

        $user = $this->getCsvUserIfExists($userInputs);
        if ($user) return $user;

        $user = new User($userInputs);
        $user->imported_from_row_id = $row->id;
        $user->imported_info = ['52f_id' => $userInputs['52f_id']];
        $user->save();

        $request = new Request([
            'country' => $userInputs['country'],
            'state' => $userInputs['state'],
            'city' => $userInputs['city'],
            'dont_share_location' => false,
            'photography_level' => $userInputs['photography_level'],
            'camera_type' => $userInputs['camera_type'],
            'photowalks' => null,
            'twitter_handle' => $userInputs['twitter_handle'],
            'instagram_handle' => $userInputs['instagram_handle'],

            'commitment_understanding' => true,
            'being_constant_understanding' => true,
            'creative_process_understanding' => true,
            'vulnerability_understanding' => true,
            'non_perfection_understanding' => true,
            'understanding_to_release_the_notion_of_perfection' => true,
            'understanding_for_the_sake_of_consistency' => true,

            'why' => $userInputs['why'],

            'skipMailchimp' => true
        ]);

        $manifestoController = new ManifestoController();
        $manifestoController->setImportingCsvUser($user);
        $manifestoController->store($request, new MailchimpService(new NewsletterFacade()));

//        $this->subscribeUserToMailchimpMergeOurUserId($user, $request, ) // not being processed

        return $user;
    }
}