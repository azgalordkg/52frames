<?php

namespace App\Http\Controllers\Tools\MigrationTool;

use App\User;
use App\Album;
use App\Photo;
use App\CsvUploadedRow;
use App\CsvUploadedFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Tools\MigrationTool\Traits\HelpsMigrateCsvRows;

class CsvMigrateController extends Controller
{
    use HelpsMigrateCsvRows;

    public function __construct()
    {
        set_time_limit(0);

        $this->middleware('auth:api');
        $this->middleware('migration.roles: csv_uploader, csv_data_merger, csv_data_manager');
    }

    protected function updateValidationSummary(CsvUploadedRow $row): ?array
    {
        $file = CsvUploadedFile::whereId($row->file_id)->first();

        $currentSummary = $file->validation_summary;

        $states = @$currentSummary['states'];
        if (!$states || !isset($states['migrated'])) return null;
        $migrated =  &$states['migrated'];

        if (!in_array($row->id, $migrated['rows'])) {
            $migrated['rows'][] = $row->id;
            $migrated['count']++;
        }

        $newStates = [];
        foreach ($states as $state => $stateData) {
            if ($state != 'migrated') {
                foreach ($stateData['rows'] as $index => $rowId) {
                    if ($rowId == $row->id) {
                        unset($stateData['rows'][$index]);
                        $stateData['count']--;
                    }
                }
                $stateData['rows'] = array_values($stateData['rows']);
            }
            $newStates[$state] = $stateData;
        }
        $currentSummary['states'] = $newStates;

        $file->validation_summary = $currentSummary;
        $file->save();

        return $currentSummary;
    }

    protected function hasMigrationErrors(CsvUploadedRow $row): ?array
    {
        $row = CsvUploadedRow::whereId($row->id)->first();

        if ($row->migrated) {
            return [
                'already_migrated' => true,
                'message' => 'Row already migrated, previously.'
            ];
        }

        $validationReport = @$row->validation_report;
        $metadata = @$validationReport['metadata'];
        $overall = @$metadata['overall'];
        if ($overall != 'ok' && $overall != 'warning') {
            return [
                'metadata' => $metadata,
                'message' => 'Row cannot be migrated. Please check validation report.'
            ];
        }

        return null;
    }

    protected function createUserProfile(CsvUploadedRow $row): ?User
    {
        $user = $this->migrateCsvUserAccount($row);
        if (!$user) return null;

        $row->migrated_to_user_id = $user->id;
        $row->migrated = true;
        $row->save();

        return $user;
    }

    protected function createAlbumRecord(CsvUploadedRow $row): ?Album
    {
        $album = $this->migrateCsvAlbumRow($row);
        if (!$album) return null;

        $row->album_id = $album->id;
        $row->migrated = true;
        $row->save();

        return $album;
    }

    protected function createPhotoRecord(CsvUploadedRow $row): ?Photo
    {
        $photo = $this->migrateCsvPhotoRow($row);
        if (!$photo) return null;

        $row->photo_id = $photo->id;
        $row->migrated = true;
        $row->save();

        return $photo;
    }

    protected function createLiveDbRecord(CsvUploadedRow $row)
    {
        switch ($row->file->category) {
            case 'users':
                return $this->createUserProfile($row);
            case 'albums':
                return $this->createAlbumRecord($row);
            case 'photos':
                return $this->createPhotoRecord($row);
        }
    }

    public function migrateRow(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'no_rows' => 'nullable|boolean',
            'row_id' => 'required|exists:csv_uploaded_rows,id'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $row = CsvUploadedRow::whereId($request->row_id)->first();

        if ($errors = $this->hasMigrationErrors($row)) {
            return response()->json(array_merge($errors, [
                'cant_migrate' => true
            ]), 403);
        }

        $record = $this->createLiveDbRecord($row);

        if ($record) $summary = $this->updateValidationSummary($row);
        else $summary = $row->file->validation_summary;

        return [
            'row' => CsvUploadedRow::whereId($row->id)->withMigratedRows()->first(),
            'validation_summary' => $this->checkIfShouldRemoveRowsArray($request, $summary)
        ];
    }

    public function migrateAllValid(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'no_rows' => 'nullable|boolean',
            'file_id' => 'required|exists:csv_uploaded_files,id'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $rows = CsvUploadedRow::whereFileId($request->file_id)
            ->where('validation_report', 'like', '%"overall":"ok"%')
            ->whereMigrated(false)
            ->get();

        $migrationIds = [];
        $latestSummary = null;
        foreach ($rows as $row) {
            if ($this->hasMigrationErrors($row)) continue;
            $migratedRow = $this->createLiveDbRecord($row);
            if (!$migratedRow) continue;
            $migrationIds[] = ['row_id' => $row->id, 'migrated_to_row_id' => $migratedRow->id];
            $latestSummary = $this->updateValidationSummary($row);
        }

        if (!$latestSummary) {
            $file = CsvUploadedFile::whereId($row->file_id)->first();
            $latestSummary = $file ? $file->validation_summary : null;
        }

        return [
//            'migration_ids' => $migrationIds, // turned-off so the browser does not freeze (not used anyway)
            'validation_summary' => $this->checkIfShouldRemoveRowsArray($request, $latestSummary)
        ];

    }
}
