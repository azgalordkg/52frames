<?php

namespace App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules;

use App\Album;
use Illuminate\Http\Request;

class PhotosCategory extends Common
{
    public static function getUploadRules(): array
    {
        return array_merge(parent::getUploadRules(), [

            'album' => 'required|array',
            'album.year' => 'required|integer|min:2010',
            'album.week_number' => 'required|integer|min:1|max:53',
            'album.same_for_all_rows' => 'required|accepted',

            'photographer' => 'required|array',
            'photographer.52f_id' => 'required|integer',
            'photographer.email' => 'required|integer',

            'photo' => 'required|array',
            'photo.entry_id' => 'required|integer',
            'photo.title' => 'required|integer',
            'photo.caption' => 'required|integer',
            'photo.url' => 'required|integer',
            'photo.screencast_critique' => 'required|integer',
            'photo.location' => 'required|integer',
            'photo.tags' => 'required|integer',

            'photo.shooting_and_processing' => 'required|array',
            'photo.shooting_and_processing.external_flash' => 'required|integer',
            'photo.shooting_and_processing.shutter' => 'required|integer',
            'photo.shooting_and_processing.aperture' => 'required|integer',
            'photo.shooting_and_processing.composite_edit' => 'required|integer',
            'photo.shooting_and_processing.hdr' => 'required|integer',
            'photo.shooting_and_processing.shot_with_a_phone' => 'required|integer',
            'photo.shooting_and_processing.black_and_white' => 'required|integer',

            'photo.subject_matter' => 'required|array',
            'photo.subject_matter.architecture' => 'required|integer',
            'photo.subject_matter.fine_art' => 'required|integer',
            'photo.subject_matter.street_photography' => 'required|integer',
            'photo.subject_matter.travel' => 'required|integer',
            'photo.subject_matter.action' => 'required|integer',
            'photo.subject_matter.humor' => 'required|integer',
            'photo.subject_matter.nature' => 'required|integer',
            'photo.subject_matter.macro' => 'required|integer',
            'photo.subject_matter.abstract' => 'required|integer',
            'photo.subject_matter.portrait' => 'required|integer',
            'photo.subject_matter.landscape' => 'required|integer',
            'photo.subject_matter.still_life' => 'required|integer',

            'photo.camera_settings' => 'required|array',
            'photo.camera_settings.shutter_speed' => 'required|integer',
            'photo.camera_settings.aperture' => 'required|integer',
            'photo.camera_settings.iso' => 'required|integer',
            'photo.camera_settings.focal_length' => 'required|integer',
            'photo.camera_settings.camera_manufacturer' => 'required|integer',
            'photo.camera_settings.camera_model' => 'required|integer',
            'photo.camera_settings.lens' => 'required|integer',
            'photo.camera_settings.flash' => 'required|integer',

            'photo.critique_level' => 'required|array',
            'photo.critique_level.use_same_column' => 'required|boolean',
            'photo.critique_level.column' => 'nullable|required_if:photo.critique_level.use_same_column,1|integer',
            'photo.critique_level.52f-cc_regular' => 'required|string',
            'photo.critique_level.extra_sensitive' => 'required|string',
            'photo.critique_level.no_critique' => 'required|string',
            'photo.critique_level.shred_away' => 'required|string',

            'photo.contains_graphic_content' => 'required|array',
            'photo.contains_graphic_content.has_nudity' => 'required|integer',
            'photo.contains_graphic_content.model_consent' => 'required|integer',

            'photo.others' => 'required|array',
            'photo.others.qualifies_extra_credit' => 'required|integer',
            'photo.others.photo_ownership_confirmation' => 'required|integer'
        ]);
    }

    public static function specialValidation(Request $request): array
    {
        $year = @$request->album['year'];
        $weekNumber = @$request->album['week_number'];

        $albumFound = false;
        if ($year && $weekNumber) {
            $query = Album::where('year', $year)->where('week_number', $weekNumber);
            if ($query->count()) $albumFound = true;
        }
        if (!$albumFound) {
            $error = ['No such Live Album exists.'];
            return [
                'album.year' => $error,
                'album.week_number' => $error
            ];
        }

        return [];
    }

    public static function checkDuplicateColumnSelection(Request $request): array
    {
        $rules = self::getUploadRules();

        unset($rules['csv_file_id']);
        unset($rules['album']);
        unset($rules['album.year']);
        unset($rules['album.week_number']);
        unset($rules['album.same_for_all_rows']);
        unset($rules['photographer']);
        unset($rules['photo']);
        unset($rules['photo.shooting_and_processing']);
        unset($rules['photo.subject_matter']);
        unset($rules['photo.camera_settings']);
        unset($rules['photo.critique_level']);
        unset($rules['photo.critique_level.use_same_column']);
        unset($rules['photo.contains_graphic_content']);
        unset($rules['photo.others']);

        if ($request->input('photo.critique_level.use_same_column')) {
            unset($rules['photo.critique_level.52f-cc_regular']);
            unset($rules['photo.critique_level.extra_sensitive']);
            unset($rules['photo.critique_level.no_critique']);
            unset($rules['photo.critique_level.shred_away']);
        } else {
            unset($rules['photo.critique_level.column']);
        }

        $errors = [];
        $chosen = [];
        foreach ($rules as $ruleKey => $rule) {
            $columnId = $request->input($ruleKey);
            if ($columnId == null) continue;
            try {
                $columnId = $columnId * 1;
            } catch (\Exception $exception) {
                continue;
            }
            if (!in_array($columnId, array_keys($chosen))) {
                $chosen[$columnId] = $ruleKey;
                continue;
            }
            $errors[$ruleKey] = ['Option already chosen for: ' . $chosen[$columnId]];
        }

        return $errors;
    }

    /*
    public static function subGroupMapping(): array
    {
        return [
            'shooting_and_processing' => [
                'external_flash' => 'about_photo_answers[2]',
                'shutter' => 'about_photo_answers[3]',
                'aperture' => 'about_photo_answers[4]',
                'composite_edit' => 'about_photo_answers[5]',
                'hdr' => 'about_photo_answers[6]',
                'show_with_a_phone' => 'about_photo_answers[7]',
                'black_and_white' => 'about_photo_answers[8]'
            ],
            'subject_matter' => [
                'architecture' => 'about_photo_answers[10]',
                'fine_art' => 'about_photo_answers[11]',
                'street_photography' => 'about_photo_answers[12]',
                'travel' => 'about_photo_answers[13]',
                'action' => 'about_photo_answers[14]',
                'humor' => 'about_photo_answers[15]',
                'nature' => 'about_photo_answers[16]',
                'macro' => 'about_photo_answers[17]',
                'abstract' => 'about_photo_answers[18]',
                'portrait' => 'about_photo_answers[19]',
                'landscape' => 'about_photo_answers[20]',
                'still_life' => 'about_photo_answers[21]'
            ],
            'camera_settings' => [
                'shutter_speed' => 'camera_settings_answers[22]',
                'aperture' => 'camera_settings_answers[34]',
                'iso' => 'camera_settings_answers[45]',
                'focal_length' => 'camera_settings_answers[52]',
                'camera_manufacturer' => 'camera_settings_answers[53]',
                'camera_model' => 'camera_settings_answers[54]',
                'lens' => 'camera_settings_answers[55]',
                'flash' => 'camera_settings_answers[298]'
            ]
        ];
    }
    */
}