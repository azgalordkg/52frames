<?php

namespace App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules;

use Illuminate\Http\Request;

class AlbumsCategory extends Common
{
    public static function getUploadRules(): array
    {
        return array_merge(parent::getUploadRules(), [
            'album' => 'required|array',
            'album.year' => 'required|integer',
            'album.week_number' => 'required|integer',
            'album.start_date' => 'required|integer',
            'album.end_date' => 'required|integer',
            'album.theme_title' => 'required|integer',
            'album.extra_credit_title' => 'required|integer'
        ]);
    }

    public static function checkDuplicateColumnSelection(Request $request): array
    {
        $rules = self::getUploadRules();

        unset($rules['csv_file_id']);
        unset($rules['album']);

        $errors = [];
        $chosen = [];
        foreach ($rules as $ruleKey => $rule) {
            $columnId = $request->input($ruleKey);
            if ($columnId == null) continue;
            try {
                $columnId = $columnId * 1;
            } catch (\Exception $exception) {
                continue;
            }
            if (!in_array($columnId, array_keys($chosen))) {
                $chosen[$columnId] = $ruleKey;
                continue;
            }
            $errors[$ruleKey] = ['Option already chosen for: ' . $chosen[$columnId]];
        }

        return $errors;
    }
}