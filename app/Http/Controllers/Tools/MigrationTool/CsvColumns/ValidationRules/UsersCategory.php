<?php

namespace App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules;

use Illuminate\Http\Request;

class UsersCategory extends Common
{
    public static function getUploadRules(): array
    {
        return array_merge(parent::getUploadRules(), [

            'user' => 'required|array',
            'user.52f_id' => 'required|integer',
            'user.firstname' => 'required|integer',
            'user.lastname' => 'required|integer',
            'user.handle' => 'nullable|integer',
            'user.email' => 'required|integer',
            'user.instagram' => 'required|integer',
            'user.twitter' => 'required|integer',
            'user.why' => 'required|integer',
            'user.country' => 'required|integer',
            'user.state' => 'required|integer',
            'user.city' => 'required|integer',

            'user.photography_level' => 'required|array',
            'user.photography_level.use_same_column' => 'required|boolean',
            'user.photography_level.column' => 'nullable|required_if:user.photography_level.use_same_column,1|integer',
            'user.photography_level.beginner' => 'required|string',
            'user.photography_level.beginner_dslr' => 'required|string',
            'user.photography_level.intermediate' => 'required|string',
            'user.photography_level.advanced' => 'required|string',
            'user.photography_level.advanced_pro' => 'required|string',

            'user.camera_type' => 'required|array',
            'user.camera_type.use_same_column' => 'required|boolean',
            'user.camera_type.column' => 'nullable|required_if:user.camera_type.use_same_column,1|integer',
            'user.camera_type.dslr_entry' => 'required|string',
            'user.camera_type.dslr_pro' => 'required|string',
            'user.camera_type.mirrorless' => 'required|string',
            'user.camera_type.point_and_shoot' => 'required|string',
            'user.camera_type.phone' => 'required|string',
            'user.camera_type.other' => 'required|string'
        ]);
    }

    public static function checkDuplicateColumnSelection(Request $request): array
    {
        $rules = self::getUploadRules();

        unset($rules['csv_file_id']);
        unset($rules['user']);
        unset($rules['user.photography_level']);
        unset($rules['user.photography_level.use_same_column']);
        unset($rules['user.camera_type']);
        unset($rules['user.camera_type.use_same_column']);

        if ($request->input('user.photography_level.use_same_column')) {
            unset($rules['user.photography_level.beginner']);
            unset($rules['user.photography_level.beginner_dslr']);
            unset($rules['user.photography_level.intermediate']);
            unset($rules['user.photography_level.advanced']);
            unset($rules['user.photography_level.advanced_pro']);
        } else {
            unset($rules['user.photography_level.column']);
        }

        if ($request->input('user.camera_type.use_same_column')) {
            unset($rules['user.camera_type.dslr_entry']);
            unset($rules['user.camera_type.dslr_pro']);
            unset($rules['user.camera_type.mirrorless']);
            unset($rules['user.camera_type.point_and_shoot']);
            unset($rules['user.camera_type.phone']);
            unset($rules['user.camera_type.other']);
        } else {
            unset($rules['user.camera_type.column']);
        }

        $errors = [];
        $chosen = [];
        foreach ($rules as $ruleKey => $rule) {
            $columnId = $request->input($ruleKey);
            if ($columnId == null) continue;
            try {
                $columnId = $columnId * 1;
            } catch (\Exception $exception) {
                continue;
            }
            if (!in_array($columnId, array_keys($chosen))) {
                $chosen[$columnId] = $ruleKey;
                continue;
            }
            $errors[$ruleKey] = ['Option already chosen for: ' . $chosen[$columnId]];
        }

        return $errors;
    }
}