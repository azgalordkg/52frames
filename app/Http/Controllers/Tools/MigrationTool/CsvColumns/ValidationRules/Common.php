<?php

namespace App\Http\Controllers\Tools\MigrationTool\CsvColumns\ValidationRules;

use Illuminate\Http\Request;

abstract class Common
{

    public static function getUploadRules()
    {
        return [
            'csv_file_id' => 'required|exists:csv_uploaded_files,id',
        ];
    }

    public static function checkDuplicateColumnSelection(Request $request): array
    {
        return [];
    }
    /*
    public static function subGroupMapping(): array
    {
        return [];
    }
    */
}