<?php

namespace App\Http\Controllers\Tools\MigrationTool\Jobs;

use App\Jobs\JobQueue;
use App\CsvUploadedFile;

class StartCsvValidationWorkers extends JobQueue
{
    protected $csvFile;

    /**
     * StartCsvValidationWorkers constructor.
     * @param CsvUploadedFile $csvFile
     */
    public function __construct(CsvUploadedFile $csvFile)
    {
        $this->csvFile = $csvFile;
    }

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        if (!$this->csvFile) return;

        if ($this->csvFile->processing) {
            sleep(1);
            StartCsvValidationWorkers::dispatch($this->csvFile);
            return;
        }

        $category = $this->csvFile->category;

        $this->csvFile->processing = true;
        $this->csvFile->save();

        foreach ($this->csvFile->rows as $row) {
            if ($row->migrated) continue;
            switch ($category) {
                case 'users':
                    UsersCsvFileValidationWorker::dispatch($row);
                    break;
                case 'albums':
                    AlbumsCsvFileValidationWorker::dispatch($row);
                    break;
                case 'photos':
                    PhotosCsvFileValidationWorker::dispatch($row);
                    break;
            }
        }
    }
}