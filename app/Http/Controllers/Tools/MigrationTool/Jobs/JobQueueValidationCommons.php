<?php

namespace App\Http\Controllers\Tools\MigrationTool\Jobs;

use Error;
use Closure;
use App\User;
use App\Jobs\JobQueue;
use App\CsvUploadedRow;

abstract class JobQueueValidationCommons extends JobQueue
{
    protected $csvRow;
    protected $metadata = [];
    protected $selectedColumns;
    protected $inputValues = [];

    /**
     * UsersCsvFileValidationWorker constructor.
     * @param CsvUploadedRow $csvRow
     */
    public function __construct(CsvUploadedRow $csvRow)
    {
        if (!$this->category) throw new Error('please define $category');

        $this->csvRow = $csvRow;
        $this->selectedColumns = $csvRow->file->columns_selected;
    }

    protected function saveToDatabase(array $validationResult)
    {
        if ($this->alreadyMigrated()) return;

        $overAllText = $this->determineOverallResult($validationResult);
        $this->metadata['overall'] = $overAllText;

        $requestingCancel = $this->requestingCancel();
        if ($requestingCancel) {
            $validationResult = [];
            $this->csvRow->cancelled = false;
            $this->metadata = ['cancelled' => true];
        }

        $this->csvRow->validation_report = [
            'values' => $this->inputValues,
            'metadata' => $this->metadata,
            'result' => $validationResult
        ];

        if (!$requestingCancel) $this->updateValidationSummary($overAllText);

        $this->csvRow->save();

        $this->checkIfCompletedAll();
    }

    protected function updateValidationSummary(string $overAllText)
    {
        $fileUpdated = $this->csvRow->file;
        $currentSummary = $fileUpdated->validation_summary;

        if (!$currentSummary) $currentSummary = [
            'total' => $fileUpdated->rows()->count(),
            'validated' => 0,
            'states' => [
                'ok' => ['count' => 0, 'rows' => []],
                'error' => ['count' => 0, 'rows' => []],
                'warning' => ['count' => 0, 'rows' => []],
                'migrated' => ['count' => 0, 'rows' => []]
            ]
        ];

        $validated = 0;
        foreach (array_keys($currentSummary['states']) as $state) {
            foreach ($currentSummary['states'][$state]['rows'] as $index => $rowId) {
                if ($rowId == $this->csvRow->id) {
                    unset($currentSummary['states'][$state]['rows'][$index]);
                    $currentSummary['states'][$state]['rows'] = array_values($currentSummary['states'][$state]['rows']);
                    break;
                }
            }
            if ($overAllText == $state) $currentSummary['states'][$state]['rows'][] = $this->csvRow->id;
            $validated += $currentSummary['states'][$state]['count'] = count($currentSummary['states'][$state]['rows']);
        }

        $currentSummary['validated'] = $validated;

        $fileUpdated->validation_summary = $currentSummary;
        $fileUpdated->save();
    }

    protected function determineOverallResult(array $validationResult)
    {
        $hasError = false;
        $hasWarning = false;
        foreach ($validationResult as $key => $value) {
            foreach ($value as $validationResult) {
                if (@$validationResult['error']) $hasError = true;
                if (@$validationResult['warning']) $hasWarning = true;
            }
        }
        if ($hasError) return 'error';
        if ($hasWarning) return 'warning';
        return 'ok';
    }

    protected function checkIfCompletedAll()
    {
        $csvFile = $this->csvRow->file;

        $notCompleted = CsvUploadedRow::whereFileId($csvFile->id)->whereValidationReport(null)->get();
        if (count($notCompleted)) return;

        $csvFile->processing = false;
        $csvFile->save();
    }

    protected function requestingCancel()
    {
        return CsvUploadedRow::whereId($this->csvRow->id)->first()->cancelled;
    }

    protected function alreadyMigrated()
    {
        return CsvUploadedRow::whereId($this->csvRow->id)->first()->migrated;
    }

    protected function validate(string $columnName, Closure $queryAddOn = null, Closure $resultHandler)
    {
        $error = [];
        if ($this->requestingCancel()) return null;
        $columnValue = $this->getColumnSelectedValue($columnName);
        if (!$columnValue) return null;
        $query = User::where('imported_info', 'like', '%"' . $columnValue . '"%');
        if ($queryAddOn) $query = $queryAddOn($query, $columnName, $columnValue);
        $result = $resultHandler($query->first(), $error);
        if ($result) $error[] = $result;
        if (!count($error)) return null;
        return $error;
    }

    protected function getColumnValue(string $columnName, string $category = null, string $subCategory = null)
    {
        $category = $category ?: $this->category;
        $output = @$this->selectedColumns[$category];
        if ($output && $subCategory) $output = @$output[$subCategory];
        return @$output[$columnName] != null ? $output[$columnName] : null;
    }

    protected function getSubGroupValue(string $columnName)
    {
        $optionValues = $this->getColumnValue($columnName);
        $optionValues = is_array($optionValues) ? $optionValues : [];

        if (!isset($optionValues['use_same_column'])) throw new Error("$columnName array should include the key name: 'use_same_column' with value of 1 (or: true)");
        if (!isset($optionValues['column'])) throw new Error("$columnName array should include the key name: 'column' with integer value of the chosen column");

        $chosenColumnNumber = $optionValues['column'];
        unset($optionValues['use_same_column']);
        unset($optionValues['column']);

        $rowData = $this->csvRow->row_data;
        $chosenValue = @$rowData[$chosenColumnNumber];
        if (!$chosenValue) return null;

        foreach ($optionValues as $optionsKey => $optionValue) {
            if ($optionValue == $chosenValue) return $optionsKey;
        }

        return null;
    }

    protected function getColumnSelectedValue(string $columnName, bool $useNull = false, string $category = null, string $subCategory = null)
    {
        $ifBlank = $useNull ? null : '';
        $rowData = $this->csvRow->row_data;
        $columnValue = $this->getColumnValue($columnName, $category, $subCategory);
        return @$rowData[$columnValue] ?: $ifBlank;
    }
}