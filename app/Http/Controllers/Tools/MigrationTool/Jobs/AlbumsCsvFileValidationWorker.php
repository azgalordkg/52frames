<?php

namespace App\Http\Controllers\Tools\MigrationTool\Jobs;

use DateTime;
use App\Album;
use Illuminate\Support\Facades\Validator;

class AlbumsCsvFileValidationWorker extends JobQueueValidationCommons
{
    protected $category = 'album';

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->alreadyMigrated() || $this->requestingCancel()) {
            $this->saveToDatabase([]);
            return;
        }

        $this->inputValues = [
            'year' => $this->getColumnSelectedValue('year', true),
            'week_number' => $this->getColumnSelectedValue('week_number', true),
            'start_date' => $this->reformatDate('start_date'),
            'end_date' => $this->reformatDate('end_date'),
            'theme_title' => $this->getColumnSelectedValue('theme_title', true),
            'shorturl' => $this->createShortUrl(),
            'extra_credit_title' => $this->getColumnSelectedValue('extra_credit_title', true)
        ];

        $normalValidationsResult = $this->execNormalValidation();
        if(!count($normalValidationsResult)) $this->metadata['new_album'] = true;

        $this->saveToDatabase($normalValidationsResult);
    }

    protected function reformatDate(string $columnName)
    {
        $dateInput = $this->getColumnSelectedValue($columnName, true);
        if (!$dateInput) return null;

        $date = DateTime::createFromFormat('M d, Y', $dateInput);

        return $date->format('Y-m-d');
    }

    protected function createShortUrl()
    {
        $themeTitle = $this->getColumnSelectedValue('theme_title', true);
        if (!$themeTitle) return null;

        $shortUrl = strtolower($themeTitle);
        $shortUrl = preg_replace('/^[^a-z0-9]/', '', $shortUrl);
        $shortUrl = preg_replace('/\s|[^-a-z.0-9_~]/', '-', $shortUrl);

        return $shortUrl;
    }

    protected function execNormalValidation(): array
    {
        $validation = Validator::make($this->inputValues, Album::csvImportRules());

        if ($validation->fails()) {
            $organized = [];
            $validationErrors = $validation->errors()->toArray();
            foreach ($validationErrors as $key => $errors) {
                if (!@$organized[$key]) $organized[$key] = [];
                foreach ($errors as $message) {
                    $organized[$key][] = ['error' => true, 'message' => $message];
                }
            }
            return $organized;
        }

        return [];
    }
}