<?php

namespace App\Http\Controllers\Tools\MigrationTool\Jobs;

use App\City;
use App\User;
use App\State;
use App\Country;
use App\ManifestoAnswer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class UsersCsvFileValidationWorker extends JobQueueValidationCommons
{
    protected $category = 'user';

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->alreadyMigrated() || $this->requestingCancel()) {
            $this->saveToDatabase([]);
            return;
        }

        $countryStateCityIds = $this->getCountryStateAndCityIds();

        $this->inputValues = [
            '52f_id' => $this->getColumnSelectedValue('52f_id', true),
            'firstname' => $this->getColumnSelectedValue('firstname', true),
            'lastname' => $this->getColumnSelectedValue('lastname', true),
            'handle' => $this->getColumnSelectedValue('handle', true),
            'email' => $this->getColumnSelectedValue('email', true),
            'display_as_handle' => false,
            'how_did_hear' => '',

            'twitter_handle' => $this->getColumnSelectedValue('twitter', true),
            'instagram_handle' => $this->getColumnSelectedValue('instagram', true),
            'why' => $this->getColumnSelectedValue('why'),
            'photography_level' => $this->getSubGroupValue('photography_level'),
            'camera_type' => $this->getSubGroupValue('camera_type'),

            'country' => $countryStateCityIds['country_id'],
            'country_name' => $this->getColumnSelectedValue('country', true),
            'state' => $countryStateCityIds['state_id'],
            'state_name' => $this->getColumnSelectedValue('state', true),
            'state_member_term' => $countryStateCityIds['state_member_term'],
            'city' => $this->getColumnSelectedValue('city', true),
            'city_id' => $countryStateCityIds['city_id'],

            'dont_share_location' => false,
            'photowalks' => null
        ];

        $userIdEmailHandleSpecialVal = $this->validateIdEmailAndHandle();
        $normalValidationsResult = $this->execNormalValidation();
        $countryStateCitySpecialVal = $this->specialValidationForCountryStateCity();

        $this->saveToDatabase(array_merge_recursive(
            $userIdEmailHandleSpecialVal,
            $normalValidationsResult,
            $countryStateCitySpecialVal
        ));
    }

    protected function execNormalValidation(): array
    {
        $rules = array_merge(User::csvImportRules(), ManifestoAnswer::csvImportRules());

        $validation = Validator::make($this->inputValues, $rules);
        if ($validation->fails()) {
            $organized = [];
            $validationErrors = $validation->errors()->toArray();
            foreach ($validationErrors as $key => $errors) {
                if (!@$organized[$key]) $organized[$key] = [];
                foreach ($errors as $message) {
                    $organized[$key][] = ['error' => true, 'message' => $message];
                }
            }
            return $organized;
        }

        return [];
    }

    protected function validateIdEmailAndHandle()
    {
        $result = [];

        $userFoundById = null;
        $resultForId = $this->validate52FId(function ($user) use (&$userFoundById) {
            $userFoundById = $user;
        });
        if ($resultForId) $result['52f_id'] = $resultForId;

        $userFoundByEmail = null;
        $resultForEmail = $this->validateEmail(function ($user) use (&$userFoundByEmail) {
            $userFoundByEmail = $user;
        });
        if ($resultForEmail) $result['email'] = $resultForEmail;

        $userFoundByHandle = null;
        $resultForHandle = $this->validateHandle(function ($user) use (&$userFoundByHandle) {
            $userFoundByHandle = $user;
        });
        if ($resultForHandle) $result['handle'] = $resultForHandle;

        $sameUserRef = true;
        if ($userFoundById && $userFoundByEmail && $userFoundById->id != $userFoundByEmail->id) {
            $sameUserRef = false;
            foreach (['52f_id' => 'email', 'email' => '52f_id'] as $key => $similarRef) {
                $result[$key][] = [
                    'error' => true,
                    'message' => 'Account found is not the same as the one found in the "' . $similarRef . '" column.'
                ];
            }
        }

        $mergeToUserId = $sameUserRef ? @$userFoundById->id ?: @$userFoundByEmail->id ?: null : null;

        if ($mergeToUserId && $userFoundByHandle && $mergeToUserId != $userFoundByHandle->id) {
            $sameUserRef = false;
            $result['handle'][] = [
                'error' => true,
                'message' => 'Account found is not the same as the one found in the "id" or "email" column(s).'
            ];
        }

        $mergeToUserId = $sameUserRef ? $mergeToUserId ?: @$userFoundByHandle->id ?: null : null;

        $this->metadata['merge_to_user_id'] = $mergeToUserId;
        $this->metadata['multiple_accounts_found'] = !$sameUserRef;
        $this->metadata['new_account'] = !$this->metadata['multiple_accounts_found'] && (!$this->metadata['merge_to_user_id'] || !$this->metadata['multiple_accounts_found']);

        return $result;
    }

    protected function validate52FId(\Closure $userFoundCallback = null)
    {
        return $this->validate(
            '52f_id',
            null,
            function (User $user = null) use ($userFoundCallback) {
                if (!$user) return null;
                if ($userFoundCallback) $userFoundCallback($user);
                return [
                    'warning' => true,
                    'info' => ['user' => $user->toArray()],
                    'message' => '52Frames ID found in database. Information from this row will overwrite that account.'
                ];
            }
        );
    }

    protected function validateEmail(\Closure $userFoundCallback = null)
    {
        return $this->validate(
            'email',
            function (Builder $query, string $column, string $value) {
                return $query->orWhere($column, 'like', "$value");
            },
            function (User $user = null) use ($userFoundCallback) {
                if (!$user) return null;
                if ($userFoundCallback) $userFoundCallback($user);
                return [
                    'warning' => true,
                    'info' => ['user' => $user->toArray()],
                    'message' => 'Email address found in database. Information from this csv will overwrite that account.'
                ];
            }
        );
    }

    protected function validateHandle(\Closure $userFoundCallback = null)
    {
        return $this->validate(
            'handle',
            function (Builder $query, string $column, string $value) {
                return $query->orWhere($column, 'like', "%$value%");
            },
            function (User $user = null) use ($userFoundCallback) {
                if (!$user) return null;
                if ($userFoundCallback) $userFoundCallback($user);
                return [
                    'warning' => true,
                    'info' => ['user' => $user->toArray()],
                    'message' => 'Account with the same Handle found in database. Information from this csv will overwrite that account.'
                ];
            }
        );
    }

    protected function getCountryStateAndCityIds()
    {
        $countryName = $this->getColumnSelectedValue('country');
        $stateName = $this->getColumnSelectedValue('state');
        $cityName = $this->getColumnSelectedValue('city');

        $country = Country::where('name', 'like', "$countryName")->first();
        if (!$country) return [
            'state_member_term' => null,
            'country_id' => null,
            'state_id' => null,
            'city_id' => null
        ];

        $stateId = null;
        $stateMemberTerm = $country->state_member_term;
        if ($stateMemberTerm && $stateName) {
            $state = State::whereCountryId($country->id)->where('name', 'like', "$stateName")->first();
            if ($state) $stateId = $state->id;
        }

        $city = City::whereCountryId($country->id)->where('name', 'like', "$cityName")->first();

        return [
            'state_member_term' => $country->state_member_term,
            'country_id' => $country->id,
            'state_id' => $stateId,
            'city_id' => $city ? $city->id : null
        ];
    }

    protected function errorMsgForCountryStateCity(string $inputText)
    {
        return "This $inputText is not found in the database. Please make sure the $inputText name in this " .
            "row is not another term (check spelling, etc) for an already existing one. If you are sure that this " .
            "is a new $inputText, you can decide to Migrate this row now. The $inputText name here will then be " .
            "added into the database as a new record. You may also choose to re-validate all these rows afterwards, " .
            "to see the updated Error Messages, and perhaps see that new City name being reused by the other rows, by then.";
    }

    protected function specialValidationForCountryStateCity()
    {
        $errors = [];
        if (!$this->inputValues['country'] && $this->inputValues['country_name'])
            $errors['country'] = [[
                'error' => true,
                'message' => $this->errorMsgForCountryStateCity('Country')
            ]];
        if (!$this->inputValues['state'] && $this->inputValues['state_member_term'] && $this->inputValues['state_name'])
            $errors['state'] = [[
                'error' => true,
                'message' => $this->errorMsgForCountryStateCity($this->inputValues['state_member_term'])
            ]];
        if (!$this->inputValues['city_id'] && $this->inputValues['city'])
            $errors['city'] = [[
                'warning' => true,
                'message' => $this->errorMsgForCountryStateCity('City')
            ]];
        return $errors;
    }
}