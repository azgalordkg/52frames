<?php

namespace App\Http\Controllers\Tools\MigrationTool\Jobs;

use App\ExifItem;
use App\User;
use App\Album;
use App\Photo;
use App\Traits\ManipulatesArray;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PhotosCsvFileValidationWorker extends JobQueueValidationCommons
{
    use ManipulatesArray;

    protected $category = 'photo';

    protected $s3ArchiveDiskName = 'photosArchiveS3';
    protected $localDiskPhotoLocation = 'local';

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->alreadyMigrated() || $this->requestingCancel()) {
            $this->saveToDatabase([]);
            return;
        }

        $userInfo = $this->validateUserInputs();
        $albumInfo = $this->validateAlbumInputs();
        $photoInfo = $this->validatePhotoInput($albumInfo['albumRow']);
        $tagsInfo = $this->validateTagsInput();
        $photoExifsInfo = $this->validatePhotoExifOptions();

        $this->inputValues = [

            'album_id' => @$albumInfo['albumRow']->id ?: null,
            'user_id' => @$userInfo['userRow']->id ?: null,

            'csv_imported_info' => [
                'wufoo_entry_id' => $this->getColumnSelectedValue('entry_id', true),
                'original_url_column_value' => $this->getColumnSelectedValue('url', true),
                'base_filename_from_orig_url' => $photoInfo['baseFilename'],
                's3_file_path' => $photoInfo['s3FilePath']
            ],

            'title' => $this->getColumnSelectedValue('title', true),
            'caption' => $this->getColumnSelectedValue('caption', true),

            'screencast_critique' => !!$this->getColumnSelectedValue('screencast_critique', true),
            'location' => $this->getColumnSelectedValue('location', true),

            'tags' => $tagsInfo['tags'],

            'about_photo' => $photoExifsInfo['result']['about_photo'],
            'about_photo_answers' => $photoExifsInfo['result']['about_photo_answers'],
            'camera_settings' => $photoExifsInfo['result']['camera_settings'],
            'camera_settings_answers' => $photoExifsInfo['result']['camera_settings_answers'],

            'critique_level' => $this->getShortnameForCritiqueLevels(),

            'is_graphic' => !!$this->getColumnSelectedValue('has_nudity', true, 'photo', 'contains_graphic_content'),
            'has_nudity' => !!$this->getColumnSelectedValue('has_nudity', true, 'photo', 'contains_graphic_content'),

            'qualifies_extra_credit' => !!$this->getColumnSelectedValue('qualifies_extra_credit', true, 'photo', 'others'),
            'ownership_confirmation' => !!$this->getColumnSelectedValue('photo_ownership_confirmation', true, 'photo', 'others'),
        ];

        if ($this->getColumnSelectedValue('model_consent', true, 'photo', 'contains_graphic_content'))
            $this->inputValues['model_consent'] = true;

        $errors = array_merge_recursive(
            $albumInfo['errors'],
            $userInfo['errors'],
            $photoInfo['errors'],
            $tagsInfo['errors'],
            $photoExifsInfo['errors'],
            $this->execNormalValidation()
        );

        if (!count($errors)) $this->metadata['new_photo'] = true;

        $this->saveToDatabase($errors);
    }

    protected function validateUserInputs(): array
    {
        $user = null;

        $original52fId = $this->getColumnSelectedValue('52f_id', true, 'photographer');
        $email = $this->getColumnSelectedValue('email', true, 'photographer');

        $userQuery = User::with('signedManifesto');
        if ($email) $userQuery->where('email', $email);
        if ($original52fId) $userQuery->orWhere('imported_info', 'REGEXP', '"52f_id":' . $original52fId . '[^0-9]');
        if ($original52fId || $email) $user = $userQuery->first();

        $result = [
            'userRow' => $user && $user->signedManifesto ? $user : null,
            'errors' => []
        ];

        if ($user) {
            if (!$user->signedManifesto) $result['errors']['photographer'][] = [
                'error' => true,
                'info' => ['user' => $user->toArray()],
                'message' => "No Manifesto found for this User account. Not a Photographer."
            ];
        } else $result['errors']['photographer'][] = [
            'error' => true,
            'info' => [
                'email' => $email,
                '52f_id' => $original52fId
            ],
            'message' => "Cannot find a Photographer on the Live databse with this Email address or 52Frames ID."
        ];

        return $result;
    }

    protected function validateAlbumInputs(): array
    {
        $album = null;

        $year = $this->getColumnValue('year', 'album');
        $weekNumber = $this->getColumnValue('week_number', 'album');

        if ($year && $weekNumber) $album = Album::where('year', $year)->where('week_number', $weekNumber)->first();

        $result = [
            'albumRow' => $album,
            'errors' => []
        ];

        if (!$album) $result['errors']['album'][] = [
            'error' => true,
            'info' => [
                'year' => $year,
                'week_number' => $weekNumber
            ],
            'message' => 'Album does not exist on the Live database. Please repick the columns and choose the right Album.'
        ];

        return $result;
    }

    protected function validatePhotoInput(Album $album = null): array
    {
        $result = [
            's3FilePath' => null,
            'baseFilename' => null,
            'errors' => []
        ];

        if (!$album) {
            $result['errors']['photo_url'][] = [
                'error' => true,
                'message' => "Album is required for the Photo's location on S3 to be checked."
            ];
            return $result;
        }

        $photosStorage = Storage::disk($this->s3ArchiveDiskName);

        $yearFolderName = str_replace('$year', $album->year, env('CSV_ARCHIVED_PHOTOS_YEAR_FOLDER_PATTERN'));

        $themeFolderPattern = str_replace('$weekNumber', sprintf("%02d", $album->week_number), env('CSV_ARCHIVED_PHOTOS_WEEK_FOLDER_PATTERN'));
        $themesFoldersList = $photosStorage->directories($yearFolderName);
        $themeFolderPath = $this->findStringFromArrayByRegex($themesFoldersList, "{$yearFolderName}/{$themeFolderPattern}");

        $entryId = $this->getColumnSelectedValue('entry_id');
        $photoUrl = $this->getColumnSelectedValue('url');
        $photoBaseFilename = preg_replace('/\s+\(.+\)$/', '', $photoUrl);

        $s3PhotosList = $photosStorage->files($themeFolderPath);
        $s3PhotoFilenamePattern = "{$themeFolderPath}/entry-{$entryId}-{$photoBaseFilename}";
        $s3FilePath = $this->findStringFromArrayByRegex($s3PhotosList, $s3PhotoFilenamePattern);

        if ($s3FilePath) {
            $this->downloadPhotoFromS3Archive($s3FilePath);
            $result['s3FilePath'] = $s3FilePath;
            $result['baseFilename'] = $photoBaseFilename;
        } else $result['errors']['photo_url'][] = [
            'error' => true,
            'message' => "Cannot find Photo in S3 Archives, expected filename and location: {$s3PhotoFilenamePattern} (does not exist)."
        ];

        return $result;
    }

    protected function downloadPhotoFromS3Archive(string $s3FilePath)
    {
        if (Photo::checkIfS3PhotoDownloadedToLocal($s3FilePath)) return;

        $photo = Storage::disk($this->s3ArchiveDiskName)->get($s3FilePath);
        Storage::disk($this->localDiskPhotoLocation)->put($s3FilePath, $photo);
    }

    protected function validateTagsInput(): array
    {
        $result = [
            'tags' => [],
            'errors' => []
        ];

        $tagsStr = $this->getColumnSelectedValue('tags', true);
        if (!is_string($tagsStr)) return $result;

        $tagsArr = preg_split("/[\s,]+/", trim($tagsStr));
        $tagsArr = array_map('trim', $tagsArr);
        $tags = $result['tags'] = array_unique($tagsArr);

        $inputs = ['tags' => $tags];
        $insertRules = Photo::insertRules();
        $rules = [
            'tags' => $insertRules['tags'],
            'tags.*' => $insertRules['tags.*']
        ];
        $wrongTags = [];
        $validation = Validator::make($inputs, $rules);
        if ($validation->fails()) {
            $validationErrors = $validation->errors()->toArray();
            foreach ($validationErrors as $key => $errors) {
                preg_match('/^tags\.([0-9]+)$/', $key, $matches);
                if ($matches && is_array($matches) && count($matches) === 2 && ($wrongTag = $tags[$matches[1]]))
                    $wrongTags[] = $wrongTag;
            }
            $result['errors']['tags'][] = [
                'error' => true,
                'info' => $tags,
                'message' => "These tags don't seem to follow the pattern: " . implode(', ', $wrongTags)
            ];
        }
        return $result;
    }

    protected function getShortnameForCritiqueLevels(): ?string
    {
        $critiqueLevelString = $this->getColumnSelectedValue('column', true, 'photo', 'critique_level');
        $options = [
            '52f-cc_regular' => $this->getColumnValue('52f-cc_regular', 'photo', 'critique_level'),
            'shred_away' => $this->getColumnValue('shred_away', 'photo', 'critique_level'),
            'extra_sensitive' => $this->getColumnValue('extra_sensitive', 'photo', 'critique_level'),
            'no_critique' => $this->getColumnValue('no_critique', 'photo', 'critique_level')
        ];
        $shortTerm = array_search($critiqueLevelString, $options);
        return $shortTerm !== false ? $shortTerm : null;
    }

    protected function validatePhotoExifOptions()
    {
        $output = [
            'result' => null,
            'errors' => []
        ];

        $result = [
            'about_photo' => false,
            'about_photo_answers' => [
//                '{exif_question_id}' => '1 or 0 (actual integer)'
            ],
            'camera_settings' => false,
            'camera_settings_answers' => [
//                '{exif_question_id}' => '{exif_answer_id, actual integer} or "text"'
            ]
        ];

        $columnsMapping = [
            'about_photo_questions' => [
                'answer_group_term' => 'about_photo',
                'answers_list_term' => 'about_photo_answers',
                'csvDbMapping' => [
                    'shooting_and_processing' => [
                        'group_term' => 'shoot_and_proc',
                        'dbGroupKeywords' => [
                            'external_flash' => 'Used External Flash',
                            'shutter' => '1" Shutter and Longer',
                            'aperture' => 'f1.8 Aperture and Wider',
                            'composite_edit' => 'Composite Edit',
                            'hdr' => 'HDR',
                            'shot_with_a_phone' => 'Shot with a Phone',
                            'black_and_white' => 'Black & White'
                        ]
                    ],
                    'subject_matter' => [
                        'group_term' => 'subject_matter',
                        'dbGroupKeywords' => [
                            'architecture' => 'Architecture',
                            'fine_art' => 'Fine Art',
                            'street_photography' => 'Street Photography',
                            'travel' => 'Travel',
                            'action' => 'Action',
                            'humor' => 'Humor',
                            'nature' => 'Nature',
                            'macro' => 'Macro',
                            'abstract' => 'Abstract',
                            'portrait' => 'Portrait/Self-Portrait',
                            'landscape' => 'Landscape',
                            'still_life' => 'Still Life'
                        ]
                    ]
                ]
            ],
            'camera_settings_questions' => [
                'answer_group_term' => 'camera_settings',
                'answers_list_term' => 'camera_settings_answers',
                'csvDbMapping' => [
                    'shutter_speed' => [
                        'group_term' => 'shutter'
                    ],
                    'aperture' => [
                        'group_term' => 'aperture'
                    ],
                    'iso' => [
                        'group_term' => 'iso'
                    ],
                    'focal_length' => [
                        'expects_text' => true,
                        'db_option_name' => 'Focal Length'
                    ],
                    'camera_manufacturer' => [
                        'expects_text' => true,
                        'db_option_name' => 'Camera Manufacturer'
                    ],
                    'camera_model' => [
                        'expects_text' => true,
                        'db_option_name' => 'Camera Model'
                    ],
                    'lens' => [
                        'group_term' => 'lens'
                    ],
                    'flash' => [
                        'group_term' => 'flash'
                    ]
                ]
            ]
        ];

        foreach ($columnsMapping as $groupName => $groupData) {
            $groupParents = ExifItem::whereParentId(null)->whereGroupName($groupName)->get();
            if (!count($groupParents)) continue;
            $groupParents = $groupParents->toArray();
            $parentIds = array_column($groupParents, 'id');
            $parents = array_combine($parentIds, $groupParents);
            foreach ($groupData['csvDbMapping'] as $csvGroupName => $subGroupData) {
                $resultHolder = &$result[$groupData['answers_list_term']];

                if (@$subGroupData['expects_text']) {

                    $value = $this->getColumnSelectedValue($csvGroupName, true, 'photo', $groupData['answer_group_term']);
                    if (!$value) continue;

                    $parentSelected = null;
                    foreach ($parents as $parentId => $parent) {
                        if ($parent['type'] != 'text') continue;
                        if ($parent['name'] == $subGroupData['db_option_name']) {
                            $parentSelected = $parent;
                            break;
                        }
                    }

                    if ($parentSelected) $resultHolder[$parentSelected['id']] = (string)$value;
                    else $output['errors'][$groupData['answer_group_term']][] = [
                        'error' => true,
                        'message' => "Code Error --> Cannot find Group Name is database: " . $subGroupData['db_option_name']
                    ];

                } else {

                    $members = ExifItem::whereIn('parent_id', $parentIds)->whereGroupName($subGroupData['group_term'])->get();
                    $membersInfo = $members->toArray();
                    if (!count($membersInfo)) continue;

                    $parentId = array_unique(array_column($membersInfo, 'parent_id'))[0];
                    $parent = $parents[$parentId];

                    $memberIds = array_column($membersInfo, 'id');
                    $memberNames = array_column($membersInfo, 'name');
                    $options = array_combine($memberNames, $membersInfo);

                    switch ($parent['type']) {

                        case 'group':
                            foreach ($subGroupData['dbGroupKeywords'] as $frontendTerm => $dbMemberName) {
                                $value = $this->getColumnSelectedValue($frontendTerm, true, 'photo', $csvGroupName);
                                if (!$value) continue;
                                $optionMatched = @$options[$value];
                                if ($optionMatched) $resultHolder[$optionMatched['id']] = 1;
                                else $output['errors'][$csvGroupName][] = [
                                    'error' => true,
                                    'info' => array_combine($memberIds, $memberNames),
                                    'message' => "The following column value has no valid ID in the database: " . $value
                                ];
                            }
                            break;

                        case 'array':
                            $value = $this->getColumnSelectedValue($csvGroupName, true, 'photo', $groupData['answer_group_term']);
                            if (!$value) break;
                            $optionMatched = @$options[$value];
                            if ($optionMatched) $resultHolder[$parentId] = (string)$optionMatched['id'];
                            else $output['errors'][$groupData['answer_group_term']][] = [
                                'error' => true,
                                'info' => array_combine($memberIds, $memberNames),
                                'message' => "The following column value has no valid ID in the database: " . $value
                            ];
                            break;
                    }
                }

                if (count($resultHolder)) $result[$groupData['answer_group_term']] = true;
            }
        }

        $output['result'] = $result;
        return $output;
    }

    protected function execNormalValidation(): array
    {
        $rules = Photo::csvImportRules();

        $validation = Validator::make($this->inputValues, $rules, Photo::insertRulesErrorMessages());
        if ($validation->fails()) {
            $organized = [];
            $validationErrors = $validation->errors()->toArray();
            foreach ($validationErrors as $key => $errors) {
                if (!@$organized[$key]) $organized[$key] = [];
                foreach ($errors as $message) {
                    $organized[$key][] = ['error' => true, 'message' => $message];
                }
            }
            return $organized;
        }

        return [];
    }
}