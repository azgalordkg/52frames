<?php

namespace App\Http\Controllers\Tools\MigrationTool\Utilities;

use App\City;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Traits\Helpers\HelpsLocalFileSystem;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as XlsxReader;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class CityNamesOnlyDbScanner extends Controller
{
    protected $saveToPath = 'excel-outputs';

    protected function getExcelSheetByIndex(string $filePath, int $worksheetIndex = 0)
    {
        $reader = new XlsxReader();
        $spreadsheet = $reader->load($filePath);
        return $spreadsheet->getSheet($worksheetIndex);
    }

    protected function getExcelHeaders(string $filePath, int $worksheetIndex = 0)
    {
        $sheet = $this->getExcelSheetByIndex($filePath, $worksheetIndex);
        $maxColumn = $sheet->getHighestColumn();
        $headers = $sheet->rangeToArray("A1:{$maxColumn}1")[0];
        return $headers;
    }

    protected function getRowsFromColumn(string $filePath, int $worksheetIndex = 0, int $columnIndex = 0)
    {
        $sheet = $this->getExcelSheetByIndex($filePath, $worksheetIndex);
        $columnLetter = Coordinate::stringFromColumnIndex($columnIndex);
        $maxRow = $sheet->getHighestRow();
        $rows = $sheet->rangeToArray("{$columnLetter}2:{$columnLetter}{$maxRow}");
        return array_column($rows, 0);
    }

    protected function scanDb(array $list)
    {
        $result = [
            'no_matches' => [],
            'already_exists' => [],
            'matched_many' => [],
        ];
        foreach ($list as $index => $cityName) {
            $default = [
                'row_id' => $index + 1,
                'city_name' => $cityName,
            ];
            $cities = City::whereName($cityName)->with('state.country')->get();
            $matchedCount = count($cities);
            if ($matchedCount) {
                if ($matchedCount > 1) {
                    foreach ($cities as $city) {
                        $default['matches.id'] = $city->id;
                        $default['matches.name'] = $city->name;
                        $default['matches.state'] = $city->state ? $city->state->name : '';
                        $default['matches.country'] = $city->country ? $city->country->name : '';
                        $category = 'matched_many';
                        $result[$category][] = $default;
                    }
                } else {
                    $category = 'already_exists';
                    $result[$category][] = $default;
                }
            } else {
                $category = 'no_matches';
                $result[$category][] = $default;
            }
        }
        return $result;
    }

    public function readSpreadsheetFile(Request $request)
    {
        set_time_limit(0);

        $fileExt = strtolower($request->file ? $request->file->getClientOriginalExtension() : '');

        $validation = Validator::make([
            'file' => $request->file,
            'extension' => $fileExt
        ], [
            'file' => 'required',
//            'extension' => 'required_with:file|in:csv,xls,xlsx',
            'extension' => 'required_with:file|in:xls,xlsx',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        if ($fileExt == 'csv') {
            $this->getCitiesFromCsv($request);
        } else {
            $reader = new XlsxReader();
            $worksheets = $reader->listWorksheetNames($request->file->getPathname());
            $numSheets = count($worksheets);
            if ($numSheets) {
                $filenamePattern = md5($request->file->getPathname() . Carbon::now()->getTimestamp() . rand());
                $filename = "$filenamePattern.$fileExt";
                $request->file->move($this->saveToPath, $filename);

                if ($numSheets < 2) {
                    return $this->getExcelHeaders($filename);
                } else {
                    return [
                        'which_worksheet' => true,
                        'filename' => $filename,
                        'worksheets' => $worksheets
                    ];
                }
            }
        }

        return response()->json([
            'error' => true,
            'message' => 'No Worksheets found. Please upload another file.'
        ], 404);
    }

    protected function validateUserChoices(Request $request, array $extraValidationRules = [])
    {
        $validation = Validator::make(array_merge($request->all(), [
            'extension' => strtolower($request->filename)
        ]), array_merge([
            'filename' => 'required|string',
            'worksheet_index' => 'required|integer',
        ], $extraValidationRules));
        if ($validation->fails()) return response()->json([
            'validation_errors' => $validation->errors()
        ], 400);

        $filePath = "$this->saveToPath/$request->filename";
        if (!file_exists($filePath)) return response()->json([
            'file_not_found' => true
        ], 404);

        $reader = new XlsxReader();
        $worksheets = $reader->listWorksheetNames($filePath);
        if ($request->worksheet_index >= count($worksheets)) return response()->json([
            'error' => true,
            'message' => 'Worksheet not found. Please choose another one from the list.'
        ], 400);

        return $filePath;
    }

    public function chosenWorksheet(Request $request)
    {
        $filePath = $this->validateUserChoices($request);
        if (!is_string($filePath)) return $filePath;
        return $this->getExcelHeaders($filePath, $request->worksheet_index);
    }

    protected function chosenColumn(Request $request)
    {
        $filePath = $this->validateUserChoices($request, [
            'column_index' => 'required|integer|min:1'
        ]);
        if (!is_string($filePath)) return $filePath;

        $headers = $this->getExcelHeaders($filePath, $request->worksheet_index);
        if ($request->column_index >= count($headers)) return response()->json([
            'error' => true,
            'message' => 'Column selected has no Value. Please choose another one from the list.'
        ], 400);

        return $filePath;
    }

    public function startCityMatching(Request $request)
    {
        $filePath = $this->chosenColumn($request);
        if (!is_string($filePath)) return $filePath;

        $cities = $this->getRowsFromColumn($filePath, $request->worksheet_index, $request->column_index);
        $dbMatches = $this->scanDb($cities);
        $exportPath = $this->exportToExcel($filePath, $dbMatches);

        return [
            'matching_result' => $dbMatches,
            'exported_no_matching_url' => $exportPath
        ];
    }

    protected function exportToExcel(string $fileSource, array $dbMatches)
    {
        $spreadsheet = new Spreadsheet();
        $filenameOnly = HelpsLocalFileSystem::getFilenameOnly($fileSource);
        $filePath = "$this->saveToPath/$filenameOnly-export.xlsx";
        @unlink($filePath);

        $sheets = [
            'no_matches' => [
                'sheet' => $spreadsheet->getActiveSheet()->setTitle('Not found in database'),
                'headers' => [
                    'row_id' => 'Row Number from Source',
                    'city_name' => 'City Name'
                ],
                'data' => @$dbMatches['no_matches']
            ],
            'matched_many' => [
                'sheet' => $spreadsheet->createSheet()->setTitle('Many matches found in database'),
                'headers' => [
                    'row_id' => 'Row Number from Source',
                    'matches.name' => 'City Name',
                    'matches.id' => 'City ID in database',
                    'matches.state' => 'State Name',
                    'matches.country' => 'Country Name',
                ],
                'subLoopKey' => 'matches',
                'data' => @$dbMatches['matched_many'],
            ],
        ];

        foreach ($sheets as $category => $info) {
            $this->makeSheetAboutCollection(
                $info['sheet'],
                $info['headers'],
                $info['data'],
                @$info['subLoopKey']
            );
        }

        $writer = new XlsxWriter($spreadsheet);
        HelpsLocalFileSystem::createPathFolders($filePath);
        $writer->save($filePath);

        return $filePath;
    }

    protected function makeSheetAboutCollection(Worksheet $sheet, array $headers, array $data = [], string $subLoopKey = null)
    {
        $headerKeys = array_keys($headers);
        $headerValues = array_values($headers);
        $dataSource = array_merge([$headers], $data);
        foreach ($dataSource as $rowIndex => $cityData) {
            $rowNumber = $rowIndex + 1;
            foreach ($headerValues as $index => $header) {
                $cityKey = $headerKeys[$index];
                $columnLetter = Coordinate::stringFromColumnIndex($index + 1);
                $cellData = $cityData[$cityKey];
                $sheet->setCellValue("{$columnLetter}{$rowNumber}", $cellData);
            }
        }
    }

}
