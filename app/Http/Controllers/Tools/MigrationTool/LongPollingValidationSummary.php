<?php

namespace App\Http\Controllers\Tools\MigrationTool;

use App\CsvUploadedFile;
use App\Services\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Tools\MigrationTool\Traits\HelpsMigrateCsvRows;

class LongPollingValidationSummary extends Controller
{
    use HelpsMigrateCsvRows;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('migration.roles: csv_uploader, csv_data_merger, csv_data_manager');
    }

    protected function pollRequestRules(): array
    {
        return [
            'no_rows' => 'nullable|boolean',
            'csv_file_ids' => 'required|array',
            'csv_file_ids.*' => 'required|integer|exists:csv_uploaded_files,id'
        ];
    }

    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), $this->pollRequestRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $result = CsvUploadedFile::whereIn('id', $request->csv_file_ids)->get()->toArray();

        $records = [];
        foreach ($request->csv_file_ids as $file_id) {
            foreach ($result as $file) {
                if ($file['id'] == $file_id) {
                    $filtered = Helpers::filterByKeysArray($file, [
                        'id',
                        'category',
                        'validation_summary'
                    ]);
                    if (@$filtered['validation_summary']) {
                        $filtered['validation_summary'] = $this->checkIfShouldRemoveRowsArray($request, $filtered['validation_summary']);
                    }
                    $records[] = $filtered;
                }
            }
        }

        return $records;
    }
}
