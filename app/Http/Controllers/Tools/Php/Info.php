<?php

namespace App\Http\Controllers\Tools\Php;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Info extends Controller
{
    public function show(Request $request)
    {
        $this->debugModeRequired($request);

        echo phpinfo();
    }
}
