<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\ResetPasswordEmailer;
use App\Mail\ResetPasswordEmail;
use App\SocialAccount;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'apiResetPassword'
        ]);
    }

    /**
     * Sets-up the account for a Password change.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiForgotPassword(Request $request)
    {
        $rules = User::loginRules();
        $rules = [
            'email' => $rules['email']
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = User::whereEmail($request->get('email'))->first();

        if (!$user) {
            return response()->json([
                'needsSignup' => true,
                'message' => trans('passwords.user')
            ], 400);
        }

        if (!$user->password) {
            $socialAccounts = SocialAccount::whereUserId($user->id)->get();
            if (count($socialAccounts)) return response()->json([
                'use_social_login' => true,
                'providers' => array_column($socialAccounts->toArray(), 'provider'),
                'message' => "You have no password. Please login some other way.",
            ], 401);
        }

        $token = $this->broker()->createToken($user);
        Mail::to($user)->send(new ResetPasswordEmail($user, $token));

        return response()->json(['email_sent' => true]);

    }

}
