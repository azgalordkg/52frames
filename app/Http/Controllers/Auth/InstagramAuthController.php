<?php

namespace App\Http\Controllers\Auth;

use App\Album;
use App\Http\Controllers\Controller;
use App\Providers\Modified\Spatie\Newsletter\Newsletter;
use App\Services\Debugger;
use App\Services\InstagramAccountService;
use App\Services\MailchimpService;
use App\Traits\HelpsLogin;
use App\User;
use App\UserSetting;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class InstagramAuthController extends Controller
{
    use HelpsLogin;

    /**
     * Logs-in the User using the API.
     * @param Request $request
     * @param InstagramAccountService $instagramService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiLogin(Request $request, InstagramAccountService $instagramService)
    {
        $tokenKeyName = 'access_token';
        $instagramAccessToken = $request->get($tokenKeyName);

        if ($instagramAccessToken) {
            try {
                $instagramUser = $instagramService->getProviderUserByToken($instagramAccessToken);
                $instagramAccount = $instagramService->getSocialAccountByProviderId();

                if ($instagramAccount) {
                    $user = $instagramAccount->user;
                    if (!$user) $instagramService->deleteSocialAccount();
                    else {
                        $accessToken = $instagramService->createOAuthToken();
                        $this->loginUsingNormalAuth($request, $user, $accessToken);
                        return [
                            'token_type' => 'Bearer',
                            'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                            'access_token' => $accessToken,
                            'user' => User::accountSummary($user),
                            'albums_recent' => Album::getRecent($request)
                        ];
                    }
                }

                $user = $instagramService->mapUserModel($instagramUser);
                $socialAccount = $instagramService->mapSocialAccountModel($instagramUser);
                $userDetails = array_merge($user, $socialAccount);

                $user = User::whereEmail($instagramUser->getEmail())->first();
                if ($user) return ['emailExists' => $userDetails];

                return ['needsSignup' => $userDetails];

            } catch (ClientException $exception) {

                $error = json_decode($exception->getResponse()->getBody());
                $httpCode = $exception->getCode();
                return response()->json($error, $httpCode);

            } catch (\Exception $exception) {

                $output = ['message' => 'Something went wrong. Please contact any of the site admins.'];
                if (Debugger::canUserDebug($request, $request->user()))
                    $output['trace'] = [
                        'error_code' => $exception->getCode(),
                        'message' => $exception->getMessage(),
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ];
                return response()->json($output, 400);

            }
        } else {
            return response()->json([
                'error' => 'invalid_request',
                'message' => 'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.',
                'hint' => 'Check the `' . $tokenKeyName . '` parameter'
            ], 400);
        }
    }

    /**
     * Signs-up the User using the API.
     * @param Request $request
     * @param User $user
     * @param InstagramAccountService $instagramService
     * @param Newsletter $newsletter
     * @param MailchimpService $mailchimpService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiSignup(Request $request, User $user, InstagramAccountService $instagramService, Newsletter $newsletter, MailchimpService $mailchimpService)
    {
        $instagramLoginResponse = $this->apiLogin($request, $instagramService);

        if (is_array($instagramLoginResponse) && array_has($instagramLoginResponse, 'access_token')) {
            return $instagramLoginResponse;
        }

        if (is_array($instagramLoginResponse) && @$instagramLoginResponse['needsSignup']) {

            $userData = $request->all();
            $validation = Validator::make($userData, User::instagramSignupRules());
            if ($validation->fails()) {
                return response()->json([
                    'validation_errors' => $validation->errors()
                ], 400);
            }

            $user->fill($userData);
            $user->save();

            $instagramAccount = $instagramService->createSocialAccountAndRelateToUser($user);
            $user->main_social_account_id = $instagramAccount->id;
            $user->save();

            UserSetting::findOrCreateRow($user);

            $this->subscribeUserToMailchimpMergeOurUserId($user, $request, $newsletter, $mailchimpService);

            $accessToken = $instagramService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken
            ];

        } else {

            return response()->json([
                'error' => 'instagram_access_token',
                'message' => 'Something went wrong. Please try again.',
                'hint' => 'Check the `access_token` parameter'
            ], 400);

        }

    }

    /**
     * Links a Instagram account to a matching/existing account that is using an Email/Password combination.
     * @param Request $request
     * @param InstagramAccountService $instagramService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiLinkAccountToEmailPass(Request $request, InstagramAccountService $instagramService)
    {
        $instagramLoginResponse = $this->apiLogin($request, $instagramService);

        if (is_array($instagramLoginResponse) && @$instagramLoginResponse['emailExists']) {

            $instagramUser = $instagramLoginResponse['emailExists'];

            $passwordRule = User::loginRules()['password'];
            $validation = Validator::make($request->all(), [
                'password' => $passwordRule
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'validation_errors' => $validation->errors()
                ], 400);
            }

            $user = User::whereEmail($instagramUser['email'])->first();

            $passwordCorrect = Hash::check($request->get('password'), $user->password);
            if (!$passwordCorrect) {
                return response()->json([
                    'validation_errors' => [
                        'password' => [
                            'Password is incorrect. Please try again.'
                        ]
                    ]
                ], 401);
            }

            $instagramAccount = $instagramService->createSocialAccountAndRelateToUser($user);
            $user->main_social_account_id = $instagramAccount->id;
            $user->save();

            $accessToken = $instagramService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken,
                'user' => User::accountSummary($user),
                'albums_recent' => Album::getRecent($request)
            ];

        } else {

            return response()->json([
                'error' => 'instagram_access_token',
                'message' => 'Something went wrong. Please try again.',
                'hint' => 'Check the `access_token` parameter'
            ], 400);

        }
    }

    /**
     * Adds a Password to an existing account that is using a Instagram signin.
     * @param Request $request
     * @param InstagramAccountService $instagramService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiAddPasswordToAccount(Request $request, InstagramAccountService $instagramService)
    {
        $loginRules = User::loginRules();
        $signupRules = User::signupRules();

        $rules = [
            'access_token' => 'required',
            'email' => $loginRules['email'],
            'password' => $signupRules['password'],
            'password_confirmation' => $signupRules['password_confirmation']
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $instagramService->getProviderUserByToken($request->access_token);

        $instagramAccount = $instagramService->getSocialAccountByProviderId();

        $user = $instagramAccount ? $instagramAccount->user : null;

        if ($user && strtolower($request->email) == strtolower($user->email)) {

            $user->password = $request->password;
            $user->save();

            $accessToken = $instagramService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken,
                'user' => User::accountSummary($user),
                'albums_recent' => Album::getRecent($request)
            ];

        } else {

            return response()->json([
                'validation_errors' => [
                    'email' => [
                        'The email does not match the one on Instagram.'
                    ]
                ]
            ], 401);

        }
    }

}
