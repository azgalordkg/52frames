<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ManifestoController;
use App\Providers\Modified\Spatie\Newsletter\Newsletter;
use App\Services\LocalOAuthService;
use App\Services\MailchimpService;
use App\Traits\HelpsLogin;
use App\User;
use App\Http\Controllers\Controller;
use App\UserSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, HelpsLogin;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'apiSignup'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, User::signupRules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->fill($data)->save();
        return $user;
    }

    /**
     * Signs-up the User based on the input.
     * @param Request $request
     * @param LocalOAuthService $localOAuth
     * @param Newsletter $newsletter
     * @param MailchimpService $mailchimpService
     * @return array|JsonResponse
     */
    public function apiSignup(Request $request, LocalOAuthService $localOAuth, Newsletter $newsletter, MailchimpService $mailchimpService, ManifestoController $manifestoController)
    {
        $validationRules = User::signupRules();
        if ($request->dont_save) {
            unset($validationRules['is_fan']);
            unset($validationRules['newsletter']);
            unset($validationRules['terms']);
        } else {
            unset($validationRules['captcha']);
        }

        $userInputs = $request->all();
        $validation = Validator::make($userInputs, $validationRules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $manifestoData = null;
        if (isset($request->is_fan) && false === (bool) $request->is_fan) {
            $manifestoData = $manifestoController->validateInput($request);
            if ($request->dont_save_manifesto || !is_array($manifestoData)) {
                return $manifestoData;
            }
        }

        if ($request->dont_save) {
            return ['valid_entry' => true];
        }

        $user = $this->create($userInputs);
        UserSetting::findOrCreateRow($user);

         $this->subscribeUserToMailchimpMergeOurUserId($user, $request, $newsletter, $mailchimpService);

        $oAuthResponse = $localOAuth->loginUsingEmail($request) ?: [];
        if (is_array($oAuthResponse)) {
            $this->loginUsingNormalAuth($request, $user, $oAuthResponse['access_token']);
        }

        if (null !== $manifestoData) {
            $manifestoController->store($request, $mailchimpService, $user);
        }

        return $oAuthResponse;
    }
}
