<?php

namespace App\Http\Controllers\Auth;

use App\Album;
use App\Http\Controllers\Controller;
use App\Services\LocalOAuthService;
use App\SocialAccount;
use App\Traits\HelpsAlbum;
use App\Traits\HelpsLogin;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, HelpsLogin, HelpsAlbum;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
            'apiLogin',
            'apiLogout'
        ]);
    }

    /**
     * Login the User based on the input.
     * @param Request $request
     * @param LocalOAuthService $localOAuth
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function apiLogin(Request $request, LocalOAuthService $localOAuth)
    {
        $validation = Validator::make($request->all(), User::loginRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = User::whereEmail($request->email)->first();

        if (!$user) {
            return response()->json([
                'needsSignup' => true,
                'message' => trans('passwords.user')
            ], 401);
        }

        if (!$user->password) {
            $socialAccounts = SocialAccount::whereUserId($user->id)->get();
            if (count($socialAccounts)) return response()->json([
                'use_social_login' => true,
                'providers' => array_column($socialAccounts->toArray(), 'provider'),
                'message' => "You have no password. Please login some other way.",
            ], 401);
        }

        $oAuthResponse = $localOAuth->loginUsingEmail($request) ?: [];

        if (is_array($oAuthResponse)) {
            $this->loginUsingNormalAuth($request, $user, $oAuthResponse['access_token']);
            return array_merge($oAuthResponse, [
                'user' => User::accountSummary($user),
                'albums_recent' => Album::getRecent($request)
            ]);
        } else {
            return $oAuthResponse;
        }
    }

    /**
     * Logout the currently logged-in User.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function apiLogout(Request $request)
    {
        $user = Auth::guard('api')->user();
        $user && $user->token()->revoke();
        $this->logoutUsingNormalAuth($request);
        return response(['success' => true]);

    }

}
