<?php

namespace App\Http\Controllers\Auth;

use App\Album;
use App\Http\Controllers\Controller;
use App\Providers\Modified\Spatie\Newsletter\Newsletter;
use App\Services\Debugger;
use App\Services\MailchimpService;
use App\Traits\HelpsLogin;
use App\User;
use App\UserSetting;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Services\FacebookAccountService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FacebookAuthController extends Controller
{
    use HelpsLogin;

    /**
     * Logs-in the User using the API.
     * @param Request $request
     * @param FacebookAccountService $facebookService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiLogin(Request $request, FacebookAccountService $facebookService)
    {
        $tokenKeyName = 'access_token';
        $fbAccessToken = $request->get($tokenKeyName);
        if ($fbAccessToken) {
            try {
                $facebookUser = $facebookService->getProviderUserByToken($fbAccessToken);
                $fbRecordOnDb = $facebookService->getSocialAccountByProviderId();

                if ($fbRecordOnDb) {
                    $user = $fbRecordOnDb->user;
                    if (!$user) $facebookService->deleteSocialAccount();
                    else {
                        $accessToken = $facebookService->createOAuthToken();
                        $this->loginUsingNormalAuth($request, $user, $accessToken);
                        return [
                            'token_type' => 'Bearer',
                            'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                            'access_token' => $accessToken,
                            'user' => User::accountSummary($user),
                            'albums_recent' => Album::getRecent($request)
                        ];
                    }
                }

                $user = $facebookService->mapUserModel($facebookUser);
                $socialAccount = $facebookService->mapSocialAccountModel($facebookUser);
                $userDetails = array_merge($user, $socialAccount);

                $user = User::whereEmail($facebookUser->getEmail())->first();
                if ($user) return ['emailExists' => $userDetails];

                return ['needsSignup' => $userDetails];

            } catch (ClientException $exception) {

                $error = json_decode($exception->getResponse()->getBody());
                $httpCode = $exception->getCode();
                return response()->json($error, $httpCode);

            } catch (\Exception $exception) {

                $output = ['message' => 'Something went wrong. Please contact any of the site admins.'];
                if (Debugger::canUserDebug($request, $request->user()))
                    $output['trace'] = [
                        'error_code' => $exception->getCode(),
                        'message' => $exception->getMessage(),
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace()
                    ];
                return response()->json($output, 400);

            }
        } else {
            return response()->json([
                'error' => 'invalid_request',
                'message' => 'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.',
                'hint' => 'Check the `' . $tokenKeyName . '` parameter'
            ], 400);
        }
    }

    /**
     * Signs-up the User using the API.
     * @param Request $request
     * @param User $user
     * @param FacebookAccountService $facebookService
     * @param Newsletter $newsletter
     * @param MailchimpService $mailchimpService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiSignup(Request $request, User $user, FacebookAccountService $facebookService, Newsletter $newsletter, MailchimpService $mailchimpService)
    {
        $fbLoginResponse = $this->apiLogin($request, $facebookService);

        if (is_array($fbLoginResponse) && @$fbLoginResponse['needsSignup']) {

            $userData = $request->all();
            $validation = Validator::make($userData, User::socialMediaSignupRules());
            if ($validation->fails()) {
                return response()->json([
                    'validation_errors' => $validation->errors()
                ], 400);
            }

            $user->fill($userData);
            $user->save();

            UserSetting::findOrCreateRow($user);

            $fbRecordOnDb = $facebookService->createSocialAccountAndRelateToUser($user);
            $user->main_social_account_id = $fbRecordOnDb->id;
            $user->save();

            $this->subscribeUserToMailchimpMergeOurUserId($user, $request, $newsletter, $mailchimpService);

            $accessToken = $facebookService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken
            ];

        } else {

            return response()->json([
                'error' => 'facebook_access_token',
                'message' => 'Something went wrong. Please try again.',
                'hint' => 'Check the `access_token` parameter'
            ], 400);

        }

    }

    /**
     * Links a Facebook account to a matching/existing account that is using an Email/Password combination.
     * @param Request $request
     * @param FacebookAccountService $facebookService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiLinkAccountToEmailPass(Request $request, FacebookAccountService $facebookService)
    {
        $fbLoginResponse = $this->apiLogin($request, $facebookService);

        if (is_array($fbLoginResponse) && @$fbLoginResponse['emailExists']) {

            $facebookUser = $fbLoginResponse['emailExists'];

            $passwordRule = User::loginRules()['password'];
            $validation = Validator::make($request->all(), [
                'password' => $passwordRule
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'validation_errors' => $validation->errors()
                ], 400);
            }

            $user = User::whereEmail($facebookUser['email'])->first();

            $passwordCorrect = Hash::check($request->get('password'), $user->password);
            if (!$passwordCorrect) {
                return response()->json([
                    'validation_errors' => [
                        'password' => [
                            'Password is incorrect. Please try again.'
                        ]
                    ]
                ], 401);
            }

            $fbRecordOnDb = $facebookService->createSocialAccountAndRelateToUser($user);
            $user->main_social_account_id = $fbRecordOnDb->id;
            $user->save();

            $accessToken = $facebookService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken,
                'user' => User::accountSummary($user),
                'albums_recent' => Album::getRecent($request)
            ];

        } else {

            return response()->json([
                'error' => 'facebook_access_token',
                'message' => 'Something went wrong. Please try again.',
                'hint' => 'Check the `access_token` parameter'
            ], 400);

        }
    }

    /**
     * Adds a Password to an existing account that is using a Facebook signin.
     * @param Request $request
     * @param FacebookAccountService $facebookService
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function apiAddPasswordToAccount(Request $request, FacebookAccountService $facebookService)
    {
        $loginRules = User::loginRules();
        $signupRules = User::signupRules();

        $rules = [
            'access_token' => 'required',
            'email' => $loginRules['email'],
            'password' => $signupRules['password'],
            'password_confirmation' => $signupRules['password_confirmation']
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $facebookService->getProviderUserByToken($request->access_token);

        $fbRecordOnDb = $facebookService->getSocialAccountByProviderId();

        $user = $fbRecordOnDb ? $fbRecordOnDb->user : null;

        if ($user && strtolower($request->email) == strtolower($user->email)) {

            $user->password = $request->password;
            $user->save();

            $accessToken = $facebookService->createOAuthToken();
            $this->loginUsingNormalAuth($request, $user, $accessToken);

            return [
                'token_type' => 'Bearer',
                'expires_in' => env('LOCAL_OAUTH_EXPIRY_MINUTES', 0) * 60,
                'access_token' => $accessToken,
                'user' => User::accountSummary($user),
                'albums_recent' => Album::getRecent($request)
            ];

        } else {

            return response()->json([
                'validation_errors' => [
                    'email' => [
                        'The email does not match the one on Facebook.'
                    ]
                ]
            ], 401);

        }
    }

}
