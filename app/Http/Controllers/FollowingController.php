<?php

namespace App\Http\Controllers;

use App\Follower;
use App\Http\Requests\FollowingRequest;
use App\Notification;
use App\User;
use Illuminate\Http\Request;

class FollowingController extends Controller
{
    /**
     * FollowingController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Helper function.
     * @param Request $request
     * @return array
     */
    protected function buildWhereArray(Request $request)
    {
        return [
            'follower_user_id' => $request->user()->id,
            'following_user_id' => $request->user_id
        ];
    }

    /**
     * Saves the new request to Follow a user.
     * @param FollowingRequest $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function store(FollowingRequest $request)
    {
        $whereArray = $this->buildWhereArray($request);

        $existing = Follower::whereArray($whereArray)->first();
        if (!$existing) {
            $follow = Follower::create($whereArray);
            Notification::saveAction(
                env('NOTIF_HOOK_PROFILE_FOLLOW'),
                $follow->following->id,
                $follow->following,
                $follow->follower
            );
        }

        $user = User::find($request->user_id);

        if ($existing) {
            return array_merge([
                'already_following' => true
            ], User::addFollowersCount($user));
        } else {
            return array_merge([
                'success' => true
            ], User::addFollowersCount($user));
        }

    }

    /**
     * Deletes the follower entry in the database.
     * @param FollowingRequest $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function destroy(FollowingRequest $request)
    {
        $whereArray = $this->buildWhereArray($request);

        $existing = Follower::whereArray($whereArray)->first();
        if ($existing) Follower::whereArray($whereArray)->delete();

        $user = User::find($request->user_id);

        if (!$existing) {
            return array_merge([
                'not_followed_before' => true
            ], User::addFollowersCount($user));
        } else {
            return array_merge([
                'success' => true
            ], User::addFollowersCount($user));
        }
    }
}
