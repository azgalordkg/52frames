<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Services\Debugger;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class MainController extends Controller
{
    /**
     * Gets the default pages, and serves it on the frontend.
     * @param Agent $agent
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allPages(Agent $agent, Request $request)
    {
        $browserSupported = true;

        $browser = $agent->browser();
        $version = $agent->version($browser);

        if ($browser == 'IE' && $version < 10) $browserSupported = false;
        if ($browser == 'Firefox' && $version < 16) $browserSupported = false;
        if ($browser == 'Chrome' && $version < 22) $browserSupported = false;
        if ($browser == 'Safari' && $version < 6) $browserSupported = false;

        $browserNames = [
            'IE' => [
                'fullName' => 'Internet Explorer',
                'installLink' => 'https://youtu.be/gk79DAuFwQY?t=2m',
                'installMessage' => 'to have this nice guy named Chris show you how to upgrade (it\'s pretty simple), and then come back to us!'
            ],
            'Firefox' => [
                'fullName' => 'Mozilla Firefox',
                'installLink' => 'https://www.youtube.com/watch?v=UmbjMEP4iJs',
                'installMessage' => 'to have this nice guy named Chris show you how to upgrade (it\'s pretty simple), and then come back to us!'
            ],
            'Chrome' => [
                'fullName' => 'Google Chrome',
                'installLink' => 'https://www.youtube.com/watch?v=RyI7RyEEPxM',
                'installMessage' => 'to have this nice guy named Chris show you how to upgrade (it\'s pretty simple), and then come back to us!'
            ],
            'Safari' => [
                'fullName' => 'Apple Safari',
                'installLink' => 'https://support.apple.com/en-us/HT204416',
                'installMessage' => 'to read up on how to upgrade Safari browser on a mac.'
            ]
        ];

        if ($browserSupported) return view('main', [
            'config' => $this->buildConfig($request)
        ]);
        else return view('unsupported-browser', [
            'agent' => $agent,
            'browserVersion' => $version,
            'browser' => $browserNames[$browser]
        ]);

    }

    protected function buildConfig($request)
    {
        $config = [];
        if (env('GOOGLE_ANALYTICS_ID')) $config['googleAnalyticsID'] = env("GOOGLE_ANALYTICS_ID");
        $config['canonicalUrl'] = env('APP_URL') ? rtrim(env('APP_URL'), '/') . '/' : null;;
        if ($config['canonicalUrl']) $config['apiUrl'] = $config['canonicalUrl'] . 'api/';
        if (env('APP_SHORTURL')) $config['shortUrlBase'] = rtrim(env('APP_SHORTURL'), '/') . '/';
        $config['photo'] = [
            'upload' => [
                'validMaxKb' => Photo::newEntryMaxKbLimit(),
                'awaitSecs' => (int)env('AWS_S3_BUCKET_PHOTO_COMPRESSION_AWAIT_COMPLETED_SECONDS', 60)
            ]
        ];
        if (Debugger::canUserDebug($request, $request->user())) $config['debugMode'] = true;
        return $config;
    }
}