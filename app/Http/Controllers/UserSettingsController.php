<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfilePhotoUploadRequest;
use App\OauthAccessTokens;
use App\Services\Debugger;
use App\Traits\HelpsFileUploads;
use App\UserProfilePhoto;
use App\UserSetting;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserSettingsController extends Controller
{
    use HelpsFileUploads;

    private $csvImportLocalPhoto;

    /**
     * UserSettingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['uploadPhoto']);
    }

    /**
     * Used only when Migrating old Albums on the CSV Import Tool.
     * @param UploadedFile $photo
     */
    public function setCsvPhotoLocalFile(UploadedFile $photo)
    {
        $this->csvImportLocalPhoto = $photo;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request, UserSetting $setting)
    {
        $validation = Validator::make($request->all(), [
            'tokenId' => 'required|string|exists:oauth_access_tokens,id',
            'settings' => 'required|array',
            'settings.*' => 'required'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $token = OauthAccessTokens::whereId($request->tokenId)
            ->whereName('emailUnsubLink')
            ->first();

        if (!$token) {
            return response()->json([
                'error' => true,
                'not_valid' => true,
                'message' => "Something is wrong with your input " . '$tokenId' . ".\nPlease try another one.\nThanks!"
            ], 400);
        }

        if ($token->revoked) {
            return response()->json([
                'error' => true,
                'revoked_expired' => true,
                'message' => 'Something went wrong while trying to use this $tokenId, maybe a newer email link would work better?'
            ], 400);
        }

        $settings = $token->user->settings;

        $nonEditableCols = UserSetting::getNotEditableColumns();
        $fillableColsOnly = $settings->getFillable();
        $editableCols = array_diff($fillableColsOnly, $nonEditableCols);

        $newSettings = [];
        foreach ($request->settings as $key => $setting) {
            if (in_array($key, $editableCols)) {
                $newSettings[$key] = $setting;
            }
        }

        $settings->fill($newSettings)->save();

        return ['success' => true];
    }

    /**
     * @param ProfilePhotoUploadRequest $request
     * @param UserProfilePhoto $photoModel
     * @return JsonResponse
     */
    public function uploadAvatar(ProfilePhotoUploadRequest $request, UserProfilePhoto $photoModel): JsonResponse
    {
        return $this->uploadPhoto($request, $photoModel, UserProfilePhoto::TYPE_AVATAR);
    }

    /**
     * @param ProfilePhotoUploadRequest $request
     * @param UserProfilePhoto $photoModel
     * @return JsonResponse
     */
    public function uploadBackground(ProfilePhotoUploadRequest $request, UserProfilePhoto $photoModel): JsonResponse
    {
        return $this->uploadPhoto($request, $photoModel, UserProfilePhoto::TYPE_BACKGROUND);
    }

    /**
     * @param ProfilePhotoUploadRequest $request
     * @param UserProfilePhoto          $photoModel
     * @param string                    $photoType
     * @return JsonResponse
     */
    private function uploadPhoto(ProfilePhotoUploadRequest $request, UserProfilePhoto $photoModel, string $photoType): JsonResponse
    {
        $user = $request->user();

        Debugger::log('PhotoController > store()');
        Debugger::measureTimeSince('PhotoController::store() method started');
        Debugger::startMeasuringTime('validating uploaded photo');

        $uploadedFilePath = $request->photo ? $request->photo->getPathname() : '';

        Debugger::stopMeasuringTime('validating uploaded photo');
        Debugger::startMeasuringTime('PhotoController --> idle --> just waiting for lambda & s3 to finish');

        $photoFile = $this->csvImportLocalPhoto ?: $request->photo;

        $randomToken = '--rand-' . rand();
        $photoFilePaths = $this->generateUserProfilePhotoPaths($user, $photoFile, $photoType);
        $filePathAndFilenameOnly = $photoFilePaths['relativeDir'] . '/' . $photoFilePaths['filenameOnly'] . $randomToken;

        $uploadedToS3 = $this->pushPhotoToCompressionQueueAndWaitOnS3($photoFile, $filePathAndFilenameOnly);

        Debugger::stopMeasuringTime('PhotoController --> idle --> just waiting for lambda & s3 to finish');

        if (!$uploadedToS3) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'reload_page' => !$this->getDebugMode($request),
                'photo_compression_timed_out' => true,
                'message' => 'Something went wrong while trying to compress your Photo.'
            ], 500);
        }

        Debugger::startMeasuringTime('saving photo to database');

        $relativeFilePath = $filePathAndFilenameOnly . '.jpg';
        $thumbnailFilePath = $photoFilePaths['relativeDir'] . '/thumbnails/' . $photoFilePaths['filenameOnly'] . $randomToken . '.jpg';

        $lambdaPaths = [
            'relativeFilePath' => $relativeFilePath,
            'hiResFilePath' => $this->generateCloudFrontUrl($relativeFilePath),
            'thumbnailFilePath' => $this->generateCloudFrontUrl($thumbnailFilePath)
        ];

        $uploadedFilePath && @unlink($uploadedFilePath);

        $existingPhoto = UserProfilePhoto::where(['user_id' => $user->id, 'type' => $photoType])->first();
        if (null !== $existingPhoto) {
            $this->removeUserProfilePhoto($existingPhoto);
        }

        $photoModel->user_id = $user->id;
        $photoModel->type = $photoType;
        $photoModel->original_res_filename = $lambdaPaths['relativeFilePath'];
        $photoModel->thumbnail_filename = $lambdaPaths['thumbnailFilePath'];
        $photoModel->hi_res_filename = $lambdaPaths['hiResFilePath'];
        $photoModel->uploaded_to_cdn = 1;
        $photoModel->save();

        $photoUploaded = UserProfilePhoto::where(['user_id' => $user->id, 'type' => $photoType])->first();

        Debugger::stopMeasuringTime('saving photo to database');

        return response()->json([
            'success' => true,
            'record' => $photoUploaded,
        ]);
    }
}
