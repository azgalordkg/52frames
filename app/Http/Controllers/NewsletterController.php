<?php

namespace App\Http\Controllers;

use App\Providers\Modified\Spatie\Newsletter\Newsletter;
use App\Services\MailchimpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsletterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @param Newsletter $newsletter
     * @param MailchimpService $mailchimpService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\Newsletter\Exceptions\InvalidNewsletterList
     */
    public function store(Request $request, Newsletter $newsletter, MailchimpService $mailchimpService)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $userInfo = $newsletter->subscribe($request->email);

        if ($userInfo) {
            $mailchimpService->updateUserMergeFields($request->email, [
                'STATUSCOM' => 'FAN'
            ]);
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'already_subscribed' => true
            ], 400);
        }

    }
}
