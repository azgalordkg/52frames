<?php

namespace App\Http\Controllers;

use App\User;
use App\City;
use App\State;
use App\Country;
use App\UserProfile;
use App\ManifestoAnswer;
use App\UserExternalPage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ManipulatesArray;
use App\Services\MailchimpService;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Tools\MigrationTool\Traits\HelpsMigrateCsvRows;

class ManifestoController extends Controller
{
    use ManipulatesArray, HelpsMigrateCsvRows;

    /**
     * ManifestoController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except([
            'validateState',
            'checkNickname',
            'validateInput'
        ]);
    }

    /**
     * Validates the User's State from the input.
     * @param Request $request
     * @return array
     */
    protected function validateState(Request $request)
    {
        $country = Country::find($request->get('country'));

        if ($country && $country->state_member_term && !$this->importingCsvUser) {

            $validation = Validator::make($request->all(), [
                'state' => 'required|integer|exists:states,id',
            ]);

            if ($validation->fails()) {
                return $validation->errors()->toArray();
            }

            $state = State::find($request->get('state'));
            if ($state->country_id != $request->get('country')) {
                return [
                    'state' => [ 'The selected state is invalid.' ]
                ];
            }
        }

        return [];
    }

    /**
     * Checks the Nickname if it exists in the database.
     * @param Request $request
     * @return array|JsonResponse
     */
    public function checkNickname(Request $request)
    {
        if ($request->nickname) {

            $rules = array_merge([
                'user_id' => 'required|exists:users,id'
            ], ManifestoAnswer::nicknameRules());

            $user = User::whereId($request->user_id)->first();

            if ($user && $request->nickname == $user->handle) {
                unset($rules['nickname']);
            }

            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                return response()->json([
                    'validation_errors' => $validation->errors()
                ], 400);
            }
        }

        return ['success' => true];
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function validateInput(Request $request)
    {
        if ($request->manifesto_intro) {
            $rules = ManifestoAnswer::introRules();
        } elseif ($request->manifesto_questions) {
            $rules = ManifestoAnswer::checkboxesRules();
        } elseif ($request->manifesto_why) {
            $rules = ManifestoAnswer::whyRules();
        } else {
            $rules = array_merge(
                ManifestoAnswer::introRules(),
                ManifestoAnswer::checkboxesRules(),
                ManifestoAnswer::whyRules()
            );
        }

        $validation = Validator::make($request->all(), $rules);

        $stateValidation = $this->validateState($request);

        if ($validation->fails() || count($stateValidation)) {
            $validationMessages = $this->deep_array_merge($validation->errors()->toArray(), $stateValidation);

            return response()->json([
                'validation_errors' => $validationMessages
            ], 400);
        }

        $mainfestoAnswer = [
            'commitment_understanding' => $request->commitment_understanding ?: false,
            'being_constant_understanding' => $request->being_constant_understanding ?: false,
            'creative_process_understanding' => $request->creative_process_understanding ?: false,
            'vulnerability_understanding' => $request->vulnerability_understanding ?: false,
            'non_perfection_understanding' => $request->non_perfection_understanding ?: false,
            'understanding_to_release_the_notion_of_perfection' => $request->understanding_to_release_the_notion_of_perfection ?: false,
            'understanding_for_the_sake_of_consistency' => $request->understanding_for_the_sake_of_consistency ?: false,
            'why' => $request->why,
        ];

        $userProfile = [
            'photowalks_level' => $request->photowalks,
            'photography_level' => $request->photography_level,
            'camera_type' => $request->camera_type
        ];

        return [
            'manifestoAnswer'  => $mainfestoAnswer,
            'userProfile'      => $userProfile,
            'city'             => $request->get('city'),
            'state'            => $request->get('state'),
            'country'          => $request->get('country'),
            'twitter_handle'   => $request->get('twitter_handle'),
            'instagram_handle' => $request->get('instagram_handle'),
            'nickname'         => $request->get('nickname'),
            'skipMailchimp'    => $request->get('skipMailchimp'),
            'is_valid'         => true,
        ];
    }

    /**
     * Saves the Manifesto based on the input.
     * @param Request          $request
     * @param MailchimpService $mailchimpService
     * @param User             $user
     * @return array|JsonResponse
     */
    public function store(Request $request, MailchimpService $mailchimpService, User $user = null)
    {
        $manifest = $this->validateInput($request);
        if (!is_array($manifest) || array_has($manifest, 'validation_errors') || false === $manifest['is_valid']) {
            return $manifest;
        }

        if (null === $user->id) {
            /** @var User $user */
            $user = $this->importingCsvUser ?: $request->user();
        }

        $manifestoAnswer = ManifestoAnswer::create(array_merge(
            ['user_id' => $user->id],
            $manifest['manifestoAnswer']
        ));

        $userProfile = UserProfile::create(array_merge(
            ['user_id' => $user->id],
            $manifest['userProfile']
        ));

        $chosenCity = City::whereName($manifest['city'])->whereCountryId($manifest['country'])->first();
        if (!$chosenCity && null !== $manifest['city']) {
            $chosenCity = City::create([
                'country_id' => $manifest['country'],
                'state_id' => $manifest['state'] ?: null,
                'name' => $manifest['city'],
            ]);
        }
        $user->city()->associate($chosenCity)->save();

        if ($manifest['twitter_handle']) {
            UserExternalPage::create([
                'user_id' => $user->id,
                'provider' => 'twitter',
                'handle' => $manifest['twitter_handle'],
                'url' => "https://twitter.com/{$manifest['twitter_handle']}"
            ]);
        }

        if ($manifest['instagram_handle']) {
            UserExternalPage::create([
                'user_id' => $user->id,
                'provider' => 'instagram',
                'handle' => $manifest['instagram_handle'],
                'url' => "https://www.instagram.com/{$manifest['instagram_handle']}/"
            ]);
        }

        if ($manifest['nickname']) {
            $user->update([
                'handle' => $manifest['nickname'],
                'display_as_handle' => $manifest['nickname'] ? !!$manifest['display_as_handle'] : false,
                'dont_share_location' => !!$manifest['dont_share_location'],
                'how_did_hear' => $manifest['how_did_hear'],
            ]);
        }

        if (!$manifest['skipMailchimp']) {
            $mailchimpService->updateUserMergeFields($user->email, [
                'STATUSCOM' => 'MANIFESTO-SIGNED',
            ]);
        }

        return ['success' => true];
    }

    /**
     * Updates the Manifesto of the User.
     * @param integer $id
     * @param Request $request
     * @return array|JsonResponse
     */
    public function update($id, Request $request)
    {
        $photographer = User::buildProfile('id', $id);

        if (!$photographer) {
            return response()->json([
                'user_not_found' => true
            ], 404);
        }

        if (!$photographer['manifesto']) {
            return response()->json([
                'manifesto_not_signed' => true
            ], 403);
        }

        if ($photographer['id'] != $request->user()->id) {
            $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
            if ($maxAdminPoints < 100000) {
                return response()->json([
                    'not_enough_admin_points' => true
                ], 403);
            }
        }

        $signupRules = User::signupRules();
        $rules = array_merge([
                'firstname' => $signupRules['firstname'],
                'lastname' => $signupRules['lastname']
            ],
            ManifestoAnswer::introRules()
        );
        unset($rules['how_did_hear']);

        if (isset($request->nickname) && $request->nickname == $photographer['handle']) {
            unset($rules['nickname']);
        }

        $validation = Validator::make($request->all(), $rules);

        $chosenCity = City::whereName($request->city)->whereCountryId($request->country)->first();
        if (!$chosenCity || $chosenCity->state_id) {
            $stateValidation = $this->validateState($request);
        }

        if ($validation->fails() || (isset($stateValidation) && count($stateValidation))) {
            $validationMessages = $this->deep_array_merge($validation->errors()->toArray(), isset($stateValidation) ? $stateValidation : []);
            return response()->json([
                'validation_errors' => $validationMessages
            ], 400);
        }

        $userModel = User::whereId($photographer['id'])->first();

        $userModel->manifesto()->update([
            'why' => $request->why
        ]);

        $userModel->profile()->update([
            'photowalks_level' => $request->photowalks,
            'photography_level' => $request->photography_level,
            'camera_type' => $request->camera_type,
            'shortbio' => $request->shortbio,
        ]);

        if (!$chosenCity && null !== $request->city) {
            $chosenCity = City::create([
                'country_id' => $request->country,
                'state_id' => $request->state ?: null,
                'name' => $request->city,
            ]);
        }
        $userModel->city()->associate($chosenCity)->save();

        if ($request->twitter_handle) {
            $handle = $request->twitter_handle;
            $provider = 'twitter';
            UserExternalPage::updateOrCreate(
                ['user_id' => $id, 'provider' => $provider],
                ['handle' => $handle, 'url' => "https://{$provider}.com/{$handle}"]
            );
        }

        if ($request->instagram_handle) {
            $handle = $request->instagram_handle;
            $provider = 'instagram';
            UserExternalPage::updateOrCreate(
                ['user_id' => $id, 'provider' => $provider],
                ['handle' => $handle, 'url' => "https://{$provider}.com/{$handle}"]
            );
        }

        $userModel->update([
            'dont_share_location' => !!$request->dont_share_location,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'handle' => $request->nickname,
            'display_as_handle' => $request->nickname ? $request->display_as_handle : false,
            'how_did_hear' => $request->how_did_hear
        ]);

        return [
            'success' => true,
            'nickname_changed' => $userModel->handle != $photographer['handle'],
            'user' => User::buildProfile('id', $userModel->id)
        ];
    }
}
