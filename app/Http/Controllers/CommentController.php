<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Notification;
use App\Photo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except([
            'index'
        ]);
    }

    /**
     * Rules for getting the list of Comments.
     * @return array
     */
    protected function indexRules()
    {
        return [
            'photo_id' => 'required_without_all:reply_to_comment_id,album_id|required_with:reply_to_comment_id|integer|exists:photos,id',
            'reply_to_comment_id' => 'required_without_all:photo_id,album_id|integer|exists:comments,id',
            'album_id' => 'required_without_all:reply_to_comment_id,photo_id|integer|exists:albums,id',
            'previous_of' => 'nullable|integer',
        ];
    }

    /**
     * Rules for add a new comment.
     * @return array
     */
    protected function insertRules()
    {
        $indexRules = $this->indexRules();
        unset($indexRules['previous_of']);
        return array_merge($indexRules, [
            'message' => 'required|string'
        ]);
    }

    /**
     * Rules for editing an existing comment.
     * @return array
     */
    protected function updateRules()
    {
        $insertRules = $this->insertRules();
        return [
            'message' => $insertRules['message']
        ];
    }

    /**
     * Adds dynamic flags, computed from the data based on the comment.
     * @param Comment $comment
     * @param User|null $user
     * @param int $maxAdminPoints
     */
    protected function addCommentFlags(Comment $comment, User $user = null, int $maxAdminPoints = 0)
    {
        $comment->edited = $comment->created_at != $comment->updated_at;
        $comment->editable = $user && $user->id == $comment->user_id;
        $comment->deletable = $user && ($user->id == $comment->user_id || $maxAdminPoints >= 100000);
        $comment->last_changed = Carbon::parse($comment->updated_at)->toW3cString();
        $comment->replies_count = Comment::whereReplyToCommentId($comment->id)->count();
        if ($comment->owner) $comment->owner->name = $comment->owner->name;
    }

    /**
     * Converts the Plain Text comment and formats it in HTML.
     * @param string $message
     * @return mixed
     */
    protected function formatCommentToHtml(string $message)
    {
        return str_replace("\n", "<br>", htmlentities(trim($message)));
    }

    /**
     * Gets the list of comments.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), $this->indexRules());
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        date_default_timezone_set('UTC');

        $user = Auth::guard('api')->user();
        $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

        $commentsQuery = Comment::with('owner.signedManifesto');
        $replyToCommentId = @$request->reply_to_comment_id ?: null;
        $commentsQuery->whereReplyToCommentId($replyToCommentId);
        if (@$request->album_id) $commentsQuery->whereAlbumId($request->album_id);
        else $commentsQuery->wherePhotoId($request->photo_id);
        $totalCount = $commentsQuery->count();

        $previousCount = $totalCount;
        $paginationCount = env('APP_COMMENTS_COUNT_INIT_LOAD');
        $defaultPaginationCount = env('APP_COMMENTS_PER_PAGE');
        if ($request->previous_of) {
            $paginationCount = $defaultPaginationCount;
            $commentsQuery->where('id', '<', $request->previous_of);
            $previousCount = $commentsQuery->count();
        }

        $result = $commentsQuery->orderBy('created_at', 'desc')->take($paginationCount)->get();

        foreach ($result as $row) {
            $this->addCommentFlags($row, $user, $maxAdminPoints);
        }

        $currentPageCount = $result->count();
        $countDiff = $previousCount - $currentPageCount;
        $remainingCount = $countDiff < 0 ? 0 : $countDiff;

        return [
            'total' => $totalCount,
            'can_load_previous' => $remainingCount > $defaultPaginationCount ? $defaultPaginationCount : $remainingCount,
            'previous_count' => $remainingCount,
            'comments' => array_reverse($result->toArray())
        ];
    }

    /**
     * Updates the Number of Comments of the input $photoId.
     * @param int $photoId
     * @return int
     */
    protected function updatePhotoNumComments(int $photoId = 0)
    {
        $photoNumComments = 0;
        $photo = Photo::whereId($photoId)->first();
        if ($photo) {
            $photoNumComments = Comment::wherePhotoId($photoId)->count();
            $photo->num_comments = $photoNumComments;
            $photo->save();
        }
        return $photoNumComments;
    }

    /**
     * Saves a new comment into the database.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), $this->insertRules());
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request->all(), [
            'message'
        ]);
        if ($validation->fails() || count($unsafeHtmlTags)) {
            $errors = array_merge($validation->errors()->toArray(), $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $loggedInUserId = $request->user()->id;

        $albumId = @$request->album_id ?: null;
        $photoId = @$request->photo_id ?: null;
        $replyToCommentId = @$request->reply_to_comment_id ?: null;
        $previousResponders = $replyToCommentId ? Comment::whereReplyToCommentId($replyToCommentId)->get()->toArray() : [];

        $comment = Comment::create([
            'user_id' => $loggedInUserId,
            'photo_id' => $photoId,
            'album_id' => $albumId,
            'reply_to_comment_id' => $replyToCommentId,
            'message' => $this->formatCommentToHtml($request->message)
        ]);

        if ($photoId) {
            Notification::saveAction(
                env('NOTIF_HOOK_PHOTO_COMMENT'),
                $comment->photo->user_id,
                $comment->photo,
                $comment
            );
        }
        if ($replyToCommentId) {
            $allResponders = array_unique(
                array_merge(
                    [$comment->parentComment->user_id],
                    array_column($previousResponders, 'user_id'))
            );
            foreach ($allResponders as $responderId) {
                Notification::saveAction(
                    env('NOTIF_HOOK_COMMENT_REPLY'),
                    $responderId,
                    $comment->parentComment,
                    $comment,
                    $comment->parentComment->photo
                );
            }
        }

        $formatted = Comment::whereId($comment->id)->with('owner.signedManifesto')->first();
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        $this->addCommentFlags($formatted, $request->user(), $maxAdminPoints);

        $photoNumComments = $this->updatePhotoNumComments($photoId);

        $commentsCount = $replyToCommentId ? Comment::whereReplyToCommentId($replyToCommentId)->count() : 1;

        return [
            'success' => true,
            'comment' => $formatted,
            'photo_num_comments' => $photoNumComments,
            'comments_level_total' => $commentsCount
        ];
    }

    /**
     * Updates the Comment in the database.
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validation = Validator::make($request->all(), $this->updateRules());
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request->all(), [
            'message'
        ]);
        if ($validation->fails() || count($unsafeHtmlTags)) {
            $errors = array_merge($validation->errors()->toArray(), $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $comment = Comment::whereId($id)->first();
        if (!$comment) {
            return response()->json([
                'not_found' => true
            ], 404);
        }

        if ($comment->user_id != $request->user()->id) {
            return response()->json([
                'not_the_same_user' => true
            ], 403);
        }

        $comment->message = $this->formatCommentToHtml($request->message);
        $comment->save();

        $formatted = Comment::whereId($id)->with('owner.signedManifesto')->first();
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        $this->addCommentFlags($formatted, $request->user(), $maxAdminPoints);

        return [
            'success' => true,
            'comment' => $formatted
        ];
    }

    /**
     * Deletes an existing comment.
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id, Request $request)
    {
        $comment = Comment::whereId($id)->first();
        if (!$comment) {
            return response()->json([
                'not_found' => true
            ], 404);
        }

        $loggedInUser = $request->user();
        $maxAdminPoints = User::getMaxAdminPoints($loggedInUser->id);

        if ($comment->user_id != $loggedInUser->id && $maxAdminPoints < 100000) {
            return response()->json([
                'not_the_same_user' => true
            ], 403);
        }

        $replyToCommentId = $comment->reply_to_comment_id;

        $photoId = $comment->photo_id;

        $comment->delete();

        $photoNumComments = $this->updatePhotoNumComments($photoId);

        $commentsCount = $replyToCommentId ? Comment::whereReplyToCommentId($replyToCommentId)->count() : 0;

        return [
            'success' => true,
            'photo_num_comments' => $photoNumComments,
            'comments_level_total' => $commentsCount
        ];
    }
}