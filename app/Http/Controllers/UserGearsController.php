<?php declare(strict_types = 1);

namespace App\Http\Controllers;

use App\User;
use App\UserGear;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserGearsController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'type' => 'required|string',
            'value' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        /** @var User $user */
        $user = $request->user();
        $gear = UserGear::create([
            'user_id' => $user->id,
            'type' => $request->type,
            'value' => $request->value,
        ]);

        return [
            'success' => true,
            'gear_id' => $gear->id
        ];
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function update(int $id, Request $request)
    {
        $gear = UserGear::whereId($id)->first();
        if (null === $gear) {
            return response()->json([
                'gear_not_found' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $user = $request->user();
        if ($user->id !== $gear->user_id || User::getMaxAdminPoints($user->id) > 10000) {
            return response()->json([
                'cant_edit_gear' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $validation = Validator::make($request->all(), [
            'type' => 'required|string',
            'value' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $gear->update([
            'type'  => $request->type,
            'value' => $request->value,
        ]);

        return [
            'success' => true,
            'gear_id' => $gear->id,
        ];
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function destroy(int $id, Request $request)
    {
        $gear = UserGear::whereId($id)->first();
        if (null === $gear) {
            return response()->json([
                'gear_not_found' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $user = $request->user();
        if ($user->id !== $gear->user_id || User::getMaxAdminPoints($user->id) > 100000) {
            return response()->json([
                'cant_edit_gear' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $gear->delete();

        return [ 'success' => true ];
    }
}
