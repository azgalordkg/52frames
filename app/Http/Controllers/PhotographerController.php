<?php

namespace App\Http\Controllers;

use App\Album;
use App\Photo;
use App\Traits\HelpsPhoto;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

class PhotographerController extends Controller
{
    use HelpsPhoto;

    /**
     * Gets all the data related to the Photographer.
     * @param $idOrUserHandle
     * @param bool $notFromFrontend
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function show($idOrUserHandle, bool $notFromFrontend = false)
    {
        $userWithId = User::whereId($idOrUserHandle)->first();

        if ($userWithId) {
            if ($userWithId->handle) {
                return response()->json([
                    'user_has_handle' => true,
                    'user' => User::accountSummary($userWithId)
                ], 400);
            } else {
                $user = User::buildProfile('id', $idOrUserHandle);
                if ($user) {
                    if ($notFromFrontend) return $user;
                    else {
                        $photographerController2 = new PhotographerController();
                        $photographerRequest = new Request([]);
                        return array_merge($user, [
                            'photosData' => $photographerController2->getPhotos($idOrUserHandle, $photographerRequest)
                        ]);
                    }
                }
            }
        } else {
            $user = User::buildProfile('handle', $idOrUserHandle);
            if ($user) {
                if ($notFromFrontend) return $user;
                else {
                    $photographerController2 = new PhotographerController();
                    $photographerRequest = new Request([]);
                    return array_merge($user, [
                        'photosData' => $photographerController2->getPhotos($idOrUserHandle, $photographerRequest)
                    ]);
                }
            }
        }

        return response()->json([
            'user_not_found' => true
        ], 404);
    }

    /**
     * Gets the list of Photos of this Photographer.
     * @param $idOrUserHandle
     * @param Request $request
     * @param boolean $showLoves
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function getPhotos($idOrUserHandle, Request $request, $showLoves = false)
    {
        $photographer = $this->show($idOrUserHandle, true);

        if (!is_array($photographer)) return $photographer;

        $validation = Validator::make($request->all(), [
            'per_page' => 'nullable|integer'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()->toArray()
            ], 400);
        }

        $loggedInUser = Auth::guard('api')->user();
        $maxAdminPoints = $loggedInUser ? User::getMaxAdminPoints($loggedInUser->id) : 0;

        $isSelfAccount = User::isSelfAccount($photographer['id']);

        if (true === $showLoves) {
            $user = User::find($photographer['id']);
            $lovesPhotosId = [];
            foreach ($user->loves()->get() as $love) {
                $lovesPhotosId[] = $love->photo_id;
            }
            $photosQuery = Photo::whereIn('photos.id', $lovesPhotosId)
                ->join('albums', 'albums.id', '=', 'photos.album_id')
                ->orderBy('albums.year', 'desc')
                ->orderBy('albums.week_number', 'desc')
                ->select('photos.*')
                ->with('album');
        } else {
            $photosQuery = Photo::whereUserId($photographer['id'])
                ->join('albums', 'albums.id', '=', 'photos.album_id')
                ->orderBy('albums.year', 'desc')
                ->orderBy('albums.week_number', 'desc')
                ->select('photos.*')
                ->with('album');
        }

        Photo::addExifData($photosQuery);

        if (!$isSelfAccount && $maxAdminPoints < 100000) $photosQuery->where('albums.live', true);

        $result = $this->paginate($request, $photosQuery, env('APP_PAGINATION_PER_PAGE'))->toArray();

        array_walk($result['data'], function (&$photo) use ($loggedInUser, $maxAdminPoints) {
            $album = Album::whereId($photo['album_id'])->first();
            $photo = $this->addPhotoFlags($photo, null, $album, $loggedInUser, $maxAdminPoints);
        });

        return $result;
    }

    /**
     * Gets the list of Photos that this Photographer loves.
     * @param User    $user
     * @param Request $request
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function getLoves(User $user, Request $request)
    {
        $idOrUserHandle = $user->handle ?? $user->id;
        return $this->getPhotos($idOrUserHandle, $request, true);
    }

    /**
     * Get the list of Followers of this Photographer.
     * @param User    $user
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowers($user, Request $request): JsonResponse
    {
        $followerUsersId = [];
        foreach ($user->followers()->get() as $follower) {
            $followerUsersId[] = $follower->follower_user_id;
        }

        $result = $this->paginate($request, User::whereIn('id', $followerUsersId), env('APP_PAGINATION_PER_PAGE'))->toArray();

        return response()->json($result);
    }

    /**
     * Get the list of Followers of this Photographer.
     * @param User    $user
     * @param Request $request
     * @return JsonResponse
     */
    public function getFollowing(User $user, Request $request): JsonResponse
    {
        $followingUsersId = [];
        foreach ($user->following()->get() as $following) {
            $followingUsersId[] = $following->following_user_id;
        }

        $result = $this->paginate($request, User::whereIn('id', $followingUsersId), env('APP_PAGINATION_PER_PAGE'))->toArray();

        return response()->json($result);
    }
}
