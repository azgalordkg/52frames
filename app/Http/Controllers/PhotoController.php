<?php

namespace App\Http\Controllers;

use App\Handlers\Photo\Filters\FilterExtraCredit;
use App\Handlers\Photo\Filters\FilterFollowing;
use App\Handlers\Photo\Filters\FilterMyPhoto;
use App\Handlers\Photo\Filters\FilterNewbies;
use App\Handlers\Photo\Filters\FilterShredAway;
use App\Handlers\Photo\Filters\FilterStaffPicks;
use App\Handlers\Photo\Search\SearchByCountry;
use App\Handlers\Photo\Search\SearchByLevel;
use App\Handlers\Photo\Search\SearchByTags;
use App\Handlers\Photo\Search\SearchByUsername;
use App\Handlers\Photo\Sorts\SortByNumber;
use App\Handlers\Photo\Sorts\SortRandom;
use App\Http\Requests\PhotoIndexRequest;
use App\Http\Requests\PhotoShowRequest;
use App\User;
use App\Love;
use App\Photo;
use App\Album;
use App\PhotoExif;
use App\Notification;
use App\Services\Helpers;
use App\BypassAlbumUpload;
use App\Services\Debugger;
use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\HelpsFileUploads;
use Illuminate\Http\UploadedFile;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Jobs\UpdateMailchimpPhotoUploadStatus;

class PhotoController extends Controller
{
    use HelpsFileUploads;

    /**
     * Used as a holder for the "alternative" User account, which will be used as the "uploader" of a Photo.
     * @var
     */
    protected $specialUploader;
    protected $csvImportLocalPhoto;

    /**
     * PhotoController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except([
            'index',
            'show',
            'compressionFeedback',
        ]);
    }

    /**
     * Gets the list of Photos.
     * @param PhotoIndexRequest $request
     * @return JsonResponse
     */
    public function index(PhotoIndexRequest $request)
    {
        $album = isset($request->album_id) ? Album::find($request->album_id) : null;
        if (null !== $album) {
            $photosQuery = Photo::whereAlbumId($request->album_id);
            $dontInclude = Album::getPhotoIdsNotForDisplay($album);
            $photosQuery->whereNotIn('photos.id', $dontInclude);
        } else {
            $coversAndSamplesIds = $this->getAllCoversAndSamplesIds();
            $photosQuery = Photo::whereNotIn('photos.id', $coversAndSamplesIds);
        }

        Photo::addExifData($photosQuery);

        $user = Auth::guard('api')->user();
        $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

        if ($maxAdminPoints < 100000) {
            $photosQuery->join('albums', 'albums.id', '=', 'photos.album_id')
                ->where('albums.live', true);
        }
        $photosQuery->select('photos.*');

        $pipeline = new Pipeline(new Container());
        $photosQuery = $pipeline->send($request)
            ->through($this->photoHandlers())
            ->then(function () use ($photosQuery) {
                return $photosQuery;
            });

        $result = $this->paginate($request, $photosQuery, env('APP_PAGINATION_PER_PAGE'))->toArray();

        array_walk($result['data'], function (&$photo) use ($album, $user, $maxAdminPoints) {
            $photo = $this->addPhotoFlags($photo, null, $album, $user, $maxAdminPoints);
        });

        return $result;
    }

    protected function getNextAndPreviousPhotos(int $photographerId, int $albumId, bool $isGlobalPage = false, bool $isProfilePage = false, bool $isLovesPage = false, ?int $photoId = null, $filterBy = null, $searchBy = null, $searchQuery = null, $sortBy = null, $sortDirection = null)
    {
        $previousPhoto = null;
        $nextPhoto = null;
        $pageNum = 0;
        $albumPhotos = [];
        $photoIndex = false;
        $photographer = ($isProfilePage || $isLovesPage) ? User::whereId($photographerId)->first() : null;
        while (true) {
            $pageNum++;
            if ($isProfilePage || $isLovesPage) {
                $photographerController = new PhotographerController();
                $photographerRequest = new Request([
                    'page' => $pageNum
                ]);
                $idOrUserHandle = $photographer && $photographer->handle ? $photographer->handle : $photographerId;
                $photosResponse = $photographerController->getPhotos($idOrUserHandle, $photographerRequest, $isLovesPage);
            } else {
                $photoController = new PhotoController();
                $photoRequest = new PhotoIndexRequest([
                    'is_global_page' => $isGlobalPage,
                    'album_id'     => $isGlobalPage ? null : $albumId,
                    'filter_by'    => $filterBy,
                    'search_by'    => $searchBy,
                    'search_query' => $searchQuery,
                    'sort_by'      => $sortBy,
                    'sort_dir'     => $sortDirection,
                    'page'         => $pageNum,
                ]);
                $photosResponse = $photoController->index($photoRequest);
            }
            if (!is_array($photosResponse) || !count($photosResponse['data'])) break;
            else {
                $albumPhotos = array_merge($albumPhotos, $photosResponse['data']);
                $pageColumnDiff = [
                    [
                        'columnName' => 'user_id',
                        'findValue' => $photographerId
                    ],
                    [
                        'columnName' => 'album_id',
                        'findValue' => $albumId
                    ],
                    [
                        'columnName' => 'id',
                        'findValue' => $photoId
                    ],
                ];
                if ($isLovesPage || $isGlobalPage) {
                    $whichPageData = $pageColumnDiff[2];
                } else {
                    $whichPageData = $pageColumnDiff[(int) $isProfilePage];
                }
                $ids = array_column($albumPhotos, $whichPageData['columnName']);
                if (($photoIndex = array_search($whichPageData['findValue'], $ids)) !== false) {
                    if ($photoIndex + 1 == count($albumPhotos) && $photosResponse['next_page_url']) continue;
                    else if ($pageNum != $photosResponse['last_page']) {
                        $pageNum = $photosResponse['last_page'] - 1;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        }
        if ($photoIndex !== false && count($albumPhotos) > 1) {
            $previousPhoto = $photoIndex === 0 ? $albumPhotos[count($albumPhotos) - 1] : $albumPhotos[$photoIndex - 1];
            $nextPhoto = $photoIndex === count($albumPhotos) - 1 ? $albumPhotos[0] : $albumPhotos[$photoIndex + 1];
        }
        return [
            'count' => count($albumPhotos),
            'index' => $photoIndex + 1,
            'previousPhoto' => $previousPhoto,
            'nextPhoto' => $nextPhoto
        ];
    }

    /**
     * Gets 1 Photo and its details.
     * @param $idOrUserHandle
     * @param PhotoShowRequest $request
     * @return array|bool|JsonResponse
     */
    public function show($idOrUserHandle, PhotoShowRequest $request)
    {
        $loggedInUser = Auth::guard('api')->user();
        $maxAdminPoints = $loggedInUser ? User::getMaxAdminPoints($loggedInUser->id) : 0;

        $isCustomView = $request->is_profile_page || $request->is_loves_page || $request->is_global_page;
        if (false === $isCustomView) {
            $albumController = new AlbumController(true);
            $albumData = $albumController->show($request->week_path, $request);
            if (!is_array($albumData)) {
                return $albumData;
            }
        }

        $photographerController = new PhotographerController();
        $photographerData = $photographerController->show($idOrUserHandle);
        if (!is_array($photographerData) && !@$photographerData->original['user']) return $photographerData;

        $photographerId = is_array($photographerData) ? $photographerData['id'] : $photographerData->original['user']['id'];

        if ($request->is_global_page) {
            $coversAndSamplesIds = $this->getAllCoversAndSamplesIds();
            $photoQuery = Photo::whereNotIn('photos.id', $coversAndSamplesIds)->where('photos.id', $request->photo_id)->with('album');
        } elseif (!$request->is_profile_page && !$request->is_loves_page) {
            $photoQuery = Photo::whereUserId($photographerId)->whereAlbumId($albumData['id']);
        } elseif ($request->is_profile_page) {
            $photoQuery = Photo::whereUserId($photographerId)->where('photos.id', $request->photo_id)->with('album');
        } elseif ($request->is_loves_page) {
            $user = User::find($photographerId);
            $lovesPhotosId = [];
            foreach ($user->loves()->get() as $love) {
                $lovesPhotosId[] = $love->photo_id;
            }
            $photoQuery = Photo::whereIn('photos.id', $lovesPhotosId)->where('photos.id', $request->photo_id)->with('album');
        }
        Photo::addExifData($photoQuery)->with([
            'owner.city.state',
            'owner.city.country',
            'owner.signedManifesto',
        ]);

        $photo = $photoQuery->first();
        if (!$photo) {
            return response()->json([
                'photo_not_found' => true,
            ], 400);
        }

        $album = $photo->album;
        if (!$album->live && $maxAdminPoints < 100000 && (!$loggedInUser || $loggedInUser->id != $photo->user_id)) {
            return response()->json([
                'show_challenge_page' => true,
                'not_live' => (bool) !$album->live,
            ], 302);
        }

        $photoArrayWithFlags = $this->addPhotoFlags($photo, null, $album, $loggedInUser, $maxAdminPoints);

        $previousAndNextPhotos = $this->getNextAndPreviousPhotos(
            $photographerId,
            $album->id,
            (bool)$request->is_global_page,
            (bool)$request->is_profile_page,
            (bool)$request->is_loves_page,
            (int) $request->photo_id,
            $request->filter_by,
            $request->search_by,
            $request->search_query,
            $request->sort_by === SortRandom::SORT_NEW_RANDOM ? null : $request->sort_by,
            $request->sort_dir
        );

        $commentController = new CommentController();
        $commentRequest = new Request(['photo_id' => $photo->id]);
        $commentsResponse = $commentController->index($commentRequest);
        if (is_array($commentsResponse)) {
            $commentsResponse['can_view_comments'] = $this->canUserComment($album, $maxAdminPoints);
        }

        $profileOwner = $isCustomView ? ['profile_owner_id' => $photographerId] : [];

        return array_merge_recursive($photoArrayWithFlags, $previousAndNextPhotos, $profileOwner, [
            'owner' => User::amIFollowing($photographerId),
            'commentsData' => $commentsResponse,
        ]);
    }

    protected function canUserComment(Album $album, int $maxAdminPoints)
    {
        return $album->live || $maxAdminPoints >= 100000;
    }

    /**
     * Changes the "uploader" to the input User.
     * @param User $user
     */
    public function setUploader(User $user)
    {
        $this->specialUploader = $user;
    }

    /**
     * Used only when Migrating old Albums on the CSV Import Tool.
     * @param UploadedFile $photo
     */
    public function setCsvPhotoLocalFile(UploadedFile $photo)
    {
        $this->csvImportLocalPhoto = $photo;
    }

    /**
     * Saves the new Photo based on the input.
     * @param Request $request
     * @param Photo $photoModel
     * @param Photo|null $existingPhotoFromUpdateMethod
     * @return array
     */
    public function store(Request $request, Photo $photoModel, Photo $existingPhotoFromUpdateMethod = null)
    {
        Debugger::log('PhotoController > store()');
        Debugger::measureTimeSince('PhotoController::store() method started');
        Debugger::startMeasuringTime('validating uploaded photo');

        $uploadedFilePath = $request->photo ? $request->photo->getPathname() : '';

        $loggedInUser = $this->specialUploader ?: $request->user();
        $maxAdminPoints = $loggedInUser ? User::getMaxAdminPoints($loggedInUser->id) : 0;

        $photographerId = $existingPhotoFromUpdateMethod ? $existingPhotoFromUpdateMethod->user_id : $loggedInUser->id;
        $photographer = User::whereId($photographerId)->first();

        $accountSummary = User::accountSummary($photographer);
        if (!$this->specialUploader && !$accountSummary['signed_manifesto']) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'manifesto_not_signed' => true
            ], 403);
        }

        $inputs = $this->forceBooleanStringsToInteger($request->all());
        $inputs = $this->forceNullStringsToNull($inputs);

        $insertRules = Photo::insertRules();
        if ($this->csvImportLocalPhoto) {
            unset($insertRules['photo']);
        }
        if (!@$inputs['has_nudity']) {
            unset($insertRules['model_consent']);
            $inputs['model_consent'] = 0;
        }
        $previousPhotosCount = Photo::howManyPhotosUploadedBefore($photographer);
        if ($previousPhotosCount > 0) unset($insertRules['ownership_confirmation']);
        $validation = Validator::make($inputs, $insertRules, Photo::insertRulesErrorMessages());
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request->all(), [
            'caption'
        ]);
        if ($validation->fails() || count($unsafeHtmlTags)) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            $errors = array_merge($validation->errors()->toArray(), $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $photoExifErrors = Photo::validateManualExifInputs($inputs);
        if (count($photoExifErrors)) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'validation_errors' => $photoExifErrors
            ], 400);
        }

        $album = Album::whereId($request->album_id)->first();
        if (!$this->specialUploader && !$existingPhotoFromUpdateMethod && !$this->canUserUpload($album, $photographer)) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'album_not_allowing_uploads' => true,
                'message' => 'This Album is not accepting Uploads'
            ], 400);
        }

        if ($existingPhotoFromUpdateMethod) $this->removeFromEverywhereExistingPhoto($existingPhotoFromUpdateMethod);

        $previousUpload = !!$this->hasUserUploadedToAlbum($album->id, $photographer->id);
        if ($previousUpload) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'user_already_uploaded' => true,
                'message' => 'You already uploaded a Photo!'
            ], 400);
        }

        Debugger::stopMeasuringTime('validating uploaded photo');
        Debugger::startMeasuringTime('PhotoController --> idle --> just waiting for lambda & s3 to finish');

        $photoFile = $this->csvImportLocalPhoto ?: $request->photo;

        $photoFilePaths = $this->generatePhotoPaths($album, $photographer, $photoFile);
        $filePathAndFilenameOnly = $photoFilePaths['relativeDir'] . '/' . $photoFilePaths['filenameOnly'];

        $uploadedToS3 = $this->pushPhotoToCompressionQueueAndWaitOnS3($photoFile, $filePathAndFilenameOnly);

        Debugger::stopMeasuringTime('PhotoController --> idle --> just waiting for lambda & s3 to finish');

        if (!$uploadedToS3) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'reload_page' => !$this->getDebugMode($request),
                'photo_compression_timed_out' => true,
                'message' => 'Something went wrong while trying to compress your Photo.'
            ], 500);
        }

        Debugger::startMeasuringTime('saving photo to database');

        $relativeFilePath = $filePathAndFilenameOnly . '.jpg';
        $thumbnailFilePath = $photoFilePaths['relativeDir'] . '/thumbnails/' . $photoFilePaths['filenameOnly'] . '.jpg';

        $lambdaPaths = [
            'relativeFilePath' => $relativeFilePath,
            'hiResFilePath' => $this->generateCloudFrontUrl($relativeFilePath),
            'thumbnailFilePath' => $this->generateCloudFrontUrl($thumbnailFilePath)
        ];

        $uploadedFilePath && @unlink($uploadedFilePath);
        $isFirstUpload = User::isFirstPhotoUpload($photographer->id);

        $photoModel->fill($inputs);
        $photoModel->user_id = $photographer->id;
        $photoModel->album_id = $album->id;
        $photoModel->original_res_filename = $lambdaPaths['relativeFilePath'];
        $photoModel->thumbnail_filename = $lambdaPaths['thumbnailFilePath'];
        $photoModel->hi_res_filename = $lambdaPaths['hiResFilePath'];
        $photoModel->uploaded_to_cdn = 1;
        $photoModel->ownership_confirmation = $previousPhotosCount > 0 ? 1 : $inputs['ownership_confirmation'];
        $photoModel->tags = implode(',', array_unique(@$inputs['tags'] ?: []));
        $photoModel->save();

        $exifAnswers = [];
        if (@$inputs['about_photo'] && @$inputs['about_photo_answers']) {
            $exifAnswers += $inputs['about_photo_answers'];
        }
        if (@$inputs['camera_settings'] && @$inputs['camera_settings_answers']) {
            $exifAnswers += $inputs['camera_settings_answers'];
        }
        foreach ($exifAnswers as $exifItemId => $exifValue) {
            PhotoExif::create([
                'photo_id' => $photoModel->id,
                'exif_item_id' => $exifItemId,
                'value' => $exifValue,
            ]);
        }

        $this->dispatch(new UpdateMailchimpPhotoUploadStatus($photographer, $isFirstUpload));

        if (!$existingPhotoFromUpdateMethod) BypassAlbumUpload::whereUserId($photographer->id)->whereAlbumId($request->album_id)->delete();

        $photoUploaded = $this->hasUserUploadedToAlbum($request->album_id, $photographer->id);
        if ($photoUploaded) $photoUploaded = $this->addPhotoFlags($photoUploaded, $existingPhotoFromUpdateMethod, $album, $loggedInUser, $maxAdminPoints);

        Debugger::stopMeasuringTime('saving photo to database');

        return [
            'success' => true,
            'record' => $photoUploaded
        ];
    }

    public function compressionFeedback(Request $request)
    {
        $startTime = microtime(1);

        $filePathTerm = env('AWS_LAMBDA_DROPBOX_SUCCESS_API_NOTIFY_FILEPATH_TERM');
        $photoFilePath = $request->get($filePathTerm);

        $validation = Validator::make($request->all(), [
            $filePathTerm => 'required|string|exists:photos,original_res_filename'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $photo = Photo::whereOriginalResFilename($photoFilePath)->first();
        $customColumns = Helpers::filterByKeysArray($photo->toArray(), ['id', 'user_id', 'album_id']);

        if ($photo->uploaded_to_dropbox) return response()->json([
            'already_reported' => true,
            'duration' => microtime(1) - $startTime,
            'photo' => $customColumns
        ], 208);

        $photo->uploaded_to_dropbox = 1;
        $photo->save();

        return [
            'success' => true,
            'duration' => microtime(1) - $startTime,
            'photo' => $customColumns
        ];
    }

    /**
     * Updates an existing Photo based on the input.
     * @param $id
     * @param Request $request
     * @param Photo $photoModel
     * @return array
     */
    public function update($id, Request $request, Photo $photoModel)
    {
        Debugger::log('PhotoController > update()');
        Debugger::measureTimeSince('PhotoController::update() method started');

        $uploadedFilePath = $request->photo ? $request->photo->getPathname() : '';

        $existingPhoto = Photo::whereId($id)->first();
        if (!$existingPhoto) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'doesnt_exist' => true
            ], 404);
        }

        $photographer = User::whereId($existingPhoto->user_id)->first();
        if (!$photographer) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            return response()->json([
                'photographer_not_found' => true
            ], 403);
        }

        $loggedInUser = $this->specialUploader ?: $request->user();
        $maxAdminPoints = $loggedInUser ? User::getMaxAdminPoints($loggedInUser->id) : 0;

        $album = Album::whereId($existingPhoto->album_id)->first();

        $canEditPhotoMeta = $this->canUserEditPhotoMeta($album, $existingPhoto, $loggedInUser, $maxAdminPoints);
        if (!$canEditPhotoMeta) {
            $uploadedFilePath && @unlink($uploadedFilePath);
            $errorCode = 'cant_edit_photo_meta';
            return response()->json([
                $errorCode => true,
                'message' => "Server responded with an error code: $errorCode"
            ], 403);
        }

        $canReplacePhoto = $this->canUserReplacePhoto($album, $existingPhoto, $loggedInUser, $maxAdminPoints);
        if ($uploadedFilePath && !$canReplacePhoto) {
            @unlink($uploadedFilePath);
            $errorCode = 'cant_replace_photo';
            return response()->json([
                $errorCode => true,
                'message' => "Server responded with an error code: $errorCode"
            ], 403);
        }

        if ($request->photo) return $this->store($request, $photoModel, $existingPhoto);

        $inputs = $this->forceBooleanStringsToInteger($request->all());
        $inputs = $this->forceNullStringsToNull($inputs);
        $rules = Photo::updateRules();
        unset($rules['ownership_confirmation']);
        if (!$uploadedFilePath) unset($rules['photo']);
        if (!@$inputs['has_nudity']) {
            unset($rules['model_consent']);
            $inputs['model_consent'] = 0;
        }
        $validation = Validator::make($inputs, $rules, Photo::insertRulesErrorMessages());
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request->all(), [
            'caption'
        ]);
        if ($validation->fails() || count($unsafeHtmlTags)) {
            $errors = array_merge($validation->errors()->toArray(), $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $photoExifErrors = Photo::validateManualExifInputs($inputs);
        if (count($photoExifErrors)) {
            return response()->json([
                'validation_errors' => $photoExifErrors
            ], 400);
        }

        $existingPhoto->fill($inputs);
        $existingPhoto->location = @$inputs['location'] ?: null;
        if ($request->tags) {
            $existingPhoto->tags = implode(',', array_unique($inputs['tags'] ?: []));
        }
        $existingPhoto->save();
        $existingPhoto->exifAnswers()->delete();

        $exifAnswers = [];
        if (@$inputs['about_photo'] && @$inputs['about_photo_answers']) {
            $exifAnswers += $inputs['about_photo_answers'];
        }
        if (@$inputs['camera_settings'] && @$inputs['camera_settings_answers']) {
            $exifAnswers += $inputs['camera_settings_answers'];
        }
        foreach ($exifAnswers as $exifItemId => $exifValue) {
            PhotoExif::create([
                'photo_id' => $existingPhoto->id,
                'exif_item_id' => $exifItemId,
                'value' => $exifValue,
            ]);
        }

        $photoUploaded = $this->hasUserUploadedToAlbum($request->album_id, $photographer->id);
        if ($photoUploaded) $photoUploaded = $this->addPhotoFlags($photoUploaded, $existingPhoto, $album, $loggedInUser, $maxAdminPoints);

        return [
            'success' => true,
            'record' => $photoUploaded
        ];
    }

    /**
     * Gets a list of suggested filenames from the database, based on the input.
     * @param Request $request
     * @return array|JsonResponse
     */
    public function suggestFilename(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'album_id' => 'required|exists:albums,id',
            'query' => 'required|string'
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }
        return $this->suggestPhotoFilename($request->album_id, $request->query('query'));
    }

    /**
     * Saves the "Love" request for a Photo.
     * @param $id
     * @param Request $request
     * @return array|JsonResponse
     */
    public function heartPhoto($id, Request $request)
    {
        $photo = Photo::whereId($id)->first();
        if (!$photo) {
            return response()->json([
                'photo_not_found' => true,
            ], 400);
        }

        $alreadyLoved = Love::whereUserId($request->user()->id)->wherePhotoId($photo->id)->first();
        if ($alreadyLoved) {
            return response()->json([
                'already_loved' => true,
            ], 403);
        }

        $loveRecord = Love::create([
            'user_id' => $request->user()->id,
            'photo_id' => $photo->id,
        ]);

        $photo->num_loves = Love::wherePhotoId($photo->id)->count();
        $photo->save();

        Notification::saveAction(
            env('NOTIF_HOOK_PHOTO_LOVE'),
            $photo->user_id,
            $photo,
            $loveRecord
        );

        return [
            'success' => true,
            'photo_num_loves' => $photo->num_loves
        ];
    }

    /**
     * Removes the "Love" from a Photo.
     * @param $id
     * @param Request $request
     * @return array|JsonResponse
     */
    public function unHeartPhoto($id, Request $request)
    {
        $photo = Photo::whereId($id)->first();
        if (!$photo) {
            return response()->json([
                'photo_not_found' => true,
            ], 400);
        }

        $loved = Love::whereUserId($request->user()->id)->wherePhotoId($photo->id)->first();
        if (!$loved) {
            return response()->json([
                'was_never_loved' => true,
            ], 403);
        }

        $loved->delete();

        $photo->num_loves = Love::wherePhotoId($photo->id)->count();
        $photo->save();

        return [
            'success' => true,
            'photo_num_loves' => $photo->num_loves
        ];
    }

    public function destroy($id, Request $request)
    {
        $photo = Photo::whereId($id)->first();
        if (!$photo) {
            return response()->json([
                'photo_not_found' => true,
            ], 400);
        }

        $album = $photo->album;

        $user = $request->user();
        $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

        $userCanDelete = $this->canUserReplacePhoto($album, $photo, $user, $maxAdminPoints);
        if (!$userCanDelete) {
            $errorCode = 'cant_delete_photo';
            return response()->json([
                $errorCode => true,
                'message' => "Server responded with an error code: $errorCode"
            ], 403);
        }

        $this->removeFromEverywhereExistingPhoto($photo);

        $previousPhotosCount = Photo::howManyPhotosUploadedBefore($user);

        return [
            'success' => true,
            'has_uploaded_before' => !!$previousPhotosCount
        ];
    }

    /**
     * @return array
     */
    private function getAllCoversAndSamplesIds(): array
    {
        $albums = Album::query()->get();
        $coversAndSamplesIds = [];
        foreach ($albums as $album) {
            $coversAndSamplesIds[] = $album->cover_photo_id;
            $coversAndSamplesIds[] = $album->sample_photo_id;
        }
        $coversAndSamplesIds = array_unique(array_values($coversAndSamplesIds));
        $coversAndSamplesIds = array_filter($coversAndSamplesIds, function ($value) {
            return !empty($value);
        });

        return $coversAndSamplesIds;
    }

    private function photoHandlers(): array
    {
        return array_merge(
            $this->photoFilterHandlers(),
            $this->photoSearchHandlers(),
            $this->photoSortHandlers()
        );
    }

    private function photoFilterHandlers(): array
    {
        return [
            FilterMyPhoto::class,
            FilterFollowing::class,
            FilterStaffPicks::class,
            FilterExtraCredit::class,
            FilterShredAway::class,
            FilterNewbies::class,
        ];
    }

    private function photoSearchHandlers(): array
    {
        return [
            SearchByUsername::class,
            SearchByCountry::class,
            SearchByLevel::class,
            SearchByTags::class,
        ];
    }

    private function photoSortHandlers(): array
    {
        return [
            SortByNumber::class,
            SortRandom::class,
        ];
    }
}
