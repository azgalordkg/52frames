<?php declare(strict_types = 1);

namespace App\Http\Controllers;

use App\User;
use App\UserSoftware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserSoftwareController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'value' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        /** @var User $user */
        $user = $request->user();
        $software = UserSoftware::create([
            'user_id' => $user->id,
            'value' => $request->value,
        ]);

        return [
            'success'  => true,
            'software' => $software
        ];
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function update(int $id, Request $request)
    {
        $sortware = UserSoftware::whereId($id)->first();
        if (null === $sortware) {
            return response()->json([
                'gear_not_found' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $user = $request->user();
        if ($user->id !== $sortware->user_id || User::getMaxAdminPoints($user->id) > 100000) {
            return response()->json([
                'cant_edit_gear' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $validation = Validator::make($request->all(), [
            'type' => 'required|string',
            'value' => 'required|string',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $sortware->update([
            'type'  => $request->type,
            'value' => $request->value,
        ]);

        return [
            'success'  => true,
            'sortware' => $sortware,
        ];
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|null
     */
    public function destroy(int $id, Request $request)
    {
        $software = UserSoftware::whereId($id)->first();
        if (null === $software) {
            return response()->json([
                'software_not_found' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $user = $request->user();
        if ($user->id !== $software->user_id || User::getMaxAdminPoints($user->id) > 10000) {
            return response()->json([
                'cant_edit_software' => true,
            ], Response::HTTP_NOT_FOUND);
        }

        $software->delete();

        return [ 'success' => true ];
    }
}
