<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Gets the list of Roles.
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse|static[]
     */
    public function index(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'dynamic' => 'required|boolean'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        if (!$request->dynamic) {

            $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);

            if ($maxAdminPoints < 1000000) {
                return response()->json([
                    'not_enough_admin_points' => true
                ], 403);
            }

        }

        $rolesQuery = Role::orderBy('access_level_points', 'asc');

        $rolesQuery->whereDynamic($request->dynamic);

        return $rolesQuery->get();
    }
}
