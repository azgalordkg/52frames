<?php

namespace App\Http\Controllers\Analytics;

use App\AnalyticsLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Analytics\Helpers\AnalyticsHelper;
use Illuminate\Support\Facades\Validator;

class MainAnalyticsController extends Controller
{
    public function saveEntry(string $term, Request $request)
    {
        $triggerTerm = AnalyticsHelper::getTriggerTermFromDb($term);
        if (!$triggerTerm)
            return response()->json([
                'not_found' => true
            ], 404);

        $inputs = $request->all();
        $rules = AnalyticsLog::insertRules();
        $validation = Validator::make($inputs, $rules);
        if ($validation->fails())
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);

        $endUser = $request->user();
        if ($triggerTerm->requires_login && !$endUser)
            return response()->json([
                'dev_message' => 'metric requires login'
            ], 200);

        $inputs['user_id'] = $endUser->id;
        $inputs['trigger_term_id'] = $triggerTerm->id;

        $log = AnalyticsLog::whereArray($inputs)->first();
        if ($triggerTerm->requires_unique_counting && $log)
            return response()->json([
                'dev_message' => 'already recorded'
            ], 200);

        $newLog = AnalyticsLog::create($inputs)->load('triggerTerm.type');

        AnalyticsHelper::processHooks($newLog);

        return $newLog;
    }
}
