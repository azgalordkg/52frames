<?php

namespace App\Http\Controllers\Analytics;

use App\AnalyticsLog;
use App\Http\Controllers\Analytics\Helpers\AnalyticsHelper;
use App\Jobs\AnalyticsAssetFailedToLoadInformAdmins;
use App\Mail\AnalyticsStaticAssetDidntLoad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StaticAssetController extends Controller
{
    public function assetFailedToLoad(string $term, Request $request)
    {
        $triggerTerm = AnalyticsHelper::getTriggerTermFromDb($term);
        if (!$triggerTerm)
            return response()->json([
                'not_found' => true
            ], 404);

        $inputs = $request->all();
        $rules = AnalyticsLog::insertRules();
        $validation = Validator::make($inputs, $rules);
        if ($validation->fails())
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);

        // TODO: Investigate.
//        AnalyticsAssetFailedToLoadInformAdmins::dispatch($triggerTerm, $request->all());
        return ['queued' => true];
    }

}
