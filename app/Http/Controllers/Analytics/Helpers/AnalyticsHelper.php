<?php

namespace App\Http\Controllers\Analytics\Helpers;

use App\Model;
use App\AnalyticsLog;
use App\AnalyticsTriggerTerm;
use App\Traits\Helpers\ErrorMessage;

abstract class AnalyticsHelper
{
    /**
     * Gets the record from the database, for the keyword/term passed in.
     *
     * @param string $term
     * @return AnalyticsTriggerTerm|null
     */
    public static function getTriggerTermFromDb(string $term): ?AnalyticsTriggerTerm
    {
        $triggerTerm = AnalyticsTriggerTerm::whereTerm($term)->first();

        // TODO: Record or Email admins, when a specific term is not found in the database.

        return $triggerTerm;
    }

    /**
     * Initiator function, to call the other Model, the target Model, so to activate its event Handler, if it exists.
     *
     * @param AnalyticsLog $log
     * @param \Closure|null $successCb
     * @param \Closure|null $failedCb
     * @return AnalyticsLog
     */
    public static function processHooks(AnalyticsLog $log, \Closure $successCb = null, \Closure $failedCb = null): AnalyticsLog
    {
        $targetModel = self::getTrackedIdModelFrom($log);
        $handlerMethod = self::getHandlerMethodFrom($targetModel);
        call_user_func_array($handlerMethod, func_get_args());
        return $log;
    }

    protected static function getTrackedIdModelFrom(AnalyticsLog $log)
    {
        $morpherMethodName = $log->analyticsMorpherMethodName;
        $methodExists = method_exists($log, $morpherMethodName);
        if ($methodExists) return $log->$morpherMethodName;

        throw new ErrorMessage(PlsOverrideMethod, $morpherMethodName, AnalyticsLog::class);
    }

    protected static function getHandlerMethodFrom(Model $targetModel)
    {
        $handlerMethodName = $targetModel->analyticsItemHandlerMethodName;
        $methodExists = method_exists($targetModel, $handlerMethodName);
        if ($methodExists) return [$targetModel, $handlerMethodName];

        throw new ErrorMessage(PlsOverrideMethod, $handlerMethodName, $targetModel);
    }
}