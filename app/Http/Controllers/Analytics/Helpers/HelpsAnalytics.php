<?php

namespace App\Http\Controllers\Analytics\Helpers;

use App\AnalyticsLog;
use App\Traits\Helpers\ErrorMessage;

trait HelpsAnalytics
{
    public $analyticsMorpherMethodName = 'analyticsTrackedItem';

    public $analyticsItemHandlerMethodName = 'analyticsItemHandler';

    /**
     * Please override this method, and target the Model being tracked directly. See method: analyticsItemHandler() below,
     * for more information on how to pick up the call from the other Model, then.
     *
     * @return mixed
     */
    public function analyticsTrackedItem()
    {
        $modelName = null;
        $foreign = null;
        $local = null;

        if ($modelName) return $this->belongsTo($modelName, $foreign, $local); // sample line

        throw new ErrorMessage(PlsOverrideMethod, __METHOD__, $this);
    }

    /**
     * Receiver or Callback processor of the Analytics event that just fired.
     * It is useful to catch these events, so your Model will be able to respond & perhaps save the record to one of the model's rows.
     * If you receive a $callback from the original caller who fired the Event, you can use this to signal that you are then finished.
     *
     * @param AnalyticsLog $newlyAddedRecord
     * @param \Closure|null $successCb
     * @param \Closure|null $failedCb
     */
    public function analyticsItemHandler(AnalyticsLog $newlyAddedRecord, \Closure $successCb = null, \Closure $failedCb = null)
    {
        // if you have a lot of analytics "item types" and you need a reference to break your code apart,
        // you can switch() on the keyword found on the $newlyAddedRecord->triggerTerm->term

        // --------------------------------
        // optional (needs replacing):

        echo "=====\n\n";
        echo "new analytics log received (Model needs a proper Handler method):\n\n";

        $sourceClass = AnalyticsLog::class;
        $trigger_term = $newlyAddedRecord->triggerTerm->term;
        echo "--- newlyLogged item > trigger_term: $trigger_term --summary from: $sourceClass ---\n\n";
        print_r($newlyAddedRecord->toArray());
        echo "\n";

        $destinationClass = get_class($this);
        echo "--- this Model's instance: $destinationClass ---\n\n";
        $this->lockForUpdate(); // use this when updating your row, so you won't have conflicts along the way.
        print_r($this->toArray());
        echo "\n";

        echo "=====\n\n";


        // optional:

        if (true) $successCb && $successCb('Success!');
        else $failedCb && $failedCb('Failed!');


        // optional:

        throw new ErrorMessage(PlsOverrideMethod, __METHOD__, $this);
    }
}