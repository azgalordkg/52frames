<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\Debugger;
use Illuminate\Http\Request;
use App\Traits\HelpsPagination;
use App\Traits\HelpsValidation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use HelpsValidation, HelpsPagination;

    protected function getDebugMode(Request $request, User $user = null): bool
    {
        return Debugger::canUserDebug($request, $user ?: $request->user());
    }

    protected function debugModeRequired(Request $request, User $user = null)
    {
        $debugMode = $this->getDebugMode($request, $user);
        if (!$debugMode) $this->respond403Forbidden(['needs_debug_mode' => true]);
    }

    protected function minAdminPointsRequired(int $points)
    {
        $user = Auth::guard('api')->user();
        if ($user) {
            $maxAdminPoints = User::getMaxAdminPoints($user->id);
            if ($maxAdminPoints >= $points) return $maxAdminPoints;
            else $this->respondNotEnoughAdminPoints();
        } else $this->respond401Unauthorized();
    }

    protected function respond401Unauthorized(array $data = ['error' => 'Unauthenticated'])
    {
        $this->respondWithJson($data, 401);
    }

    protected function respond403Forbidden(array $data = ['error' => 'Forbidden'])
    {
        $this->respondWithJson($data, 403);
    }

    protected function respondNotEnoughAdminPoints(array $data = ['not_enough_admin_points' => true])
    {
        $this->respond403Forbidden($data);
    }

    protected function respond404(array $data = ['not_found' => true])
    {
        $this->respondWithJson($data, 404);
    }

    protected function respondSuccess(array $data = ['success' => true], int $httpCode = 200)
    {
        $this->respondWithJson($data, $httpCode);
    }

    protected function respondWithJson(array $data = [], int $httpCode = 200)
    {
        response()->json($data, $httpCode)->send();
        die();
    }
}
