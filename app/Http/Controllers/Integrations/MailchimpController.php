<?php

namespace App\Http\Controllers\Integrations;

use App\Services\MailchimpHelper;
use App\Services\UserSettingsHelper;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MailchimpController extends MailchimpHelper
{
    public function getRequest(): JsonResponse
    {
        return response()->json([]);
    }

    public function postRequest(Request $request): JsonResponse
    {
        return $this->execRequestedMethod($request);
    }

    protected function mapRequestedMethods(Request $request): ?JsonResponse
    {
        switch ($request->type) {
            case 'unsubscribe':
                return $this->destroy($request);

        }
        return null;
    }

    public function destroy(Request $request): JsonResponse
    {
        $laravelIdTerm = env('MAILCHIMP_LIST_LARAVEL_ID_MERGE_FIELD_NAME', 'ID_LARAVEL');
        $idHierarchy = 'data.merges.' . $laravelIdTerm;

        $validation = Validator::make($request->all(), [
            $idHierarchy => 'required|integer|exists:users,id'
        ], [
            $idHierarchy . '.required' => "The $laravelIdTerm field is required.",
            $idHierarchy . '.integer' => "The $laravelIdTerm field must be an integer.",
            $idHierarchy . '.exists' => "The selected $laravelIdTerm is invalid."
        ]);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $laravelId = $request->input($idHierarchy);
        $user = User::whereId($laravelId)->first();

        UserSettingsHelper::updateAndSave([
            'notif_emails_daily_dont_receive' => 1
        ], $user);

        return response()->json(['queued' => true]);
    }
}