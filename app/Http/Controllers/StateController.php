<?php

namespace App\Http\Controllers;

use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StateController extends Controller
{
    /**
     * Get the list of States, based on the input country.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'query'      => 'string|nullable',
            'country_id' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $query = @$request->get('query') ?: "";

        return State::suggest('name', $query)->whereCountryId($request->country_id)->orderBy('name')->get();
    }
}
