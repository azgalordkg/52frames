<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    private const MOST_POPULAR_COUNTRIES_LIST = [
        "United States of America",
        "United Kingdom of Great Britain and Northern Ireland",
        "Canada",
        "Australia",
    ];

    private function getMostPopularCountries(): array
    {
        $popularCountries = Country::whereIn('name', self::MOST_POPULAR_COUNTRIES_LIST)->get()->sortByDesc('id')->toArray();

        return $popularCountries;
    }

    /**
     * Gets the list of Countries.
     * @return array
     */
    public function index(Request $request)
    {
        $query = @$request->get('query') ?: "";

        $getCountriesQuery = Country::suggest('name', $query)->orderBy('name');

        $popularCountries = [];
        if ("" === $query) {
            $popularCountries = $this->getMostPopularCountries();
            $countryIds = array_column($popularCountries, 'id');
            $countryIds = array_values(array_unique($countryIds));
            $getCountriesQuery->whereNotIn('id', $countryIds);
        }

        $countries = $getCountriesQuery->orderBy('name')->get()->toArray();
        $allCountries = array_merge($popularCountries, $countries);

        return $allCountries;
    }
}
