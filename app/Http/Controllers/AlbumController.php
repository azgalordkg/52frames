<?php

namespace App\Http\Controllers;

use App\Album;
use App\AlbumSort;
use App\Http\Requests\PhotoIndexRequest;
use App\Jobs\UpdateMailchimpUsersDidntUpload;
use App\Photo;
use App\Services\Helpers;
use App\Traits\HelpsFileUploads;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{
    use HelpsFileUploads;

    /**
     * Used as a flag when calling this Controller from the PhotoController.php
     * @var bool
     */
    protected $isFromPhotoController;

    /**
     * Contains the middleware, etc.
     * @param bool $isFromPhotoController
     */
    public function __construct(bool $isFromPhotoController = false)
    {
        $this->isFromPhotoController = $isFromPhotoController;
        $this->middleware('auth:api')->except([
            'index',
            'show',
            'getRecentAlbums',
            'futureAlbums',
            'tags',
            'photographers',
        ]);
    }

    /**
     * Rules when updating an Album.
     * @param array $request
     * @param Album $album
     * @return array
     */
    protected function updateRules(array $request, Album $album)
    {
        $rules = Album::insertRules();
        $rules['temp_cover_photo.replace_file'] = 'required|boolean';
        $rules['sample_photo.replace_file'] = 'required|boolean';
        if (!@$request['temp_cover_photo']['replace_file']) unset($rules['temp_cover_photo.file']);
        if (!@$request['sample_photo']['replace_file']) unset($rules['sample_photo.file']);
        if (@$request['shorturl'] == $album->shorturl) unset($rules['shorturl']);
        return $rules;
    }

    protected function coverAndSamplePhotosValidationTerms()
    {
        return [
            'temp_cover_photo.file' => 'cover photo',
            'temp_cover_photo.owner_name' => "owner",
            'temp_cover_photo.album_theme' => "album theme",
            'temp_cover_photo.album_week_number' => "week number",
            'temp_cover_photo.url' => "URL",
            'sample_photo.file' => 'sample photo',
            'sample_photo.owner_name' => "owner",
            'sample_photo.album_theme' => "album theme",
            'sample_photo.album_week_number' => "week number",
            'sample_photo.url' => "URL"
        ];
    }

    /**
     * Validates the year and week entries from the input.
     * Returns an array of errors, if there are any.
     * @param Request $request
     * @param Album|null $album
     * @return array
     */
    protected function validateWeekYear(Request $request, Album $album = null)
    {
        $errors = [];
        $albumExists = ['An album already exists'];

        $yearWeekExistsQuery = Album::where('week_number', @$request->week_number ?: 0)
            ->where('year', @$request->year ?: 0);
        if ($album) $yearWeekExistsQuery->where('id', '!=', $album->id);
        $yearWeekExists = $yearWeekExistsQuery->first();
        if ($yearWeekExists) {
            $errors = array_merge($errors, [
                'week_number' => $albumExists,
                'year' => $albumExists
            ]);
        }
        $startDate = @$request->start_date ?: 0;
        $endDate = @$request->end_date ?: 0;
        $dateRangeExistsQuery = Album::where('start_date', '>=', $startDate)
            ->where('start_date', '<=', $endDate)
            ->where('end_date', '>=', $startDate)
            ->where('end_date', '<=', $endDate);
        if ($album) $dateRangeExistsQuery->where('id', '!=', $album->id);
        $dateRangeExists = $dateRangeExistsQuery->first();
        if ($dateRangeExists) {
            $errors = array_merge($errors, [
                'start_date' => $albumExists,
                'end_date' => $albumExists
            ]);
        }
        return $errors;
    }

    /**
     * Validates the shorturl and checks if it doesn't exist.
     * Returns an array of errors, if there are any.
     * @param Request $request
     * @return array
     */
    protected function validateShorturl(Request $request, Album $album = null)
    {
        $existsQuery = Album::where('week_number', $request->week_number)
            ->where('year', $request->year)
            ->where('shorturl', $request->shorturl);

        if ($album) $existsQuery->where('id', '!=', $album->id);

        $exists = $existsQuery->first();
        if ($exists) {
            return [
                'shorturl' => ['The shorturl has already been taken.']
            ];
        }

        return [];
    }

    /**
     * Public interface from the api.php
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function checkShorturl(Request $request)
    {
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $insertRules = Album::insertRules();

        $validation = Validator::make($request->all(), [
            'year' => $insertRules['year'],
            'week_number' => $insertRules['week_number'],
            'shorturl' => $insertRules['shorturl'],
            'album_id' => 'nullable|exists:albums,id'
        ]);

        $album = null;
        if (@$request->album_id) $album = Album::whereId($request->album_id)->first();
        $shorturlValidation = $this->validateShorturl($request, $album);

        if ($validation->fails() || count($shorturlValidation)) {
            $errors = array_merge($validation->errors()->toArray(), $shorturlValidation);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        return ['success' => true];
    }

    /**
     * Gets the list of all Albums, organized by years
     * @return array
     */
    public function index()
    {
        $result = [];

        $years = Album::select('year')->distinct()
            ->orderBy('year', 'desc')
            ->get();

        if ($years->count()) {
            foreach ($years as $yearData) {

                $albumsQuery = Album::where('year', $yearData->year)
                    ->orderBy('week_number', 'desc');

                $user = Auth::guard('api')->user();
                $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

                if ($maxAdminPoints < 100000) {
                    $albumsQuery->wherePubliclyVisible(true);
                }

                $albums = $albumsQuery->get();

                if ($albums->count()) {

                    $albumsArray = [];
                    foreach ($albums as $album) {
                        $albumSummary = Album::getSummary($album);
                        $albumsArray[] = array_merge($albumSummary, [
                            'cover_photo' => $album->cover_photo,
                            'num_photos' => Album::getNumberOfPhotos($album),
                        ]);
                    }

                    $result[] = [
                        'year' => $yearData->year,
                        'albums' => $albumsArray
                    ];
                }

            }
        }

        return $result;
    }

    /**
     * Gets 1 Album from the Database.
     * @param $idOrWeekUrl
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function show($idOrWeekUrl, Request $request)
    {
        $inputs = $this->forceBooleanStringsToInteger($request->all());
        $rules = [
            'year' => 'required|integer',
            'is_challenge_page' => 'required|boolean'
        ];
        if ($this->isFromPhotoController) {
            $inputs['is_challenge_page'] = 0;
        }
        $validation = Validator::make($inputs, $rules);

        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $isWeekOnly = false;
        $isWeekWithTheme = false;
        preg_match('/^week-([0-9]+)(-(.+))?/i', $idOrWeekUrl, $urlFragments);
        if (@$urlFragments[1]) {
            if (@$urlFragments[3]) {
                $isWeekWithTheme = true;
                $weekNumber = $urlFragments[1];
                $shorturl = $urlFragments[3];
            } else {
                $isWeekOnly = true;
                $weekNumber = $urlFragments[1];
            }
        }

        $albumQuery = Album::where('year', $request->year);

        if ($isWeekWithTheme) {
            $albumQuery->whereWeekNumber($weekNumber)->whereShorturl($shorturl);
        } else if ($isWeekOnly) {
            $albumQuery->whereWeekNumber($weekNumber);
        } else {
            $albumQuery->whereId($idOrWeekUrl);
        }

        $albumExists = $albumQuery->count();

        $user = Auth::guard('api')->user();
        $maxAdminPoints = $user ? User::getMaxAdminPoints($user->id) : 0;

        if (!$inputs['is_challenge_page'] && $maxAdminPoints < 100000) {
            $albumQuery->wherePubliclyVisible(true);
        }

        $album = $albumQuery->first();

        if (!$album) {
            if ($albumExists) {
                return response()->json([
                    'not_enough_admin_points' => true
                ], 403);
            } else {
                return response()->json([
                    'no_such_album' => true
                ], 404);
            }
        }

        if ($isWeekOnly) {
            return response()->json([
                'redirect' => true,
                'shorturl' => $album->shorturl
            ], 302);
        }

        if (!$this->isFromPhotoController && !$inputs['is_challenge_page'] && !$album->live && $maxAdminPoints < 100000) {
            return response()->json([
                'show_challenge_page' => true,
                'not_live' => (bool) !$album->live,
            ], 302);
        }

        $otherColumns = [];
        if (!$this->isFromPhotoController && $maxAdminPoints >= 0) {
            $otherColumns = Helpers::filterByKeysModel($album, [
                'temp_cover_photo',
                'sample_photo'
            ]);
        }
        $albumArray = array_merge($album->toArray(), $otherColumns);

        if ($inputs['is_challenge_page']) {

            $challengePageInfo = $this->addChallengePageFlags($album, $user, false, true, true, [
                'is_current_album'
            ]);
            $albumArray = array_merge($challengePageInfo, $otherColumns);

        } else {
            $challengePageInfo = $this->addChallengePageFlags($album, $user, false, true, false);
            $topPhotos = [];
            $flierPhotos = [];
            $adminFlags = [];
            if ($maxAdminPoints >= 200000) {
                $topPhotos = Helpers::getTopPhotos($album);
                $flierPhotos = $this->getFlierPhotos($album);
                $adminFlags = [
                    'can_go_live' => !$album->live && !$this->isBeforeUploadSchedTime($album),
                    'after_upload_sched' => $this->isAfterUploadSchedTime($album)
                ];
            }

            $albumArray = array_merge($albumArray, $topPhotos, $flierPhotos, $adminFlags, $challengePageInfo);

            if (!$this->isFromPhotoController) {
                $photoController = new PhotoController();
                $photoRequest = new PhotoIndexRequest([
                    'album_id' => $album->id,
                    'filter_by' => $request->filter_by
                ]);
                $photosResponse = $photoController->index($photoRequest);
                $albumArray = array_merge($albumArray, [
                    'photosData' => $photosResponse
                ]);
            }
        }

        return $albumArray;
    }

    /**
     * Removes the previously uploaded flier.
     * @param string $uploaderName
     * @param User $user
     * @param Album $album
     */
    protected function removePreviouslyUploadedCoverOrSamplePhoto(string $uploaderName, User $user, Album $album)
    {
        $photos = Photo::whereUserId($user->id)->whereAlbumId($album->id)->get();
        foreach ($photos as $photo) {
            $this->removeFromEverywhereExistingPhoto($photo);
        }
        $columnName = $uploaderName . '_id';
        $album->$columnName = null;
        $album->save();
    }

    /**
     * Uploads the Fliers and saves them in the database.
     * @param array $request
     * @param Album $album
     * @return array
     */
    protected function uploadCoverAndSamplePhotosAndSaveToDb(array $request, Album $album)
    {
        $uploadersNames = [
            'cover_photo' => [
                'request_name' => 'temp_cover_photo',
                'photo' => @$request['temp_cover_photo']['file'],
                'title' => 'Cover Photo',
                'caption' => 'Cover Photo',
                'extra_data' => [
                    'owner_name' => @$request['temp_cover_photo']['owner_name'],
                    'album_theme' => @$request['temp_cover_photo']['album_theme'],
                    'album_week_number' => @$request['temp_cover_photo']['album_week_number'],
                    'url' => @$request['temp_cover_photo']['url']
                ]
            ],
            'sample_photo' => [
                'request_name' => 'sample_photo',
                'photo' => @$request['sample_photo']['file'],
                'title' => 'Sample Photo',
                'caption' => 'Sample Photo',
                'extra_data' => [
                    'owner_name' => @$request['sample_photo']['owner_name'],
                    'album_theme' => @$request['sample_photo']['album_theme'],
                    'album_week_number' => @$request['sample_photo']['album_week_number'],
                    'url' => @$request['sample_photo']['url']
                ]
            ]
        ];
        $outputPhotoIds = [];
        foreach ($uploadersNames as $uploaderName => $requestData) {
            if (isset($request[$requestData['request_name']])) {
                $uploaderNameUC = strtoupper($uploaderName);
                $uploaderEmail = env('APP_' . $uploaderNameUC . '_UPLOADER_EMAIL');
                $uploaderUser = User::whereEmail($uploaderEmail)->first();
                if ($uploaderUser) {
                    if (@$requestData['photo']) {
                        $this->removePreviouslyUploadedCoverOrSamplePhoto($uploaderName, $uploaderUser, $album);
                        $photoController = new PhotoController();
                        $photoController->setUploader($uploaderUser);
                        $photoRequest = new Request([
                            'album_id' => $album->id,
                            'critique_level' => '52f-cc_regular',
                            'ownership_confirmation' => 1,
                            'screencast_critique' => 0,
                            'photo' => $requestData['photo'],
                            'title' => $requestData['title'],
                            'caption' => $requestData['caption']
                        ]);
                        $response = $photoController->store($photoRequest, new Photo());
                        if (is_array($response) && @$response['success']) {
                            $photoId = $response['record']['id'];
                            $outputPhotoIds[$uploaderName] = $photoId;
                            $idColumnName = $uploaderName . '_id';
                            $album->$idColumnName = $photoId;
                            $dataColumnName = $uploaderName . '_data';
                            $album->$dataColumnName = $requestData['extra_data'];
                            $album->save();
                        }
                    } else {
                        $dataColumnName = $uploaderName . '_data';
                        $album->$dataColumnName = $requestData['extra_data'];
                        $album->save();
                    }
                }
            }
        }
        return $outputPhotoIds;
    }

    /**
     * Validates and save a new Album into the Database.
     * @param Request $request
     * @param Album $album
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Album $album)
    {
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $data = $this->forceBooleanStringsToInteger($request->all());
        $data = $this->forceNullStringsToNull($data);
        $validation = Validator::make($data, Album::insertRules(), [], $this->coverAndSamplePhotosValidationTerms());

        $weekYearValidation = $this->validateWeekYear($request);
        $shorturlValidation = $this->validateShorturl($request);
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($data, [
            'blurb',
            'short_blurb',
            'extra_credit_body',
            'tips_body'
        ]);

        if ($validation->fails() || count($weekYearValidation) || count($shorturlValidation) || count($unsafeHtmlTags)) {
            $errors = array_merge($validation->errors()->toArray(), $weekYearValidation, $shorturlValidation, $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        if (isset($request->tags)) {
            $data['tags'] = implode(',', array_unique($data['tags']));
        }

        $album->fill($data)->save();

        $this->uploadCoverAndSamplePhotosAndSaveToDb($data, $album);

        return ['success' => true];
    }

    /**
     * Validates and updates an Album in the Database.
     * @param int $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update(int $id = 0, Request $request)
    {
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $album = Album::whereId($id)->first();
        if (!$album) {
            return response()->json([
                'no_such_album' => true
            ], 404);
        }

        $data = $this->forceBooleanStringsToInteger($request->all());
        $data = $this->forceNullStringsToNull($data);
        $rules = $this->updateRules($data, $album);

        $validation = Validator::make($request->all(), $rules, [], $this->coverAndSamplePhotosValidationTerms());

        $weekYearValidation = $this->validateWeekYear($request, $album);
        $shorturlValidation = $this->validateShorturl($request, $album);
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request->all(), [
            'blurb',
            'short_blurb',
            'extra_credit_body',
            'tips_body',
        ]);

        if ($validation->fails() || count($weekYearValidation) || count($shorturlValidation) || count($unsafeHtmlTags)) {
            $errors = array_merge($validation->errors()->toArray(), $weekYearValidation, $shorturlValidation, $unsafeHtmlTags);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $data = $request->all();

        if (isset($request->tags)) {
            $data['tags'] = implode(',', array_unique($data['tags']));
        }

        $album->fill($data)->save();

        $this->uploadCoverAndSamplePhotosAndSaveToDb($data, $album);

        return ['success' => true];
    }

    /**
     * Deletes an Album from the Database.
     * @param int $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy(int $id = 0, Request $request)
    {
        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $album = Album::whereId($id)->first();
        if (!$album) {
            return response()->json([
                'no_such_album' => true
            ], 404);
        }

        $photos = Photo::whereAlbumId($album->id)->get();
        $albumsWithCoverFromRemoving  = Album::whereIn('cover_photo_id', $photos)->get();
        foreach ($albumsWithCoverFromRemoving as $albumWithCover) {
            $albumWithCover->cover_photo_id = 0;
            $albumWithCover->save();
        }
        $albumsWithSampleFromRemoving = Album::whereIn('sample_photo_id', $photos)->get();
        foreach ($albumsWithSampleFromRemoving as $albumWithSample) {
            $albumWithSample->sample_photo_id = 0;
            $albumWithSample->save();
        }

        $album->delete();

        return ['success' => true];
    }

    /**
     * Validates the input to makes sure they are unique.
     * @param int $album_id
     * @param array $inputs
     * @return array
     */
    protected function uniqueWinnersAndRunnersup(int $album_id, array $inputs)
    {
        $filenames = [];
        $result = [];
        if (isset($inputs['filenames']) && is_array($inputs['filenames'])) {
            foreach (['winners', 'runners_up'] as $section) {
                if (isset($inputs['filenames'][$section]) && is_array($inputs['filenames'][$section])) {
                    foreach ($inputs['filenames'][$section] as $index => $filename) {
                        if ($filename) {
                            $suggestedFilenames = $this->suggestPhotoFilename($album_id, $filename);
                            if (!count($suggestedFilenames) || $suggestedFilenames[0]['filename'] != $filename) {
                                $result['filenames.' . $section . '.' . $index] = ['The file does not exist.'];
                                continue;
                            }
                            if (in_array($filename, $filenames)) {
                                $result['filenames.' . $section . '.' . $index] = ['The same photo may not be chosen again.'];
                            }
                            $filenames[] = $filename;
                        }
                    }
                }
            }
        }
        return $result;
    }

    protected function validateAlbumShortBlurb(Album $album, array $inputs)
    {
        $result = [];
        if (!@$inputs['save_only'] && !$album->short_blurb) {
            $result['short_blurb'] = ["The album's short blurb is required."];
        }
        return $result;
    }

    /**
     * Maps the inputs into their equivalent columns in the database.
     * @param Album $album
     * @param array $inputs
     */
    protected function mapWinnersToDbColumns(Album $album, array $inputs)
    {
        $photosIdMap = [
            'winners' => [
                'top1_photo_id',
                'top2_photo_id',
                'top3_photo_id'
            ],
            'runners_up' => [
                'top4_photo_id',
                'top5_photo_id',
                'top6_photo_id'
            ]
        ];
        foreach ($photosIdMap as $section => $photoColumnNames) {
            foreach ($photoColumnNames as $index => $photoIdColumn) {
                $photo_id = null;
                $note = null;
                if (isset($inputs['filenames']) && is_array($inputs['filenames'])) {
                    if (isset($inputs['filenames'][$section]) && is_array($inputs['filenames'][$section])) {
                        if (isset($inputs['filenames'][$section][$index])) {
                            $filename = $inputs['filenames'][$section][$index];
                            $photo = $this->suggestPhotoFilename($album->id, $filename);
                            if (count($photo) && $photo[0]['filename'] == $filename) {
                                $photo_id = $photo[0]['id'];
                                if (isset($inputs['notes']) && is_array($inputs['notes'])) {
                                    if (isset($inputs['notes'][$section]) && is_array($inputs['notes'][$section])) {
                                        if (isset($inputs['notes'][$section][$index])) {
                                            $note = $inputs['notes'][$section][$index];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $album->$photoIdColumn = $photo_id;
                if ($photo_id && $note) {
                    $photo = Photo::whereId($photo_id)->first();
                    if ($photo) {
                        $photo->notes_from_yosef = $note;
                        $photo->save();
                    }
                }
            }
        }
    }

    /**
     * Removes the previously uploaded flier.
     * @param User $user
     * @param Album $album
     */
    protected function removePreviouslyUploadedFlier(string $uploaderName, User $user, Album $album)
    {
        $photos = Photo::whereUserId($user->id)->whereAlbumId($album->id)->get();
        foreach ($photos as $photo) {
            $this->removeFromEverywhereExistingPhoto($photo);
        }
        $columnName = $uploaderName . '_photo_id';
        $album->$columnName = null;
        $album->save();
    }

    /**
     * Uploads the Fliers and saves them in the database.
     * @param array $request
     * @param Album $album
     * @return array
     */
    protected function uploadFliersAndSaveToDb(array $request, Album $album)
    {
        $uploadersNames = [
            'patreon',
            'flier'
        ];
        $outputPhotoIds = [];
        foreach ($uploadersNames as $uploaderName) {
            if (isset($request[$uploaderName])) {
                $uploaderNameUC = strtoupper($uploaderName);
                $uploaderEmail = env('APP_' . $uploaderNameUC . '_UPLOADER_EMAIL');
                $uploaderUser = User::whereEmail($uploaderEmail)->first();
                if ($uploaderUser) {
                    $photoController = new PhotoController();
                    $photoController->setUploader($uploaderUser);
                    $forRequest = [
                        'album_id' => $album->id,
                        'critique_level' => '52f-cc_regular',
                        'ownership_confirmation' => 1,
                        'screencast_critique' => 0,
                        'title' => $request[$uploaderName]['title'],
                        'caption' => $request[$uploaderName]['caption']
                    ];
                    if (@$request[$uploaderName]['photo']) {
                        $this->removePreviouslyUploadedFlier($uploaderName, $uploaderUser, $album);
                        $forRequest = array_merge($forRequest, [
                            'photo' => $request[$uploaderName]['photo']
                        ]);
                        $photoRequest = new Request($forRequest);
                        $response = $photoController->store($photoRequest, new Photo());
                        if (is_array($response) && @$response['success']) {
                            $photoId = $response['record']['id'];
                            $outputPhotoIds[$uploaderName] = $photoId;
                            $columnName = $uploaderName . '_photo_id';
                            $album->$columnName = $photoId;
                        }
                    } else if (@$request[$uploaderName]['remove']) {
                        $this->removePreviouslyUploadedFlier($uploaderName, $uploaderUser, $album);
                    } else if (@$request[$uploaderName]['title'] || @$request[$uploaderName]['caption']) {
                        $columnName = $uploaderName . '_photo_id';
                        $photoId = $album->$columnName;
                        if ($photoId) {
                            $photoRequest = new Request($forRequest);
                            $response = $photoController->update($photoId, $photoRequest, new Photo());
                            if (is_array($response) && @$response['success']) {
                                $outputPhotoIds[$uploaderName] = $photoId;
                            }
                        }
                    }
                }
            }
        }
        return $outputPhotoIds;
    }

    /**
     * Set the Album to Public.
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function makePublic($id, Request $request)
    {
        $album = Album::whereId($id)->first();
        if (!$album) {
            return response()->json([
                'album_doesnt_exist' => true
            ], 403);
        }

        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $inputs = $this->forceBooleanStringsToInteger($request->all());
        $inputs = $this->forceNullStringsToNull($inputs);
        $validation = Validator::make($inputs, [
            'publicly_visible' => 'required|boolean',
            'live' => 'boolean',
        ]);

        if ($validation->fails()) {
            $errors = $validation->errors()->toArray();
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $album->publicly_visible = $inputs['publicly_visible'];
        // TODO: Remove it
        if (isset($inputs['live'])) {
            $album->live = $inputs['live'];
        }

        $album->save();

        return [
            'success' => true,
            'album_id' => $album->id,
            'publicly_visible' => $album->publicly_visible,
            'live' => $album->live,
        ];
    }

    /**
     * Set the Album to Live.
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function makeLive($id, Request $request)
    {
        $album = Album::whereId($id)->first();
        if (!$album) {
            return response()->json([
                'album_doesnt_exist' => true
            ], 403);
        }

        $maxAdminPoints = User::getMaxAdminPoints($request->user()->id);
        if ($maxAdminPoints < 200000) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }

        $rules = [
            'save_only' => 'required|boolean',
            'filenames' => 'array',
            'filenames.winners' => 'array',
            'filenames.winners.0' => 'required|nullable|string',
            'filenames.winners.1' => 'required|nullable|string',
            'filenames.winners.2' => 'required|nullable|string',
            'filenames.runners_up' => 'array',
            'filenames.runners_up.*' => 'nullable|string',
            'notes' => 'array',
            'notes.winners' => 'array',
            'notes.winners.*' => 'nullable|string',
            'notes.runners_up' => 'array',
            'notes.runners_up.*' => 'nullable|string',
            'patreon' => 'array',
            'patreon.photo' => 'nullable|mimes:jpeg,png|max:5120',
            'patreon.title' => 'required_with:patreon.photo|nullable|string',
            'patreon.caption' => 'required_with:patreon.photo|nullable|string',
            'patreon.remove' => 'nullable|boolean',
            'flier' => 'array',
            'flier.photo' => 'nullable|mimes:jpeg,png|max:5120',
            'flier.title' => 'required_with:flier.photo|nullable|string',
            'flier.caption' => 'required_with:flier.photo|nullable|string',
            'flier.remove' => 'nullable|boolean'
        ];
        $inputs = $this->forceBooleanStringsToInteger($request->all());
        $inputs = $this->forceNullStringsToNull($inputs);
        if (isset($inputs['save_only']) && $inputs['save_only']) {
            foreach (['filenames.winners.0', 'filenames.winners.1', 'filenames.winners.2'] as $i) {
                $rules[$i] = str_replace('required|', '', $rules[$i]);
            }
        }
        $validation = Validator::make($inputs, $rules, [
            'filenames.winners.0.required' => 'The First-winning photo is required',
            'filenames.winners.1.required' => 'The Second-winning photo is required',
            'filenames.winners.2.required' => 'The Third-winning photo is required',
            'patreon.title.required_with' => 'The patreon title field is required when patreon photo is present.',
            'patreon.caption.required_with' => 'The patreon caption field is required when patreon photo is present.',
            'flier.title.required_with' => 'The flier title field is required when the flier photo is present.',
            'flier.caption.required_with' => 'The flier caption field is required when the flier photo is present.',
        ]);
        $unsafeHtmlTags = $this->hasUnsafeHtmlTags($request, [
            'notes.winners.0',
            'notes.winners.1',
            'notes.winners.2',
            'notes.runners_up.1',
            'notes.runners_up.2',
            'notes.runners_up.3',
            'patreon.caption',
            'flier.caption'
        ]);
        $uniqueValidations = $this->uniqueWinnersAndRunnersup($album->id, $inputs);
        $albumHasShortBlurb = $this->validateAlbumShortBlurb($album, $inputs);
        if ($validation->fails() || count($unsafeHtmlTags) || count($uniqueValidations) || count($albumHasShortBlurb)) {
            $errors = array_merge($validation->errors()->toArray(), $unsafeHtmlTags, $uniqueValidations, $albumHasShortBlurb);
            return response()->json([
                'validation_errors' => $errors
            ], 400);
        }

        $this->mapWinnersToDbColumns($album, $inputs);

        $this->uploadFliersAndSaveToDb($inputs, $album);

        if (!$inputs['save_only']) {

            $album->live = 1;

            $this->dispatch(new UpdateMailchimpUsersDidntUpload($album));
            $this->shouldDeleteLocalCopiesIfAllPhotosAreUploaded($album);

            $nextAlbum = Album::getNextAlbum($album);
            if ($nextAlbum) {
                $nextAlbum->publicly_visible = 1;
                $nextAlbum->save();
            }

        }

        $album->save();

        AlbumSort::whereAlbumId($album->id)->delete();

        return ['success' => true];
    }

    /**
     * Gets the Album for the current week.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getRecentAlbums(Request $request)
    {
        $rules = [
            'max_upcoming_albums_count' => 'nullable|integer',
            'max_previous_albums_count' => 'nullable|integer',
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $user = $request->user();

        $currentAlbumArray = Album::currentWeek();
        if ($currentAlbumArray) {
            $currentAlbumModel = Album::whereId($currentAlbumArray['id'])->first();
            $flags = $this->addChallengePageFlags($currentAlbumModel, $user, true, true, false);
            $otherColumns = Helpers::filterByKeysModel($currentAlbumModel, [
                'short_blurb',
                'cover_photo',
                'sample_photo'
            ]);
            $currentAlbumArray = array_merge($currentAlbumArray, $flags, $otherColumns);
        }

        $upcomingAlbums = Album::getUpcomingAlbums($request->max_upcoming_albums_count ?: 1);
        $previousAlbums = Album::getPreviousAlbums($request->max_previous_albums_count ?: 1, true);

        $framersYouFollow = Album::randomFramersYouFollow($user);
        $maxPhotosCount = env('APP_HOMEPAGE_FRAMERS_COUNT');
        $framersIds = array_column($framersYouFollow, 'following_user_id');
        $framersPhotos = Album::getPhotosOfFramersById(@$previousAlbums[0]['id'] ?: 0, $framersIds, $maxPhotosCount);

        return [
            'success' => true,
            'album_current_week' => $currentAlbumArray,
            'upcoming_albums' => $upcomingAlbums,
            'previous_albums' => $previousAlbums,
            'photos_by_framers_you_follow' => $framersPhotos
        ];
    }

    /**
     * Gets the Album for the current week.
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function futureAlbums(Request $request)
    {
        $rules = [
            'max_upcoming_albums_count' => 'nullable|integer',
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json([
                'validation_errors' => $validation->errors()
            ], 400);
        }

        $defaultMaxCount = env('APP_UPCOMING_ALBUMS_COUNT');
        $upcomingAlbums = Album::getUpcomingAlbums($request->max_upcoming_albums_count ?: $defaultMaxCount);

        return $upcomingAlbums;
    }

    /**
     * @param Album   $album
     * @param Request $request
     * @return JsonResponse
     */
    public function tags(Album $album, Request $request): JsonResponse
    {
        $input = $request->query->get('input');

        $suggestTags = $this->suggestTags($album, $input);

        return response()->json($suggestTags);
    }

    /**
     * @param Album   $album
     * @param Request $request
     * @return JsonResponse
     */
    public function photographers(Album $album, Request $request): JsonResponse
    {
        $input = $request->query->get('input');
        $limit = $request->query->getInt('limit', 10);

        $suggestPhotographers = $this->suggestPhotographers($album, $input, $limit);
        $result = [];
        foreach ($suggestPhotographers as $photographer) {
            $result[] = Helpers::filterByKeysModel($photographer, [
                'id', 'name', 'handle'
            ]);
        }

        return response()->json($result);
    }
}
