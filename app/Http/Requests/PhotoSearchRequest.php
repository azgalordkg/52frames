<?php

namespace App\Http\Requests;

use App\Handlers\Photo\Filters\FilterExtraCredit;
use App\Handlers\Photo\Filters\FilterFollowing;
use App\Handlers\Photo\Filters\FilterMyPhoto;
use App\Handlers\Photo\Filters\FilterNewbies;
use App\Handlers\Photo\Filters\FilterShredAway;
use App\Handlers\Photo\Filters\FilterStaffPicks;
use App\Handlers\Photo\Search\SearchByCountry;
use App\Handlers\Photo\Search\SearchByLevel;
use App\Handlers\Photo\Search\SearchByTags;
use App\Handlers\Photo\Search\SearchByUsername;
use App\Handlers\Photo\Sorts\SortByNumber;
use App\Handlers\Photo\Sorts\SortRandom;
use Illuminate\Validation\Rule;

class PhotoSearchRequest extends ApiRequest
{
    private const SORT_DIR_ASC  = 'asc';
    private const SORT_DIR_DESC = 'desc';

    private const FIELDS_SEARCH_BY = [
        SearchByUsername::SEARCH_BY_USERNAME,
        SearchByCountry::SEARCH_BY_COUNTRY,
        SearchByLevel::SEARCH_BY_LEVEL,
        SearchByTags::SEARCH_BY_TAGS,
    ];

    private const FIELDS_FILTER_BY = [
        FilterFollowing::FILTER_FOLLOWING,
        FilterMyPhoto::FILTER_MY_PHOTO,
        FilterExtraCredit::FILTER_EXTRA_CREDIT,
        FilterStaffPicks::FILTER_STAFF_PICKS,
        FilterShredAway::FILTER_SHRED_AWAY,
        FilterNewbies::FILTER_NEWBIES,
    ];

    private const FIELDS_SORT_BY = [
        SortRandom::SORT_NEW_RANDOM,
        SortByNumber::SORT_BY_COMMENTS,
        SortByNumber::SORT_BY_LOVES,
        SortByNumber::SORT_BY_VIEWS,
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_by' => [
                'nullable',
                'string',
                Rule::in(self::FIELDS_SEARCH_BY),
            ],
            'search_query' => [
                'nullable',
                'string',
                'regex:/^[a-zA-z0-9%#-_,.\s]+$/',
            ],
            'filter_by' => [
                'nullable',
                'string',
                Rule::in(self::FIELDS_FILTER_BY),
            ],
            'sort_by' => [
                'nullable',
                'string',
                Rule::in(self::FIELDS_SORT_BY),
            ],
            'sort_dir' => [
                'nullable',
                'string',
                Rule::in([self::SORT_DIR_ASC, self::SORT_DIR_DESC]),
            ],
        ];
    }
}
