<?php

namespace App\Http\Requests;

use App\UserProfilePhoto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ProfilePhotoUploadRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimes:jpeg,png|max:' . UserProfilePhoto::newEntryMaxKbLimit(),
        ];
    }

    /**
     * @param  array  $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 400);
        }

        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
