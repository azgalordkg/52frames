<?php

namespace App\Http\Requests;

class PhotoIndexRequest extends PhotoSearchRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            'album_id' => [
                'exists:albums,id',
            ],
            'per_page' => 'nullable|integer',
        ]);

        return $rules;
    }
}
