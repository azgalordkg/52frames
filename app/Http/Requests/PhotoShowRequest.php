<?php

namespace App\Http\Requests;

class PhotoShowRequest extends PhotoSearchRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            'year' => 'integer|required_without_all:is_profile_page,is_loves_page,is_global_page',
            'week_path' => 'string|required_without_all:is_profile_page,is_loves_page,is_global_page',
            'is_profile_page' => 'nullable|boolean',
            'is_loves_page' => 'nullable|boolean',
            'is_global_page' => 'nullable|boolean',
            'photo_id' => 'nullable|integer|required_if:is_profile_page,true,1|required_if:is_loves_page,true,1|required_if:is_global_page,true,1',
        ]);

        return $rules;
    }
}
