<?php

namespace App;

class Follower extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'follower_user_id',
        'following_user_id'
    ];

    public function following()
    {
        return $this->belongsTo(User::class, 'following_user_id', 'id');
    }

    public function follower()
    {
        return $this->belongsTo(User::class, 'follower_user_id', 'id');
    }
}
