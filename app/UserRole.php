<?php

namespace App;

class UserRole extends Model
{
    protected $fillable = [
        'user_id',
        'role_id',
        'enabled',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

}
