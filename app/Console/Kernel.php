<?php

namespace App\Console;

use App\Http\ScheduleHelper;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $systemHook
     * @return void
     */
    protected function schedule(Schedule $systemHook)
    {
        ScheduleHelper::scheduleDailyEmailNotifications($systemHook);
//        ScheduleHelper::scheduleDropboxPhotoCompressFromWufooDump($systemHook);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
