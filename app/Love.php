<?php

namespace App;

class Love extends Model
{
    protected $table = 'loves';

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'user_id',
        'photo_id',
        'album_id',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

}
