<?php

namespace App;

class WufooDumpCompressPhotosList extends Model
{
    protected $table = 'wufoo_dump_compress_photos_list';

    protected $fillable = [
        'processing',
        'processing_error',
        'completed',
        'filename'
    ];
}
