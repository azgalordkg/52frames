<?php

namespace App;

class BypassAlbumUpload extends Model
{
    protected $fillable = [
        'album_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
