<?php

namespace App;

use App\Scopes\UserAccountNotDeleted;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends UserAuthenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imported_from_row_id',
        'imported_info',
        'import_claimed',
        'city_id',
        'dont_share_location',
        'firstname',
        'lastname',
        'handle',
        'display_as_handle',
        'email',
        'password',
        'how_did_hear',
    ];

    protected $appends = [
        'avatar',
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'imported_from_row_id',
        'imported_info',
        'import_claimed',
        'main_social_account_id',
        'password',
        'remember_token',
        'how_did_hear',
        'birthday',
        'delete_in_progress',
    ];

    protected $casts = [
        'imported_info' => 'array'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UserAccountNotDeleted());
    }

    public static function getDisplayText(User $user, bool $mergeData = true)
    {
        $output = [
            'handleOrId' => $user->handle ?: $user->id,
            'displayName' => $user->display_as_handle ? $user->handle : $user->name
        ];
        if ($mergeData) $output = array_merge($user->toArray(), $output);
        return $output;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getAvatarAttribute(): ?array
    {
        if ($this->mainSocialAccount) {
            return $this->mainSocialAccount->toArray();
        }

        $avatar = $this->avatar();
        if (null !== $avatar) {
            return [
                'avatar_small' => $avatar->thumbnail_filename,
                'avatar_big' => $avatar->hi_res_filename,
            ];
        }

        return null;
    }

    public function getBackgroundAttribute(): ?array
    {
        $background = $this->background();
        if (null !== $background) {
            return [
                'background_small' => $background->thumbnail_filename,
                'background_big' => $background->hi_res_filename,
            ];
        }

        return null;
    }

    public function getFollowersCountAttribute()
    {
        return $this->followers()->count();
    }

    public function getFollowingCountAttribute()
    {
        return $this->following()->count();
    }

    public static function loginRules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public static function signupRules(bool $dontRequireUnique = false)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'handle' => [
                'nullable',
                'string',
                'min:6'
            ],
            'display_as_handle' => 'boolean',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6|max:30',
            'password_confirmation' => 'required',
            'how_did_hear' => 'string',
            'newsletter' => 'accepted',
            'captcha' => 'required|captcha',
            'is_fan' => 'required|boolean',
            'terms' => 'accepted'
        ];
        if (!$dontRequireUnique) {
            $rules['email'] .= '|unique:users';
            $rules['handle'][] = 'unique:users,handle';
        }
        $rules['handle'][] = 'regex:/^[a-z][-a-z.0-9_~]+$/';
        return $rules;
    }

    public static function instagramSignupRules()
    {
        $rules = self::socialMediaSignupRules();

        return $rules;
    }

    public static function socialMediaSignupRules()
    {
        $rules = self::signupRules();
        $rules['email'] .= '|confirmed';
        $rules['email_confirmation'] = 'required';
        unset($rules['password']);
        unset($rules['password_confirmation']);
        return $rules;
    }

    public static function csvImportRules()
    {
        $rules = self::signupRules(true);
        unset($rules['password_confirmation']);
        unset($rules['password']);
        unset($rules['newsletter']);
        unset($rules['captcha']);
        unset($rules['is_fan']);
        unset($rules['terms']);
        return $rules;
    }

    public static function accountSummary(User $user): array
    {
        return [
            'id' => $user->id,
            'handle' => $user->handle,
            'display_as_handle' => $user->display_as_handle,
            'signed_manifesto' => !!$user->manifesto()->count(),
            'max_admin_points' => User::getMaxAdminPoints($user->id)
        ];
    }

    public static function buildProfile($column = 'id', $idOrShorturl)
    {
        $profile = User::where($column, $idOrShorturl)->with([
            'manifesto',
            'profile',
            'city.state',
            'city.country',
            'gears',
            'software',
            'externalPages',
        ])->first();

        if ($profile) {
            $followerInfo = self::amIFollowing($profile->id);

            $output = array_merge($profile->toArray(), $followerInfo, [
                'name' => $profile->name,
                'avatar' => $profile->avatar,
                'background' => $profile->background,
                'max_admin_points' => User::getMaxAdminPoints($profile->id)
            ], self::addFollowersCount($profile));

            return $output;
        }

        return false;
    }

    public static function addFollowersCount(User $user): array
    {
        return [
            'followers_count' => $user->followers_count,
            'following_count' => $user->following_count
        ];
    }

    public static function isSelfAccount(int $userId)
    {
        $loggedInUser = Auth::guard('api')->user();
        return $loggedInUser && $loggedInUser->id == $userId;
    }

    public static function isNewbie(int $userId)
    {
        $user = User::find($userId);
        $timezone = env('APP_TIMEZONE');
        $weeks    = env('APP_NEWBIE_WEEKS_COUNT');
        $registeredAt = Carbon::parse($user->created_at, $timezone);
        $endNewbiePeriod = $registeredAt->addWeek($weeks);
        $now = Carbon::now($timezone);

        return $endNewbiePeriod->timestamp > $now->timestamp;
    }

    public static function amIFollowing(int $userId)
    {
        $loggedInUser = Auth::guard('api')->user();

        $selfAccount = self::isSelfAccount($userId);
        $imFollowing = $loggedInUser && !$selfAccount && Follower::whereFollowerUserId($loggedInUser->id)
                ->whereFollowingUserId($userId)->first();
        $FollowsMe   = $loggedInUser && !$selfAccount && Follower::whereFollowerUserId($userId)
                ->whereFollowingUserId($loggedInUser->id)->first();

        return [
            'self_account' => $selfAccount,
            'im_following' => $imFollowing,
            'follows_me'   => $FollowsMe,
        ];
    }

    public static function isFirstPhotoUpload(int $userId)
    {
        $photo = Photo::whereUserId($userId)->first();
        return !$photo;
    }

    public static function flagDeleting(User $user): User
    {
        $user->delete_in_progress = true;
        $user->save();
        return $user;
    }

    public function userRoles(bool $onlyEnabled = true)
    {
        $query = $this->hasMany(UserRole::class, 'user_id', 'id');
        if ($onlyEnabled) $query->whereEnabled($onlyEnabled);
        return $query;
    }

    public function roles(array $keywords = [], bool $dynamic = false, bool $onlyEnabled = true)
    {
        $userRoles = $this->userRoles($onlyEnabled)->get();
        $userRoleIds = array_column($userRoles->toArray(), 'role_id');
        $roles = Role::whereDynamic($dynamic)->whereIn('id', $userRoleIds);
        if (count($keywords)) $roles->whereIn('keyword', $keywords);
        return $roles->orderBy('access_level_points', 'desc');
    }

    public function getMaxPointsByRoles(array $keywords = [], bool $dynamic = false, bool $onlyEnabled = true): int
    {
        $role = $this->roles($keywords, $dynamic, $onlyEnabled)->first();
        return $role ? $role->access_level_points : 0;
    }

    public function getMaxPointsByRolesOrHigher(array $keywords, bool $dynamic = false, bool $onlyEnabled = true): int
    {
        $maxAdminPoints = $this->getMaxPointsByRoles();
        $maxPointsByKw = $this->getMaxPointsByRoles($keywords, $dynamic, $onlyEnabled);
        return $maxAdminPoints > $maxPointsByKw ? $maxAdminPoints : $maxPointsByKw;
    }

    public function hasRolesOrHigher(array $keywords, bool $dynamic = false, bool $onlyEnabled = true): bool
    {
        return !!$this->getMaxPointsByRolesOrHigher($keywords, $dynamic, $onlyEnabled);
    }

    public static function getMaxAdminPoints($userId): int
    {
        $user = User::whereId($userId)->first();
        return $user ? $user->getMaxPointsByRoles() : 0;
    }

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function mainSocialAccount()
    {
        return $this->belongsTo(SocialAccount::class, 'main_social_account_id', 'id');
    }

    public function settings()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function manifesto()
    {
        return $this->hasOne(ManifestoAnswer::class);
    }

    public function signedManifesto()
    {
        return $this->hasOne(ManifestoAnswer::class)->select('manifesto_answers.user_id');
    }

    public function followers()
    {
        return $this->hasMany(Follower::class, 'following_user_id', 'id');
    }

    public function following()
    {
        return $this->hasMany(Follower::class, 'follower_user_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function gears()
    {
        return $this->hasMany(UserGear::class);
    }

    public function software()
    {
        return $this->hasMany(UserSoftware::class);
    }

    public function externalPages()
    {
        return $this->hasMany(UserExternalPage::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function notifs()
    {
        return $this->hasMany(Notification::class);
    }

    public function migratedFromCsvRow()
    {
        return $this->belongsTo(CsvUploadedRow::class, 'imported_from_row_id', 'id');
    }

    public function loves()
    {
        return $this->hasMany(Love::class, 'user_id', 'id');
    }

    public function profilePhotos()
    {
        return $this->hasMany(UserProfilePhoto::class, 'user_id', 'id');
    }

    private function avatar()
    {
        return $this->profilePhotos()->where('type', UserProfilePhoto::TYPE_AVATAR)->first();
    }

    private function background()
    {
        return $this->profilePhotos()->where('type', UserProfilePhoto::TYPE_BACKGROUND)->first();
    }
}
