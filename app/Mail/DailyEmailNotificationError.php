<?php

namespace App\Mail;

use App\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DailyEmailNotificationError extends Mailable
{
    use Queueable, SerializesModels;

    public $notifWithError = [];

    /**
     * Create a new message instance.
     *
     * @param array $errorDetails
     * @return void
     */
    public function __construct(array $errorDetails)
    {
        $this->notifWithError = $errorDetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $server = env('APP_URL', '');
        $userInfo = @$this->notifWithError['extra_info'] ?: null;
        $aboutUserInSubject = @$userInfo['email'] ?: '';
        return $this->view('emails.error-sending-daily-email-notif')
            ->subject("$server --> Error sending Daily Notification Email to: $aboutUserInSubject");
    }
}
