<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GenericEmailAdminBackendError extends Mailable
{
    use Queueable, SerializesModels;

    public $errors = [];
    public $script = '';
    public $server = '';

    /**
     * Create a new message instance.
     *
     * @param array $errors
     * @return void
     */
    public function __construct(array $errors = [])
    {
        $this->errors = $errors;
        $this->script = @$errors['source'] ?: '';
        $this->server = env('APP_URL', '');
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.generic-email-admin-backend-error')
            ->subject("$this->server --> Generic Backend Error: $this->script");
    }
}
