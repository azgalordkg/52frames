<?php

namespace App\Mail;

use App\AnalyticsTriggerTerm;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AnalyticsStaticAssetDidntLoad extends Mailable
{
    use Queueable, SerializesModels;

    public $triggerTerm;
    public $targetItem;
    public $inputs;

    /**
     * AnalyticsStaticAssetDidntLoad constructor.
     *
     * @param AnalyticsTriggerTerm $triggerTerm
     * @param array $inputs
     */
    public function __construct(AnalyticsTriggerTerm $triggerTerm, array $inputs)
    {
        $this->triggerTerm = $triggerTerm->load('type');
        $this->inputs = $inputs;

        $modelExists = class_exists($triggerTerm->db_model);
        $this->targetItem = $modelExists ? $triggerTerm->db_model::whereId($inputs['tracked_item_id'])->first() : null;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.analytics-asset-didnt-load')
            ->subject("Analytics: Asset Didn't Load, path: " . $this->inputs['relative_page_path']);
    }
}
