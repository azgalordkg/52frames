<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DailyEmailNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $organizedNotifs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $organizedNotifs)
    {
        $this->organizedNotifs = $organizedNotifs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $numRows = $this->organizedNotifs['total_updates'];
        $notifications = str_plural('Notification', $numRows);

        $user = User::whereId($this->organizedNotifs['for_user']['id'])->first();
        $tokenId = $user ? $user->createToken('emailUnsubLink')->token->id : '';

        $this->organizedNotifs['unsubscribeUrl'] = implode('/', [
            $this->organizedNotifs['base_url'],
            'email-links',
            'daily-email-notifs',
            'unsubscribe?tokenId=' . $tokenId
        ]);

        return $this->view('emails.daily-email-notification')->subject("$numRows $notifications waiting for You!! 🎉");
    }
}
