<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//class ResetPasswordEmail extends Mailable implements ShouldQueue
class ResetPasswordEmail extends Mailable
{
//    use Queueable, SerializesModels;
    use SerializesModels;

    public $recipient;

    public $data = [];

    /**
     * Create a new message instance.
     *
     * @param User $recipient
     * @param string $token
     */
    public function __construct(User $recipient, string $token)
    {
        $this->recipient = $recipient;
        $this->data['token'] = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Forgot your password, ay? 😕')
            ->view('emails.reset-password');
    }
}
