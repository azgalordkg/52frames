<?php

namespace App\Mail;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//class UploadErrorEmail extends Mailable implements ShouldQueue
class UploadErrorEmail extends Mailable
{
//    use Queueable, SerializesModels;
    use SerializesModels;

    public $data = [];

    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $server = env('APP_URL', '');
        return $this->view('emails.upload-error')
            ->subject('UPLOAD ERROR --server: ' . $server);
    }
}
