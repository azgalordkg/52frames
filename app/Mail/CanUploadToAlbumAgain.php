<?php

namespace App\Mail;

use App\Album;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

//class CanUploadToAlbumAgain extends Mailable implements ShouldQueue
class CanUploadToAlbumAgain extends Mailable
{
//    use Queueable, SerializesModels;
    use SerializesModels;

    public $album;
    public $recipient;

    public $data = [];

    /**
     * Create a new message instance.
     *
     * @param Album $album
     * @param User $recipient
     */
    public function __construct(Album $album, User $recipient)
    {
        $this->album = $album;
        $this->recipient = $recipient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailFromAddress = env('MAIL_HELP_FROM_ADDRESS');
        $mailFromName = env('MAIL_FROM_NAME');
        $this->from($mailFromAddress, $mailFromName);
        $this->data['contactAsLink'] = "mailto:$mailFromAddress";

        $year = $this->album->year;
        $weekNum = $this->album->week_number;
        $albumTheme = $this->album->theme_title;
        $this->data['fullName'] = $this->recipient->name;
        $this->data['weekNumber'] = $weekNum;
        $this->data['albumTheme'] = $albumTheme;
        $this->data['yearLastDigits'] = substr((string) $year, 2);
        $this->data['challengePageUrl'] = Album::generateChallengePagePublicUrl($this->album);

        return $this->subject("Upload link to Week $weekNum: $albumTheme ($year)")
            ->view('emails.can-upload-to-album-again');
    }
}
