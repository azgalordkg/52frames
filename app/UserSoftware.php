<?php

namespace App;

class UserSoftware extends Model
{
    protected $table = 'user_software';

    protected $fillable = [
        'user_id',
        'value',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
