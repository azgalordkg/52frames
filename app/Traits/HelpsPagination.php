<?php

namespace App\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

trait HelpsPagination
{
    protected function paginate(Request $request, Builder $query, int $defaultPerPageCount = null, int $pageNumber = null): LengthAwarePaginator
    {
        $pageNumber = $request->page ?: $pageNumber;
        if ($pageNumber) Paginator::currentPageResolver(function () use ($pageNumber) {
            return $pageNumber;
        });
        $perPageOverrideTerm = env('APP_PAGINATION_CLIENT_OVERRIDE_PER_PAGE_TERM');
        $perPage = $request[$perPageOverrideTerm] ?: $defaultPerPageCount ?: 3;
        return $query->paginate($perPage);
    }
}