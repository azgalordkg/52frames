<?php

namespace App\Traits;

use Illuminate\Console\Scheduling\CallbackEvent;
use Illuminate\Console\Scheduling\Schedule;

trait HelpsSchedules
{
    /**
     * @param Schedule $systemHook
     * @param array $options
     * @return CallbackEvent
     */
    protected static function formToSchedule(Schedule $systemHook, array $options): CallbackEvent
    {
        $when = is_string(@$options['when']) ? $options['when'] : null;
        $callback = is_callable(@$options['callback']) ? $options['callback'] : null;
        $noOverlapTerm = is_string(@$options['noOverlapTerm']) ? $options['noOverlapTerm'] : null;
        if (!$callback || !$when) throw new \Error('the "callback" and "when" parameters within the array, are required');
        $schedule = $systemHook->call($callback);
        if ($noOverlapTerm) $schedule->name($noOverlapTerm)->withoutOverlapping();
        if (!method_exists($schedule, $when)) throw new \Error('the "when" parameters is not valid');
        return $schedule->$when()->timezone(env('APP_TIMEZONE'));
    }
}