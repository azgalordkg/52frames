<?php

namespace App\Traits;

use App\CsvUploadedFile;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

trait HelpsCsvFileProcessing
{
    protected function readCsvFile(UploadedFile $file)
    {
        $rows = [];
        $headers = [];
        Excel::load($file, function (LaravelExcelReader $excelRows) use (&$headers, &$rows) {
            foreach ($excelRows->toArray() as $excelRow) {
                if (count($headers)) $rows[] = $excelRow;
                else foreach ($excelRow as $value) {
                    $headers[] = trim($value);
                }
            }
        });
        return ['headers' => $headers, 'rows' => $rows];
    }

    public function addSamplesToHeaders(array $headerColumns, array $rows)
    {
        $sampleCount = env('CSV_IMPORT_HEADER_SAMPLE_COUNT');
        $headersWithSamples = [];

        for ($i = 0; $i < count($headerColumns); $i++) {
            $samples = [];
            $headerValue = $headerColumns[$i];
            foreach ($rows as $row) {
                $value = trim($row[$i]);
                if ($value) {
                    if (!count($samples)) $samples[] = $value;
                    else if ($value != $samples[count($samples) - 1]) $samples[] = $value;
                    if (count($samples) >= $sampleCount) break;
                }
            }
            if (count($samples)) $headerValue .= " [" . implode(', ', $samples) . "]";
            $headersWithSamples[] = trim($headerValue);
        }

        return $headersWithSamples;
    }

    protected function getCsvFileClass(Request $request): ?string
    {
        $csvFile = $request->csv_file_id ? CsvUploadedFile::whereId($request->csv_file_id)->first() : null;
        return $csvFile ? @CsvUploadedFile::$categoryClassMapping[$csvFile->category] ?: null : null;
    }

    public function getUploadRules(Request $request): array
    {
        $class = self::getCsvFileClass($request);
        return $class ? $class::getUploadRules($request) : [];
    }

    public function specialValidation(Request $request): array
    {
        $class = self::getCsvFileClass($request);
        return $class && method_exists($class, 'specialValidation') ? $class::specialValidation($request) : [];
    }

    public function checkDuplicateColumnSelection(Request $request): array
    {
        $class = self::getCsvFileClass($request);
        return $class ? $class::checkDuplicateColumnSelection($request) : [];
    }
}