<?php

namespace App\Traits;

use Carbon\Carbon;

trait HelpsFormatTimestamps
{
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toW3cString();
    }
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->toW3cString();
    }
}