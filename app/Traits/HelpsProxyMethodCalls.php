<?php

namespace App\Traits;

trait HelpsProxyMethodCalls
{
    protected function showErrorReporting($input = E_ALL)
    {
        error_reporting($input);
    }

    protected function proxifyMethodCall(\Closure $conditionCb, \Closure $success, \Closure $failed, \Closure $init = null)
    {
        if ($init) call_user_func($init);
        $response = $backtrace = debug_backtrace();
        if (!$backtrace || !count($backtrace)) return $failed ? call_user_func($failed, $backtrace) : null;
        if ($conditionCb) $response = call_user_func($conditionCb, $backtrace);
        if (!$response) return $failed ? call_user_func($failed, $backtrace) : null;
        return $success ? call_user_func($success, $response, $backtrace) : $response;
    }
}