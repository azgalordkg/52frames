<?php

namespace App\Traits\Helpers;

use Error;

abstract class ErrorMessageBase extends Error
{
    protected $originalBacktrace;

    public function __construct($method, $primarySubj, ...$etc)
    {
        $this->originalBacktrace = debug_backtrace();
        $message = @$this->$method($primarySubj, ...$etc);
        parent::__construct($message);
    }
}