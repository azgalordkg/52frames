<?php

namespace App\Traits\Helpers;

define('PlsOverrideMethod', 'PlsOverrideMethod');

class ErrorMessage extends ErrorMessageBase
{
    public function __construct($devCode, $whichOne, ...$otherInfo)
    {
        parent::__construct(camel_case($devCode), $whichOne, ...$otherInfo);
    }

    protected function plsOverrideMethod(string $methodName, $classInput = null, string $prefixMessage = null): string
    {
        $className = is_object($classInput) ? get_class($classInput) : $classInput;
        $message = $prefixMessage ?: "Please override or create method:";
        $namespace = $className ? "$className::$methodName" : $methodName;
        return "$message $namespace()";
    }
}