<?php

namespace App\Traits\Helpers;

class HelpsLocalFileSystem
{
    /**
     * Creates the necessary folders going to the $filePath.
     *
     * @param string $filePath
     * @param int $permission
     * @param bool $recursive
     * @return string
     * @throws \Exception
     */
    public static function createPathFolders(string $filePath, int $permission = 0755, bool $recursive = true)
    {
        try {
            $baseFolder = dirname($filePath);
            mkdir($baseFolder, $permission, $recursive);
        } catch (\Exception $exception) {
            if (!is_dir($baseFolder)) throw $exception;
        }
        return $baseFolder;
    }

    public static function getFilenameOnly(string $filePath)
    {
        return preg_replace('/\.[^.\s]{3,4}$/', '', basename($filePath));
    }
}
