<?php

namespace App\Traits;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\MailchimpHelper;
use App\Services\MailchimpService;
use Illuminate\Support\Facades\Auth;
use App\Providers\Modified\Spatie\Newsletter\Newsletter;

trait HelpsLogin
{
    protected function loginUsingNormalAuth(Request $request, User $user, $accessToken)
    {
        Auth::guard('web')->login($user);
        $request->session()->put([
            'oauth_token_data' => [
                'access_token' => $accessToken,
                'created_at' => Carbon::now()
            ]
        ]);
    }

    protected function isSessionExpired(Request $request)
    {
        $oAuthData = $request->session()->get('oauth_token_data');
        if (!$oAuthData || !count($oAuthData) || !($oAuthData['access_token'] && $oAuthData['created_at'])) {
            $this->logoutUsingNormalAuth($request);
            return true;
        }
        $sessionMinutes = $oAuthData['created_at']->diffInMinutes(Carbon::now());
        $oAuthExpiry = env('LOCAL_OAUTH_EXPIRY_MINUTES');
        if ($sessionMinutes >= $oAuthExpiry) {
            $this->logoutUsingNormalAuth($request);
            return true;
        }
        return false;
    }

    protected function logoutUsingNormalAuth(Request $request)
    {
        $request->session()->remove('oauth_token_data');
        Auth::guard('web')->logout();
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse|User
     */
    protected function stopExpiredSession($request)
    {
        $user = Auth::guard('web')->user();
        $sessionExpired = $this->isSessionExpired($request);
        if (!$user || $sessionExpired) {
            return response()->json([
                'error' => 'Session Expired. Please login again.'
            ], 401);
        }
        return $user;
    }

    /**
     * @param $user
     * @param int $requiredPoints
     * @return \Illuminate\Http\JsonResponse|int
     */
    protected function checkMaxAdminPoints(User $user, int $requiredPoints = 0)
    {
        $maxAdminPoints = User::getMaxAdminPoints($user->id);
        if ($maxAdminPoints < $requiredPoints) {
            return response()->json([
                'not_enough_admin_points' => true
            ], 403);
        }
        return $maxAdminPoints;
    }

    protected function subscribeUserToMailchimpMergeOurUserId(User $user, Request $request, Newsletter $newsletter, MailchimpService $mailchimpService)
    {
        if (!MailchimpHelper::shouldProcess()) return null;

        $newsletter->subscribe($user->email, [
            'FNAME' => $user->firstname,
            'LNAME' => $user->lastname,
            'STATUSCOM' => 'NEW',
        ]);

        $mailchimpUserIdColumnName = env('MAILCHIMP_LIST_LARAVEL_ID_MERGE_FIELD_NAME', 'ID_LARAVEL');
        $mergeFields = [$mailchimpUserIdColumnName => $user->id];

        if ($request->is_fan) {
            $mergeFields = array_merge($mergeFields, ['STATUSCOM' => 'WAITING-MANIFESTO']);
        } else {
            $mergeFields = array_merge($mergeFields, ['STATUSCOM' => 'MANIFESTO-SIGNED']);
        }

        $mailchimpService->updateUserMergeFields($user->email, $mergeFields);
    }
}