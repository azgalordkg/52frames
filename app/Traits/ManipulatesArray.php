<?php

namespace App\Traits;

trait ManipulatesArray
{
    protected function deep_array_merge(array $array1, array $array2)
    {
        $merged = array_merge($array1, $array2);
        return array_map("unserialize", array_unique(array_map("serialize", $merged)));
    }

    protected function findStringFromArrayByRegex(array $list, string $regexPattern): ?string
    {
        foreach ($list as $item) {
            preg_match("~$regexPattern~", $item, $match);
            if (count($match)) return $item;
        }
        return null;
    }
}