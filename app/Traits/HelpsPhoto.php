<?php

namespace App\Traits;

use App\Album;
use App\Photo;
use App\Services\Helpers;
use App\User;

trait HelpsPhoto
{
    use HelpsAlbum;

    protected function canUserEditPhotoMeta(Album $album, $photoModelOrArray, User $user = null, int $maxAdminPoints)
    {
        if ($maxAdminPoints >= 100000) return true;
        else {
            if ($user && $user->id == $photoModelOrArray['user_id']) {
                $numOfHoursAfterDeadline = env('APP_PHOTO_OWNER_EDIT_META_AFTER_DEADLINE_HOURS');
                if ($this->isWithinAlbumStartDatePlusNumberOfHoursAfterEndDate($album, $numOfHoursAfterDeadline)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function canUserReplacePhoto(Album $album, $photoModelOrArray, User $user = null, int $maxAdminPoints)
    {
        if ($maxAdminPoints >= 200000) return true;
        else {
            if ($user && $user->id == $photoModelOrArray['user_id']) {
                if ($this->isWithinSubmitTime($album)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function hasPhotoBeenReplaced($startingPhotoModelOrArray = null, $currentPhotoModelOrArray = null)
    {
        return $startingPhotoModelOrArray && $currentPhotoModelOrArray && $startingPhotoModelOrArray['id'] != $currentPhotoModelOrArray['id'];
    }

    protected function addPhotoFlags($currentPhotoModelOrArray, $startingPhotoModelOrArray = null, Album $album = null, User $user = null, int $maxAdminPoints): array
    {
        $currentPhotoModelOrArray = is_array($currentPhotoModelOrArray) ? $currentPhotoModelOrArray : $currentPhotoModelOrArray->toArray();

        $photoViewFlags = null === $album ? [] : [
            'meta_editable' => $this->canUserEditPhotoMeta($album, $currentPhotoModelOrArray, $user, $maxAdminPoints),
            'photo_replaceable' => $this->canUserReplacePhoto($album, $currentPhotoModelOrArray, $user, $maxAdminPoints),
            'photo_replaced' => $this->hasPhotoBeenReplaced($startingPhotoModelOrArray, $currentPhotoModelOrArray),
        ];

        return array_merge($currentPhotoModelOrArray, $photoViewFlags, [
            'hearted' => Photo::hasUserHearted($currentPhotoModelOrArray['id']),
            'newbie' => User::isNewbie($currentPhotoModelOrArray['owner']['id']),
            'staff_pick' => in_array($currentPhotoModelOrArray['id'], [@$album->top1_photo_id, @$album->top2_photo_id, @$album->top3_photo_id]),
        ], User::amIFollowing($currentPhotoModelOrArray['owner']['id']));
    }

}
































