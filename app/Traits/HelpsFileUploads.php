<?php

namespace App\Traits;

use App\User;
use App\Album;
use App\Photo;
use App\Services\Debugger;
use App\Services\DropboxService;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Services\Aws\LambdaPhotoCompressionService;

trait HelpsFileUploads
{
    use HelpsPhoto, HelpsCharacterEncoding;

    protected function generatePhotoPaths(Album $album, User $user, UploadedFile $file)
    {
        $basePath = $this->getFileSystemAlbumFolder($album);

        $suggestedFilenamePattern = implode('-', [
            $user->id,
            $user->firstname,
            $user->lastname,
            $album->year,
            $album->week_number,
            $album->shorturl,
        ]);

        return $this->generatePhotoPathsOnBase($basePath, $suggestedFilenamePattern, $file);
    }

    protected function generateUserProfilePhotoPaths(User $user, UploadedFile $file, string $photoType)
    {
        $basePath = "profile-photos/$photoType";

        $suggestedFilenamePattern = implode('-', [
            $user->id,
            $user->firstname,
            $user->lastname,
        ]);

        return $this->generatePhotoPathsOnBase($basePath, $suggestedFilenamePattern, $file);
    }

    private function generatePhotoPathsOnBase(string $basePath, string $suggestedFilenamePattern, UploadedFile $file)
    {
        $filenameOnly = strtolower($suggestedFilenamePattern);
        $filenameOnly = $this->normalizeAccentedChars($filenameOnly);
        $filenameOnly = preg_replace('/\s+/', '-', $filenameOnly);
        $filenameOnly = preg_replace('/[^a-z-0-9_]/', '', $filenameOnly);

        $pathAndFilename = "$basePath/$filenameOnly";

        $extension = strtolower($file->getClientOriginalExtension());

        return [
            'relativePath' => "$pathAndFilename.$extension",
            'relativeDir' => $basePath,
            'filename' => "$filenameOnly.$extension",
            'filenameOnly' => $filenameOnly,
            'extensionOnly' => $extension
        ];
    }

    protected function getUploadsFolder()
    {
        return public_path(env('APP_UPLOADS_FOLDER', ''));
    }

    protected function folderShouldExist(string $folderName)
    {
        if (!file_exists($folderName)) {
            return mkdir($folderName, 755, true);
        }
        return true;
    }

    /* not used anymore, checkout PhotoController::store::$origResRelativeFilePath

    protected function moveUploadedFile(UploadedFile $file, $uploadsFolder, string $filename)
    {
        if ($file->getError() == UPLOAD_ERR_OK) {
            $tmp_name = $file->getPathname();
            $destination = "$uploadsFolder/$filename";
            if (file_exists($destination)) return $filename; else {
                $destFolder = dirname($destination);
                $this->folderShouldExist($destFolder);
                if (move_uploaded_file($tmp_name, $destination)) {
                    return $filename;
                }
            }
        }
        return false;
    }

    protected function moveFileToFrontend(UploadedFile $file, string $filename): array
    {
        $uploadsFolder = $this->getUploadsFolder();
        $success = $this->folderShouldExist($uploadsFolder);
        if (!$success) return ['error' => 'cant_create_uploads_folder'];

        $outputFilename = $this->moveUploadedFile($file, $uploadsFolder, $filename);
        if (!$outputFilename) return ['error' => 'cant_move_uploaded_file'];

        return ['success' => true];
    }
    */
    protected function pushPhotoToCompressionQueueAndWaitOnS3(UploadedFile $photo, string $targetRelativePathAndFilenameOnly): bool
    {
        $compressionService = new LambdaPhotoCompressionService($photo, $targetRelativePathAndFilenameOnly);

        Debugger::startMeasuringTime('--> HelpsFileUploads > pushPhotoToCompressionQueueAndWaitOnS3: uploading photo to s3');
        $compressionService->pushToQueue();
        Debugger::stopMeasuringTime('--> HelpsFileUploads > pushPhotoToCompressionQueueAndWaitOnS3: uploading photo to s3');

        Debugger::startMeasuringTime('--> HelpsFileUploads > pushPhotoToCompressionQueueAndWaitOnS3: waiting for compressed version');
        $result = $compressionService->awaitMainFileExistInCompletedFolder();
        Debugger::stopMeasuringTime('--> HelpsFileUploads > pushPhotoToCompressionQueueAndWaitOnS3: waiting for compressed version');

        return $result !== false;
    }

    protected function resizePhotoAndCopyTo(UploadedFile $photo, string $targetRelativePathAndFilenameOnly, $thumbnailSize = false)
    {
        ini_set('memory_limit', '512M');

        $width = $thumbnailSize ? env('APP_PHOTO_THUMB_SIZE') : env('APP_PHOTO_RESIZE_TO');
        $height = $thumbnailSize ? env('APP_PHOTO_THUMB_SIZE') : env('APP_PHOTO_RESIZE_TO');

        $validMaxFilesize = env('APP_PHOTO_VALID_UPLOAD_FILESIZE', 0) * 1024; //bytes

        $image = Image::make($photo->getPathname());

        $skipCompression = false;
        if (!$thumbnailSize) {
            $extension = $photo->getClientOriginalExtension();
            if (strtolower($extension) == 'jpg') {
                $photoFilesize = $photo->getClientSize(); //bytes
                $isPhotoValid = $photoFilesize && $photoFilesize <= $validMaxFilesize;
                if ($isPhotoValid) {
                    $longside = $image->width() > $image->height() ? 'width' : 'height';
                    $longsideLengthValid = $image->$longside() <= env('APP_PHOTO_RESIZE_TO', 0); //px
                    if ($longsideLengthValid) {
                        $skipCompression = true;
                    }
                }
            }
        }

        $photoQuality = 100;
        $publicFolder = $this->getUploadsFolder() . '/';
        $photoFolder = dirname($targetRelativePathAndFilenameOnly);
        $this->folderShouldExist($publicFolder . $photoFolder);
        $photoFilename = $targetRelativePathAndFilenameOnly . '.jpg';
        $photoFilePath = $publicFolder . $photoFilename;

        if ($skipCompression) {

            copy($photo->getPathname(), $photoFilePath);

        } else {

            if ($thumbnailSize) {
                $image->width() > $image->height() ? $width = null : $height = null;
            } else {
                $image->width() > $image->height() ? $height = null : $width = null;
            }
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image->backup();

            $photoQuality = 90;
            while (true) {
                $image->reset();
                $tempPhotoFilePath = $photoFilePath . '.tmp.jpg';
                $image->save($tempPhotoFilePath, $photoQuality);

                $reOpenedImage = Image::make($tempPhotoFilePath);
                $outputFilesize = $reOpenedImage->filesize();
                $isFilesizeValid = $outputFilesize <= $validMaxFilesize;
                if ($photoQuality > 50 && !$isFilesizeValid) {
                    unlink($tempPhotoFilePath);
                    $photoQuality -= 5;
                    continue;
                }
                rename($tempPhotoFilePath, $photoFilePath);
                break;
            }

        }

        return [
            'success' => true,
            'photoQuality' => $photoQuality,
            'publicUrl' => env('APP_URL') . env('APP_UPLOADS_FOLDER') . '/' . $photoFilename,
            'relativeFolderUrl' => $photoFilename,
            'localFilePath' => $photoFilePath
        ];
    }

    protected function generateS3Url(string $resourcePath = null)
    {
        $bucketName = env('AWS_S3_BUCKET');
        $urlPattern = env('AWS_S3_URL_PATTERN');
        $bucketUrl = str_replace('$bucketName', $bucketName, $urlPattern);
        return str_replace('$resourcePath', $resourcePath ?: '', $bucketUrl);
    }

    protected function generateCloudFrontUrl(string $resourcePath = null)
    {
        $cloudFrontCname = env('AWS_CLOUDFRONT_CNAME');
        $urlPattern = env('AWS_CLOUDFRONT_URL_PATTERN');
        $cloudFrontBaseUrl = str_replace('$cloudFrontCname', $cloudFrontCname, $urlPattern);
        return str_replace('$resourcePath', $resourcePath ?: '', $cloudFrontBaseUrl);
    }

    protected function decodePhotoFilePaths(string $filePath, bool $skipCheckFileExistence = false)
    {
        $uploadsFolderUrlQouted = preg_quote(env('APP_URL') . env('APP_UPLOADS_FOLDER') . '/', '/');
        $baseFilePath = preg_replace("/^$uploadsFolderUrlQouted/i", "", $filePath);

        $s3FolderUrlQouted = preg_quote($this->generateS3Url(), '/');
        $baseFilePath = preg_replace("/^$s3FolderUrlQouted/i", "", $baseFilePath);

        $cloudFrontFolderUrlQouted = preg_quote($this->generateCloudFrontUrl(), '/');
        $baseFilePath = preg_replace("/^$cloudFrontFolderUrlQouted/i", "", $baseFilePath);

        $localFilename = public_path() . env('APP_UPLOADS_FOLDER') . '/' . $baseFilePath;

        if (file_exists($localFilename) || $skipCheckFileExistence) {
            return [
                'filePath' => $baseFilePath,
                'localFilePath' => $localFilename
            ];
        }

        return false;
    }

    protected function removePhotoFromDropbox(Photo $photo)
    {
        $photoPaths = $this->decodePhotoFilePaths($photo->original_res_filename, true);
        $dropboxClient = new DropboxService();
        try {
            return $dropboxClient->delete($photoPaths['filePath']);
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    protected function removePhotoFromS3(Photo $photo)
    {
        $photoColumns = [
            'thumbnail_filename',
            'hi_res_filename',
        ];
        foreach ($photoColumns as $photoColumn) {
            $photoPaths = $this->decodePhotoFilePaths($photo->$photoColumn, true);
            try {
                Storage::disk('s3')->delete($photoPaths['filePath']);
            } catch (\Exception $exception) {

            }
        }
        return true;
    }

    protected function removePhotoFromPublicFolder(Photo $photo)
    {
        $photoColumns = [
            'original_res_filename',
            'thumbnail_filename',
            'hi_res_filename',
        ];
        foreach ($photoColumns as $photoColumn) {
            $photoPaths = $this->decodePhotoFilePaths($photo->$photoColumn, true);
            @unlink($photoPaths['localFilePath']);
        }
        return true;
    }

    protected function removeFromEverywhereExistingPhoto(Photo $photo)
    {
        $this->removePhotoFromAlbumWinners($photo);
        $this->removePhotoFromSortingTable($photo);
        $this->removePhotoFromDropbox($photo);
        $this->removePhotoFromS3($photo);
        $this->removePhotoFromPublicFolder($photo);
        $photo->delete();
    }

    protected function removeUserProfilePhoto(Photo $photo)
    {
        $this->removePhotoFromDropbox($photo);
        $this->removePhotoFromS3($photo);
        $this->removePhotoFromPublicFolder($photo);
        $photo->delete();
    }
}
