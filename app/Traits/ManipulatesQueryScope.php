<?php

namespace App\Traits;

trait ManipulatesQueryScope
{
    public function scopeWhereArray($query, $array)
    {
        foreach ($array as $field => $value) {
            $value = is_array($value) ? json_encode($value) : $value;
            $query->where($field, $value);
        }
        return $query;
    }

    public function scopeOrWhereArray($query, string $keyword, array $array)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($i === 0) {
                $query->where($keyword, $array[$i]);
            } else {
                $query->orWhere($keyword, $array[$i]);
            }
        }
        return $query;
    }

    public function scopeOrWhereColumnsArray($query, array $fieldNames, string $keyword, string $operator = '=')
    {
        for ($i = 0; $i < count($fieldNames); $i++) {
            if ($i === 0) {
                $query->where($fieldNames[$i], $operator, $keyword);
            } else {
                $query->where($fieldNames[$i], $operator, $keyword);
            }
        }
        return $query;
    }
}