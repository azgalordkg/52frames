<?php

namespace App\Traits;

use App\Album;
use App\AlbumSort;
use App\BypassAlbumUpload;
use App\Photo;
use App\Services\Helpers;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait HelpsAlbum
{
    protected function getFileSystemAlbumFolder(Album $album)
    {
        return $album->year . '/week-' . sprintf("%02d", $album->week_number) . '-' . $album->shorturl;
    }

    protected function getAlbumFolderLocalPath(Album $album)
    {
        $albumFolder = $this->getFileSystemAlbumFolder($album);
        return public_path() . env('APP_UPLOADS_FOLDER') . '/' . $albumFolder;
    }

    protected function rmdirRecursive($dir)
    {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) $this->rmdirRecursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }

    protected function isBeforeUploadSchedTime(Album $album)
    {
        $timezone = env('APP_TIMEZONE');
        $uploadGap = env('APP_UPLOAD_ALLOW_START_MINUTES');

        $startUploadTime = Carbon::parse($album->start_date, $timezone)
            ->addMinutes($uploadGap);

        $now = Carbon::now($timezone);

        return $now->timestamp < $startUploadTime->timestamp;
    }

    protected function isWithinSubmitTime(Album $album)
    {
        $timezone = env('APP_TIMEZONE');
        $uploadGracePeriod = env('APP_UPLOAD_GRACE_PERIOD_MINUTES');

        $isBeforeSched = $this->isBeforeUploadSchedTime($album);

        $endUploadTime = Carbon::parse($album->end_date, $timezone)
            ->addDays(1)
            ->addMinutes($uploadGracePeriod);

        $now = Carbon::now($timezone);

        return !$isBeforeSched && $now->timestamp <= $endUploadTime->timestamp;
    }

    protected function isAfterUploadSchedTime(Album $album)
    {
        $isBeforeSched = $this->isBeforeUploadSchedTime($album);
        $withinSubmitTime = $this->isWithinSubmitTime($album);
        return !$isBeforeSched && !$withinSubmitTime;
    }

    protected function canUserUpload(Album $album, User $user = null)
    {
        $withinUploadTime = $this->isWithinSubmitTime($album);

        $userCanUpload = false;
        if ($user) $userCanUpload = !!BypassAlbumUpload::whereUserId($user->id)->whereAlbumId($album->id)->first();

        return $withinUploadTime || $userCanUpload;
    }

    protected function isWithinAlbumStartDatePlusNumberOfHoursAfterEndDate(Album $album, int $numOfHours)
    {
        $isBeforeSched = $this->isBeforeUploadSchedTime($album);

        $timezone = env('APP_TIMEZONE');

        $endTime = Carbon::parse($album->end_date, $timezone)
            ->addDays(1)
            ->addHours($numOfHours);

        $now = Carbon::now($timezone);

        return !$isBeforeSched && $now->timestamp <= $endTime->timestamp;
    }

    /**
     * @param $albumId
     * @param int $userId
     * @return Photo|null
     */
    protected function hasUserUploadedToAlbum(int $albumId, int $userId = 0): ?array
    {
        $photoQuery = Photo::whereAlbumId($albumId)->whereUserId($userId);
        $photoUploaded = Photo::addExifData($photoQuery)->orderBy('id', 'desc')->first();
        if ($photoUploaded) $photoUploaded = $photoUploaded->toArray();
        return $photoUploaded;
    }

    protected function addChallengePageFlags(Album $album, User $loggedInUser = null, bool $dontMergeSourceArr = false, bool $getUploaded = true, bool $includeUpcomingAlbums = true, array $otherColumns = null): array
    {
        $flags = [
            'before_upload_sched' => $this->isBeforeUploadSchedTime($album),
            'allows_uploads' => $this->canUserUpload($album, $loggedInUser),
            'after_upload_sched' => $this->isAfterUploadSchedTime($album),
            'end_date_utc' => Album::getEndDateUtc($album)
        ];
        if ($getUploaded) {
            $flags['user_has_uploaded'] = $this->hasUserUploadedToAlbum($album->id, @$loggedInUser->id ?: 0);
        }
        if ($includeUpcomingAlbums) {
            $flags['upcoming_albums'] = Album::getUpcomingAlbums();
        }
        $albumArr = $album->toArray();
        if ($otherColumns) {
            $filtered = Helpers::filterByKeysModel($album, $otherColumns);
            $albumArr = array_merge($albumArr, $filtered);
        }
        return $dontMergeSourceArr ? $flags : array_merge($albumArr, $flags);
    }

    protected function suggestPhotoFilename(int $album_id = 0, string $query = '')
    {
        $finalList = [];
        $album = Album::whereId($album_id)->first();
        if ($album) {
            $albumPath = $album->year . '/week-%/';
            $dontInclude = Album::getPhotoIdsNotForDisplay($album);
            $photos = $album->photos()
                ->where('original_res_filename', 'like', "%$albumPath%$query%")
                ->whereNotIn('id', $dontInclude)
                ->get();
            foreach ($photos as $photo) {
                $finalList[] = array_merge($photo->toArray(), [
                    'filename' => $photo->filename,
                    'name' => $photo->filename
                ]);
            }
        }
        return $finalList;
    }

    /**
     * Gets the Flier Cards attached to the album.
     * @param Album $album
     * @return array
     */
    protected function getFlierPhotos(Album $album): array
    {
        $flierPhotos = [];
        $columnsMap = [
            'patreon_photo_id' => 'patreon_photo',
            'flier_photo_id' => 'flier_photo'
        ];
        foreach ($columnsMap as $columnName => $targetName) {
            if ($album->$columnName) {
                $photo = Photo::whereId($album->$columnName)->first();
                if ($photo) {
                    $photoData = array_merge($photo->toArray(), [
                        'name' => $photo->filename,
                        'filename' => $photo->filename
                    ]);
                    $flierPhotos[$targetName] = $photoData;
                }
            }
        }
        return $flierPhotos;
    }

    protected function getNewbieUploaders(): array
    {
        $numOfUploads = env('APP_NEWBIE_UPLOADS_COUNT');
        $query = Photo::groupBy('user_id')
            ->select('user_id', DB::raw('count(*) as num_uploads'))
            ->orderBy('num_uploads', 'asc')
            ->get()->toArray();
        $output = array_filter($query, function ($val) use ($numOfUploads) {
            return $val['num_uploads'] <= $numOfUploads;
        });
        return $output;
    }

    protected function generateAlbumSort(Album $album, array $updateThisArray = null)
    {
        $top3Ids = array_column(Helpers::getTopPhotos($album, 3), 'id');
        $fliersIds = array_column($this->getFlierPhotos($album), 'id');

        $newbies = array_column($this->getNewbieUploaders(), 'user_id');
        if (count($newbies)) {
            $newbiesPhotos = Photo::whereIn('user_id', $newbies)
                ->orderByRaw("FIELD(photos.id," . implode(',', $newbies) . ")")
                ->select('photos.id')
                ->whereAlbumId($album->id)
                ->get()->toArray();
            $newbieIds = array_column($newbiesPhotos, 'id');
        } else $newbieIds = [];

        $staticSort = array_values(array_unique(array_merge($top3Ids, $fliersIds, $newbieIds)));

        $allOthers = Photo::whereAlbumId($album->id)
            ->whereNotIn('id', $staticSort)
            ->select('photos.id')
            ->get()->toArray();
        $randomizedIds = array_column($allOthers, 'id');
        shuffle($randomizedIds);

        $allIds = array_values(array_merge($staticSort, $randomizedIds));

        if ($updateThisArray) $allIds = array_values(array_unique(array_merge($updateThisArray, $allIds)));

        return $allIds;
    }

    protected function getOrGenerateUserSortForAlbumId(Album $album, int $userId = null)
    {
        $personalSortQuery = AlbumSort::whereAlbumId($album->id)->whereUserId($userId)->first();
        $personalSort = $personalSortQuery ? json_decode($personalSortQuery->photo_ids) : null;

        $listChanged = false;
        if ($personalSortQuery) {
            $notInTheList = $album->photos()->whereNotIn('id', $personalSort)->count();
            if ($notInTheList) {
                $listChanged = true;
                AlbumSort::whereAlbumId($album->id)->whereUserId($userId)->delete();
            }
        }

        if (!$personalSortQuery || $listChanged) {
            $personalSort = $this->generateAlbumSort($album, $personalSort);
            AlbumSort::create([
                'album_id' => $album->id,
                'user_id' => $userId,
                'photo_ids' => json_encode($personalSort),
            ]);
        }

        return $personalSort;
    }

    protected function removePhotoFromAlbumWinners(Photo $photo)
    {
        $newValues = [];
        $albumColumns = [
            'top1_photo_id',
            'top2_photo_id',
            'top3_photo_id',
            'top4_photo_id',
            'top5_photo_id',
            'top6_photo_id',
        ];
        $album = $photo->album;
        foreach ($albumColumns as $albumColumn) {
            if ($album->$albumColumn != $photo->id) {
                array_push($newValues, $album->$albumColumn);
            }
        }
        for ($i = 0; $i < count($albumColumns); $i++) {
            $albumColumn = $albumColumns[$i];
            $album->$albumColumn = isset($newValues[$i]) ? $newValues[$i] : null;
        }
        $album->save();
    }

    protected function removePhotoFromSortingTable(Photo $photo)
    {
        $result = AlbumSort::whereAlbumId($photo->album->id)
            ->where('photo_ids', 'like', '%' . $photo->id . '%')
            ->get();
        foreach ($result as $row) {
            AlbumSort::whereAlbumId($row->album_id)->whereUserId($row->user_id)->delete();
            $ids = json_decode($row->photo_ids);
            if (count($ids) > 1) {
                if (($key = array_search($photo->id, $ids)) !== false) unset($ids[$key]);
                $newValue = json_encode(array_values($ids));
                AlbumSort::create([
                    'album_id' => $row->album_id,
                    'user_id' => $row->user_id,
                    'photo_ids' => $newValue
                ]);
            }
        }
    }

    protected function checkIfSomePhotosAreNotUploaded(Album $album)
    {
        $query = $album->photos()
            ->where('uploaded_to_dropbox', false)
            ->orWhere('uploaded_to_cdn', false);
        return $query->count();
    }

    protected function shouldDeleteLocalCopiesIfAllPhotosAreUploaded(Album $album)
    {
        if ($album->live) {
            $numPhotosNotUploaded = $this->checkIfSomePhotosAreNotUploaded($album);
            if (!$numPhotosNotUploaded) {
                $albumFolder = $this->getAlbumFolderLocalPath($album);
                @$this->rmdirRecursive($albumFolder);
            }
        }
    }

    /**
     * @param Album       $album
     * @param string|null $substring
     * @return array
     */
    protected function suggestTags(Album $album, ?string $substring): array
    {
        $photos = $album->photos()
            ->where("tags", "!=", "")
            ->where("tags", "like", "%$substring%")
            ->get();

        $albumTags = explode(",", $album->tags);

        foreach ($photos as $photo) {
            $photoTags = explode(",", $photo->tags);
            $albumTags = array_merge($albumTags, $photoTags);
        }

        $albumTags = array_unique($albumTags);

        if (null !== $substring) {
            foreach ($albumTags as $i => $tag) {
                if (false === strpos($tag, $substring)) {
                    unset($albumTags[$i]);
                }
            }
        }

        return array_values($albumTags);
    }

    /**
     * @param Album        $album
     * @param string|null  $username
     * @param integer|null $limit
     * @return Collection|array
     */
    protected function suggestPhotographers(Album $album, ?string $username, ?int $limit = 10): Collection
    {
        $photographers = User::whereNotIn('users.id', Helpers::getUploaderAccountsId())
            ->join('photos', 'users.id', '=', 'photos.user_id')
            ->where('photos.album_id', '=', $album->id)
            ->where(function ($query) use ($username) {
                $query->where('firstname', 'like', "%$username%")
                    ->orWhere('lastname', 'like', "%$username%")
                    ->orWhere(DB::raw("CONCAT(`firstname`, ' ', `lastname`)"), 'like', "%$username%")
                    ->orWhere(DB::raw("CONCAT(`lastname`, ' ', `firstname`)"), 'like', "%$username%")
                    ->orWhere('handle', 'like', "%$username%");
            })->limit($limit)->get();

        return $photographers;
    }
}
