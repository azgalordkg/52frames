<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait HelpsValidation
{
    protected function fixRequestInputs(Request $request)
    {
        $inputs = $this->forceBooleanStringsToInteger($request->all());
        return $this->forceNullStringsToNull($inputs);
    }

    protected function forceBooleanStringsToInteger(array $inputs)
    {
        foreach ($inputs as $inputKey => $inputValue) {
            if (is_array($inputValue)) {
                $inputs[$inputKey] = $this->forceBooleanStringsToInteger($inputValue);
            } else {
                if ($inputValue === 'true') {
                    $inputs[$inputKey] = 1;
                } else if ($inputValue === 'false') {
                    $inputs[$inputKey] = 0;
                }
            }
        }
        return $inputs;
    }

    protected function forceNullStringsToNull(array $inputs)
    {
        foreach ($inputs as $inputKey => $inputValue) {
            if (is_array($inputValue)) {
                $inputs[$inputKey] = $this->forceNullStringsToNull($inputValue);
            } else if ($inputValue === 'null') $inputs[$inputKey] = null;
        }
        return $inputs;
    }

    protected function hasUnsafeHtmlTags($inputsArrOrRequest, array $columns = [])
    {
        $errors = [];
        foreach ($columns as $column) {
            if (isset($inputsArrOrRequest[$column])) {
                preg_match('/<script/', $inputsArrOrRequest[$column], $matches);
                if ($matches && $matches[0]) {
                    if (!isset($errors[$column])) $errors[$column] = [];
                    $errors[$column][] = 'Scripts tags are not allowed';
                }
                preg_match('/<style/', $inputsArrOrRequest[$column], $matches);
                if ($matches && $matches[0]) {
                    if (!isset($errors[$column])) $errors[$column] = [];
                    $errors[$column][] = 'Style tags are not allowed';
                }
                preg_match('/<link/', $inputsArrOrRequest[$column], $matches);
                if ($matches && $matches[0]) {
                    if (!isset($errors[$column])) $errors[$column] = [];
                    $errors[$column][] = 'Link tags are not allowed';
                }
            }
        }
        return $errors;
    }

    protected function removeDotFromValidationErrors(array $errors): array
    {
        $output = [];
        foreach ($errors as $key => $value) {
            $output[$key] = preg_replace('/(\w)\.|_(\w)/', '$1 $2', $value);
        }
        return $output;
    }
}