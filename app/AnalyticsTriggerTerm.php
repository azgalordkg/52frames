<?php

namespace App;

class AnalyticsTriggerTerm extends Model
{
    protected $fillable = [
        'creator_id',
        'type_id',
        'term',
        'db_model',
        'description',
        'requires_login',
        'requires_unique_counting'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'creator_id',
        'db_model',
        'description',
        'requires_login',
        'requires_unique_counting',
    ];

    public static function insertRules()
    {
        return [
            'term' => 'required|string',
            'db_model' => 'required|string',
            'description' => 'nullable|string',
            'requires_login' => 'required|bool',
            'requires_unique_counting' => 'required_with:requires_login|bool'
        ];
    }

    public function type()
    {
        return $this->belongsTo(AnalyticsType::class, 'type_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function memberLogs()
    {
        return $this->hasMany(AnalyticsLog::class, 'trigger_term_id', 'id');
    }
}
