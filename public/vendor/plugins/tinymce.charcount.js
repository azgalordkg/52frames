tinymce.PluginManager.add('charactercount', function (editor) {

    var self = this;
    var max_chars = editor.settings.max_chars;

    function update() {
        editor.theme.panel.find('#charactercount').text([countText(), self.getCount()]);
    }

    function countText() {
        var text = 'Characters: {0}';
        if (max_chars) text += '/' + max_chars;
        return text;
    }

    editor.on('init', function () {
        var statusbar = editor.theme.panel && editor.theme.panel.find('#statusbar')[0];

        if (statusbar) {
            window.setTimeout(function () {
                statusbar.insert({
                    type: 'label',
                    name: 'charactercount',
                    text: [countText(), self.getCount()],
                    classes: 'charactercount',
                    disabled: editor.settings.readonly
                }, 0);

                editor.on('setcontent beforeaddundo', update);

                var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
                editor.on('keydown', function (e) {
                    if (allowedKeys.indexOf(e.keyCode) != -1) return true;
                    if (max_chars && self.getCount() + 1 > max_chars) {
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                    return true;
                });

                editor.on('keyup', function (e) {
                    update();
                });
            }, 0);
        }
    });

    if (max_chars) {
        editor.settings.paste_preprocess = function (plugin, args) {
            var currentCount = self.getCount();
            var incomingCount = self.getCount(args.content);
            if (currentCount + incomingCount > max_chars) {
                alert('Pasting this exceeds the maximum allowed number of ' + editor.settings.max_chars + ' characters.');
                args.content = '';
            }
        }
    }

    self.getCount = function (raw) {
        var tx = raw || editor.getContent({format: 'raw'});
        var decoded = decodeHtml(tx);
        var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, "").trim();
        var tc = decodedStripped.length;
        return tc;
    };

    function decodeHtml(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
});