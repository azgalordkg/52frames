appSettings.app.controller('SessionExpiredController', [
    '$rootScope', '$scope', '$timeout', '$location', 'Auth', 'FacebookAuth',
    function ($rootScope, $scope, $timeout, $location, Auth, FacebookAuth) {

        $scope.showLoginScreen = function () {
            $('#sessionExpired').modal('hide');
            setTimeout(function () {
                window.localStorage && localStorage.setItem('intendedPage', window.location.href);
                $('#loginModal').modal('show');
            }, 400);
        };

    }
]);