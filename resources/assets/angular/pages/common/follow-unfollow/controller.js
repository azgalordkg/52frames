appSettings.app.controller('FollowUnfollowController', ['$scope', 'Following', 'Variables', '$rootScope', function ($scope, Following, Variables, $rootScope) {

    $scope.variables = Variables;

    Variables.helpers.preparePopover();

    $scope.follow = function () {
        $scope.processing = true;
        Following.save({user_id: $scope.targetUserId}).then(function (response) {
            console.log('follow this person success', response);
            $scope.processing = false;
            $scope.imFollowing = true;
            $rootScope.$broadcast('follow', $scope.targetUserId, response);
        }, function (response) {
            console.log('follow this person failed', response);
            $scope.processing = false;
        });
    };

    $scope.unfollow = function () {
        $scope.processing = true;
        Following.delete({user_id: $scope.targetUserId}).then(function (response) {
            console.log('unfollow this person success', response);
            $scope.processing = false;
            $scope.imFollowing = false;
            $rootScope.$broadcast('unfollow', $scope.targetUserId, response);
        }, function (response) {
            console.log('unfollow this person success', response);
            $scope.processing = false;
        });
    };

}]);