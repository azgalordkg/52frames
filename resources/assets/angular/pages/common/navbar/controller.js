appSettings.app.controller('navController', [
  '$rootScope', '$scope', 'Auth', 'Token', '$cookies', '$timeout', '$interval', '$q', '$stateParams', 'Helpers', 'Boot', 'DirectivesControl', '$location', 'Variables', '$http',
  function ($rootScope, $scope, Auth, Token, $cookies, $timeout, $interval, $q, $stateParams, Helpers, Boot, DirectivesControl, $location, Variables, $http) {

    $scope.openLogin = function () { // Method for open LigIn tab
      $('[data-target=".log-in-tab"]').tab('show');
    };

    // $('#modal-upload-image').on('hide.bs.modal', function () {
    //   delete $rootScope.oldAlbumForUpload;
    //   $scope.currentAlbum = $rootScope.currentAlbum;
    // });

    $scope.openNavbar = function () {
      document.getElementById('wrapper').classList.toggle('open_navbar');
      if ($('#wrapper').hasClass('open_navbar')) {
        $('.row__desktop_frame .link_nav').click(function () {
          $('#wrapper').removeClass('open_navbar')
        });
      }
    };

    $scope.openSignUp = function () { // Method for open SignUp tab
      switch ($rootScope.activeStepOfManifesto) {
        case 1:
          $('#userChoice').modal('show');
          break;
        case 2:
          $('#signFramer').modal('show');
          break;
        case 3:
          $('#signAccept').modal('show');
          break;
        case 4:
          $('#createAccountPhotographer').modal('show');
          break;
        case 99:
          $('#userChoiceType').modal('show');
          break;
        default:
          $('[data-target=".sign-up-tab"]').tab('show');
          break;
      }
    };

    // Emit functions for call SignIn SignUp methods
    $rootScope.$on('CallLogInMethod', function () {
      $scope.openLogin();
    });

    $rootScope.$on('CallSignUpMethod', function () {
      $scope.openSignUp();
    });

    // Mobile View Fixes (Navbar Menu not collapsing, after User chooses an option)
    (function () {
      var $navbar = $('.navbar-collapse');
      var $navbarMenu = $('.navbar-header button');
      var $navbarButtons = $navbar.find('ul');
      var isMenuOpen = function () {
        return $navbarMenu.is(':visible') && $navbarButtons.is(':visible');
      };
      $('body').click(function (event) {
        if (!isMenuOpen()) return;
        var $element = $(event.target);
        if (!$element.parents('.navbar').length || !$element.parent().hasClass('dropdown')) {
          $navbar.collapse('hide');
        }
      });
    })();

    // functions for dropZones reaction
    function onDragOver() {
      $scope.dragOver = true;
    }

    function onDragLeave() {
      $scope.dragOver = false;
    }

    // Drag events for change view of dropZone in first step
    $('#dropZone').on('dragenter', onDragOver).on('dragleave', onDragLeave);

    // checking if user accepted message about cookies before
    const cookies = localStorage.getItem('cookies');
    if (cookies === 'true') {
      $scope.ifCookiesAccepted = true;
    }

    $('.submit-photo-btn').popover({ // for showing message if not authorized user clicked to button Submit photo
      trigger: 'focus'
    });

    $scope.helpers = Helpers;
    $rootScope.$navbar = this;
    $scope.$stateParams = $stateParams;

    var $navbar = this;
    var apiUrl = $location.$$protocol // main url of api for ajax-requests
      + '://' + $location.$$host
      + ($location.$$port ? (':' + $location.$$port + '/') : '/')
      + 'api/';

    $http.get(apiUrl + 'user', { // ajax-request for getting information about Current challenge and it's album
      headers: {
        'Authorization': 'Bearer ' + Token.get()
      }
    }).then(function (response) {
      console.log('Current album success', response.data);
      $scope.currentAlbum = response.data.albums_recent.album_current_week;
    }, function (error) {
      // if we have an error, we making another request for getting information about Current challenge and it's album
      $http.get(apiUrl + 'album/recent').then(function (res) {
        $scope.currentAlbum = res.data.album_current_week;
      })
    });

    $scope.$on('session:expired', function (event, args) {
      $('#sessionExpired').modal('show');
    });

    $scope.sendingPhotoForm = { // main Object with info for sending form with uploaded photo
      album_id: null,
      title: '',
      caption: '',
      ownership_confirmation: true, // don't remember what is it
      critique_level: "52f-cc_regular",
      tags: [],
      qualifies_extra_credit: false,
      has_nudity: false,
      camera_settings: false,
      camera_settings_answers: [],
    };

    // default value for sending upload-photo form

    $scope.uploadme = {};
    $scope.uploadme.src = '';
    $scope.file = null;
    $scope.isDropdownOpen = false;

    // default values for countries and cities drop-downs ***not used in current build

    $scope.countries = [];
    $scope.states = [];
    $scope.cities = [];

    $scope.ifSignManifestoifSignManifesto = false;

    $http.get(apiUrl + 'exif-item').then(function (res) { // ajax-request for getting info for exif items for camera settings
      console.log('Exif items success: ', res.data);
      $scope.exifItems = res.data;
    }, function (error) {
      console.log('Exif items error: ', error);
    });

    $scope.uploadFile = function (files) { // method for saving uploaded file and saving url for preview of uploaded photo
      $scope.file = files[0];
      if ($scope.file.type === 'image/jpeg' || $scope.file.type === 'image/png') {
        $scope.goToStep2();

        var reader = new FileReader();
        reader.onloadend = function () {
          $scope.uploadme.src = reader.result;
        };
        delete $rootScope.photoDeleted;
        reader.readAsDataURL($scope.file);
      } else {
        $scope.uploadme = {};
        $scope.uploadme.src = '';
        $scope.file = null;
        $scope.dragOver = false;
        alert("Oopsie daisy! Your photo must be either a .jpg or .png! :)");
      }
    };

    // function in rootScope for use uploadFile method in other directives and components

    var $body = $('body');
    var $cookieReminder = $('.cookie_reminder_holder');

    if ($cookies.get('cookie-consent')) {
      $timeout(function () {
        $cookieReminder.remove();
      }, 0);
    } else {
      $(function () {
        $cookieReminder.animate({bottom: 0});
        $body.animate({marginBottom: $cookieReminder.outerHeight() - 1});
      });
    }

    $navbar.consentCookie = function () {
      $cookies.put('cookie_reminder_holder', 1);
      $cookieReminder.animate({bottom: -$cookieReminder.outerHeight()}, function () {
        $cookieReminder.remove();
      });
      $body.animate({marginBottom: 0});
      localStorage.setItem('cookies', 'true');
    };

    $navbar.openCookieReminderMessage = function () {
      $('#cookieReminderMessageModal').modal('show');
    };

    $navbar.cookieRemiderMayNotSignup = function () {
      $('#loginModal').modal('hide');
      $timeout(function () {
        $rootScope.$broadcast('cookieConsent:removeFlags');
        alert("You don't seem to have an account.\nAll good!");
        $navbar.openCookieReminderMessage();
      }, 400);
    };

    $navbar.isSelfIdOrHandle = function (idOrHandle) {
      var user = $rootScope.user;
      return idOrHandle && user && (user.handle == idOrHandle || user.id == idOrHandle);
    };

    $navbar.getSelfHandleOrId = function (user) {
      user = user || $rootScope.user;
      return user && (user.handle || user.id);
    };

    $navbar.getSelfHandleOrName = function (user, dontUseLocalUser) {
      if (!dontUseLocalUser) user = user || $rootScope.user;
      if (user) {
        if (user.display_as_handle) return user.handle;
        else if (user.firstname) {
          var separator = '';
          var name = [user.firstname];
          if (user.lastname) {
            name.push(user.lastname);
            separator = ' ';
          }
          return name.join(separator);
        }
      } else if (dontUseLocalUser) return '';
      return false;
    };

    $navbar.logout = function (promise) {
      var deferred = promise && $q.defer();
      Auth.logout().then(function (response) {
        Boot.removeFlags();
        promise && deferred.resolve(response);
        if ($rootScope.pageRequiresAdminRole) {
          window.location.href = '/';
        } else {
          window.location.reload(false);
        }
      }, function (response) {
        promise && deferred.reject(response);
      });
      return promise && deferred.promise;
    };

    var routeToChallenge = true;
    $navbar.uploadPhotoThisWeekChallenge = function () {
      if (routeToChallenge) $location.path('/submit');
      else $('#albumChallengeHasExpiredModal').modal('show');
    };

    $navbar.currentChallengeExpired = function () {
      routeToChallenge = false;
    };

    $scope.submitPhoto = function (photoForEdit) { // method with make some modal window shown if user clicked on button 'Submit photo'
      $scope.ifFileEditing = false;
      // if ($rootScope.oldAlbumForUpload) $scope.currentAlbum = $rootScope.oldAlbumForUpload;
      if (!$scope.currentAlbum) {
        alert('No Challenges available at the moment!');
      } else {
        if (photoForEdit) {
          $scope.currentAlbum.user_has_uploaded = photoForEdit;
          $scope.onChangeUploadedPhoto();
          $scope.ifFileEditing = true;
        } else if ($scope.uploadme.src || $scope.currentAlbum.user_has_uploaded && !$rootScope.oldAlbumForUpload) { // else if user just uploaded photo but closed modal or if user has uploaded photo to current challenge we open second step with uploaded photo's preview
          $scope.onChangeUploadedPhoto();
          $('#modal-upload-image-step-2').modal('show');
        } else { // or open first step as default and make some default values empty
          $('#modal-upload-image').modal('show');
        }
      }
    };

    $scope.showManifestoModal = function () { // don't remember what is this
      $scope.ifSignManifesto = !$scope.ifSignManifesto;
    };

    $scope.goToStep2 = function ($event) { // method for going to step 2
      $scope.dragOver = false;
      if ($event) $event.preventDefault();
      $('#modal-upload-image').modal('hide');
      $('#modal-upload-image-step-3').modal('hide');
      $('#modal-upload-image-step-2').modal('show');
    };

    $scope.onChangeUploadedPhoto = function ($event) { // method for click-event of button 'Review my submission
      if ($event) $event.preventDefault();

      var uploaded = $scope.currentAlbum.user_has_uploaded;
      $scope.shouldFileBeHere = true;
      $scope.isFileChanging = true;
      $scope.uploadme.src = uploaded.thumbnail_filename;

      $scope.sendingPhotoForm = { // here we setting values about submission to main Object with data for sending to server
        album_id: uploaded.album_id,
        title: uploaded.title,
        caption: uploaded.caption,
        ownership_confirmation: true,
        critique_level: uploaded.critique_level,
        tags: [],
        qualifies_extra_credit: uploaded.qualifies_extra_credit > 0,
        // has_nudity: uploaded.has_nudity > 0,
        camera_settings: false,
        camera_settings_answers: [],
        _method: 'patch',
        location: uploaded.location,
        sensitive_content: uploaded.has_nudity > 0 || uploaded.is_graphic > 0,
        sensitive_content_value: uploaded.has_nudity ? 'has_nudity' : null || uploaded.is_graphic ? 'is_graphic' : null,
        model_consent: uploaded.model_consent > 0,
        photoCaptionLimit: uploaded.caption.length,
        photoTitleLimit: uploaded.title.length,
        photoTagsLimit: uploaded.tags.split(',').length,
        // is_graphic: uploaded.is_graphic > 0,
      };

      if ($scope.sendingPhotoForm.has_nudity) $scope.sendingPhotoForm.model_consent = true;

      if (uploaded.tags.length) { // here if submission has tags, we make string from response to array
        uploaded.tags.split(',').forEach(function (tag) {
          $scope.sendingPhotoForm.tags.push(tag);
        });
      }

      if (uploaded.exif_answers.length) { // here if submission has answers about camera we making one array with all answers for main Object
        $scope.sendingPhotoForm.camera_settings = true;
        uploaded.exif_answers.forEach(function (answer) {
          $scope.sendingPhotoForm.camera_settings_answers[answer.exif_item_id] = answer.exif_member.id + '';
        });
      }

      $scope.goToStep2(); // and at the end of function we going to step 2 with all values for form
    };

    // method for change path of location and go to current Challenge page
    $scope.goToCurrentChallenge = function (album) {
      var currentAlbum = $scope.currentAlbum;
      if (currentAlbum) {
        var url = currentAlbum.year + '/week-' + currentAlbum.week_number + '-' + currentAlbum.shorturl;
        if (!album) url += '/challenge';
        $location.path('/albums/' + url);
      } else {
        alert('There are no challenges!')
      }
    };

    // method for end of timer

    var $timerSwapSource;
    $scope.timerEnded = function () {
      $scope.album.grace_period_running = true;
      var i, $sourceModals = [
        $('#uploadPhotoModal'),
        $('#editPhotoModal')
      ];
      for (i = 0; i < $sourceModals.length; i++) {
        var sourceModal = $sourceModals[i];
        if (sourceModal.is(':visible')) {
          $timerSwapSource = sourceModal;
          $timerSwapSource.modal('hide');
          setTimeout(function () {
            $('#timerEndedModal').modal('show');
          }, 400);
          break;
        }
      }
    };

    // function for getting value about uploaded photo for edit it
    function getValueAboutUploadedPhoto(user_has_uploaded) {
      $scope.currentAlbum.user_has_uploaded = user_has_uploaded;
    }

    // Method for go to all challenges page
    $scope.goToAllChallenges = function () {
      $location.path('/challenges');
    };

    // emit functions and methods;
    $rootScope.$on('CallTimerEndedMethod', function () {
      $scope.timerEnded();
    });

    $rootScope.$on('CallSubmitPhotoMethod', function ($event, photoForEdit) {
      $scope.submitPhoto(photoForEdit);
    });

    $rootScope.$on('CallUploadFileMethod', function (event, files) {
      $scope.uploadFile(files);
    });

    $rootScope.$on('CallChangeUploadedPhotoMethod', function () {
      $scope.onChangeUploadedPhoto();
    });

    $rootScope.$on('CallSignManifestoMethod', function () {
      $scope.signManifesto();
    });

    $rootScope.$on('CallUploadedPhotoFunction', function (event, user_has_uploaded) {
      getValueAboutUploadedPhoto(user_has_uploaded);
    });

    $scope.signManifesto = function () {
      switch ($rootScope.activeStepOfManifesto) {
        case 3:
          $('#signAccept').modal('show');
          break;
        case 4:
          $('#createAccountPhotographer').modal('show');
          break;
        default:
          $('#signFramer').modal('show');
          break;
      }
    }
  }
]);

