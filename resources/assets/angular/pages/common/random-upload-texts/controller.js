appSettings.app.controller('randomUploadTextsController', ['$scope', '$timeout', function ($scope, $timeout) {

    var messages = [
        'Ooh ya, drop that thang!!',
        'Woohoo, do it do it do it!!',
        'What a lovely picture!! :O',
        'Drag an’ Drop is a real thing of beauty, ain’t it?',
        'Initiating Photo Goodness Sequence!',
        'Hero Alert! ;)',
        'Ahh, today is gonna be a good day!',
        'Unclick that mouse button and let it fly!',
        'Send that little birdy home!',
        'Initiating countdown to awesomeness...!',
        'DROP THAT THANG! :)',
        'Just set ‘er down right there, ya!',
        'Ooh, that’s the perfect place to drop it!',
        'Yes, drop it right there! :)',
        'You are soooo good at this!',
        'Ninja Level Unlocked!',
        'Awesomeness alert!! ;)',
        'Now drop it drop it drop it drop it!',
        'Woohoo!! Now LET GO!!',
        'Annnddd… drop it!',
        'Photo file.. Meet your new home.',
        'Oh photo, you are going to love it here!',
        'Fly little photo, flyyyyy!'
    ];

    var generateRandomNumber = function () {
        var minimum = 0;
        var maximum = messages.length - 1;
        return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
    };

    $scope.randomText = messages[generateRandomNumber()];

}]);