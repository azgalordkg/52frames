appSettings.app.controller('footerController', ['$scope', 'Newsletter', function ($scope, Newsletter) {

    $scope.email = '';

    $scope.subscribeToNewsletter = function () {

        if (!$scope.email) return;

        $scope.email_error = '';

        Newsletter.subscribe({
            email: $scope.email
        }).then(function (response) {
            $scope.already_subscribed = false;
            $scope.subscribed = true;
        }, function (response) {
            if (response.already_subscribed) {
                $scope.already_subscribed = true;
                $scope.subscribed = true;
            } else if (response.validation_errors) {
                $scope.email_error = response.validation_errors.email;
            } else {
                console.log('failed: ', response);
            }
        });
    };

}]);