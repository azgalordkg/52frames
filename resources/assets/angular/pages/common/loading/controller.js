appSettings.app.controller('loadingController', [
    '$scope', '$timeout', 'RandomLoadingTexts',
    function ($scope, $timeout, RandomLoadingTexts) {

        var animateLoadingHolder = function ($loadingHolder) {
            $loadingHolder.fadeIn();
            $timeout(function () {
                $loadingHolder.fadeOut();
                $timeout(function () {
                    animateLoadingHolder($loadingHolder);
                }, 3000);
            }, 3000);
        };

        $timeout(function () {
            var randomText = RandomLoadingTexts.generate();
            $scope.loadingText = randomText.join(' ');
            if (!$scope.loadingHolder) return;
            var $loadingHolder = $($scope.loadingHolder[0]).hide();
            $timeout(function () {
                animateLoadingHolder($loadingHolder);
            }, 500);
        }, 0);

    }
]);