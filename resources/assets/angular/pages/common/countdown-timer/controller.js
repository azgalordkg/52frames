appSettings.app.controller('CountdownTimerController', ['$scope', '$interval', '$filter', function ($scope, $interval, $filter) {

    var utcTargetTime = new Date($scope.utcTargetTime).getTime();

    var timeUpdate = $interval(function () {

        var utcNow = new Date().getTime();
        var utcDiff = utcTargetTime - utcNow;
        var days = Math.floor(utcDiff / (1000 * 60 * 60 * 24));
        var hours = Math.floor((utcDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((utcDiff % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((utcDiff % (1000 * 60)) / 1000);

        var hoursAndMinorsFormatted = hours + "h " + minutes + "m " + seconds + "s";

        if ($scope.dayOrHrsOnly) {
            if (days) $scope.remainingTimeFormatted = days + ' day' + (days > 1 ? 's' : '');
            else $scope.remainingTimeFormatted = hoursAndMinorsFormatted
        } else {
            $scope.remainingTimeFormatted = days + "d " + hoursAndMinorsFormatted;
        }

        if (utcDiff < 0) {
            $interval.cancel(timeUpdate);
            $scope.remainingTimeFormatted = $scope.timeArrivedDisplay || '';
            if ($scope.timeArrivedCallback) $scope.timeArrivedCallback();
        }

        $scope.saveTimerDataHere = {
            utcNow: utcNow,
            utcDiff: utcDiff,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds
        };

        // values for custom counter
        $scope.days = addZero(days);
        $scope.hours = addZero(hours);
        $scope.minutes = addZero(minutes);
        $scope.seconds = addZero(seconds);

    }, 100);

    // function for adding "Zero" if number.length is less then 1.
    function addZero (value) {
        var stringValue = value.toString();
        if (stringValue.length < 2) {
            stringValue = '0' + stringValue;
        }
        return stringValue;
    }

    $scope.$on('$destroy', function () {
        $interval.cancel(timeUpdate);
    });

}]);