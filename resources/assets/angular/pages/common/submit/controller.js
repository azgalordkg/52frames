appSettings.app.controller('submitToChallengeController', ['$location', function ($location) {
    localStorage.setItem('openSubmitForm', 1);
    $location.path('/challenge');
}]);