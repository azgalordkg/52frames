appSettings.app.controller('communityGuidelinesController', ['Loading', '$scope', '$location', function (Loading, $scope, $location) {

    Loading.pageLoaded($scope);

    $('a[href="#' + $location.hash() + '"]').tab('show');

}]);