appSettings.app.controller('AllowUploadPhoto2Controller', [
  '$rootScope', '$scope', 'Auth', 'Token', '$cookies', '$timeout', '$interval', '$q', '$stateParams',
  'Helpers', 'Boot', 'DirectivesControl', '$location', 'Variables', '$http', 'SmoothScroll',
  function ($rootScope, $scope, Auth, Token, $cookies, $timeout, $interval, $q, $stateParams,
            Helpers, Boot, DirectivesControl, $location, Variables, $http, SmoothScroll) {
    // functions for dropZones reaction
    function onDragOver() {
      $scope.dragOver = true;
    }

    function onDragLeave() {
      $scope.dragOver = false;
    }

    // Drag events for change view of dropZone in first step
    $('#dropZoneAllowUpload2').on('dragenter', onDragOver).on('dragleave', onDragLeave);

    // Init for help notes and functions for hover
    $scope.onFieldMouseOver = function (index, className, block) {
      var helpNotes = $(className);
      for (var i = 0; i < helpNotes.length; i++) {
        helpNotes.eq(i).css({'display': 'none'});
      }
      if (block) {
        helpNotes.eq(index).css({'display': 'block'});
      }
    };

    (function () {
      var star = $('#stars li');
      star.on('mouseover', function () {
        var onStar = parseInt($(this).data('value'), 10);

        $(this).parent().children('li.star').each(function (e) {
          if (e < onStar) {
            $(this).addClass('hover');
          } else {
            $(this).removeClass('hover');
          }
        });

      }).on('mouseout', function () {
        $(this).parent().children('li.star').each(function (e) {
          $(this).removeClass('hover');
        });
      });

      star.on('click', function () {
        var onStar = parseInt($(this).data('value'), 10);
        var stars = $(this).parent().children('li.star');
        for (i = 0; i < stars.length; i++) {
          $(stars[i]).removeClass('selected');
        }
        for (i = 0; i < onStar; i++) {
          $(stars[i]).addClass('selected');
        }
        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        var msg = "";
        if (ratingValue > 1) {
          msg = "Thanks! You rated this " + ratingValue + " stars.";
        } else msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
        // responseMessage(msg);
      });
    })();

    // Method for check char limit of fields
    $scope.onFieldChange = function ($event) {
      $scope.sendingPhotoForm[$event.target.name] = $event.target.textLength || $event.target.selectionStart;
    };

    // init data for controller
    $scope.sendingPhotoForm = { // main Object with info for sending form with uploaded photo
      album_id: null,
      title: '',
      caption: '',
      critique_level: "52f-cc_regular",
      tags: [],
      qualifies_extra_credit: false,
      has_nudity: false,
      is_graphic: false,
      sensitive_content: false,
      camera_settings: false,
      camera_settings_answers: [],
      photoCaptionLimit: 0,
      photoTitleLimit: 0,
      photoTagsLimit: 0,
    };
    // main url of api for ajax-requests
    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';

    // init function for controller
    function init() {
      // event for open camera settings and location information
      var btn_t = document.getElementById('technical-btn');
      var btn_l = document.getElementById('location-btn');
      btn_t.onclick = function () {
        this.style.display = "none";
        document.getElementById('technical-frame').style.display = "block";
      };
      btn_l.onclick = function () {
        this.style.display = "none";
        document.getElementById('location-frame').style.display = "block";
      };
      // ajax-request for getting info for exif items for camera settings
      $http.get(apiUrl + 'exif-item').then(function (res) {
        $scope.exifItems = res.data;
      });
    }

    init();

    $scope.$on('session:expired', function (event, args) {
      $('#sessionExpired').modal('show');
    });

    // main functionality for actions with uploading photo

    $scope.onDeletePhoto = function () { // method for delete uploaded photo if user wants to reload another photo
      $scope.dragOver = false;
      $rootScope.photoDeleted = true;
      $scope.uploadme = {};
      $scope.uploadme.src = '';
      $scope.file = null;
      if ($scope.isFileChanging) $scope.shouldFileBeHere = false;
    };

    $scope.uploadFile = function (files) { // method for saving uploaded file and saving url for preview of uploaded photo
      $rootScope.$emit('CallUploadFileToAllowedAlbumMethod', files);
    };

    $scope.addNewTag = function () { // method for adding tag
      if ($scope.tagText) {
        if ($scope.tagText[0] === '#') {
          $scope.tagText = $scope.tagText.split('');
          $scope.tagText.splice(0, 1);
          $scope.tagText = $scope.tagText.join('');
        }

        $scope.tagText = $scope.tagText.split('').map(function (char) {
          return (char === ',' || char === '.') ? '' : char;
        }).join('');

        var ifHasSimilar = false;
        var regexp = new RegExp("^[a-zA-z0-9]+$");
        var ifRegexpTrue = regexp.test($scope.tagText);
        $scope.tagText = '#' + $scope.tagText;

        if ($scope.sendingPhotoForm.tags.length) { // checking if new tag is exists it tags array
          $scope.sendingPhotoForm.tags.forEach(function (tag) {
            if ($scope.tagText === tag) {
              ifHasSimilar = true;
            }
          });
        }

        if (!ifHasSimilar && $scope.tagText && ifRegexpTrue && $scope.sendingPhotoForm.photoTagsLimit < 10) { // if tag is not exists in tags array, we push it there
          $scope.sendingPhotoForm.tags.push($scope.tagText);
          $scope.sendingPhotoForm.photoTagsLimit++;
        }
        $scope.tagText = ''; // and making binding data for new tag empty
      }
    };

    $('#tagsAllowUploadTextarea').keydown(function (e) {
      if (e.which === 9) {
        e.preventDefault();
        $scope.addNewTag();
      }
    });

    $scope.keyupFunction = function ($event) { // method for check if user clicked space button for add new tag
      $event.preventDefault();
      if ($event.keyCode === 32 || $event.keyCode === 13 || $event.keyCode === 188 || $event.keyCode === 190) {
        $scope.addNewTag();
      }
    };

    $scope.deleteTag = function ($index, $event) { // if user clicked button for delete tag
      $event.preventDefault();
      $scope.sendingPhotoForm.tags.splice($index, 1);
      $scope.sendingPhotoForm.photoTagsLimit--;
    };

    // method for going to step 3
    $scope.goToStep3 = function ($event) {
      $event.preventDefault();
      $scope.errors = {};
      if ($rootScope.photoDeleted) $scope.errors.photo = 'Please, choose photo for submit.';
      if (!$scope.sendingPhotoForm.title) {
        $scope.errors.title = [];
        $scope.errors.title[0] = 'Please fill out this field.';
      }
      if (!$scope.sendingPhotoForm.caption) {
        $scope.errors.caption = [];
        $scope.errors.caption[0] = 'Please fill out this field.';
      }

      if (!$rootScope.photoDeleted && $scope.sendingPhotoForm.title && $scope.sendingPhotoForm.caption) {
        prepareFormDataForPost();
      } else SmoothScroll.toFirstError($scope.errors);
    };

    // main function for prepare values from Object to formData for send them to server
    function prepareFormDataForPost() {
      if ($scope.sendingPhotoForm.camera_settings_answers.length) $scope.sendingPhotoForm.camera_settings = true;
      // $scope.sendingPhotoForm.location = getLocation(); // old init
      $scope.sendingPhotoForm.album_id = $scope.currentAlbum.id;
      $scope.sendingPhotoForm.is_graphic = $scope.sendingPhotoForm.sensitive_content && $scope.sendingPhotoForm.sensitive_content_value === 'is_graphic';
      $scope.sendingPhotoForm.has_nudity = $scope.sendingPhotoForm.sensitive_content && $scope.sendingPhotoForm.sensitive_content_value === 'has_nudity';
      delete $scope.sendingPhotoForm.photoCaptionLimit;
      delete $scope.sendingPhotoForm.photoTitleLimit;
      delete $scope.sendingPhotoForm.photoTagsLimit;

      var formData = new FormData();
      if ($scope.file) formData.append('photo', $scope.file);
      if ($scope.sendingPhotoForm.sensitive_content) delete $scope.sendingPhotoForm.sensitive_content;

      Object.keys($scope.sendingPhotoForm).forEach(function (key) {
        if ((key === 'camera_settings_answers' || key === 'tags') && $scope.sendingPhotoForm[key].length) {
          $scope.sendingPhotoForm[key].forEach(function (cameraKey, index) {
            if (cameraKey) {
              formData.append((key + '[' + index + ']'), cameraKey);
            }
          });
        } else if (key !== 'camera_settings_answers' && key !== 'tags') {
          if ($scope.sendingPhotoForm[key] === 'undefined' || $scope.sendingPhotoForm[key] === undefined) {
            $scope.sendingPhotoForm[key] = '';
          }
          formData.append(key, $scope.sendingPhotoForm[key]);
        }
      });

      postFormData(formData);
    }

    // HelpsUploadProgress.registerTextHolder($scope, 'uploadProgress');
    function postFormData(formData) { // function with ajax-request for send formData with uploaded photo to server
      $scope.errors = {};
      var url = apiUrl + 'photo';
      $scope.processing = true;
      $scope.uploadStarted();

      $http.post(url, formData,
        {
          transformRequest: angular.identity,
          headers: {'Authorization': 'Bearer ' + Token.get(), "Content-Type": undefined},
        })
        .then(function (success) {
          console.log("Form has been submitted successfully! ", success);

          $scope.uploadEnded();
          $scope.processing = false;

          $rootScope.$emit('CallUploadedPhotoFunction', success.data.record);
          $scope.currentAlbum.user_has_uploaded = success.data.record;
          $rootScope.user_has_uploaded = success.data.record;
          $rootScope.album_for_upload = $scope.currentAlbum;

          $('#modal-allow-upload-image-2').modal('hide');

          if (!$scope.ifFileEditing) {
            $('#modal-allow-upload-image-3').modal('show');
          } else {
            $rootScope.$emit('CallInitPageFunction', {});
            $rootScope.$emit('CallFunctionForCurrentPhoto', {});
          }
        }, function (error) {
          console.log("Something went wrong: ", error.data);
          $scope.sendingPhotoForm.photoCaptionLimit = $scope.sendingPhotoForm.caption.length;
          $scope.sendingPhotoForm.photoTitleLimit = $scope.sendingPhotoForm.title.length;
          $scope.sendingPhotoForm.photoTagsLimit = $scope.sendingPhotoForm.tags.length;

          $scope.uploadEnded();
          $scope.processing = false;
          $scope.errors = error.data.validation_errors;
        });
    }

    $scope.count = 0;
    $scope.uploadStarted = function () {
      $scope.interval = setInterval(function () {
        if ($scope.count < 100) {
          $scope.count++;
        } else {
          $scope.startIntervalWithMessages();
        }
      }, 25);
    };

    $scope.startIntervalWithMessages = function() {
      var messages = [
        'Validating Inputs...',
        'Compressing Photo...',
        'Compressing Photo...',
        'Creating thumbnail...',
        'Waiting for server...',
      ];

      clearInterval($scope.interval);
      $scope.count = 0;
      $scope.messagesCount = 0;
      $scope.interval = setInterval(function () {
        if ($scope.messagesCount < messages.length) {
          $scope.message = messages[$scope.messagesCount];
          $scope.messagesCount++;
        }
      }, 1000);
    };

    // Method for check char limit of fields
    $scope.onFieldChange = function($event) {
      $scope.sendingPhotoForm[$event.target.name] = $event.target.textLength || $event.target.selectionStart;
    };

    $scope.uploadEnded = function () {
      clearInterval($scope.interval);
      $scope.message = null;
    };

    $scope.timerEnded = function () {
      $rootScope.$emit('CallTimerEndedMethod', {});
    };

    $scope.hideErrors = function (error) {
      if ($scope.errors) {
        delete $scope.errors[error];
      }
    }
  }
]);