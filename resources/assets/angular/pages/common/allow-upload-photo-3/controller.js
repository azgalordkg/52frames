appSettings.app.controller('AllowUploadPhoto3Controller', [
  '$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {
    $scope.timerEnded = function () {
      $rootScope.$emit('CallTimerEndedMethod', {});
    };

    $scope.reviewSubmission = function ($event) {
      $event.preventDefault();
      var url = '/albums/' + $rootScope.album_for_upload.year + '/week-' + $rootScope.album_for_upload.week_number + '-' + $rootScope.album_for_upload.shorturl;
      var photo = $rootScope.user_has_uploaded;

      $location.path(url + '/photo/' + (photo.owner.handle ? photo.owner.handle : photo.owner.id));
      $('#modal-allow-upload-image-3').modal('hide');
    }
  }
]);