appSettings.app.controller('AllowUploadPhotoController', [
  '$scope', '$rootScope', function ($scope, $rootScope) {

    // functions for dropZones reaction
    function onDragOver() {
      $scope.dragOver = true;
    }

    function onDragLeave() {
      $scope.dragOver = false;
    }

    // Drag events for change view of dropZone in first step
    $('#dropZoneAllowUpload').on('dragenter', onDragOver).on('dragleave', onDragLeave);

    // method for saving uploaded file and saving url for preview of uploaded photo
    $scope.uploadFile = function (files) {
      $scope.file = files[0];
      $scope.uploadme = {};
      if ($scope.file.type === 'image/jpeg' || $scope.file.type === 'image/png') {
        $scope.goToStep2();

        var reader = new FileReader();
        reader.onloadend = function () {
          $scope.uploadme.src = reader.result;
        };
        reader.readAsDataURL($scope.file);
        $rootScope.uploadme = $scope.uploadme;
        $rootScope.file = $scope.file;
        $rootScope.photoDeleted = false;
      } else {
        $scope.uploadme = {};
        $scope.uploadme.src = '';
        $scope.file = null;
        $scope.dragOver = false;
        alert("Oopsie daisy! Your photo must be either a .jpg or .png! :)");
      }
    };

    $scope.goToStep2 = function ($event) { // method for going to step 2
      $scope.dragOver = false;
      if ($event) $event.preventDefault();
      $('#modal-allow-upload-image').modal('hide');
      $('#modal-allow-upload-image-2').modal('show');
    };

    $rootScope.$on('CallUploadFileToAllowedAlbumMethod', function (event, files) {
      $scope.uploadFile(files);
    });
  }
]);