appSettings.app.controller('faqController', ['Loading', '$scope', '$location', 'SmoothScroll', function (Loading, $scope, $location, SmoothScroll) {

    Loading.pageLoaded($scope);

    $('a[href="#' + $location.hash() + '"]').tab('show');

    $scope.scrollTo = SmoothScroll.scrollTo;

    $scope.openDeleteAccountModal = function () {
        $('#deleteAccountModal').modal('show');
    };

}]);