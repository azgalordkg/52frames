appSettings.app.controller('dailyEmailNotifController', ['$scope', '$state', 'UserSettings', '$q', function ($scope, $state, UserSettings, $q) {

    $scope.goToParentUrl = function () {
        $state.go('emailLinks');
    };

    $scope.fadeBackdrop = function () {
        $('.modal-backdrop').fadeOut();
    };

    $scope.saveChanges = function (queryString) {
        $scope.processing = true;
        return UserSettings.update({
            tokenId: queryString.tokenId,
            settings: {
                notif_emails_daily_dont_receive: 1
            }
        }).then(function (response) {
            $scope.processing = false;
            return response;
        }, function (response) {
            $scope.processing = false;
            return $q.reject(response);
        });
    };

}]);