appSettings.app.controller('unsubDailyNotifController', ['$scope', '$location', function ($scope, $location) {

    $scope.$parent.$parent.saveChanges($location.search()).then(function (response) {
        console.log('new settings, saved (success)', response);
        $scope.$success = true;
    }, function (response) {
        console.log('new settings, saved (failed)', response);
        $scope.$success = false;
    });

}]);