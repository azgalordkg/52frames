appSettings.app.controller('emailLinksController', ['$scope', 'Loading', '$state', function ($scope, Loading, $state) {

    Loading.pageLoaded($scope);

    $scope.openDailyEmailNotifModal = function () {
        $state.go('emailLinks.dailyEmailNotif.unsub');
    };

}]);