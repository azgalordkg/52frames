appSettings.app.controller('CookieReminderMessageController', ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {

    $scope.consentCookie = $rootScope.$navbar.consentCookie;

    $scope.deleteAccount = function () {
        $timeout(function () {
            $('#deleteAccountModal').modal('show');
        }, 400);
    }

}]);