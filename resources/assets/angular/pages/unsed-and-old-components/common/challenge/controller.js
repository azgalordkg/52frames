appSettings.app.controller('challengeController', ['Album', '$location', function (Album, $location) {

    Album.recentAlbums().then(function (response) {
        console.log('get current challenge success: ', response);
        var album = response.album_current_week;
        if (album) $location.path('/albums/' + album.year + '/week-' + album.week_number + '-' + album.shorturl + '/challenge');
        else {
            localStorage.removeItem('openSubmitForm');
            $location.path('/albums');
        }
    }, function (response) {
        console.log('get current challenge failed: ', response);
    });

}]);