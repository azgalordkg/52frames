appSettings.app.controller('DeleteAccountController', [
    '$scope', '$rootScope', 'User', 'Auth', 'Helpers', '$timeout', 'Variables', '$cookies',
    function ($scope, $rootScope, User, Auth, Helpers, $timeout, Variables, $cookies) {

        var $deleteAccountModal = $('#deleteAccountModal');

        var reOpenModalKw = 'modal:open:deleteAccountModal';

        var cookieConsentRemoveFlags = function () {
            if (Variables.localStorage(reOpenModalKw)) {
                Variables.localStorage(reOpenModalKw, null);
                Variables.helpers.removeIntendedPage();
            }
        };

        $scope.$on('cookieConsent:removeFlags', cookieConsentRemoveFlags);

        $scope.$on('backendChecked', function () {
            if ($rootScope.resetPasswordToken) return;
            if (Variables.localStorage(reOpenModalKw)) {
                if (!$rootScope.user && Variables.localStorage('ns:resetPassword:emailSent'))
                    return $('#forgotPassEmailSentModal').modal('show');
                if (!$cookies.get('cookie-consent')) $deleteAccountModal.modal('show');
                cookieConsentRemoveFlags();
            }
        });

        $scope.setDefaultUser = function () {
            $scope.target_user = '';
            $scope.self_account = true;
            $scope.chosenSuggested = {};
            $scope.userData = angular.copy($rootScope.user);
        };

        $scope.$on('backendChecked', $scope.setDefaultUser);

        $deleteAccountModal.on('hidden.bs.modal', function (e) {
            $scope.setDefaultUser();
        });

        $scope.openLoginModal = function () {
            $timeout(function () {
                Variables.localStorage(reOpenModalKw, 1);
                Variables.helpers.rememberPage();
                $('#loginModal').modal('show');
            }, 400);
        };

        $scope.suggestUsers = function () {
            var $inputField;
            $scope.chosenSuggested = {};
            if ($scope.target_user) {
                User.suggest({
                    query: $scope.target_user
                }).then(function (response) {
                    if ($scope.dontSuggestUser) {
                        $scope.dontSuggestUser = false;
                        return;
                    }
                    $inputField = $('#target-user');
                    $inputField.data('typeahead').source = response;
                    $inputField.typeahead('lookup').focus();
                }, function () {
                });
            }
        };

        $scope.suggestionMatcher = function (user) {
            return true;
        };

        $scope.generateSuggestDisplay = function (user) {
            if (user.handle && user.handle.toLowerCase().match($scope.target_user)) return user.handle;
            if (user.email && user.email.toLowerCase().match($scope.target_user)) return user.email;
            if ((user.id + '').match($scope.target_user)) return user.id + '';
            return user.name;
        };

        $scope.autoSuggestHighlighter = function (generatedDisplay, user) {
            var imgSrc = user.avatar ? ' src="' + user.avatar.avatar_small + '"' : '';
            var handleText = user.handle ? ' (' + user.handle + ')' : '';
            return '<div class="suggestion-holder">' +
                '<img' + imgSrc + ' class="avatar">' +
                '<div class="name-holder">' +
                '<div>' + user.name + handleText + '</div>' +
                '<div class="email-holder">' + user.email + '</div>' +
                '</div>' +
                '</div>';
        };

        $scope.userSelected = function (user) {
            console.log('userSelected: ', user);
            $scope.dontSuggestUser = true;
            $scope.chosenSuggested = user;
        };

        $scope.$on('deleteUser:requested', function (event, user) {
            $scope.setDefaultUser();
            if ($scope.userData.id !== user.id && $rootScope.user.max_admin_points >= 1000000) {
                $scope.self_account = false;
                $scope.userSelected(user);
                $scope.target_user = user.id + ' (' + user.name + ')';
            }
        });

        $scope.deleteAccount = function () {
            var userData = $scope.self_account ? $scope.userData : $scope.chosenSuggested;
            console.log('$scope.self_account: ', $scope.self_account);
            console.log('userData: ', userData);
            if (!userData.id) return $scope.errors = {
                target_user: ['Please select a User account from the suggestions to delete that account.']
            };
            $scope.processing = true;
            User.delete({id: userData.id}).then(function (response) {
                $scope.processing = false;
                alert("Account has been queue for deletion.\n" +
                    "An Email will be sent when everything is then deleted from all our servers.\n" +
                    "Thank you!");
                $deleteAccountModal.modal('hide');
                $timeout(function () {
                    cookieConsentRemoveFlags();
                    var selfAccount = response.user_id === $rootScope.user.id;
                    if (selfAccount) Auth.logoutSuccess();
                    if (!Helpers.isProfilePage()) Helpers.reloadPage();
                    else Helpers.gotoPath(selfAccount ? '/get-started' : '/');
                }, 400);
            }, function (response) {
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                }
            });
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            delete $scope.errors[fieldname];
        };

    }
]);