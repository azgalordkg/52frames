appSettings.app.controller('EditPhotoModalController', ['$scope', function ($scope) {
  console.log($scope.photo, ' photo');

  var btn_t = document.getElementById('technical-btn-2');
  var btn_l = document.getElementById('location-btn-2');
  btn_t.onclick = function () {
    this.style.display = "none";
    document.getElementById('technical-frame-2').style.display = "block";
  };
  btn_l.onclick = function () {
    this.style.display = "none";
    document.getElementById('location-frame-2').style.display = "block";
  };
}]);