appSettings.app.controller('MigrationToolAlbumsTabController', ['$filter', '$scope', function ($filter, $scope) {

    $scope.subMenuData = {

        settings: {
            menuVisible: false
        },

        list: [
            {
                name: 'CSV File Management',
                pageLoaded: false,
                active: true,
                badge: 0
            },
            {
                name: 'Valid Entries',
                pageLoaded: false,
                active: false,
                badge: 0
            },
            {
                name: 'Migrated Over',
                pageLoaded: false,
                active: false,
                badge: 0
            }
        ],

        getMenu: function (menuName) {
            var result = $filter('filter')($scope.subMenuData.list, function (tab, index) {
                return tab.name === menuName;
            });
            if (!result.length) return;
            return result[0];
        },

        swapSubMenu: function (subMenu) {
            var result = $filter('filter')($scope.subMenuData.list, function (tab, index) {
                return tab.active;
            });
            if (!result.length) return;
            result[0].active = false;
            subMenu.active = true;
        }
    };

    $scope.openCsvUploadWizardModal = function () {
        $('#' + $scope.modalsConnection.uploadWizard.id).modal('show');
    };

}]);