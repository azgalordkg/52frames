appSettings.app.directive('migrationToolAlbumsTab', function () {
    return {
        scope: {
            modalsConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolAlbumsTabController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/albums/template.html'
    };
});