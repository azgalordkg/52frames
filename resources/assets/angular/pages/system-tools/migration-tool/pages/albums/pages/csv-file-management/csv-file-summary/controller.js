appSettings.app.controller('MigrationToolAlbumsTabCsvFileManagementFileSummaryController', [
    'Loading', '$scope', '$rootScope', 'Helpers', 'CsvFile', 'CsvRow', 'CsvMigrate', '$timeout', 'CsvValidationWidgets', 'Common',
    function (Loading, $scope, $rootScope, Helpers, CsvFile, CsvRow, CsvMigrate, $timeout, CsvValidationWidgets, Common) {


        // Init

        $scope.widgets = CsvValidationWidgets.init($scope, 'albums');

        $timeout(function () {
            checkStatus();
        });

        var checkStatus = function () {
            if (!$scope.csvFile) return;
            var method = $scope.csvFile.columns_selected ? 'continueValidating' : 'continueColumnsMatching';
            $scope.widgets.repos.barProgress.statesList.showProgress.active = true;
            $scope.widgets.setStateResetOthers('statuses', method);
        };


        // Admin Actions

        $scope.repickColumns = function () {
            $scope.widgets.repos.wizardButtons.statesList.continueColumnsMatching.clickHandler(true);
        };

        $scope.deleteCsv = function () {
            var ok = confirm('This will stop processing the file and permanently remove it and all its associated records so far. Confirm to delete.');
            if (!ok) return;
            CsvFile.delete({id: $scope.csvFile.id}).then(function (response) {
                console.log('delete success: ', response);
                $scope.onDelete({csvFile: $scope.csvFile});
            }, function (response) {
                console.log('delete failed: ', response);
                alert('Something went wrong, please try to refresh the page.');
            });
        };


        // Callback Hook For the opened Modal

        $scope.csvFile.updateFileUploaded = function (csvFile) {
            $scope.csvRows = {};
            $scope.csvFile.showMembers = false;
            CsvFile.get({id: csvFile.id}).then(function (response) {
                $scope.csvFile = response;
                console.log('update success: ', response);
                checkStatus();
            }, function (response) {
                console.log('update failed: ', response);
            });
        };


        // Rows Pagination

        $scope.csvRows = {};

        var resetRowsPagination = function () {
            Object.assign($scope.csvRows, {
                isFetching: false,
                tryNextPage: true,
                currentPage: 0,
                records: [],
                total: 0
            });
        };

        var pushRowsToArray = function (response) {
            $scope.csvRows.total = response.total;
            if (!response.next_page_url) $scope.csvRows.tryNextPage = false;
            for (var i = 0; i < response.data.length; i++) {
                var row = response.data[i];
                $scope.csvRows.records.push(row);
            }
        };

        var getMoreRows = function (success, failed) {
            CsvRow.get({
                page: $scope.csvRows.currentPage,
                file_id: $scope.csvFile.id,
                filter: $scope.rowsFilter
            }).then(function (response) {
                $scope.csvRows.isFetching = false;
                pushRowsToArray(response);
                success && success(response);
            }, function (response) {
                console.log('CsvRow.get() > failed: ', response);
                $scope.csvRows.isFetching = false;
                failed && failed(response);
            });
        };

        var getRowsAndPushToArray = function (success, failed) {
            if ($scope.csvRows.isFetching) return;
            if (!$scope.csvRows.tryNextPage) return;
            $scope.csvRows.isFetching = true;
            $scope.csvRows.currentPage++;
            getMoreRows(success, failed);
        };

        var automateRowsReloading = function (targetPageNum, onComplete) {
            getRowsAndPushToArray(function (response) {
                if (response.next_page_url && $scope.csvRows.currentPage < targetPageNum)
                    automateRowsReloading(targetPageNum, onComplete);
                else onComplete && onComplete();
            });
        };

        $scope.reloadingRows = false;
        $scope.refreshRowsList = function () {
            if ($scope.reloadingRows) return;
            var targetPageNum = $scope.csvRows.currentPage;
            if (!targetPageNum) return;
            resetRowsPagination();
            $scope.reloadingRows = true;
            automateRowsReloading(targetPageNum, function () {
                $scope.reloadingRows = false;
            });
        };

        $scope.rowsFilter = 'all';
        $scope.rowsFilters = {
            all: 'Show All',
            ok: 'Valid records only',
            warning: 'Warnings only',
            error: 'Errors only',
            migrated: 'Migrated records',
            waiting: 'On Validation queue'
        };
        $scope.filterRows = function (kw) {
            $scope.rowsFilter = kw;
            resetRowsPagination();
            getRowsAndPushToArray();
        };

        $scope.showHideRows = function () {
            $scope.csvFile.showMembers = !$scope.csvFile.showMembers;
            if ($scope.csvRows.records) return;
            resetRowsPagination();
            getRowsAndPushToArray();
        };

        $scope.loadMoreRows = getRowsAndPushToArray;

        $scope.migratingAllValidRecords = false;
        $scope.migrateAllValidRecords = function () {
            var rowsFilter = $scope.rowsFilter;
            $scope.migratingAllValidRecords = true;
            CsvMigrate.allValidRows({file_id: $scope.csvFile.id, no_rows: true}).then(function (response) {
                console.log('CsvMigrate > allValidRows > response (success): ', response);
                $scope.migratingAllValidRecords = false;
                var summary = $scope.csvFile.validation_summary = response.validation_summary || {};
                $scope.widgets.setStateResetOthers('barProgress', 'showProgress', [summary, summary.validated === summary.total]);
                if ($scope.rowsFilter === rowsFilter) $scope.refreshRowsList();
            }, function (response) {
                console.log('CsvMigrate > allValidRows > response (failed): ', response);
                $scope.migratingAllValidRecords = false;
            });
        };

        $scope.showErrorMessages = function (row) {
            $rootScope.$broadcast('modals:validationReport::show:csvRow', 'album', row, function (success) {
                $('#validationReport').modal('hide');
                angular.copy(success.row, row);
                var summary = $scope.csvFile.validation_summary = success.validation_summary || {};
                $scope.widgets.setStateResetOthers('barProgress', 'showProgress', [summary, summary.validated === summary.total]);
            }, function (failed) {
                console.log('migrate 1 row (failed): ', failed);
                alert('Something went wrong. Please contact one of the Admins.');
            });
            $('#validationReport').modal('show');
        };

        $scope.alreadyMigrated = function (row) {
            console.log('alreadyMigrated > row: ', row);
            var domain = Common.rTrim(window.appSettings.canonicalUrl, '/');
            var liveUrl = domain + Helpers.getWeekThemeUrl(row.album);
            var confirm = window.confirm("Row already migrated to:\n\n" + liveUrl + "\n\nWould you like to open a new tab for it now?");
            if (confirm) window.open(liveUrl, '_blank');
        };

    }
]);