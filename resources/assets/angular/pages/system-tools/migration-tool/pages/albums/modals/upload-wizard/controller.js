appSettings.app.controller('MigrationToolAlbumsPageUploadWizardController', [
    'Loading', '$scope', '$q', 'CsvFile', 'CsvColumnsSelection', '$timeout', 'WizardWindow', 'Common', 'SmoothScroll',
    function (Loading, $scope, $q, CsvFile, CsvColumnsSelection, $timeout, WizardWindow, Common, SmoothScroll) {


        // Wizard window configuration

        $scope.pagesInfo = WizardWindow.setupPages($scope, {

            guideTexts: [
                'CSV File',
                'Columns',
                'Validation',
                'Finish'
            ],

            pages: {

                csvChooseFile: {
                    guideTextIndex: 0,
                    initScopeMethod: 'csvFilePageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'csvFilePageCompleted',
                    nextPageKw: 'columnsMatching'
                },

                columnsMatching: {
                    guideTextIndex: 1,
                    prevPageKw: 'csvChooseFile',
                    initScopeMethod: 'columnsMatchingPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'columnsMatchingPageCompleted',
                    nextPageKw: 'validationInfoPage'
                },

                validationInfoPage: {
                    guideTextIndex: 2,
                    prevPageKw: 'columnsMatching',
                    initScopeMethod: 'validationPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'validationPageCompleted',
                    nextPageKw: 'lastPage'
                },

                lastPage: {
                    guideTextIndex: 3,
                    prevPageKw: 'validationInfoPage',
                    initScopeMethod: 'lastPageInit',
                    submitText: 'Finish',
                    submitScopeMethod: 'lastPageCompleted'
                }
            }

        });


        // Broadcast listener (expected from list of summaries, behind the modal)

        $scope.$on('resetAndTransitionWizardThruPagesWithVars:albums', function (event, pagesWithVars, success, failed) {
            $scope.pagesInfo.startOver();
            $scope.fromBroadcast = true;
            $timeout(function () {
                for (var currPageKw in $scope.pagesInfo.pages) {
                    if (!pagesWithVars[currPageKw]) break;
                    var vars = pagesWithVars[currPageKw].variables;
                    if (vars) {
                        for (var scopeKey in vars) {
                            $scope[scopeKey] = vars[scopeKey];
                        }
                    }
                    $scope.pagesInfo.goToNextPage();
                }
                success && success(modalElem);
            });
        });


        // Initialization

        var modalElem;
        $scope.errors = {};
        $scope.$scope = $scope;

        $timeout(function () {
            modalElem = $('#albumPage-uploadWizardModal');
            $scope.pagesInfo.startOver();
        });

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };


        // CSV File Upload Page

        $scope.csvFilePageInit = function (nextStep, abort) {
            $scope.errors = {};
            $scope.csvFile = null;
            $scope.isRepick = false;
            resetFileChosenLoading();
            $scope.numMigratedRows = 0;
            $scope.showFileInput = true;
            $scope.fromBroadcast = false;
            $scope.inputData = {file: null, category: 'albums'};
            nextStep && $timeout(nextStep);
        };

        $scope.resetFileInputField = function ($event) {
            $scope.csvFilePageInit();
            var element = $event.currentTarget;
            if (element) element.value = '';
        };

        $scope.fileChanged = function ($element) {
            var commonCb = function (errMsg) {
                $scope.errors = {file: errMsg ? [errMsg] : []};
                $scope.inputData.file = file;
                resetFileChosenLoading({
                    ok: (file && !errMsg)
                });
            };
            var chooseValidFile = 'Please choose a valid CSV file.';
            var file = $element[0].files && $element[0].files[0] || null;
            if (!file) return commonCb();
            var extension = file.name.split('.').pop();
            var matchedIndex = $.inArray(extension, ['csv']);
            commonCb(matchedIndex < 0 ? chooseValidFile : null);
        };

        $scope.csvFilePageCompleted = function () {
            $scope.errors = {};
            if ($scope.csvFile)
                return $scope.pagesInfo.goToNextPageWithoutInit();
            $scope.processing = true;
            var onComplete = function () {
                $scope.processing = false;
            };
            CsvFile.save($scope.inputData).then(function (response) {
                console.log('success: ', response);
                $scope.csvFile = response;
                onComplete();
                $scope.pagesInfo.goToNextPage(function () {
                    $scope.showFileInput = false;
                });
                updateParentScope(response);
            }, function (response) {
                console.log('failed: ', response);
                onComplete();
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };

        var updateParentScope = function (response) {
            var callbacks = $scope.modalConnection.callbacks;
            if (callbacks.newFileUploaded) callbacks.newFileUploaded({
                record: response
            });
        };

        var resetFileChosenLoading = function (input) {
            $scope.fileChosenLoading = {
                indicator: false,
                ok: false
            };
            if (!input) return;
            for (var i in input) {
                $scope.fileChosenLoading[i] = input[i];
            }
        };


        // Columns Matching Page

        var colsValidation = {

            album: {

                year: {required: true, column_id: null, value: null},
                week_number: {required: true, column_id: null, value: null},
                start_date: {required: true, column_id: null, value: null},
                end_date: {required: true, column_id: null, value: null},
                theme_title: {required: true, column_id: null, value: null},
                extra_credit_title: {required: true, column_id: null, value: null}

            }
        };

        $scope.columnsMatchingPageInit = function (nextStep, abort) {
            $scope.csvHeaderColumns = $scope.csvFile.header_columns_with_samples;
            if (!$scope.dbColumns) $scope.dbColumns = {};
            Object.assign($scope.dbColumns, colsValidation);
            $scope.showElemsThatNeedTimeout = false;
            $timeout($scope.initAfterTimeout);
            $scope.chosenColumns = {};
            nextStep && $timeout(nextStep);
        };

        $scope.initAfterTimeout = function () {
            $scope.showElemsThatNeedTimeout = true;
        };

        var getInputValue = function (subGroupOrMember, subGroup) {
            var source = 'column_id';
            if (subGroupOrMember._config) {
                var editableControlBy = subGroupOrMember._config.control_editable_by;
                var editableCtlr = editableControlBy ? subGroup[editableControlBy] : null;
                if (subGroupOrMember._config.use_manual_input || subGroupOrMember._config.type === 'checkbox' || (editableCtlr && editableCtlr.value))
                    source = 'value';
            }
            return subGroupOrMember[source];
        };

        var collectInputs = function () {
            var output = {};
            for (var mainGroup in $scope.dbColumns) {
                if (mainGroup === '_config' || mainGroup === '_extras') continue;
                if (!output[mainGroup]) output[mainGroup] = {};
                var subGroupObj = $scope.dbColumns[mainGroup];
                for (var subGroup in subGroupObj) {
                    if (subGroup === '_config' || subGroup === '_extras') continue;
                    var storage = $scope.dbColumns[mainGroup][subGroup];
                    if (storage._config && storage._config.group) {
                        for (var member in storage) {
                            if (member === '_config' || member === '_extras') continue;
                            if (!output[mainGroup][subGroup]) output[mainGroup][subGroup] = {};
                            output[mainGroup][subGroup][member] = getInputValue(storage[member], storage);
                        }
                    } else {
                        output[mainGroup][subGroup] = getInputValue(storage);
                    }
                }
            }
            return output;
        };

        $scope.columnsMatchingPageCompleted = function () {
            $scope.errors = {};
            $scope.processing = true;
            var inputs = collectInputs();
            inputs.csv_file_id = $scope.csvFile.id;
            CsvColumnsSelection.save(inputs).then(function (response) {
                console.log('success: ', response);
                $scope.processing = false;
                $scope.pagesInfo.goToNextPage(function () {
                    var callbacks = $scope.modalConnection.callbacks;
                    if (callbacks.updateFileUploaded) callbacks.updateFileUploaded($scope.csvFile);
                });
            }, function (response) {
                console.log('failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };


        // Validation Info Page

        $scope.validationPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.validationPageCompleted = function () {
            $scope.pagesInfo.goToNextPage();
        };


        // Last Page

        $scope.lastPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.lastPageCompleted = function () {
            modalElem.modal('hide');
            $timeout(function () {
                $scope.pagesInfo.startOver();
            }, 400);
        };


    }
]);