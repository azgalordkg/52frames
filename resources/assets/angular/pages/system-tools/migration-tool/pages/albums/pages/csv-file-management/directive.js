appSettings.app.directive('migrationToolAlbumsTabCsvFileManagement', function () {
    return {
        scope: {
            modalsConnection: '=',
            subMenuSettings: '=',
            subMenuSelf: '='
        },
        restrict: 'E',
        controller: 'MigrationToolAlbumsTabCsvFileManagementController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/albums/pages/csv-file-management/template.html'
    };
});