window.appSettings.app.directive("migrationToolAlbumsPageUploadWizard", function () {
    return {
        scope: {
            modalConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolAlbumsPageUploadWizardController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/albums/modals/upload-wizard/template.html'
    };
});