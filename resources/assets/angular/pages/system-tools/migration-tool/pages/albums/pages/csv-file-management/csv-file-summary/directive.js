appSettings.app.directive("migrationToolAlbumsTabCsvFileManagementUploadSummary", function () {
    return {
        scope: {
            csvFile: '=',
            onDelete: '&'
        },
        restrict: 'E',
        controller: 'MigrationToolAlbumsTabCsvFileManagementFileSummaryController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/albums/pages/csv-file-management/csv-file-summary/template.html'
    };
});