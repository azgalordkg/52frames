appSettings.app.directive("migrationToolUsersTabCsvFileManagementUploadSummary", function () {
    return {
        scope: {
            csvFile: '=',
            onDelete: '&'
        },
        restrict: 'E',
        controller: 'MigrationToolUsersTabCsvFileManagementFileSummaryController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/users/pages/csv-file-management/csv-file-summary/template.html'
    };
});