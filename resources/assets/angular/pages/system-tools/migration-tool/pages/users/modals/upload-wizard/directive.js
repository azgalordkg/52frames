window.appSettings.app.directive("migrationToolUsersPageUploadWizard", function () {
    return {
        scope: {
            modalConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolUsersPageUploadWizardController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/users/modals/upload-wizard/template.html'
    };
});