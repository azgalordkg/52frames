appSettings.app.controller('MigrationToolUsersPageUploadWizardController', [
    'Loading', '$scope', '$q', 'CsvFile', 'CsvColumnsSelection', '$timeout', 'WizardWindow', 'Common', 'SmoothScroll',
    function (Loading, $scope, $q, CsvFile, CsvColumnsSelection, $timeout, WizardWindow, Common, SmoothScroll) {


        // Wizard window configuration

        $scope.pagesInfo = WizardWindow.setupPages($scope, {

            guideTexts: [
                'CSV File',
                'Columns',
                'Validation',
                'Finish'
            ],

            pages: {

                csvChooseFile: {
                    guideTextIndex: 0,
                    initScopeMethod: 'csvFilePageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'csvFilePageCompleted',
                    nextPageKw: 'columnsMatching'
                },

                columnsMatching: {
                    guideTextIndex: 1,
                    prevPageKw: 'csvChooseFile',
                    initScopeMethod: 'columnsMatchingPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'columnsMatchingPageCompleted',
                    nextPageKw: 'validationInfoPage'
                },

                validationInfoPage: {
                    guideTextIndex: 2,
                    prevPageKw: 'columnsMatching',
                    initScopeMethod: 'validationPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'validationPageCompleted',
                    nextPageKw: 'lastPage'
                },

                lastPage: {
                    guideTextIndex: 3,
                    prevPageKw: 'validationInfoPage',
                    initScopeMethod: 'lastPageInit',
                    submitText: 'Finish',
                    submitScopeMethod: 'lastPageCompleted'
                }
            }

        });


        // Broadcast listener (expected from list of summaries, behind the modal)

        $scope.$on('resetAndTransitionWizardThruPagesWithVars:users', function (event, pagesWithVars, success, failed) {
            $scope.pagesInfo.startOver();
            $scope.fromBroadcast = true;
            $timeout(function () {
                for (var currPageKw in $scope.pagesInfo.pages) {
                    if (!pagesWithVars[currPageKw]) break;
                    var vars = pagesWithVars[currPageKw].variables;
                    if (vars) {
                        for (var scopeKey in vars) {
                            $scope[scopeKey] = vars[scopeKey];
                        }
                    }
                    $scope.pagesInfo.goToNextPage();
                }
                success && success(modalElem);
            });
        });


        // Initialization

        var modalElem;
        $scope.errors = {};
        $scope.$scope = $scope;

        $timeout(function () {
            modalElem = $('#userPage-uploadWizardModal');
            $scope.pagesInfo.startOver();
        });

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };


        // CSV File Upload Page

        $scope.csvFilePageInit = function (nextStep, abort) {
            $scope.errors = {};
            $scope.csvFile = null;
            $scope.isRepick = false;
            resetFileChosenLoading();
            $scope.numMigratedRows = 0;
            $scope.showFileInput = true;
            $scope.fromBroadcast = false;
            $scope.inputData = {file: null, category: 'users'};
            nextStep && $timeout(nextStep);
        };

        $scope.resetFileInputField = function ($event) {
            $scope.csvFilePageInit();
            var element = $event.currentTarget;
            if (element) element.value = '';
        };

        $scope.fileChanged = function ($element) {
            var commonCb = function (errMsg) {
                $scope.errors = {file: errMsg ? [errMsg] : []};
                $scope.inputData.file = file;
                resetFileChosenLoading({
                    ok: (file && !errMsg)
                });
            };
            var chooseValidFile = 'Please choose a valid CSV file.';
            var file = $element[0].files && $element[0].files[0] || null;
            if (!file) return commonCb();
            var extension = file.name.split('.').pop();
            var matchedIndex = $.inArray(extension, ['csv']);
            commonCb(matchedIndex < 0 ? chooseValidFile : null);
        };

        $scope.csvFilePageCompleted = function () {
            $scope.errors = {};
            if ($scope.csvFile)
                return $scope.pagesInfo.goToNextPageWithoutInit();
            $scope.processing = true;
            var onComplete = function () {
                $scope.processing = false;
            };
            CsvFile.save($scope.inputData).then(function (response) {
                console.log('success: ', response);
                $scope.csvFile = response;
                onComplete();
                $scope.pagesInfo.goToNextPage(function () {
                    $scope.showFileInput = false;
                });
                updateParentScope(response);
            }, function (response) {
                console.log('failed: ', response);
                onComplete();
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };

        var updateParentScope = function (response) {
            var callbacks = $scope.modalConnection.callbacks;
            if (callbacks.newFileUploaded) callbacks.newFileUploaded({
                record: response
            });
        };

        var resetFileChosenLoading = function (input) {
            $scope.fileChosenLoading = {
                indicator: false,
                ok: false
            };
            if (!input) return;
            for (var i in input) {
                $scope.fileChosenLoading[i] = input[i];
            }
        };


        // Columns Matching Page

        var colsValidation = {

            user: {

                '52f_id': {
                    _config: {alias: '52Frames ID'},
                    required: true,
                    column_id: null,
                    value: null
                },

                firstname: {_config: {alias: 'First name'}, required: true, column_id: null, value: null},
                lastname: {_config: {alias: 'Last name'}, required: true, column_id: null, value: null},
                handle: {required: false, column_id: null, value: null},
                email: {required: true, column_id: null, value: null},

                instagram: {required: true, column_id: null, value: null},
                twitter: {required: true, column_id: null, value: null},
                why: {required: true, column_id: null, value: null},

                country: {required: true, column_id: null, value: null},
                state: {required: true, column_id: null, value: null},
                city: {required: true, column_id: null, value: null},

                photography_level: {
                    _config: {
                        group: true
                    },
                    use_same_column: {
                        _config: {
                            read_only: true,
                            type: 'checkbox',
                            alias: 'These options share the same Column'
                        },
                        required: false,
                        column_id: null,
                        value: true
                    },
                    column: {
                        _config: {
                            show_with: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: null
                    },
                    beginner: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Beginner - I enjoy taking photos'
                    },
                    beginner_dslr: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            alias: 'Has DSLR',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Beginner DSLR - I shoot with a DSLR, I understand my different lenses and some camera features, but not much more'
                    },
                    intermediate: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Intermediate - I shoot with a DSLR and understand most of my settings, but still learning more.'
                    },
                    advanced: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Advanced - I understand my camera settings and will also occassionally shoot with external flashes and strobes.'
                    },
                    advanced_pro: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Advanced Professional - I understand everything about how to take a photo, as well as studio lighting and at times make money with my photography'
                    }
                },
                camera_type: {
                    _config: {
                        group: true
                    },
                    use_same_column: {
                        _config: {
                            read_only: true,
                            type: 'checkbox',
                            alias: 'These options share the same Column'
                        },
                        required: false,
                        column_id: null,
                        value: true
                    },
                    column: {
                        _config: {
                            show_with: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: null
                    },
                    dslr_entry: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            alias: 'Entry Level',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'DSLR - Entry Level'
                    },
                    dslr_pro: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            alias: 'DSLR: Pro',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'DSLR - Professional Level'
                    },
                    mirrorless: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Mirrorless'
                    },
                    point_and_shoot: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            alias: 'Non-DSLR',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Point and Shoot'
                    },
                    phone: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Phone'
                    },
                    other: {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            disable_dropdown: true,
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Other'
                    }
                }
            }
        };

        $scope.columnsMatchingPageInit = function (nextStep, abort) {
            $scope.csvHeaderColumns = $scope.csvFile.header_columns_with_samples;
            if (!$scope.dbColumns) $scope.dbColumns = {};
            Object.assign($scope.dbColumns, colsValidation);
            $scope.showElemsThatNeedTimeout = false;
            $timeout($scope.initAfterTimeout);
            $scope.chosenColumns = {};
            nextStep && $timeout(nextStep);
        };

        $scope.initAfterTimeout = function () {
            $scope.showElemsThatNeedTimeout = true;
        };

        var getInputValue = function (subGroupOrMember, subGroup) {
            var source = 'column_id';
            if (subGroupOrMember._config) {
                var editableControlBy = subGroupOrMember._config.control_editable_by;
                var editableCtlr = editableControlBy ? subGroup[editableControlBy] : null;
                if (subGroupOrMember._config.use_manual_input || subGroupOrMember._config.type === 'checkbox' || (editableCtlr && editableCtlr.value))
                    source = 'value';
            }
            return subGroupOrMember[source];
        };

        var collectInputs = function () {
            var output = {};
            for (var mainGroup in $scope.dbColumns) {
                if (mainGroup === '_config' || mainGroup === '_extras') continue;
                if (!output[mainGroup]) output[mainGroup] = {};
                var subGroupObj = $scope.dbColumns[mainGroup];
                for (var subGroup in subGroupObj) {
                    if (subGroup === '_config' || subGroup === '_extras') continue;
                    var storage = $scope.dbColumns[mainGroup][subGroup];
                    if (storage._config && storage._config.group) {
                        for (var member in storage) {
                            if (member === '_config' || member === '_extras') continue;
                            if (!output[mainGroup][subGroup]) output[mainGroup][subGroup] = {};
                            output[mainGroup][subGroup][member] = getInputValue(storage[member], storage);
                        }
                    } else {
                        output[mainGroup][subGroup] = getInputValue(storage);
                    }
                }
            }
            return output;
        };

        $scope.columnsMatchingPageCompleted = function () {
            $scope.errors = {};
            $scope.processing = true;
            var inputs = collectInputs();
            inputs.csv_file_id = $scope.csvFile.id;
            CsvColumnsSelection.save(inputs).then(function (response) {
                console.log('success: ', response);
                $scope.processing = false;
                $scope.pagesInfo.goToNextPage(function () {
                    var callbacks = $scope.modalConnection.callbacks;
                    if (callbacks.updateFileUploaded) callbacks.updateFileUploaded($scope.csvFile);
                });
            }, function (response) {
                console.log('failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };


        // Validation Info Page

        $scope.validationPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.validationPageCompleted = function () {
            $scope.pagesInfo.goToNextPage();
        };


        // Last Page

        $scope.lastPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.lastPageCompleted = function () {
            modalElem.modal('hide');
            $timeout(function () {
                $scope.pagesInfo.startOver();
            }, 400);
        };


    }
]);