appSettings.app.directive('migrationToolUsersTab', function () {
    return {
        scope: {
            modalsConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolUsersTabController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/users/template.html'
    };
});