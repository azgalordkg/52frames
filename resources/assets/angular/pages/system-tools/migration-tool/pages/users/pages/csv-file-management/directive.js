appSettings.app.directive('migrationToolUsersTabCsvFileManagement', function () {
    return {
        scope: {
            modalsConnection: '=',
            subMenuSettings: '=',
            subMenuSelf: '='
        },
        restrict: 'E',
        controller: 'MigrationToolUsersTabCsvFileManagementController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/users/pages/csv-file-management/template.html'
    };
});