appSettings.app.directive('migrationToolPhotosTab', function () {
    return {
        scope: {
            modalsConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolPhotosTabController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/photos/template.html'
    };
});