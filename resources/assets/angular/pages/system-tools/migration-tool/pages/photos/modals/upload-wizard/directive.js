window.appSettings.app.directive("migrationToolPhotosPageUploadWizard", function () {
    return {
        scope: {
            modalConnection: '='
        },
        restrict: 'E',
        controller: 'MigrationToolPhotosPageUploadWizardController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/photos/modals/upload-wizard/template.html'
    };
});