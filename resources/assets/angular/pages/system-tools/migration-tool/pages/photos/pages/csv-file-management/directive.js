appSettings.app.directive('migrationToolPhotosTabCsvFileManagement', function () {
    return {
        scope: {
            modalsConnection: '=',
            subMenuSettings: '=',
            subMenuSelf: '='
        },
        restrict: 'E',
        controller: 'MigrationToolPhotosTabCsvFileManagementController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/photos/pages/csv-file-management/template.html'
    };
});