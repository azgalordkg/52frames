appSettings.app.controller('MigrationToolPhotosTabCsvFileManagementController', [
    'Loading', '$scope', '$rootScope', 'Helpers', 'CsvFile', '$timeout', '$filter',
    function (Loading, $scope, $rootScope, Helpers, CsvFile, $timeout, $filter) {


        // Pagination

        $scope.csvFiles = {};

        var updatePageTotalDisplay = function (total) {
            $scope.subMenuSettings.menuVisible = !!total;
            $scope.subMenuSelf.badge = total;
            $scope.csvFiles.total = total;
            return total;
        };

        var resetPagination = function () {
            Object.assign($scope.csvFiles, {
                total: updatePageTotalDisplay(0),
                isFetching: false,
                tryNextPage: true,
                currentPage: 0,
                ready: false,
                records: []
            });
        };

        var pushRowsToArray = function (response) {
            updatePageTotalDisplay(response.total);
            if (!response.next_page_url) $scope.csvFiles.tryNextPage = false;
            for (var i = 0; i < response.data.length; i++) {
                var row = response.data[i];
                $scope.csvFiles.records.push(row);
            }
        };

        var getMoreRows = function (success, failed) {
            CsvFile.get({
                page: $scope.csvFiles.currentPage,
                category: 'photos'
            }).then(function (response) {
                console.log('success: ', response);
                $scope.csvFiles.isFetching = false;
                pushRowsToArray(response);
                success && success();
            }, function (response) {
                console.log('failed: ', response);
                $scope.csvFiles.isFetching = false;
                failed && failed();
            });
        };

        var getRowsAndPushToArray = function (success, failed) {
            if ($scope.csvFiles.isFetching) return;
            if (!$scope.csvFiles.tryNextPage) return;
            $scope.csvFiles.isFetching = true;
            $scope.csvFiles.currentPage++;
            getMoreRows(success, failed);
        };


        // Initialization

        $timeout(function () {
            resetPagination();
            getRowsAndPushToArray(function () {
                $scope.subMenuSelf.pageLoaded = true;
            });
        });


        // Hooks

        $scope.modalsConnection.uploadWizard.callbacks.newFileUploaded = function (wizardUpload) {
            $scope.csvFiles.records.unshift(wizardUpload.record);
            updatePageTotalDisplay($scope.csvFiles.total + 1);
        };

        $scope.modalsConnection.uploadWizard.callbacks.updateFileUploaded = function (csvFile) {
            var localCopy = $filter('filter')($scope.csvFiles.records, function (record, index) {
                return record.id === csvFile.id;
            });
            if (!localCopy.length || !localCopy[0].updateFileUploaded) return;
            localCopy[0].updateFileUploaded(localCopy[0]);
        };


        // Local function that will be called by children

        $scope.fileDeleted = function (csvFile) {
            $scope.csvFiles.records = $filter('filter')($scope.csvFiles.records, function (record, index) {
                return record.id !== csvFile.id;
            });
            updatePageTotalDisplay($scope.csvFiles.total - 1);
        };

    }
]);