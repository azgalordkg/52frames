appSettings.app.directive("migrationToolPhotosTabCsvFileManagementUploadSummary", function () {
    return {
        scope: {
            csvFile: '=',
            onDelete: '&'
        },
        restrict: 'E',
        controller: 'MigrationToolPhotosTabCsvFileManagementFileSummaryController',
        templateUrl: 'angular/pages/system-tools/migration-tool/pages/photos/pages/csv-file-management/csv-file-summary/template.html'
    };
});