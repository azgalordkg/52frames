appSettings.app.controller('MigrationToolPhotosPageUploadWizardController', [
    'Loading', '$scope', '$q', 'CsvFile', 'CsvColumnsSelection', '$timeout', 'WizardWindow', 'Common', 'SmoothScroll',
    function (Loading, $scope, $q, CsvFile, CsvColumnsSelection, $timeout, WizardWindow, Common, SmoothScroll) {


        // Wizard window configuration

        $scope.pagesInfo = WizardWindow.setupPages($scope, {

            guideTexts: [
                'CSV File',
                'Columns',
                'Validation',
                'Finish'
            ],

            pages: {

                csvChooseFile: {
                    guideTextIndex: 0,
                    initScopeMethod: 'csvFilePageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'csvFilePageCompleted',
                    nextPageKw: 'columnsMatching'
                },

                columnsMatching: {
                    guideTextIndex: 1,
                    prevPageKw: 'csvChooseFile',
                    initScopeMethod: 'columnsMatchingPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'columnsMatchingPageCompleted',
                    nextPageKw: 'validationInfoPage'
                },

                validationInfoPage: {
                    guideTextIndex: 2,
                    prevPageKw: 'columnsMatching',
                    initScopeMethod: 'validationPageInit',
                    submitText: 'Next',
                    submitScopeMethod: 'validationPageCompleted',
                    nextPageKw: 'lastPage'
                },

                lastPage: {
                    guideTextIndex: 3,
                    prevPageKw: 'validationInfoPage',
                    initScopeMethod: 'lastPageInit',
                    submitText: 'Finish',
                    submitScopeMethod: 'lastPageCompleted'
                }
            }

        });


        // Broadcast listener (expected from list of summaries, behind the modal)

        $scope.$on('resetAndTransitionWizardThruPagesWithVars:photos', function (event, pagesWithVars, success, failed) {
            $scope.pagesInfo.startOver();
            $scope.fromBroadcast = true;
            $timeout(function () {
                for (var currPageKw in $scope.pagesInfo.pages) {
                    if (!pagesWithVars[currPageKw]) break;
                    var vars = pagesWithVars[currPageKw].variables;
                    if (vars) {
                        for (var scopeKey in vars) {
                            $scope[scopeKey] = vars[scopeKey];
                        }
                    }
                    $scope.pagesInfo.goToNextPage();
                }
                success && success(modalElem);
            });
        });


        // Initialization

        var modalElem;
        $scope.errors = {};
        $scope.$scope = $scope;

        $timeout(function () {
            modalElem = $('#photosPage-uploadWizardModal');
            $scope.pagesInfo.startOver();
        });

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };


        // CSV File Upload Page

        $scope.csvFilePageInit = function (nextStep, abort) {
            $scope.errors = {};
            $scope.csvFile = null;
            $scope.isRepick = false;
            resetFileChosenLoading();
            $scope.numMigratedRows = 0;
            $scope.showFileInput = true;
            $scope.fromBroadcast = false;
            $scope.inputData = {file: null, category: 'photos'};
            nextStep && $timeout(nextStep);
        };

        $scope.resetFileInputField = function ($event) {
            $scope.csvFilePageInit();
            var element = $event.currentTarget;
            if (element) element.value = '';
        };

        $scope.fileChanged = function ($element) {
            var commonCb = function (errMsg) {
                $scope.errors = {file: errMsg ? [errMsg] : []};
                $scope.inputData.file = file;
                resetFileChosenLoading({
                    ok: (file && !errMsg)
                });
            };
            var chooseValidFile = 'Please choose a valid CSV file.';
            var file = $element[0].files && $element[0].files[0] || null;
            if (!file) return commonCb();
            var extension = file.name.split('.').pop();
            var matchedIndex = $.inArray(extension, ['csv']);
            commonCb(matchedIndex < 0 ? chooseValidFile : null);
        };

        $scope.csvFilePageCompleted = function () {
            $scope.errors = {};
            if ($scope.csvFile)
                return $scope.pagesInfo.goToNextPageWithoutInit();
            $scope.processing = true;
            var onComplete = function () {
                $scope.processing = false;
            };
            CsvFile.save($scope.inputData).then(function (response) {
                console.log('success: ', response);
                $scope.csvFile = response;
                onComplete();
                $scope.pagesInfo.goToNextPage(function () {
                    $scope.showFileInput = false;
                });
                updateParentScope(response);
            }, function (response) {
                console.log('failed: ', response);
                onComplete();
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };

        var updateParentScope = function (response) {
            var callbacks = $scope.modalConnection.callbacks;
            if (callbacks.newFileUploaded) callbacks.newFileUploaded({
                record: response
            });
        };

        var resetFileChosenLoading = function (input) {
            $scope.fileChosenLoading = {
                indicator: false,
                ok: false
            };
            if (!input) return;
            for (var i in input) {
                $scope.fileChosenLoading[i] = input[i];
            }
        };


        // Columns Matching Page

        var colsValidation = {

            album: {

                year: {
                    _config: {
                        use_manual_input: true,
                        editable: {
                            placeholder: '2016, 2017, or 2018 (examples)'
                        }
                    },
                    required: true,
                    column_id: null,
                    value: null
                },
                week_number: {
                    _config: {
                        use_manual_input: true,
                        editable: {
                            placeholder: '1, 21, or 52 (examples)'
                        }
                    },
                    required: true,
                    column_id: null,
                    value: null
                },
                same_for_all_rows: {
                    _config: {
                        type: 'checkbox',
                        alias: 'All rows use this information'
                    },
                    required: true,
                    column_id: null,
                    value: false
                },
                _extras: {
                    note: {
                        _config: {
                            suggestion: 'Album must first exist on the Live database, before validation can begin. You may need to "Migrate" over Albums first.'
                        }
                    }
                }
            },

            photographer: {
                '52f_id': {
                    _config: {alias: '52Frames ID'},
                    required: true,
                    column_id: null,
                    value: null
                },
                email: {
                    required: true,
                    column_id: null,
                    value: null
                },
                _extras: {
                    note: {
                        _config: {
                            suggestion: 'Owner Accounts must first exist on the Live database, before imported row(s) here can be considered "valid".'
                        }
                    }
                }
            },

            photo: {
                entry_id: {required: true, column_id: null, value: null},
                title: {required: true, column_id: null, value: null},
                caption: {required: true, column_id: null, value: null},
                url: {_config: {alias: 'URL'}, required: true, column_id: null, value: null},
                screencast_critique: {
                    _config: {alias: 'Consider photo for a screencast critique'},
                    required: true,
                    column_id: null,
                    value: null
                },
                location: {required: true, column_id: null, value: null},
                tags: {required: true, column_id: null, value: null},
                shooting_and_processing: {
                    _config: {group: true, required: true},
                    external_flash: {
                        _config: {alias: 'Used External Flash'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    shutter: {
                        _config: {alias: '1" Shutter/Longer'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    aperture: {
                        _config: {alias: 'f1.8 Aperture/Wider'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    composite_edit: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    hdr: {
                        _config: {alias: 'HDR'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    shot_with_a_phone: {
                        _config: {alias: 'Shot with a Phone'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    black_and_white: {
                        _config: {alias: 'Black & White'},
                        required: true,
                        column_id: null,
                        value: null
                    }
                },
                subject_matter: {
                    _config: {group: true, required: true},
                    architecture: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    fine_art: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    street_photography: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    travel: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    action: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    humor: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    nature: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    macro: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    abstract: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    portrait: {
                        _config: {alias: 'Portrait/Self-Portrait'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    landscape: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    still_life: {
                        required: true,
                        column_id: null,
                        value: null
                    }
                },
                camera_settings: {
                    _config: {group: true, required: true},
                    shutter_speed: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    aperture: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    iso: {
                        _config: {alias: 'ISO'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    focal_length: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    camera_manufacturer: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    camera_model: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    lens: {
                        required: true,
                        column_id: null,
                        value: null
                    },
                    flash: {
                        required: true,
                        column_id: null,
                        value: null
                    }
                },
                critique_level: {
                    _config: {
                        group: true,
                        required: true
                    },
                    use_same_column: {
                        _config: {
                            read_only: true,
                            type: 'checkbox',
                            alias: 'These options share the same Column'
                        },
                        required: false,
                        column_id: null,
                        value: true
                    },
                    column: {
                        _config: {
                            show_with: 'use_same_column'
                        },
                        required: false,
                        column_id: null,
                        value: true
                    },
                    '52f-cc_regular': {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            alias: '52F-CC Regular',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: '52F-CC Regular'
                    },
                    'shred_away': {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            alias: 'SHRED AWAY!',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'SHRED AWAY! I feel no pain.'
                    },
                    'extra_sensitive': {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            alias: 'Extra sensitive',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Please be extra sensitive with your critique.'
                    },
                    'no_critique': {
                        _config: {
                            editable: {
                                placeholder: 'Expected value'
                            },
                            alias: 'Not this week, but comment',
                            control_editable_by: 'use_same_column'
                        },
                        required: true,
                        column_id: null,
                        value: 'Not interested in a critique this week, but feel free to comment! :)'
                    }
                },
                contains_graphic_content: {
                    _config: {group: true, required: true},
                    'has_nudity': {
                        _config: {alias: 'Column'},
                        required: true,
                        column_id: null,
                        value: null
                    },
                    'model_consent': {
                        required: true,
                        column_id: null,
                        value: null
                    }
                },
                others: {
                    _config: {group: true, required: true},
                    qualifies_extra_credit: {required: true, column_id: null, value: null},
                    photo_ownership_confirmation: {required: true, column_id: null, value: null}
                },
                _extras: {
                    note: {
                        _config: {
                            suggestion: 'If a Photo submission is already found for a User, the system will remove all copies of the previous upload, from all known Storage locations (the photo, its thumbnail, database record, cdn entries, etc), before attempting to insert the replacement Photo found on this upload.'
                        }
                    },
                    has_nudity: {
                        _config: {
                            class_type: 'info',
                            suggestion: 'When a photo here is marked/flagged as having Graphic/Nudity content (single dropdown), both those checkboxes on the new system (2 separate choices) will both be checked automatically.'
                        }
                    }
                }
            }

        };

        $scope.columnsMatchingPageInit = function (nextStep, abort) {
            $scope.csvHeaderColumns = $scope.csvFile.header_columns_with_samples;
            if (!$scope.dbColumns) $scope.dbColumns = {};
            Object.assign($scope.dbColumns, colsValidation);
            $scope.showElemsThatNeedTimeout = false;
            $timeout($scope.initAfterTimeout);
            $scope.chosenColumns = {};
            nextStep && $timeout(nextStep);
        };

        $scope.initAfterTimeout = function () {
            $scope.showElemsThatNeedTimeout = true;
        };

        var getInputValue = function (subGroupOrMember, subGroup) {
            var source = 'column_id';
            if (subGroupOrMember._config) {
                var editableControlBy = subGroupOrMember._config.control_editable_by;
                var editableCtlr = editableControlBy ? subGroup[editableControlBy] : null;
                if (subGroupOrMember._config.use_manual_input || subGroupOrMember._config.type === 'checkbox' || (editableCtlr && editableCtlr.value))
                    source = 'value';
            }
            return subGroupOrMember[source];
        };

        var collectInputs = function () {
            var output = {};
            for (var mainGroup in $scope.dbColumns) {
                if (mainGroup === '_config' || mainGroup === '_extras') continue;
                if (!output[mainGroup]) output[mainGroup] = {};
                var subGroupObj = $scope.dbColumns[mainGroup];
                for (var subGroup in subGroupObj) {
                    if (subGroup === '_config' || subGroup === '_extras') continue;
                    var storage = $scope.dbColumns[mainGroup][subGroup];
                    if (storage._config && storage._config.group) {
                        for (var member in storage) {
                            if (member === '_config' || member === '_extras') continue;
                            if (!output[mainGroup][subGroup]) output[mainGroup][subGroup] = {};
                            output[mainGroup][subGroup][member] = getInputValue(storage[member], storage);
                        }
                    } else {
                        output[mainGroup][subGroup] = getInputValue(storage);
                    }
                }
            }
            return output;
        };

        $scope.columnsMatchingPageCompleted = function () {
            $scope.errors = {};
            $scope.processing = true;
            var inputs = collectInputs();
            inputs.csv_file_id = $scope.csvFile.id;
            CsvColumnsSelection.save(inputs).then(function (response) {
                console.log('success: ', response);
                $scope.processing = false;
                $scope.pagesInfo.goToNextPage(function () {
                    var callbacks = $scope.modalConnection.callbacks;
                    if (callbacks.updateFileUploaded) callbacks.updateFileUploaded($scope.csvFile);
                });
            }, function (response) {
                console.log('failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else {
                    alert('Something went wrong, please check with Admins.');
                }
            });
        };


        // Validation Info Page

        $scope.validationPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.validationPageCompleted = function () {
            $scope.pagesInfo.goToNextPage();
        };


        // Last Page

        $scope.lastPageInit = function (nextStep, abort) {
            nextStep && $timeout(nextStep);
        };

        $scope.lastPageCompleted = function () {
            modalElem.modal('hide');
            $timeout(function () {
                $scope.pagesInfo.startOver();
            }, 400);
        };


    }
]);