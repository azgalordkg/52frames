appSettings.app.controller('MigrationToolController', [
    'Loading', '$scope', '$rootScope', 'Helpers', '$timeout', '$filter',
    function (Loading, $scope, $rootScope, Helpers, $timeout, $filter) {


        // Access Check

        var checkAccess = function () {
            if ($rootScope.user && $rootScope.user.max_admin_points >= 90000)
                $timeout(function () {
                    Loading.pageLoaded($scope);
                });
            else Helpers.gotoPath('/404');
        };
        if ($rootScope.backendChecked) checkAccess();
        else $scope.$on('backendChecked', checkAccess);


        // Common

        $scope.tabsData = {

            list: [
                {
                    name: 'Users',
                    active: false,
                    margin0: false,
                    pullRight: false,
                    modals: {
                        uploadWizard: {
                            id: 'userPage-uploadWizardModal',
                            callbacks: {}
                        }
                    }
                },
                {
                    name: 'Albums',
                    active: false,
                    margin0: false,
                    pullRight: false,
                    modals: {
                        uploadWizard: {
                            id: 'albumPage-uploadWizardModal',
                            callbacks: {}
                        }
                    }
                },
                {
                    name: 'Photos',
                    active: true,
                    margin0: false,
                    pullRight: false,
                    modals: {
                        uploadWizard: {
                            id: 'photosPage-uploadWizardModal',
                            callbacks: {}
                        }
                    }
                },
                {
                    name: 'Comments',
                    active: false,
                    margin0: false,
                    pullRight: false,
                    modals: {
                        // uploadWizard: {
                        //     id: 'userPage-uploadWizardModal',
                        //     callbacks: {}
                        // }
                    }
                },
                {
                    name: 'Countries',
                    active: false,
                    margin0: true,
                    pullRight: true,
                    modals: {
                        // uploadWizard: {
                        //     id: 'userPage-uploadWizardModal',
                        //     callbacks: {}
                        // }
                    }
                },
                {
                    name: 'States',
                    active: false,
                    margin0: false,
                    pullRight: true,
                    modals: {
                        // uploadWizard: {
                        //     id: 'userPage-uploadWizardModal',
                        //     callbacks: {}
                        // }
                    }
                },
                {
                    name: 'Cities',
                    active: false,
                    margin0: false,
                    pullRight: true,
                    modals: {
                        // uploadWizard: {
                        //     id: 'userPage-uploadWizardModal',
                        //     callbacks: {}
                        // }
                    }
                }
            ],

            swapTabs: function (tab) {
                var result = $filter('filter')($scope.tabsData.list, function (tab, index) {
                    return tab.active;
                });
                if (!result.length) return;
                result[0].active = false;
                tab.active = true;
            }

        };

    }
]);