appSettings.app.controller('validationReportController', ['$scope', 'CsvMigrate', function ($scope, CsvMigrate) {

    $scope.row = null;
    $scope.category = null;
    $scope.categories = {
        user: {
            '52f_id': '52Frames ID'
        }
    };

    var migrateCallbacks = {
        success: null,
        failed: null
    };

    $scope.$on('modals:validationReport::show:csvRow', function (event, category, row, success, failed) {
        $scope.row = row;
        $scope.category = category;
        migrateCallbacks.success = success;
        migrateCallbacks.failed = failed;
    });

    $scope.migrateRow = function () {
        if (!$scope.row) return;
        $scope.processing = true;
        CsvMigrate.singleRow({row_id: $scope.row.id, no_rows: true}).then(function (response) {
            console.log('CsvMigrate > singleRow > response (success): ', response);
            $scope.processing = false;
            migrateCallbacks.success && migrateCallbacks.success(response);
        }, function (response) {
            console.log('CsvMigrate > singleRow > response (failed): ', response);
            $scope.processing = false;
            migrateCallbacks.success && migrateCallbacks.failed(response);
        });
    };

}]);