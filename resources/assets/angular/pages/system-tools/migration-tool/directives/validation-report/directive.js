window.appSettings.app.directive("validationReport", function () {
    return {
        scope: true,
        restrict: 'E',
        controller: 'validationReportController',
        templateUrl: 'angular/pages/system-tools/migration-tool/directives/validation-report/template.html'
    };
});