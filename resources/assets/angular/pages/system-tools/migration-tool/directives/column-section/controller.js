appSettings.app.controller('CsvColumnSectionController', ['$timeout', '$scope', function ($timeout, $scope) {

    $scope.hideErrors = function (fieldname) {
        if (!$scope.validationErrors || !$scope.validationErrors[fieldname]) return;
        $scope.validationErrors[fieldname] = '';
    };

    var weekPickerTargetObj = null;

    var calendarDateChange = function (dateText, inputFormat) {
        if (!weekPickerTargetObj) return;
        var startDate, endDate, holder;
        inputFormat = inputFormat || 'MM/DD/YYYY';
        var isSunday = moment(dateText, inputFormat).day() === 0;
        var displayFormat = 'ddd MMM D YYYY';
        var dateFormat = 'YYYY-MM-DD';
        if (isSunday) {
            startDate = moment(dateText, inputFormat).add(-1, 'week').add(1, 'days');
            endDate = moment(dateText, inputFormat).startOf('week');
        } else {
            startDate = moment(dateText, inputFormat).startOf('week').add(1, 'days');
            endDate = moment(dateText, inputFormat).add(1, 'week').startOf('week');
        }
        if (holder = weekPickerTargetObj.startDateAttrName) holder.object[holder.attrName] = startDate.format(dateFormat);
        if (holder = weekPickerTargetObj.startDateAttrNameDisplay) holder.object[holder.attrName] = startDate.format(displayFormat);
        if (holder = weekPickerTargetObj.endDateAttrName) holder.object[holder.attrName] = endDate.format(dateFormat);
        if (holder = weekPickerTargetObj.endDateAttrNameDisplay) holder.object[holder.attrName] = endDate.format(displayFormat);
    };

    var formatWeekPickerObj = function (saveToObj) {
        weekPickerTargetObj = {
            startDateAttrName: {
                object: saveToObj,
                    attrName: 'value'
            },
            startDateAttrNameDisplay: {
                object: saveToObj,
                    attrName: 'displayText'
            }
        };
    };

    var makeCalendar = function (saveToObj, errorTerm, textfield) {
        $timeout(function () {
            formatWeekPickerObj(saveToObj);
            var inputField = $('#' + textfield).click(function () {
                $timeout(function () {
                    inputField.next().click();
                });
            });
            var $calendarIcon = inputField.next('.calendar-icon').weekpicker({
                onSelect: function (dateText, startDateText, startDateInput, endDateInput, inst) {
                    calendarDateChange(dateText);
                    $calendarBox.hide();
                }
            });
            var $calendarBox = $calendarIcon.find('.ui-datepicker-inline').hide();
            var clickEvent = function (event) {
                if (event.target == $calendarIcon[0]) {
                    $calendarBox.toggle();
                    if ($calendarBox.is(':visible') && $scope.validationErrors) {
                        delete $scope.validationErrors[errorTerm];
                    }
                } else {
                    if (!$calendarBox.is(':visible')) return;
                    if ($(event.target).hasClass('ui-icon')) return;
                    if ($(event.target).hasClass('ui-corner-all')) return;
                    if (!$.contains($calendarIcon[0], event.target)) {
                        $calendarBox.hide();
                    }
                }
            };
            $('body').click(clickEvent);
            $scope.$on('destroy', function () {
                $('body').off(clickEvent);
            });
        });

        return textfield;
    };

    // calendarDateChange(albumCopy.start_date, 'YYYY-MM-DD');

    $scope.registerElemId = function ($elemId, fieldData, fieldKw) {
        if (!fieldData._config || !fieldData._config.special || fieldData._config.special !== 'week_picker') return;
        $timeout(function () {
            var $elemIdManualInputWeekPicker = $elemId + '-manual-input-week-picker';
            var concatParentObjKey = $scope.parentObject ? $scope.parentObject.keyword + '.' : '';
            makeCalendar(fieldData, concatParentObjKey + $scope.columnInfo.keyword + '.' + fieldKw, $elemIdManualInputWeekPicker);
        });
    };

    $scope.changeWeekPickerTarget = function (fieldData) {
        formatWeekPickerObj(fieldData);
    };

}]);