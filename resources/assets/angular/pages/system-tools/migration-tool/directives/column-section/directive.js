window.appSettings.app.directive("csvColumnSection", function () {
    return {
        scope: {
            columnInfo: '=',
            csvHeaderColumns: '<',
            validationErrors: '<',
            parentObject: '<?'
        },
        restrict: 'E',
        controller: 'CsvColumnSectionController',
        templateUrl: 'angular/pages/system-tools/migration-tool/directives/column-section/template.html'
    };
});