(function () {

    function CsvMigrate($q, $http, Upload) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'tools/migration/csv/migrate';

        this.singleRow = function (input) {
            return $http({
                data: input,
                method: 'POST',
                url: this.endpoint + '/row'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.allValidRows = function (input) {
            return $http({
                data: input,
                method: 'POST',
                url: this.endpoint + '/all-valid'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

    }

    CsvMigrate.prototype = new appSettings.BaseApi();
    appSettings.app.service('CsvMigrate', ['$q', '$http', 'Upload', CsvMigrate]);

})();