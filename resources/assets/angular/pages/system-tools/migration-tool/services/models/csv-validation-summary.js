(function () {

    function CsvValidationSummary($q, $http, Upload) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'tools/migration/csv/validation/summary';

        this.getUpdate = this.update;
    }

    CsvValidationSummary.prototype = new appSettings.BaseApi();
    appSettings.app.service('CsvValidationSummary', ['$q', '$http', 'Upload', CsvValidationSummary]);

})();