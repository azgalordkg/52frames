(function () {

    function CsvColumnsSelection($q, $http, Upload) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'tools/migration/csv/columns';

    }

    CsvColumnsSelection.prototype = new appSettings.BaseApi();
    appSettings.app.service('CsvColumnsSelection', ['$q', '$http', 'Upload', CsvColumnsSelection]);

})();