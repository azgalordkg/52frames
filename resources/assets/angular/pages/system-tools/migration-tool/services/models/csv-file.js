(function () {

    function CsvFile($q, $http, Upload) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'tools/migration/csv';

        this.save = function (input) {
            return Upload.upload({
                url: this.endpoint,
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };
    }

    CsvFile.prototype = new appSettings.BaseApi();
    appSettings.app.service('CsvFile', ['$q', '$http', 'Upload', CsvFile]);

})();