(function () {

    function CsvRow($q, $http, Upload) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'tools/migration/csv/rows';

    }

    CsvRow.prototype = new appSettings.BaseApi();
    appSettings.app.service('CsvRow', ['$q', '$http', 'Upload', CsvRow]);

})();