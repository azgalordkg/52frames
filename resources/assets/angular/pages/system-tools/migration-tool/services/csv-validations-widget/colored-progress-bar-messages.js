appSettings.app.factory('ProgressBarMessages', [
    '$rootScope', '$timeout', 'StateBasedDataList', 'StateBasedDataListHost',
    function ($rootScope, $timeout, StateBasedDataList, StateBasedDataListHost) {

        return {
            init: function (name) {
                return StateBasedDataList.init(name,
                    [
                        {
                            keyword: 'empty',
                            state: {}
                        },
                        {
                            keyword: 'initializing',
                            state: {
                                active: true,
                                progress: [
                                    {value: 100, type: 'info'}
                                ]
                            }
                        },
                        {
                            keyword: 'showProgress',
                            state: {
                                active: true,
                                progress: [],
                                initCallback: function (thisObj, args) {
                                    var progress = [];
                                    var csvUpdate = args[0], completed = args[1];
                                    if (completed) thisObj.active = !completed;
                                    var total = csvUpdate.total;
                                    var typesMapping = {
                                        migrated: 'migrated',
                                        success: 'ok',
                                        warning: 'warning',
                                        failed: 'error'
                                    };
                                    var remaining = 100;
                                    for (var type in typesMapping) {
                                        var status = typesMapping[type];
                                        if (!csvUpdate.states || !csvUpdate.states[status]) continue;
                                        var count = csvUpdate.states[status].count;
                                        if (!count) continue;
                                        var computed = (count / total) * 100;
                                        remaining -= computed;
                                        progress.push({
                                            value: computed,
                                            type: type
                                        });
                                    }
                                    if (remaining >= 1) progress.push({
                                        value: remaining,
                                        type: 'blank'
                                    });
                                    thisObj.progress = progress;
                                }
                            }
                        }
                    ],
                    true,
                    {
                        migrated: 'migrated',
                        info: 'loading',
                        success: 'ok',
                        warning: 'warning',
                        failed: 'error',
                        blank: 'blank'
                    }
                );
            }
        };
    }
]);