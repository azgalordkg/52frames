appSettings.app.factory('CsvValidationWidgets', [
    '$rootScope', '$timeout', 'WizardButtons', 'ProgressBarMessages', 'StatusMessages', 'StateBasedDataListHost',
    function ($rootScope, $timeout, WizardButtons, ProgressBarMessages, StatusMessages, StateBasedDataListHost) {

        return {
            init: function ($scope, csvCategory) {
                return StateBasedDataListHost.init($scope, [
                    WizardButtons.init('wizardButtons', csvCategory),
                    ProgressBarMessages.init('barProgress', csvCategory),
                    StatusMessages.init('statuses', csvCategory)
                ]);
            }
        }
    }
]);