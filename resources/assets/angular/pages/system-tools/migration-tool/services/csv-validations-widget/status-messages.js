appSettings.app.factory('StatusMessages', [
    '$rootScope', '$timeout', 'StateBasedDataList', 'LongPollValidationSummary',
    function ($rootScope, $timeout, StateBasedDataList, LongPollValidationSummary) {

        return {
            init: function (name, csvCategory) {
                var _this = StateBasedDataList.init(name,
                    [
                        {
                            keyword: 'continueColumnsMatching',
                            state: {
                                type: -1,
                                text: 'Columns not aligned',
                                initCallback: function () {
                                    _this.host.setStateResetOthers('wizardButtons', 'continueColumnsMatching');
                                }
                            }
                        },
                        {
                            keyword: 'continueValidating',
                            state: {
                                type: null, //type: NULL means "initializing" (color: blue: info)
                                text: 'Checking progress...',
                                initCallback: function () {
                                    _this.host.setStateResetOthers('barProgress', 'initializing');
                                    LongPollValidationSummary.addListener(csvCategory, _this.host.$scope.csvFile, function (csvUpdate) {
                                        _this.host.setStateResetOthers('statuses', 'showProgress', csvUpdate);
                                    });
                                }
                            }
                        },
                        {
                            keyword: 'showProgress',
                            state: {
                                type: null,
                                text: 'On Queue...',
                                origText: 'On Queue...',
                                initCallback: function (thisObj, csvUpdate) {
                                    thisObj.type = null;
                                    thisObj.text = thisObj.origText;
                                    var summary = csvUpdate.validation_summary;
                                    if (!summary) {
                                        _this.host.setStateResetOthers('barProgress', 'empty');
                                        return;
                                    }
                                    var completed = summary.validated == summary.total;
                                    if (completed) {
                                        thisObj.type = 1;
                                        thisObj.text = 'Validation Complete';
                                        LongPollValidationSummary.removeListener(csvCategory, _this.host.$scope.csvFile);
                                    } else thisObj.text = 'Validated ' + summary.validated + '/' + summary.total;
                                    _this.host.setStateResetOthers('barProgress', 'showProgress', [summary, completed]);
                                }
                            }
                        }
                    ]
                );
                return _this;
            }
        };
    }
]);