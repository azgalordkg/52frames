appSettings.app.factory('LongPollValidationSummary', [
    '$rootScope', '$timeout', '$filter', 'CsvValidationSummary',
    function ($rootScope, $timeout, $filter, CsvValidationSummary) {

        var csvFileIds = [];

        var categoryUpdateListeners = {
            photos: [],
            users: []
        };

        var pollingStarted = false;
        var checkOrStartPolling = function () {
            if (pollingStarted) return;
            pollingStarted = true;
            initPolling();
        };

        var initPolling = function () {
            if (!csvFileIds.length) return timeoutThenInitPolling();
            CsvValidationSummary.getUpdate({csv_file_ids: csvFileIds, no_rows: 1}).then(function (response) {
                for (var i = 0; i < response.length; i++) {
                    fireSummaryHandlers(response[i]);
                }
                timeoutThenInitPolling();
            }, function (response) {
                console.log('LongPollValidationSummary > checkOrStartPolling > CsvValidationSummary.get() > failed: ', response);
                timeoutThenInitPolling();
            });
        };

        var timeoutThenInitPolling = function () {
            $timeout(initPolling, 1000);
        };

        var fireSummaryHandlers = function (fileSummary) {
            var category = fileSummary.category;
            if (!categoryUpdateListeners[category]) return;
            var handlers = categoryUpdateListeners[category];
            for (var i = 0; i < handlers.length; i++) {
                if (handlers[i].id === fileSummary.id) {
                    var handler = handlers[i].handler;
                    handler.call(handler, fileSummary);
                }
            }
        };

        return {

            addListener: function (category, csvFile, handler) {
                if (!category || !csvFile || !csvFile.id || !handler) return;
                if (!categoryUpdateListeners[category]) categoryUpdateListeners[category] = [];
                categoryUpdateListeners[category].push({id: csvFile.id, handler: handler});
                csvFileIds.push(csvFile.id);
                checkOrStartPolling();
            },

            removeListener: function (category, csvFile) {
                if (!category || !csvFile || !csvFile.id || !categoryUpdateListeners[category]) return;
                categoryUpdateListeners[category] = $filter('filter')(categoryUpdateListeners[category], function (handlerData) {
                    return handlerData.id != csvFile.id;
                });
                csvFileIds = $filter('filter')(csvFileIds, function (id) {
                    return id != csvFile.id;
                });
            }

        };
    }
]);