appSettings.app.factory('WizardButtons', [
    '$rootScope', '$timeout', 'StateBasedDataList', 'StateBasedDataListHost',
    function ($rootScope, $timeout, StateBasedDataList, StateBasedDataListHost) {

        var getMigratedCount = function (csvFile) {
            if (!csvFile || !csvFile.validation_summary || !csvFile.validation_summary.states || !csvFile.validation_summary.states.migrated) return null;
            return csvFile.validation_summary.states.migrated.count;
        };

        return {
            init: function (name, csvCategory) {
                var _this = StateBasedDataList.init(name,
                    [
                        {
                            keyword: 'continueColumnsMatching',
                            state: {
                                text: 'Complete Wizard',
                                clickHandler: function (repick) {
                                    $rootScope.$broadcast('resetAndTransitionWizardThruPagesWithVars:' + csvCategory, {
                                        csvChooseFile: {
                                            variables: {
                                                showFileInput: false,
                                                csvFile: _this.host.csvFile,
                                                numMigratedRows: getMigratedCount(_this.host.csvFile),
                                                isRepick: !!repick
                                            }
                                        }
                                    }, function (modalElem) {
                                        $timeout(function () {
                                            modalElem.modal('show');
                                        });
                                    });
                                }
                            }
                        }
                    ],
                    true
                );
                return _this;
            }
        };
    }
]);