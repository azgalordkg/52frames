appSettings.app.controller('FbAttachPasswordModalController', ['$scope', 'FacebookAuth', function ($scope, FacebookAuth) {

    $('#fbAttachPasswordModal').on('show.bs.modal', function (e) {

        $scope.userData = FacebookAuth.userData || {};
        delete FacebookAuth.userData;

        $scope.errors = {};

    });

    $scope.addPassword = function () {

        $scope.errors = {};

        $scope.processing = true;
        FacebookAuth.addPassword($scope.userData).then(function (response) {
            console.log('add password success: ', response);
            $scope.processing = false;
            redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
        }, function (response) {
            console.log('add password failed: ', response);
            $scope.processing = false;
            if (response.validation_errors) {
                $scope.errors = response.validation_errors;
            } else if (response.message) {
                alert(response.message);
            }
        });

    };

    $scope.loginScreen = function () {
        setTimeout(function () {
            $('#loginModal').modal('show');
        }, 400);
    };

    $scope.hideErrors = function () {
        $scope.errors = {};
    };

}]);