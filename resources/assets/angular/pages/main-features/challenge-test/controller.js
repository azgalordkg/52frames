appSettings.app.controller('challengeTestController', ['Loading', '$scope', '$rootScope', 'Album', '$state', function (Loading, $scope, $rootScope, Album, $state) {

    var albumPage = '/challengeTest/{pathYear}/{pathWeekTheme}';

    Album.get().then(function (response) {
        console.log('get list of albums, success: ', response);
        $scope.allYearsAlbums = response;
        Loading.pageLoaded($scope);
    }, function (response) {
        console.log('get list of albums, failed: ', response);
    });

    $scope.showAlbumOrChallengePage = function (yearData, album) {
        var maxAdminPoints = $rootScope.user ? $rootScope.user.max_admin_points : 0;
        var showAlbumPage = album.live || maxAdminPoints >= 100000;
        $state.go(showAlbumPage ? 'weekTheme' : 'weekChallenge', {
            pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
            pathYear: yearData.year
        });
    };

    $scope.openAddNewAlbumModal = function () {
        $('#newAlbumModal').modal('show');
    };

}]);