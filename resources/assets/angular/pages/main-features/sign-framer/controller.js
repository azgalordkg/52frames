appSettings.app.controller('signFramerController', [
  '$scope', '$timeout', 'Manifesto', 'Country', 'State', 'City', '$rootScope', '$q', 'Variables', 'Token', '$http', '$location',
  function ($scope, $timeout, Manifesto, Country, State, City, $rootScope, $q, Variables, Token, $http, $location) {

    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';
    $scope.manifestoInput = Manifesto.manifestoInput || {};
    $scope.ifValidForm = !$scope.manifestoInput.creative_process_understanding || !$scope.manifestoInput.understanding_for_the_sake_of_consistency
      || !$scope.manifestoInput.being_constant_understanding || !$scope.manifestoInput.understanding_to_release_the_notion_of_perfection
      || !$scope.manifestoInput.commitment_understanding;

    if ($rootScope.willSignManifesto) {
      var modalWait = setInterval(function () {
        var signManifestoModal = $('#signFramer');
        if (!signManifestoModal.length) return;
        clearInterval(modalWait);
        signManifestoModal.modal('show');
        Variables.helpers.removeManifestoMarker();
      }, 250);
    }

    $('#signFramer').on('show.bs.modal', function (e) {
      $rootScope.manifestoPageOpened = true;

      $scope.manifestoInput = Manifesto.manifestoInput || $scope.manifestoInput || {};
      delete Manifesto.manifestoInput;

      if (!$scope.manifestoInput || !$scope.manifestoInput.country) {
        Country.get().then(function (countries) {
          $scope.countries = countries;
          $scope.countries.splice(4, 0, {name: '------------------', disabled: true});
          $scope.changeCountry();
        });
      }
      $scope.errors = {};

    });

    $scope.shortUrlBase = window.appSettings.shortUrlBase;

    $scope.nicknameLoading = {};

    var nicknameRequestCanceler = null;
    $scope.nicknameChanged = function () {
      $scope.nicknameLoading.indicator = false;
      $scope.nicknameLoading.ok = false;
      var nickname = $scope.manifestoInput.nickname;
      nickname = nickname && nickname.toLowerCase().replace(/^[^a-z]/, '').replace(/\s|[^-a-z.0-9_~]/g, '-');
      $scope.manifestoInput.nickname = nickname;
      if (nickname) {
        $scope.nicknameLoading.indicator = true;
        $scope.nicknameLoading.ok = false;
        if (nicknameRequestCanceler) nicknameRequestCanceler();
        var canceler = $q.defer();
        nicknameRequestCanceler = canceler.resolve;
        Manifesto.checkNickname({
          user_id: $rootScope.user.id,
          nickname: nickname
        }, {timeout: canceler.promise}).then(function (response) {
          console.log('nickname check success: ', response);
          $scope.nicknameLoading.indicator = false;
          $scope.nicknameLoading.ok = true;
          delete $scope.errors.nickname;
          nicknameRequestCanceler = null;
        }, function (response) {
          console.log('nickname check failed: ', response);
          if (!response) return;
          $scope.nicknameLoading.indicator = false;
          $scope.nicknameLoading.ok = false;
          if (response.validation_errors) {
            $scope.errors.nickname = response.validation_errors.nickname;
          } else {
            $scope.nicknameLoading.fail = true;
          }
          $scope.manifestoInput.display_as_handle = false;
          nicknameRequestCanceler = null;
        });
      } else {
        delete $scope.errors.nickname;
        $scope.manifestoInput.display_as_handle = false;
      }
    };

    $scope.singupScreen = function () {
      $('#signFramer').modal('hide');
      setTimeout(function () {
        $('#userChoice').modal('show');
      }, 400)
    };

    $scope.changeCountry = function () {
      $scope.states = {};
      $scope.manifestoInput.city = '';
      var country = $scope.manifestoInput.country;
      if (country && country.state_member_term) {
        State.get({
          country_id: country.id
        }).then(function (states) {
          $scope.states = states;
        }, function () {
        });
      }
    };

    $scope.changeState = function () {
      $scope.manifestoInput.city = '';
    };

    $scope.suggestCity = function () {
      if (!$scope.manifestoInput) return;
      var country = $scope.manifestoInput.country;
      var state = $scope.manifestoInput.state;
      var cityInput = $scope.manifestoInput.city;
      if (country && cityInput) {
        var requestInput = {
          country_id: country.id,
          query: cityInput
        };
        if (country.state_member_term && state) {
          requestInput.state_id = state.id;
        }
        City.get(requestInput).then(function (cities) {
          if ($scope.dontSuggestCity) {
            $scope.dontSuggestCity = false;
            return;
          }
          console.log('city autosuggest: ', cities);
          var $cityInputField = $('#city');
          $cityInputField.data('typeahead').source = cities;
          $cityInputField.typeahead('lookup').focus();
        }, function () {
        });
      }
    };

    $scope.citySelected = function () {
      $scope.dontSuggestCity = true;
    };

    $scope.country_accept = true;
    $scope.submit = function () {
      $scope.errors = {};

      var manifestoInput = angular.copy($scope.manifestoInput);
      manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
      manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;
      manifestoInput.manifesto_intro = 1;
      manifestoInput.dont_share_location = !$scope.country_accept;
      $scope.processing = true;

      if ($rootScope.user && !$rootScope.user.signed_manifesto) {
        manifestoInput.dont_save = 1;
      }

      $http({
        method: 'POST',
        data: manifestoInput,
        url: apiUrl + 'manifesto/validate',
        headers: {
          'Authorization': 'Bearer ' + Token.get()
        }
      }).then(function (success) {
        console.log('validation success: ', success.data);
        $scope.processing = false;
        Manifesto.manifestoInput = $scope.manifestoInput;
        Manifesto.manifestoInput.dont_share_location = !$scope.country_accept;
        Manifesto.manifestoInput.how_did_hear = $scope.manifestoInput.how_did_hear;

        $('#signFramer').modal('hide');
        setTimeout(function () {
          $rootScope.activeStepOfManifesto = 3;
          $('#signAccept').modal('show');
        }, 400);
      }, function (error) {
        console.log('validation failed: ', error.data);
        $scope.processing = false;
        if (error.data.validation_errors) {
          $scope.errors = error.data.validation_errors;
        } else if (error.data.message) {
          alert(error.data.message);
        }
      });
      // Manifesto.validate(manifestoInput).then(function (response) {
      //     console.log('validation success: ', response, manifestoInput);
      //     $scope.processing = false;
      //     Manifesto.manifestoInput = $scope.manifestoInput;
      //     $('#signFramer').modal('hide');
      //     setTimeout(function () {
      //         $('#signAccept').modal('show');
      //     }, 400);
      // }, function (response) {
      //     console.log('validation failed: ', response);
      //     $scope.processing = false;
      //     if (response.validation_errors) {
      //         $scope.errors = response.validation_errors;
      //     } else if (response.message) {
      //         alert(response.message);
      //     }
      // });
    };

    $scope.hideErrors = function (fieldname) {
      if (!$scope.errors) return;
      $scope.errors[fieldname] = '';
    };

    $scope.onFieldMouseOver = function(index, className, block) {
      $rootScope.$emit('CallFieldMouseOver', index, className, block);
    };
  }
]);