appSettings.app.controller('userChoiceTypeController', [
    '$scope', '$timeout', 'Auth', 'FacebookAuth', '$q', 'Variables', 'InstagramAuth',
    function ($scope, $timeout, Auth, FacebookAuth, $q, Variables, InstagramAuth) {

        var userChoiceType = $('#userChoiceType');

        userChoiceType.on('show.bs.modal', function (e) {
            $scope.isFbSignup = FacebookAuth.userData;
            $scope.isInstSignup = InstagramAuth.userData;

            if ($scope.isFbSignup) {
                $scope.userData = FacebookAuth.userData;
                delete FacebookAuth.userData;
            } else if ($scope.isInstSignup) {
                $scope.userData = InstagramAuth.userData;
                delete InstagramAuth.userData;
            }  else {
                $scope.userData = Auth.userData || {};
                delete Auth.userData;
            }

            if (!$scope.userData.hasOwnProperty('newsletter')) {
                $scope.userData.newsletter = true;
            }

            $scope.showCaptcha = true;

            $scope.errors = {};

        });

        userChoiceType.on('hide.bs.modal', function (e) {
            Auth.userData = $scope.userData;
            delete $scope.showCaptcha;
            delete $scope.isFbSignup;
            delete $scope.isInstSignup;
        });

        $scope.signupScreen = function () {
            setTimeout(function () {
                $('#userChoiceType').modal('hide');
            }, 400);
            Auth.userData = $scope.userData;
            setTimeout(function () {
                $('#userChoice').modal('show');
            }, 400);
        };

        $scope.createAccount = function (promise) {

            var deferred = promise && $q.defer();

            $scope.errors = {};

            var userData = $scope.userData;
            userData['captcha'] = true;
            userData.is_fan = !promise;

            function registrationSuccess(response) {
                promise && deferred.resolve(response);
                if ($scope.willSignManifesto) {
                    window.location.reload(false);
                } else {
                    var intendedPage = getIntendedPage();
                    if (intendedPage) {
                        window.location.href = intendedPage;
                    } else {
                        window.location.reload(false);
                    }
                }
                $scope.processing = false;
            }

            function registrationFailed(response) {
                console.log('registration failed: ', response);
                $scope.processing = false;
                $scope.showCaptcha = false;
                $timeout(function () {
                    $scope.showCaptcha = true;
                }, 150);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.message) {
                    alert(response.message);
                }
                promise && deferred.reject(response);
            }

            $scope.processing = true;
            if ($scope.isFbSignup) {
                FacebookAuth.registerUser(userData).then(registrationSuccess, registrationFailed);
            } else if ($scope.isInstSignup) {
                InstagramAuth.registerUser(userData).then(registrationSuccess, registrationFailed);
            } else {
                Auth.registerUser(userData).then(registrationSuccess, registrationFailed);
            }

            return promise && deferred.promise;

        };

    }]
);