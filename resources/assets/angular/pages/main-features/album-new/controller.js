appSettings.app.controller('NewAlbumController', [
  '$scope', '$timeout', 'Album', 'TinymceHelper', 'HelpsUploadProgress', 'SmoothScroll',
  function ($scope, $timeout, Album, TinymceHelper, HelpsUploadProgress, SmoothScroll) {

    var newAlbumModal = $('#newAlbumModal');

    if ($scope.closeModalCallback && !newAlbumModal.attr('closeCallback')) {
      newAlbumModal.attr('closeCallback', true);
      newAlbumModal.on('hidden.bs.modal', function (e) {
        $scope.closeModalCallback();
      });
    }

    $scope.newAlbum = {
      sample_photo: {},
      temp_cover_photo: {},
      tips_photo: {}
    };

    $timeout(function () {

      var makeCalendar = function (textfield) {
        var $calendarIcon = $(textfield).next('.calendar-icon').weekpicker({
          onSelect: function (dateText, startDateText, startDateInput, endDateInput, inst) {
            var isSunday = moment(dateText, 'MM/DD/YYYY').day() === 0;
            var displayFormat = 'ddd MMM D YYYY';
            var dateFormat = 'YYYY-MM-DD';
            var startDate, endDate;
            if (isSunday) {
              startDate = moment(dateText, 'MM/DD/YYYY').add(-1, 'week').add(1, 'days');
              endDate = moment(dateText, 'MM/DD/YYYY').startOf('week');
            } else {
              startDate = moment(dateText, 'MM/DD/YYYY').startOf('week').add(1, 'days');
              endDate = moment(dateText, 'MM/DD/YYYY').add(1, 'week').startOf('week');
            }
            $scope.newAlbum.start_date = startDate.format(dateFormat);
            $scope.newAlbum.display_start_date = startDate.format(displayFormat);
            $scope.newAlbum.end_date = endDate.format(dateFormat);
            $scope.newAlbum.display_end_date = endDate.format(displayFormat);
            $calendarBox.hide();
          }
        });
        var $calendarBox = $calendarIcon.find('.ui-datepicker-inline').hide();
        $('body').click(function (event) {
          if (event.target == $calendarIcon[0]) {
            $calendarBox.toggle();
            if ($calendarBox.is(':visible') && $scope.errors) {
              delete $scope.errors.start_date;
              delete $scope.errors.end_date;
            }
          } else {
            if (!$calendarBox.is(':visible')) return;
            if ($(event.target).hasClass('ui-icon')) return;
            if ($(event.target).hasClass('ui-corner-all')) return;
            if (!$.contains($calendarIcon[0], event.target)) {
              $calendarBox.hide();
            }
          }
        });
      };

      makeCalendar('#newAlbum-start-date');

      $('#newAlbum-start-date, #newAlbum-end-date').click(function () {
        $timeout(function () {
          $('#newAlbum-start-date').next().click();
        });
      });

      tinymce.init({
        selector: '#newAlbum-blurb',
        height: 350,
        menubar: false,
        plugins: TinymceHelper.plugins,
        toolbar: TinymceHelper.toolbar,
        content_css: TinymceHelper.content_css,
        file_browser_callback: TinymceHelper.file_browser_callback,
        relative_urls: false,
        setup: function (editor) {
          editor.on('change', function () {
            TinymceHelper.setup(editor);
            if ($scope.errors) $scope.errors.blurb = {};
          });
        }
      });

      tinymce.init({
        selector: '#newAlbum-short-blurb',
        height: 200,
        menubar: false,
        plugins: TinymceHelper.plugins_shortblur,
        toolbar: TinymceHelper.toolbar_short,
        content_css: TinymceHelper.content_css,
        relative_urls: false,
        setup: function (editor) {
          editor.on('change', function () {
            TinymceHelper.setup(editor);
            if ($scope.errors) $scope.errors.short_blurb = {};
          });
        }
      });

      tinymce.init({
        selector: '#newAlbum-extra-credit-body',
        height: 300,
        menubar: false,
        plugins: TinymceHelper.plugins,
        toolbar: TinymceHelper.toolbar,
        content_css: TinymceHelper.content_css,
        file_browser_callback: TinymceHelper.file_browser_callback,
        relative_urls: false,
        setup: function (editor) {
          editor.on('change', function () {
            TinymceHelper.setup(editor);
            if ($scope.errors) $scope.errors.extra_credit_body = {};
          });
        }
      });

      tinymce.init({
        selector: '#newAlbum-tips-body',
        height: 300,
        menubar: false,
        plugins: TinymceHelper.plugins,
        toolbar: TinymceHelper.toolbar,
        content_css: TinymceHelper.content_css,
        file_browser_callback: TinymceHelper.file_browser_callback,
        relative_urls: false,
        setup: function (editor) {
          editor.on('change', function () {
            TinymceHelper.setup(editor);
            if ($scope.errors) $scope.errors.upload_custom_msg = {};
          });
        }
      });

      tinymce.init({
        selector: '#custom_message',
        height: 300,
        menubar: false,
        plugins: TinymceHelper.plugins,
        toolbar: TinymceHelper.toolbar,
        content_css: TinymceHelper.content_css,
        file_browser_callback: TinymceHelper.file_browser_callback,
        relative_urls: false,
        setup: function (editor) {
          editor.on('change', function () {
            TinymceHelper.setup(editor);
            if ($scope.errors) $scope.errors.upload_custom_msg = {};
          });
        }
      });

      $('#newAlbum-temp-cover-photo-file').on('click', function () {
        delete $scope.newAlbum.temp_cover_photo.file;
      }).on('change', function () {
        $scope.newAlbum.temp_cover_photo.file = this.files[0];
      });

      $('#newAlbum-sample-photo-file').on('click', function () {
        delete $scope.newAlbum.sample_photo.file;
      }).on('change', function () {
        $scope.newAlbum.sample_photo.file = this.files[0];
      });

      $('#albumAdImage').on('click', function () {
        delete $scope.newAlbum.tips_photo.image;
      }).on('change', function () {
        $scope.newAlbum.tips_photo.image = this.files[0];
      });

    });

    $scope.baseurl = window.appSettings.canonicalUrl;

    $scope.updateShorturl = function () {
      $scope.shorturlChanged($scope.newAlbum.theme_title);
    };

    $scope.shorturlChanged = function (text) {
      $scope.newAlbum.shorturlLoadingFail = false;
      $scope.newAlbum.shorturlLoadingOk = false;
      $scope.newAlbum.shorturlLoading = false;

      text = text || $scope.newAlbum.shorturl;
      $scope.newAlbum.shorturl = text && text.toLowerCase().replace(/^[^a-z0-9]/, '').replace(/\s|[^-a-z.0-9_~]/g, '-');

      if (!$scope.newAlbum.week_number || !$scope.newAlbum.year) return;

      if ($scope.newAlbum.shorturl) {
        $scope.newAlbum.shorturlLoadingFail = false;
        $scope.newAlbum.shorturlLoadingOk = false;
        $scope.newAlbum.shorturlLoading = true;

        Album.checkShorturl({
          year: $scope.newAlbum.year,
          week_number: $scope.newAlbum.week_number,
          shorturl: $scope.newAlbum.shorturl
        }).then(function (response) {
          console.log('success', response);
          $scope.newAlbum.shorturlLoadingFail = false;
          $scope.newAlbum.shorturlLoadingOk = true;
          $scope.newAlbum.shorturlLoading = false;
          if ($scope.errors) delete $scope.errors.shorturl;
        }, function (response) {
          console.log('failed', response);
          $scope.newAlbum.shorturlLoadingFail = true;
          $scope.newAlbum.shorturlLoadingOk = false;
          $scope.newAlbum.shorturlLoading = false;
          if (response.validation_errors && response.validation_errors.shorturl) {
            if (!$scope.errors) $scope.errors = {};
            $scope.errors.shorturl = response.validation_errors.shorturl;
          }
        });

      }
    };

    $scope.focusShorturl = function () {
      $('#newAlbum-shorturl').focus();
    };

    $scope.focusTagsInput = function () {
      $('#newAlbum-tags').focus();
    };

    $scope.tagsList = [];

    var trimText = function (text) {
      return text.replace(/^\s+/, '').replace(/\s+$/, '');
    };

    var checkDuplicates = function (word, inputArr) {
      for (var i = 0; i < inputArr.length; i++) {
        if (inputArr[i] == word) return true;
      }
      return false;
    };

    $scope.addToTags = function (word) {
      if (!checkDuplicates(word, $scope.tagsList)) {
        $scope.tagsList.push(word);
      }
    };

    $scope.checkTagInput = function () {
      $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
      var cleaned = $scope.tagInput.replace(/^\s+/, '').replace(/^#/, '');
      var hasNewWord = cleaned.match(/[, #]/);
      if (hasNewWord && hasNewWord.length) {
        var words = cleaned.split(hasNewWord[0]);
        var previousWord = trimText(words[0]);
        $scope.tagInput = trimText(words[1]);
        if (previousWord) {
          $scope.addToTags(previousWord);
        }
      }
    };

    $scope.tagInputBlur = function () {
      if (!$scope.tagInput) return;
      $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
      var cleaned = $scope.tagInput.replace(/[# ,]/g, '');
      var lastInput = trimText(cleaned);
      if (lastInput) {
        $scope.addToTags(lastInput);
        $scope.tagInput = '';
      }
      return lastInput;
    };

    $scope.onEnterKeyInsideTagInput = function () {
      var lastInput = $scope.tagInputBlur();
      if (!lastInput) return true;
    };

    $scope.onBackspaceInsideTagInput = function () {
      if (!$scope.tagInput) {
        $scope.tagsList.pop();
      }
    };

    $scope.removeTag = function (index) {
      if ($scope.processing) return;
      $scope.tagsList.splice(index, 1);
    };

    HelpsUploadProgress.registerTextHolder($scope, 'uploadProgress');
    $scope.addNewAlbum = function () {
      $scope.processing = true;
      $scope.errors = {};
      var newAlbumInput = $scope.newAlbum;
      newAlbumInput.blurb = $('#newAlbum-blurb').val();
      newAlbumInput.short_blurb = $('#newAlbum-short-blurb').val();
      newAlbumInput.extra_credit_body = $('#newAlbum-extra-credit-body').val();
      newAlbumInput.tips_body = $('#newAlbum-tips-body').val();
      newAlbumInput.upload_custom_msg = $('#custom_message').val();
      newAlbumInput.tags = $scope.tagsList;
      HelpsUploadProgress.uploadStarted();
      console.log(newAlbumInput);
      Album.save(newAlbumInput).then(function (response) {
        $scope.processing = false;
        console.log('save new album success: ', response);
        HelpsUploadProgress.uploadCompleted(true);
        window.location.reload(false);
      }, function (response) {
        $scope.processing = false;
        console.log('save new album failed: ', response);
        HelpsUploadProgress.uploadCompleted(false);
        if (response.validation_errors) {
          $scope.errors = response.validation_errors;
          SmoothScroll.toFirstError($scope.errors);
        } else if (response.message) {
          alert(response.message);
        }
      }, HelpsUploadProgress.uploadProgress);
    };

    $scope.hideErrors = function (fieldname) {
      if (!$scope.errors) return;
      delete $scope.errors[fieldname];
    };

  }
]);