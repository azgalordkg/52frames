appSettings.app.controller('MakeAlbumLive', [
    '$rootScope', '$scope', '$timeout', 'Album', 'Photo', 'TinymceHelper', 'Variables',
    function ($rootScope, $scope, $timeout, Album, Photo, TinymceHelper, Variables) {

        $('#albumSetLive').on('show.bs.modal', function (e) {
            $scope.errors = {};
        });

        $scope.filenames = {
            winners: [],
            runners_up: []
        };

        $scope.selectedPhoto = {
            winners: [],
            runners_up: []
        };

        $scope.notes = {
            winners: [],
            runners_up: []
        };

        $scope.wysiwyg = {
            winners: [],
            runners_up: []
        };

        var flierFiles = {
            patreon: null,
            flier: null
        };
        $timeout(function () {
            if ($scope.album) {
                var columnName, columnsMap = {
                    'top1_photo': {sectionName: 'winners', index: 0},
                    'top2_photo': {sectionName: 'winners', index: 1},
                    'top3_photo': {sectionName: 'winners', index: 2},
                    'top4_photo': {sectionName: 'runners_up', index: 0},
                    'top5_photo': {sectionName: 'runners_up', index: 1},
                    'top6_photo': {sectionName: 'runners_up', index: 2}
                };
                for (columnName in columnsMap) {
                    var map = columnsMap[columnName];
                    if ($scope.album[columnName]) {
                        $scope.filenames[map.sectionName][map.index] = $scope.album[columnName].filename;
                        $scope.selectedPhoto[map.sectionName][map.index] = $scope.album[columnName];
                        var $filenameBox = $('#' + map.sectionName + map.index);
                        $filenameBox.data('typeahead').source = [$scope.album[columnName]];
                    } else {
                        $scope.filenames[map.sectionName][map.index] = null;
                        $scope.selectedPhoto[map.sectionName][map.index] = null;
                    }
                    if ($scope.album[columnName]) {
                        $scope.notes[map.sectionName][map.index] = $scope.album[columnName].notes_from_yosef;
                    }
                    (function (columnName, sectionName, index) {
                        tinymce.init({
                            selector: '#notesFromYosef' + sectionName + index,
                            max_chars: Variables.captionLimit.total,
                            height: 150,
                            menubar: false,
                            plugins: TinymceHelper.plugins_caption,
                            toolbar: TinymceHelper.toolbar_notes_from_yosef,
                            content_css: TinymceHelper.content_css,
                            setup: function (editor) {
                                editor.on('change', function () {
                                    TinymceHelper.setup(editor);
                                    if ($scope.errors) $scope.errors['notes.' + sectionName + '.' + index] = {};
                                });
                            },
                            init_instance_callback: function (inst) {
                                $scope.wysiwyg[sectionName][index] = {
                                    inst: inst,
                                    element: $(inst.editorContainer).hide()
                                };
                                if ($scope.album[columnName]) {
                                    $scope.currentFilenameBox = [sectionName, index];
                                    $scope.filenameSelected($scope.album[columnName]);
                                }
                            }
                        });
                    })(columnName, map.sectionName, map.index);
                }
                var fliersMap = {
                    patreon: 'patreon_photo',
                    flier: 'flier_photo'
                };
                for (sectionName in flierFiles) {
                    var dbColumnName = fliersMap[sectionName];
                    if ($scope.album[dbColumnName]) {
                        // flierFiles[sectionName] = $scope.album[dbColumnName];
                        $scope.selectedPhoto[sectionName] = {name: $scope.album[dbColumnName].filename};
                        $('#photoTitle' + sectionName).val($scope.album[dbColumnName].title);
                        $('#photoCaption' + sectionName).val($scope.album[dbColumnName].caption);
                        var img = $('#' + sectionName + 'CardPreview')[0];
                        if (img) img.src = $scope.album[dbColumnName].thumbnail_filename;
                    } else {
                        flierFiles[sectionName] = null;
                        $scope.selectedPhoto[sectionName] = null;
                        $('#photoTitle' + sectionName).val('');
                        $('#photoCaption' + sectionName).val('');
                    }
                    (function (flier) {
                        tinymce.init({
                            selector: '#photoCaption' + flier,
                            max_chars: Variables.captionLimit.total,
                            height: 175,
                            menubar: false,
                            plugins: TinymceHelper.plugins_caption,
                            toolbar: TinymceHelper.toolbar_notes_from_yosef,
                            content_css: TinymceHelper.content_css,
                            setup: function (editor) {
                                editor.on('change', function () {
                                    TinymceHelper.setup(editor);
                                    if ($scope.errors) $scope.errors[flier + '.caption'] = {};
                                });
                            },
                            init_instance_callback: function (inst) {
                                $scope.wysiwyg[flier] = {
                                    inst: inst,
                                    element: $(inst.editorContainer)
                                };
                            }
                        });
                        $('#' + flier + 'CardPhoto').on('click', function () {
                            delete $scope.errors[flier];
                        }).on('change', function () {
                            flierToRemove[flier] = null;
                            flierFiles[flier] = this.files[0];
                            $('#photoTitle' + flier).val('');
                            $scope.wysiwyg[flier].inst.setContent('');
                            if (flierFiles[flier]) {
                                $scope.selectedPhoto[flier] = {name: flierFiles[flier].name};
                                if (window.File && window.FileReader && window.FileList && window.Blob) {
                                    var reader = new FileReader();
                                    reader.onload = function (e) {
                                        var img = $('#' + flier + 'CardPreview')[0];
                                        img.src = reader.result;
                                    };
                                    reader.readAsDataURL(flierFiles[flier]);
                                }
                            } else $scope.selectedPhoto[flier] = null;
                        });
                    })(sectionName);
                }
            }
        });

        var currentlyOnFocus = null;
        $scope.saveOnFocus = function (sectionName, index) {
            $filenameBox = $('#' + sectionName + index);
            if ($filenameBox.length) currentlyOnFocus = $filenameBox[0];
        };

        $scope.suggestFilename = function (sectionName, index, autoSelect) {
            if (!autoSelect) {
                $scope.wysiwyg[sectionName][index].element.hide();
                if ($scope.dontSuggestFilename) $scope.dontSuggestFilename = false;
            }
            $scope.wysiwyg[sectionName][index].inst.setContent('');
            delete $scope.selectedPhoto[sectionName][index];
            var value = $scope.filenames[sectionName][index];
            if (!value) return;
            $filenameBox = $('#' + sectionName + index);
            if (autoSelect) {
                var i, $source = $filenameBox.data('typeahead').source;
                if ($source.length) {
                    for (i = 0; i < $source.length; i++) {
                        if ($source[i].filename == value) {
                            $scope.currentFilenameBox = [sectionName, index];
                            $scope.filenameSelected($source[i]);
                        }
                    }
                }
                return;
            }
            $scope.currentFilenameBox = [sectionName, index];
            $scope.selectedPhoto[sectionName][index] = null;
            Photo.suggestFilename({
                album_id: $scope.album.id,
                query: value
            }).then(function (response) {
                if ($scope.dontSuggestFilename) {
                    $scope.dontSuggestFilename = false;
                    return;
                }
                if (currentlyOnFocus !== $filenameBox[0]) return;
                console.log('filenames autosuggest: ', response);
                $filenameBox.data('typeahead').source = response;
                $filenameBox.typeahead('lookup').focus();
            }, function () {
            });
        };

        $scope.filenameSelected = function (photo) {
            $scope.dontSuggestFilename = true;
            var sectionName = $scope.currentFilenameBox[0];
            var index = $scope.currentFilenameBox[1];
            $scope.selectedPhoto[sectionName][index] = photo;
            $scope.wysiwyg[sectionName][index].element.show();
        };

        var flierToRemove = {
            patreon: null,
            flier: null
        };
        $scope.removeFlier = function (sectionName, $event) {
            $scope.errors[sectionName + '.photo'] = null;
            $scope.selectedPhoto[sectionName] = null;
            flierToRemove[sectionName] = true;
            flierFiles[sectionName] = null;
            if ($event) {
                $event.stopPropagation();
                $event.preventDefault();
                return false;
            }
        };

        $scope.showEditAlbumModal = function () {
            $('#albumSetLive').modal('hide');
            setTimeout(function () {
                $('#editAlbumModal').modal('show');
            }, 400);
        };

        $scope.saveOnly = function () {
            $scope.makeLive(true);
        };

        $scope.makeLive = function (saveOnly) {
            $scope.processing = true;
            $scope.errors = {};
            Album.makeLive({
                id: $scope.album.id,
                save_only: !!saveOnly,
                filenames: $scope.filenames,
                notes: {
                    winners: [
                        $('#notesFromYosefwinners0').val(),
                        $('#notesFromYosefwinners1').val(),
                        $('#notesFromYosefwinners2').val()
                    ],
                    runners_up: [
                        $('#notesFromYosefrunners_up0').val(),
                        $('#notesFromYosefrunners_up1').val(),
                        $('#notesFromYosefrunners_up2').val()
                    ]
                },
                patreon: {
                    photo: flierFiles.patreon,
                    title: $('#photoTitlepatreon').val(),
                    caption: $('#photoCaptionpatreon').val(),
                    remove: flierToRemove.patreon
                },
                flier: {
                    photo: flierFiles.flier,
                    title: $('#photoTitleflier').val(),
                    caption: $('#photoCaptionflier').val(),
                    remove: flierToRemove.flier
                }
            }).then(function (response) {
                $scope.processing = false;
                console.log('make album live success: ', response);
                $scope.album.live = !saveOnly;
                if (response.warning) {
                    alert(response.warning);
                    $scope.album.live = false;
                }
                location.reload(true);
            }, function (response) {
                $scope.processing = false;
                console.log('make album live failed: ', response);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else {
                    alert('Something went wrong!');
                }
            });
        };

        $scope.hideErrors = function (fieldnameOrSection, index) {
            if (!$scope.errors) return;
            if (fieldnameOrSection) $scope.errors[fieldnameOrSection] = [];
            var fieldname = fieldnameOrSection + '.' + index;
            if ($scope.errors[fieldname]) $scope.errors[fieldname] = [];
        };

    }
]);