appSettings.app.controller('userChoiceController', [
    '$scope', '$timeout', 'Auth', 'FacebookAuth', '$q', 'Variables', '$rootScope',
    function ($scope, $timeout, Auth, FacebookAuth, $q, Variables, $rootScope) {

        $scope.toSignAcceptPage = function () {
            $('#userChoice').modal('hide');
            setTimeout(function () {
                $rootScope.activeStepOfManifesto = 2;
                $('#signFramer').modal('show');
            }, 400);
        };

        $scope.signupScreen = function () {
            setTimeout(function () {
                $('#loginModal').modal('show');
            })
        };

        $scope.createAccount = function () {
            $('#userChoice').modal('hide');
            setTimeout(function () {
                $rootScope.activeStepOfManifesto = 99;
                $('#userChoiceType').modal('show');
            }, 400);
        };

    }]
);