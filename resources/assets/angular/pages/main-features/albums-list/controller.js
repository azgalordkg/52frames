appSettings.app.controller('albumsListController', [
  'Loading', '$scope', '$rootScope', 'Album', '$state', 'Photo', '$location',
  function (Loading, $scope, $rootScope, Album, $state, Photo, $location) {

    // code for make optimal height of album-items
    var width2 = $('.album_index__list .item .wrap').width();
    $('.album_index__list .item .wrap').css('height', width2 + 'px');

    $scope.path = $location.$$path;

    Album.get().then(function (response) {
      // var pagination = {
      //   ready: false,
      //   currentPage: 0,
      //   isFetching: false,
      //   tryNextPage: true,
      // };
      // for (var i = 0; i < response.length; i++) { // Old functionality with ajax-requests for getting photos data.
      //   response[i].albums.forEach(function (album, index) {
      //     var self = i;
      //     Photo.get({
      //       album_id: album.id,
      //       page: pagination.currentPage,
      //     }).then(function (photosResponse) {
      //       console.log(photosResponse);
      //       response[self].albums[index].photos = photosResponse.data.length;
      //       $scope.allYearsAlbums = response;
      //       $scope.allYearsAlbums = $scope.allYearsAlbums.map(function (yearsAlbum) { // here we setting active or not active values for tabs.
      //         yearsAlbum.active = yearsAlbum.year === new Date().getFullYear();
      //         if (yearsAlbum.active) $scope.ifActiveFound = true;
      //         return yearsAlbum;
      //       });
      //
      //       if (!$scope.ifActiveFound) $scope.allYearsAlbums[0].active = true;
      //     });
      //   });
      // }

      $scope.allYearsAlbums = response;
      $scope.allYearsAlbums = $scope.allYearsAlbums.map(function (yearsAlbum) { // here we setting active or not active values for tabs.
        yearsAlbum.active = yearsAlbum.year === new Date().getFullYear();
        if (yearsAlbum.active) $scope.ifActiveFound = true;
        return yearsAlbum;
      });

      console.log('get list of albums, success: ', response);
      Loading.pageLoaded($scope);
    }, function (response) {
      console.log('get list of albums, failed: ', response);
    });

    $scope.changeAlbumChallengePage = function() {
      if ($scope.path === '/albums') {
        $location.path('/challenges');
      } else {
        $location.path('/albums');
      }
    };

    $scope.showAlbumOrChallengePage = function (yearData, album) {
      var maxAdminPoints = $rootScope.user ? $rootScope.user.max_admin_points : 0;
      var showAlbumPage = album.live || maxAdminPoints >= 100000;
      // if (showAlbumPage) {
      //   $state.go('weekTheme', {
      //     pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
      //     pathYear: yearData.year
      //   });
      // } else {
      //   $state.go('weekChallenge', {
      //     weekNumber: album.week_number,
      //     shortUrl: album.shorturl,
      //     pathWeekTheme: album.id,
      //     pathYear: yearData.year
      //   });
      // }
      if ($scope.path === '/albums' && showAlbumPage) {
        $state.go('weekTheme', {
          pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
          pathYear: yearData.year
        });
      } else {
        $state.go('weekChallenge', {
          weekNumber: album.week_number,
          shortUrl: album.shorturl,
          pathWeekTheme: album.id,
          pathYear: yearData.year
        });
      }
    };

    $scope.openAddNewAlbumModal = function () {
      $('#newAlbumModal').modal('show');
    };

    $scope.changeActiveTab = function ($index) {
      $scope.allYearsAlbums = $scope.allYearsAlbums.map(function (yearsAlbum, index) {
        yearsAlbum.active = index === $index;
        return yearsAlbum;
      })
    }
  }]);