appSettings.app.controller('SignManifestoController', [
    '$scope', '$timeout', 'Manifesto', 'Country', 'State', 'City', '$rootScope', '$q', 'Variables',
    function ($scope, $timeout, Manifesto, Country, State, City, $rootScope, $q, Variables) {

        if ($rootScope.willSignManifesto) {
            var modalWait = setInterval(function () {
                var signManifestoModal = $('#signManifestoModal');
                if (!signManifestoModal.length) return;
                clearInterval(modalWait);
                signManifestoModal.modal('show');
                Variables.helpers.removeManifestoMarker();
            }, 250);
        }

        $('#signManifestoModal').on('show.bs.modal', function (e) {

            $rootScope.manifestoPageOpened = true;

            $scope.manifestoInput = Manifesto.manifestoInput || {};
            delete Manifesto.manifestoInput;

            if (!$scope.manifestoInput || !$scope.manifestoInput.country) {
                Country.get().then(function (countries) {
                    $scope.countries = countries;
                });
            }

            $scope.errors = {};

        });

        $scope.shortUrlBase = window.appSettings.shortUrlBase;

        $scope.nicknameLoading = {};

        var nicknameRequestCanceler = null;
        $scope.nicknameChanged = function () {
            $scope.nicknameLoading.indicator = false;
            $scope.nicknameLoading.ok = false;
            var nickname = $scope.manifestoInput.nickname;
            nickname = nickname && nickname.toLowerCase().replace(/^[^a-z]/, '').replace(/\s|[^-a-z.0-9_~]/g, '-');
            $scope.manifestoInput.nickname = nickname;
            if (nickname) {
                $scope.nicknameLoading.indicator = true;
                $scope.nicknameLoading.ok = false;
                if (nicknameRequestCanceler) nicknameRequestCanceler();
                var canceler = $q.defer();
                nicknameRequestCanceler = canceler.resolve;
                Manifesto.checkNickname({
                    user_id: $rootScope.user.id,
                    nickname: nickname
                }, {timeout: canceler.promise}).then(function (response) {
                    console.log('nickname check success: ', response);
                    $scope.nicknameLoading.indicator = false;
                    $scope.nicknameLoading.ok = true;
                    delete $scope.errors.nickname;
                    nicknameRequestCanceler = null;
                }, function (response) {
                    console.log('nickname check failed: ', response);
                    if (!response) return;
                    $scope.nicknameLoading.indicator = false;
                    $scope.nicknameLoading.ok = false;
                    if (response.validation_errors) {
                        $scope.errors.nickname = response.validation_errors.nickname;
                    } else {
                        $scope.nicknameLoading.fail = true;
                    }
                    $scope.manifestoInput.display_as_handle = false;
                    nicknameRequestCanceler = null;
                });
            } else {
                delete $scope.errors.nickname;
                $scope.manifestoInput.display_as_handle = false;
            }
        };

        $scope.focusNickname = function () {
            $('#signmanifesto-nickname').focus();
        };

        $scope.changeCountry = function () {
            $scope.states = {};
            $scope.manifestoInput.city = '';
            var country = $scope.manifestoInput.country;
            if (country && country.state_member_term) {
                State.get({
                    country_id: country.id
                }).then(function (states) {
                    $scope.states = states;
                }, function () {
                });
            }
        };

        $scope.changeState = function () {
            $scope.manifestoInput.city = '';
        };

        $scope.suggestCity = function () {
            if (!$scope.manifestoInput) return;
            var country = $scope.manifestoInput.country;
            var state = $scope.manifestoInput.state;
            var cityInput = $scope.manifestoInput.city;
            if (country && cityInput) {
                var requestInput = {
                    country_id: country.id,
                    query: cityInput
                };
                if (country.state_member_term && state) {
                    requestInput.state_id = state.id;
                }
                City.get(requestInput).then(function (cities) {
                    if ($scope.dontSuggestCity) {
                        $scope.dontSuggestCity = false;
                        return;
                    }
                    console.log('city autosuggest: ', cities);
                    var $cityInputField = $('.manifesto input[name=city]');
                    $cityInputField.data('typeahead').source = cities;
                    $cityInputField.typeahead('lookup').focus();
                }, function () {
                });
            }
        };

        $scope.citySelected = function () {
            $scope.dontSuggestCity = true;
        };

        $scope.nextPage = function () {
            $scope.errors = {};

            var manifestoInput = angular.copy($scope.manifestoInput);
            manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
            manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;
            manifestoInput.manifesto_intro = 1;

            $scope.processing = true;
            Manifesto.validate(manifestoInput).then(function (response) {
                console.log('validation success: ', response, manifestoInput);
                $scope.processing = false;
                Manifesto.manifestoInput = $scope.manifestoInput;
                $('#signManifestoModal').modal('hide');
                setTimeout(function () {
                    $('#signManifestoCheckboxesModal').modal('show');
                }, 400);
            }, function (response) {
                console.log('validation failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.message) {
                    alert(response.message);
                }
            });
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };

    }
]);