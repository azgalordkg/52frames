appSettings.app.controller('SignupController', ['$scope', '$timeout', '$rootScope', 'Auth', '$location', 'Manifesto',
  function ($scope, $timeout, $rootScope, Auth, $location, Manifesto) {

    window.fbRenderButtons();

    var loginModal = $('#loginModal');
    loginModal.on('show.bs.modal', function (e) {
      $rootScope.signupPageOpened = true;

      if (Auth.userData) {
        $scope.userData = Auth.userData;
        delete Auth.userData;
      }

      $scope.errors = {};
      $scope.showCaptcha = true;
    });

    loginModal.on('hide.bs.modal', function (e) {
      delete $scope.showCaptcha;
    });

    window.facebookSignupSuccess = function (response) {
      $promise = window.facebookLoginSuccess(response, true, 'loginModal');
      if ($promise) {
        $scope.processing = true;
        $promise.then(function () {
          $scope.processing = false;
          loginModal.modal('hide');
        }, function () {
          $scope.processing = false;
        });
      }
    };

    var instaId = $('meta[property="inst:app_id"]').attr('content');
    var instaRedirect = location.origin;
    $scope.instaLink = 'https://api.instagram.com/oauth/authorize/?client_id=' + instaId + '&redirect_uri=' + instaRedirect + '&response_type=token';

    $scope.nocaptchaSiteKey = $('meta[property="nocaptcha:sitekey"]').attr('content');

    $scope.submitSignup = function () {
      $scope.errors = {};
      var userData = $scope.userData;
      userData.captcha = $('#loginModal').find('.g-recaptcha textarea').val();

      $scope.processing = true;
      Auth.validate(userData).then(function (response) {
        $scope.processing = false;
        console.log('validation success: ', response);
        Auth.userData = userData;
        loginModal.modal('hide');
        Manifesto.manifestoInput = userData;
        setTimeout(function () {
          $rootScope.activeStepOfManifesto = 1;
          $('#userChoice').modal('show');
        }, 400);
      }, function (response) {
        $scope.processing = false;
        console.log('registration failed: ', response);

        $scope.showCaptcha = false;
        $timeout(function () {
          $scope.showCaptcha = true;
        }, 150);

        if (response.validation_errors) {
          $scope.errors = response.validation_errors;
        } else if (response.message) {
          alert(response.message);
        }
      });

    };

    $scope.loginScreen = function () {
      setTimeout(function () {
        loginModal.modal('show');
      }, 400);
    };

    $scope.signManifesto = function () {
      setTimeout(function () {
        $('#signManifestoModal').modal('show');
      }, 400);
    };

    $scope.logout = function () {
      $scope.processing = true;
      $rootScope.$navbar.logout(true).then(function (response) {
        $scope.processing = false;
      }, function (response) {
        $scope.processing = false;
      });
    };

    $scope.hideErrors = function (fieldname) {
      if(!$scope.errors) return;
      delete $scope.errors[fieldname];
    };

    $scope.onFieldMouseOver = function(index, className, block) {
      $rootScope.$emit('CallFieldMouseOver', index, className, block);
    };

  }]);