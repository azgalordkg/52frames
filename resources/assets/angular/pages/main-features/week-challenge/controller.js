appSettings.app.controller('weekChallengeController', [
  'Loading', '$scope', '$rootScope', '$stateParams', 'Album', '$location', 'Variables', 'SmoothScroll', '$timeout', '$interval',
  function (Loading, $scope, $rootScope, $stateParams, Album, $location, Variables, SmoothScroll, $timeout, $interval) {

    if (!window.moment) angular.element('head').append('<scr' + 'ipt src="/vendor/momentjs/2.10.6/moment.min.js"></scr' + 'ipt>');
    $scope.albumPath = '/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme;

    $scope.variables = Variables;

    Variables.helpers.preparePopover();

    $scope.goToAllChallenges = function() {
      $location.path('/challenges');
    };

    Album.get({
      id: $stateParams.pathWeekTheme,
      year: $stateParams.pathYear,
      is_challenge_page: true
    }).then(function (response) {
      console.log('get album entry success: ', response);
      $scope.submitBtnVisible = true;
      $scope.album = response;
      Loading.pageLoaded($scope);
      SmoothScroll.scrollToHash();
      $scope.checkIfShouldOpenForm();
    }, function (response) {
      console.log('get album entry failed: ', response);
      if (response.redirect) {
        $location.path($scope.albumPath + '-' + response.shorturl + '/challenge');
      } else $location.path('/404');
    });

    $scope.checkIfShouldOpenForm = function () {
      var shouldOpen = localStorage.getItem('openSubmitForm');
      if (shouldOpen) {
        var timeUpdate = $interval(function () {
          var submitButtonSection = $('#submitButtonSection');
          var button = submitButtonSection.find('button:visible');
          if (!button.length) return;
          $interval.cancel(timeUpdate);
          $timeout(function () {
            SmoothScroll.scrollToElementWhenPageisLoaded(submitButtonSection, function () {
              localStorage.removeItem('openSubmitForm');
              button.click().focus();
            });
          });
        }, 100);
      }
    };

    $scope.shouldShowDeadline = function () {
      return $scope.album && ($scope.album.grace_period_running || ($scope.album.after_upload_sched && !$scope.album.allows_uploads));
    };

    var $timerSwapSource;
    $scope.timerEnded = function () {
      $scope.album.grace_period_running = true;
      var i, $sourceModals = [
        $('#uploadPhotoModal'),
        $('#editPhotoModal')
      ];
      for (i = 0; i < $sourceModals.length; i++) {
        var sourceModal = $sourceModals[i];
        if (sourceModal.is(':visible')) {
          $timerSwapSource = sourceModal;
          $timerSwapSource.modal('hide');
          setTimeout(function () {
            $('#timerEndedModal').modal('show');
          }, 400);
          break;
        }
      }
    };

    $scope.swapTimerModalWithUploadForm = function () {
      $('#timerEndedModal').modal('hide');
      setTimeout(function () {
        $timerSwapSource.modal('show');
      }, 400);
    };

    $scope.submitPhoto = function () {
      if ($scope.album.user_has_uploaded) {
        $('#uploadPhotoSuccessModal').modal('show');
      } else {
        $('#uploadPhotoModal').modal('show');
      }
    };

    $scope.uploadSuccessCallback = function (photo) {
      console.log('uploadSuccessCallback: ', $scope.album);
      $scope.album.user_has_uploaded = photo;
      if ($rootScope.user) $rootScope.user.has_uploaded_before = true;
    };

    $scope.editAlbum = function () {
      $('#editAlbumModal').modal('show');
    };

    $scope.photoDeleteCallback = function (has_uploaded_before) {
      if ($rootScope.user) $rootScope.user.has_uploaded_before = !!has_uploaded_before;
    };

    $scope.onSubmitClicked = function () {
      if ($scope.album.allows_uploads) {
        $('#modal-allow-upload-image').modal('show');
        // $rootScope.oldAlbumForUpload = $scope.album;
      } else {
        $rootScope.$emit('CallSubmitPhotoMethod', null);
      }
    };

    $scope.signManifesto = function() {
      $rootScope.$emit('CallSignManifestoMethod', {});
    };
    
    $scope.goToAlbumPage = function () {
      $location.path('/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme);
    }
  }
]);