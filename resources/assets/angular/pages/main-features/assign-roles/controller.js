appSettings.app.controller('AssignRolesController', ['$scope', '$timeout', 'Role', 'UserRole', function ($scope, $timeout, Role, UserRole) {

    $scope.roles = [];

    Role.get({dynamic: 0}).then(function (response) {
        console.log('roles list success: ', response);
        $scope.roles = response;
        $scope.selectedRole = $scope.findCurrentRole(response);
    }, function (response) {
        console.log('roles list failed: ', response);
    });

    $scope.findCurrentRole = function (roles) {
        for (var i = 0; i < roles.length; i++) {
            var role = roles[i];
            if (role.access_level_points == $scope.userData.max_admin_points) {
                return role;
            }
        }
    };

    $scope.hasRole = function (role) {
        return $scope.userData.max_admin_points >= role.access_level_points;
    };

    $scope.roleClicked = function (role) {
        $scope.selectedRole = role;
        $scope.userData.max_admin_points = role.access_level_points;
    };

    $scope.saveChanges = function () {
        $scope.processing = true;
        if ($scope.selectedRole) {
            UserRole.save({
                user_id: $scope.userData.id,
                role_id: $scope.selectedRole.id
            }).then(function (response) {
                $scope.processing = false;
                console.log('saving user role success', response);
                $scope.closeModal();
            }, function (response) {
                $scope.processing = false;
                console.log('saving user role failed', response);
                alert('Something went wrong');
            });
        } else {
            UserRole.delete({
                user_id: $scope.userData.id,
                dynamic: 0
            }).then(function (response) {
                $scope.processing = false;
                console.log('delete user role success', response);
                $scope.closeModal();
            }, function (response) {
                $scope.processing = false;
                console.log('delete user role failed', response);
                alert('Something went wrong');
            });
        }
    };

    $scope.revokeAccess = function () {
        $scope.userData.max_admin_points = 0;
        delete $scope.selectedRole;
    };

    $scope.closeModal = function () {
        $('#assignRolesModal').modal('hide');
        if ($scope.userData.self_account) {
            $timeout(function () {
                window.location.reload(false);
            }, 250);
        }
    };

}]);