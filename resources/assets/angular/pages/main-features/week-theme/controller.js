appSettings.app.controller('weekThemeController', [
    'Loading', '$scope', '$rootScope', '$stateParams', 'Album', '$location', 'Photo', '$filter', '$state', 'PhotosAggregate', 'InterPageHelper',
    function (Loading, $scope, $rootScope, $stateParams, Album, $location, Photo, $filter, $state, PhotosAggregate, InterPageHelper) {

        var pageLoaded = false;

        var folderPath = '/albums/' + $stateParams.pathYear + '/';

        var pagination = {};

        var resetPagination = function () {
            pagination = {
                ready: false,
                currentPage: 0,
                isFetching: false,
                tryNextPage: true,
            };
            $scope.total_photos = 0;
            $scope.photos = [];
        };

        var pushPhotosToArray = function (photosResponse) {
            $scope.total_photos = photosResponse.total;
            if (!photosResponse.next_page_url) pagination.tryNextPage = false;
            for (var i = 0; i < photosResponse.data.length; i++) {
                var photo = photosResponse.data[i];
                $scope.photos.push(photo);
            }
        };

        var getPhotosAndPushToArray = function (successCb, failCb) {
            if (pagination.isFetching) return;
            if (!pagination.tryNextPage) return;
            pagination.isFetching = true;
            pagination.currentPage++;
            Photo.get({
                album_id: $scope.album.id,
                page: pagination.currentPage,
                filter_by: $location.search().filter_by
            }).then(function (response) {
                console.log('get list of photos success: ', response);
                pagination.isFetching = false;
                pushPhotosToArray(response);
                if (successCb) successCb();
            }, function (response) {
                console.log('get list of photos failed: ', response);
                pagination.isFetching = false;
                if (failCb) failCb();
            });
        };

        resetPagination();

        var initPage = function () {
            Album.get({
                id: $stateParams.pathWeekTheme,
                year: $stateParams.pathYear,
                filter_by: $location.search().filter_by,
                is_challenge_page: false
            }).then(function (response) {
                console.log('get album entry success: ', response);

                if (!response.publicly_visible) Loading.pageRequiresAdminRole($scope);

                $scope.album = response;
                $scope.fullPath = folderPath + 'week-' + $scope.album.week_number + '-' + $scope.album.shorturl;

                pagination.currentPage++;
                pushPhotosToArray(response.photosData);
                pagination.ready = true;

                pageLoaded = Loading.pageLoaded($scope);

            }, function (response) {
                console.log('get album entry failed: ', response);
                if (response.redirect) {
                    $location.path(folderPath + $stateParams.pathWeekTheme + '-' + response.shorturl);
                } else if (response.show_challenge_page) {
                    $location.path(folderPath + $stateParams.pathWeekTheme + '/challenge');
                } else $location.path('/404');
            });
        };

        $scope.queryString = function () {
            return $location.search();
        };

        $scope.applyFilter = function (filterBy) {
            $location.search('filter_by', filterBy);
        };

        if (!$stateParams.pathUser) initPage();
        else $scope.$on('photoPageLoaded', function (event, args) {
            if (!pageLoaded) initPage();
        });

        $scope.editAlbum = function () {
            $('#editAlbumModal').modal('show');
        };

        $scope.$on('update:photoNumLoves', function (event, photo) {
            var targetPhoto = $filter('filter')($scope.photos, function (value, index) {
                return value.id === photo.id;
            });
            if (targetPhoto.length) {
                targetPhoto[0].num_loves = photo.num_loves;
                targetPhoto[0].hearted = photo.hearted;
            }
        });

        $scope.$on('update:photoNumComments', function (event, photo) {
            var targetPhoto = $filter('filter')($scope.photos, function (value, index) {
                return value.id === photo.id;
            });
            if (targetPhoto.length) targetPhoto[0].num_comments = photo.num_comments;
        });

        $scope.$on('follow', function (event, userId) {
            if ($location.search().filter_by === 'following') {
                var originalPageCount = pagination.currentPage;
                resetPagination();
                var getNextPage = function () {
                    getPhotosAndPushToArray(function () {
                        if (pagination.tryNextPage && pagination.currentPage < originalPageCount)
                            getNextPage();
                    });
                };
                getNextPage();
            }
        });

        $scope.$on('unfollow', function (event, userId) {
            if ($location.search().filter_by === 'following') {
                var found = false;
                $scope.photos = $filter('filter')($scope.photos, function (photo, index) {
                    found = photo.user_id === userId;
                    return photo.user_id !== userId;
                });
                if ($scope.total_photos) $scope.total_photos--;
            }
        });

        $scope.loadMorePhotos = function () {
            if (!pagination.ready) return;
            getPhotosAndPushToArray();
        };

        $scope.viewPhoto = function (photo) {
            InterPageHelper.forNextPage.resolveIntendedModelThenSave(function () {
                return PhotosAggregate.findOne(photo.id);
            });
            $state.go('weekTheme.photo', {
                pathUser: $rootScope.$navbar.getSelfHandleOrId(photo.owner)
            });
        };

        $scope.showAlbumSetLiveModal = function () {
            $('#albumSetLive').modal('show');
        };


        // If Photo has Nudity flag, check if User has already clicked Show on this page

        $scope.$on('fromPhotoView:get:photo.show', function (event, photo) {
            var localPhoto = $filter('filter')($scope.photos, function (value, index) {
                return value.id === photo.id;
            });
            photo.show = localPhoto.length ? !!localPhoto[0].show : false;
        });

        $scope.$on('fromPhotoView:set:photo.show', function (event, photo) {
            var localPhoto = $filter('filter')($scope.photos, function (value, index) {
                return value.id === photo.id;
            });
            if (localPhoto.length) localPhoto[0].show = !!photo.show;
        });


        // When Photo is Deleted from the Photo view

        $scope.$on('fromPhotoView:photoDeleted', function (event, photoId, dontReloadPage) {
            $scope.photos = $filter('filter')($scope.photos, function (value, index) {
                return value.id !== photoId;
            });
            if ($scope.total_photos) $scope.total_photos--;
            if (!dontReloadPage) location.reload(true);
        });


        // Related to Editing a Photo

        var interPageCallbacks = {};
        var resetCallbacks = function () {
            interPageCallbacks = {
                modalClosedCb: angular.noop,
                editSuccessCb: angular.noop
            };
        };
        resetCallbacks();

        $scope.$on('fromPhotoView:updateThumbnail', function (event, targetPhotoId, editedVersion) {
            var photo = $filter('filter')($scope.photos, function (value, index) {
                return value.id === targetPhotoId;
            });
            if (photo && photo[0]) angular.copy(editedVersion, photo[0]);
        });

        $scope.$on('fromPhotoView:requesting:showEditPhotoModal', function (event, photo, modalClosedCb, editSuccessCb) {
            resetCallbacks();
            $scope.editPhoto(photo);
            if (angular.isFunction(modalClosedCb)) interPageCallbacks.modalClosedCb = modalClosedCb;
            if (angular.isFunction(editSuccessCb)) interPageCallbacks.editSuccessCb = editSuccessCb;
        });

        $scope.editPhoto = function (photo) {
            $scope.photoIsForEditing = photo;
            setTimeout(function () {
                $('#editPhotoModalOnAlbumPage').modal('show');
            }, 100);
        };

        $scope.editPhotoModalClosed = function () {
            delete $scope.photoIsForEditing;
            interPageCallbacks.modalClosedCb();
            resetCallbacks();
        };

        $scope.editPhotoSuccess = function (editedPhoto) {
            if (!$scope.photoIsForEditing) return;
            if ($scope.photoIsForEditing.id !== editedPhoto.id)
                location.reload(true);
            else {
                editedPhoto.show = $scope.photoIsForEditing.show;
                angular.copy(editedPhoto, $scope.photoIsForEditing);
                interPageCallbacks.editSuccessCb(editedPhoto);
            }
        };

        $scope.openBypassUploadModal = function () {
            $('#allowSomeoneToUploadModal').modal('show');
        };

    }
]);
































