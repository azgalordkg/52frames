appSettings.app.controller('SignupConfirmController', [
    '$scope', '$timeout', 'Auth', 'FacebookAuth', '$q', 'Variables', 'InstagramAuth',
    function ($scope, $timeout, Auth, FacebookAuth, $q, Variables, InstagramAuth) {

        $scope.fbAuth = FacebookAuth;
        $scope.instAuth = InstagramAuth;
        $scope.normalAuth = Auth;

        $scope.nocaptchaSiteKey = $('meta[property="nocaptcha:sitekey"]').attr('content');

        console.log($scope.nocaptchaSiteKey);

        var signupConfirmModal = $('#signupConfirmModal');

        signupConfirmModal.on('show.bs.modal', function (e) {

            $scope.isFbSignup = FacebookAuth.userData;
            $scope.isInstSignup = InstagramAuth.userData;

            if ($scope.isFbSignup) {
                $scope.userData = FacebookAuth.userData;
                delete FacebookAuth.userData;
            } else if ($scope.isInstSignup) {
                $scope.userData = InstagramAuth.userData;
                delete InstagramAuth.userData;
            } else {
                $scope.userData = Auth.userData || {};
                delete Auth.userData;
            }

            if (!$scope.userData.hasOwnProperty('newsletter')) {
                $scope.userData.newsletter = true;
            }

            $scope.showCaptcha = true;

            $scope.errors = {};
        });

        signupConfirmModal.on('hide.bs.modal', function (e) {
            delete $scope.showCaptcha;
            delete $scope.isFbSignup;
            delete $scope.isInstSignup;
        });

        $scope.createAccount = function (promise) {

            var deferred = promise && $q.defer();

            $scope.errors = {};

            var userData = $scope.userData;
            userData.captcha = $('#signupConfirmModal').find('.g-recaptcha textarea').val();
            userData.is_fan = !promise;

            function registrationSuccess(response) {
                promise && deferred.resolve(response);
                if ($scope.willSignManifesto) {
                    window.location.reload(false);
                } else {
                    var intendedPage = getIntendedPage();
                    if (intendedPage) {
                        window.location.href = intendedPage;
                    } else {
                        window.location.reload(false);
                    }
                }
                $scope.processing = false;
            }

            function registrationFailed(response) {
                console.log('registration failed: ', response);
                $scope.processing = false;
                $scope.showCaptcha = false;
                $timeout(function () {
                    $scope.showCaptcha = true;
                }, 150);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.message) {
                    alert(response.message);
                }
                promise && deferred.reject(response);
            }

            $scope.processing = true;
            if ($scope.isFbSignup) {
                FacebookAuth.registerUser(userData).then(registrationSuccess, registrationFailed);
            } else  if ($scope.isInstSignup) {
                InstagramAuth.registerUser(userData).then(registrationSuccess, registrationFailed);
            } else {
                Auth.registerUser(userData).then(registrationSuccess, registrationFailed);
            }

            return promise && deferred.promise;

        };

        $scope.signManifesto = function () {
            $scope.willSignManifesto = 1;
            $scope.createAccount(true).then(function (response) {
                Variables.helpers.leaveManifestoMarker($scope.willSignManifesto);
            }, function () {});
        };

        $scope.signupScreen = function () {
            Auth.userData = $scope.userData;
            setTimeout(function () {
                $('#loginModal').modal('show');
            }, 400);
        };

        $scope.fbLoginGoBack = function () {
            setTimeout(function () {
                $('#' + FacebookAuth.fromWhere).modal('show');
            }, 400);
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            delete $scope.errors[fieldname];
        };

    }]
);