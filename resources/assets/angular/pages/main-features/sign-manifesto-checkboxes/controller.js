appSettings.app.controller('SignManifestoCheckboxesController', ['$scope', '$timeout', 'Manifesto', function ($scope, $timeout, Manifesto) {

    $('#signManifestoCheckboxesModal').on('show.bs.modal', function (e) {

        $scope.manifestoInput = Manifesto.manifestoInput || {};
        delete Manifesto.manifestoInput;

        $scope.errors = {};

    });

    $scope.submit = function () {
        $scope.errors = {};

        var manifestoInput = angular.copy($scope.manifestoInput);
        manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
        manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;
        manifestoInput.manifesto_questions = 1;

        $scope.processing = true;
        Manifesto.validate(manifestoInput).then(function (response) {
            console.log('success: ', response);
            $scope.processing = false;
            Manifesto.manifestoInput = $scope.manifestoInput;
            $('#signManifestoCheckboxesModal').modal('hide');
            setTimeout(function () {
                $('#signManifestoWhyModal').modal('show');
            }, 400);
        }, function (response) {
            console.log('failed: ', response);
            $scope.processing = false;
            if (response.validation_errors) {
                $scope.errors = response.validation_errors;
            } else if (response.message) {
                alert(response.message);
            }
        });
    };

    $scope.back = function () {
        Manifesto.manifestoInput = $scope.manifestoInput;
        setTimeout(function () {
            $('#signManifestoModal').modal('show');
        }, 400);
    };

    $scope.hideErrors = function (fieldname) {
        if (!$scope.errors) return;
        $scope.errors[fieldname] = '';
    };

}]);