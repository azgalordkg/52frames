appSettings.app.controller('AllowUploadBypassController', [
    '$rootScope', '$scope', '$timeout', 'BypassUpload', 'User', '$q',
    function ($rootScope, $scope, $timeout, BypassUpload, User, $q) {

        var thisModal = $('#allowSomeoneToUploadModal');

        $scope.input = "";
        $scope.usersList = [];

        thisModal.on('show.bs.modal', function (e) {
            $scope.getUsersList();
        });

        $scope.replaceUsersList = function (usersData) {
            $scope.usersList = usersData;
        };

        $scope.getUsersList = function () {
            $scope.loadingUsersList = true;
            BypassUpload.get({
                album_id: $scope.album.id
            }).then(function (response) {
                console.log('get list of bypass uploaders success: ', response);
                $scope.replaceUsersList(response);
                $scope.loadingUsersList = false;
            }, function (response) {
                console.log('get list of bypass uploaders failed: ', response);
                $scope.loadingUsersList = false;
            });
        };

        var dbCanceler;
        var $inputField;
        $scope.suggestUsers = function () {
            var userQuery = $scope.input;
            if (userQuery) {
                if (dbCanceler) dbCanceler.resolve();
                dbCanceler = $q.defer();
                User.suggestPhotographer({query: userQuery}, {timeout: dbCanceler.promise}).then(function (response) {
                    if ($scope.input !== userQuery) return;
                    if ($scope.dontSuggestUser) {
                        $scope.dontSuggestUser = false;
                        return;
                    }
                    console.log('user autosuggest: ', response);
                    $inputField = $('#new-user-input');
                    $inputField.data('typeahead').source = response;
                    $inputField.typeahead('lookup').focus();
                }, function () {
                });
            }
        };

        $scope.suggestionMatcher = function (user) {
            return true;
        };

        $scope.generateSuggestDisplay = function (user) {
            if (user.handle && user.handle.toLowerCase().match($scope.input)) return user.handle;
            if (user.email && user.email.toLowerCase().match($scope.input)) return user.email;
            if ((user.id + '').match($scope.input)) return user.id + '';
            return user.name;
        };

        $scope.autoSuggestHighlighter = function (generatedDisplay, user) {
            var imgSrc = user.avatar ? ' src="' + user.avatar.avatar_small + '"' : '';
            var handleText = user.handle ? ' (' + user.handle + ')' : '';
            return '<div class="suggestion-holder">' +
                '<img' + imgSrc + ' class="avatar">' +
                '<div class="name-holder">' +
                '<div>' + user.name + handleText + '</div>' +
                '<div class="email-holder">' + user.email + '</div>' +
                '</div>' +
                '</div>';
        };

        $scope.userSelected = function (user) {
            $scope.dontSuggestUser = true;
            $scope.addNewUser(user);
        };

        $scope.addNewUser = function (user) {
            console.log('add new user: ', user);
            var confirmAddNewUser = confirm('Send an email to ' + user.name + ' (' + user.email + ')?');
            if (confirmAddNewUser) {
              $scope.processing = true;
              BypassUpload.save({
                user_id: user.id,
                album_id: $scope.album.id
              }).then(function (response) {
                console.log('add user success: ', response);
                $scope.replaceUsersList(response.new_list);
                $scope.processing = false;
                $scope.input = '';
                if (response.photo_id_removed) $rootScope.$broadcast('fromPhotoView:photoDeleted', response.photo_id_removed, true);
              }, function (response) {
                console.log('add user failed: ', response);
                $scope.processing = false;
                $scope.input = '';
                $scope.getUsersList();
                alert('Something went wrong, please considering adding the user again, so they will receive the Email notification, etc');
              });
            }
        };

        $scope.removeUser = function (userData) {
            userData.processing = true;
            BypassUpload.delete({
                album_id: userData.album_id,
                user_id: userData.user_id
            }).then(function (response) {
                console.log('delete bypass uploader success: ', response);
                $scope.replaceUsersList(response.new_list);
                userData.processing = false;
            }, function (response) {
                console.log('delete bypass uploader failed: ', response);
                userData.processing = false;
            });
        };

    }
]);