appSettings.app.controller('photographerController', [
  'Loading', '$scope', 'Photographer', '$location', '$stateParams', '$rootScope', '$filter',
  '$state', 'PhotosAggregate', 'InterPageHelper', '$http', 'Following', 'Token',
  function (Loading, $scope, Photographer, $location, $stateParams, $rootScope, $filter,
            $state, PhotosAggregate, InterPageHelper, $http, Following, Token) {

    var pageLoaded = false;
    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';

    $scope.total_photos = 0;
    $scope.userPhotos = [];

    var pushPhotosToArray = function (photosResponse) {
      $scope.total_photos = photosResponse.total;
      if (!photosResponse.next_page_url) pagination.tryNextPage = false;
      for (var i = 0; i < photosResponse.data.length; i++) {
        var photo = photosResponse.data[i];
        $scope.userPhotos.push(photo);
      }
    };

    var getPhotosAndPushToArray = function () {
      if (pagination.isFetching) return;
      if (!pagination.tryNextPage) return;
      pagination.isFetching = true;
      pagination.currentPage++;
      Photographer.getPhotos({
        id: $stateParams.userIdOrShorturl,
        page: pagination.currentPage
      }).then(function (response) {
        console.log('getting photos success: ', response);
        pagination.isFetching = false;
        pushPhotosToArray(response);
      }, function (response) {
        console.log('getting photos failed: ', response);
        pagination.isFetching = false;
      });
    };

    var pagination = {
      ready: false,
      currentPage: 0,
      isFetching: false,
      tryNextPage: true
    };

    $scope.loadMorePhotos = function () {
      if (!pagination.ready) return;
      getPhotosAndPushToArray();
    };

    // function for getting data for Loves, Followers and Following tabs;
    function getTabsData (userData) {
      var requests = ['loves', 'followers', 'following'];
      requests.forEach(function (requestName) {
        $http.get(apiUrl + 'photographer/' + userData.id + '/' + requestName).then(
          function (success) {
            userData[requestName] = success.data;
            console.log('Success of ' + requestName + ': ', success.data);
          }, function (error) {
            console.log('Success of ' + requestName + ': ', error);
          }
        );
      });

      return userData;
    }

    var initPage = function () {
      Photographer.get({
        id: $stateParams.userIdOrShorturl
      }).then(function (response) {
        console.log('get profile success:', response);

        if (!response.manifesto) {
          return $location.path('/404');
        }

        $scope.userData = response;
        $scope.userData = getTabsData($scope.userData);
        var created_at = new Date(response.created_at);
        $scope.framerSince = created_at.getFullYear();

        pagination.currentPage++;
        pushPhotosToArray(response.photosData);
        pagination.ready = true;

        pageLoaded = Loading.pageLoaded($scope);

      }, function (response) {
        console.log('get profile failed', response);
        if (response.user_has_handle) {
          $location.path('/photographer/' + response.user.handle);
        } else if (response.user_not_found) {
          $location.path('/404');
        }
      });
    };

    if (!$stateParams.pathYear || !$stateParams.pathWeekTheme) initPage();
    else $scope.$on('photoPageLoaded', function (event, args) {
      if (!pageLoaded) initPage();
    });

    $scope.$on('update:photoNumLoves', function (event, photo) {
      var targetPhoto = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id === photo.id;
      });
      if (targetPhoto.length) {
        targetPhoto[0].num_loves = photo.num_loves;
        targetPhoto[0].hearted = photo.hearted;
      }
    });

    $scope.$on('update:photoNumComments', function (event, photo) {
      var targetPhoto = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id === photo.id;
      });
      if (targetPhoto.length) targetPhoto[0].num_comments = photo.num_comments;
    });


    $scope.$on('follow', function (event, userId, values) {
      if ($scope.userData && $scope.userData.id === userId && $rootScope.user) {
        $scope.userData.followers_count = values.followers_count;
        $scope.userData.following_count = values.following_count;
        $scope.userData.im_following = true;
      }
    });

    $scope.$on('unfollow', function (event, userId, values) {
      if ($scope.userData && $scope.userData.id === userId && $rootScope.user) {
        $scope.userData.followers_count = values.followers_count;
        $scope.userData.following_count = values.following_count;
        $scope.userData.im_following = false;
      }
    });

    $scope.openEditManifestoModal = function () {
      $scope.editingManifesto = true;
      $('#editManifestoModal').modal('show');
    };

    $scope.editProfileSuccess = function (userData) {
      Object.keys(userData).forEach(function (key) {
        $scope.userData[key] = userData[key];
      });
    };

    $scope.viewPhoto = function (photo) {
      InterPageHelper.forNextPage.resolveIntendedModelThenSave(function () {
        return PhotosAggregate.findOne(photo.id);
      });
      $state.go('photographer.photo', {
        pathYear: photo.album.year,
        pathWeekTheme: 'week-' + photo.album.week_number + '-' + photo.album.shorturl
      });
    };

    // Admin-related scripts

    $scope.adminActions = {
      openAssignRolesModal: function () {
        $('#assignRolesModal').modal('show');
      }
    };

    // If Photo has Nudity flag, check if User has already clicked Show on this page

    $scope.$on('fromPhotoView:get:photo.show', function (event, photo) {
      var localPhoto = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id === photo.id;
      });
      photo.show = localPhoto.length ? !!localPhoto[0].show : false;
    });

    $scope.$on('fromPhotoView:set:photo.show', function (event, photo) {
      var localPhoto = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id === photo.id;
      });
      if (localPhoto.length) localPhoto[0].show = !!photo.show;
    });


    // When Photo is Deleted from the Photo view

    $scope.$on('fromPhotoView:photoDeleted', function (event, photoId) {
      $scope.userPhotos = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id !== photoId;
      });
      location.reload(true);
    });

    // Related to Editing a Photo

    var interPageCallbacks = {};
    var resetCallbacks = function () {
      interPageCallbacks = {
        modalClosedCb: angular.noop,
        editSuccessCb: angular.noop
      };
    };
    resetCallbacks();

    $scope.$on('fromPhotoView:updateThumbnail', function (event, targetPhotoId, editedVersion) {
      var photo = $filter('filter')($scope.userPhotos, function (value, index) {
        return value.id === targetPhotoId;
      });
      if (photo && photo[0]) angular.copy(editedVersion, photo[0]);
    });

    $scope.$on('fromPhotoView:requesting:showEditPhotoModal', function (event, photo, modalClosedCb, editSuccessCb) {
      resetCallbacks();
      $scope.editPhoto(photo);
      if (angular.isFunction(modalClosedCb)) interPageCallbacks.modalClosedCb = modalClosedCb;
      if (angular.isFunction(editSuccessCb)) interPageCallbacks.editSuccessCb = editSuccessCb;
    });

    $scope.editPhoto = function (photo) {
      $scope.photoIsForEditing = photo;
      setTimeout(function () {
        $('#editPhotoModalOnProfilePage').modal('show');
      }, 100);
    };

    $scope.editPhotoModalClosed = function () {
      delete $scope.photoIsForEditing;
      interPageCallbacks.modalClosedCb();
      resetCallbacks();
    };

    $scope.editPhotoSuccess = function (editedPhoto) {
      if (!$scope.photoIsForEditing) return;
      if ($scope.photoIsForEditing.id !== editedPhoto.id)
        location.reload(true);
      else {
        editedPhoto.album = $scope.photoIsForEditing.album;
        editedPhoto.show = $scope.photoIsForEditing.show;
        angular.copy(editedPhoto, $scope.photoIsForEditing);
        interPageCallbacks.editSuccessCb(editedPhoto);
      }
    };

    // functionality for tabs

    $scope.tabs = [{active: true}, {active: false}, {active: false}];

    $scope.toggleTabs = function (index) {
      $scope.tabs = $scope.tabs.map((tabItem, i) => {
        tabItem.active = i === index;
        return tabItem;
      });
    };

    $scope.goToUsersPage = function (followingUser) {
      $location.path('/photographer/' + (followingUser.handle || followingUser.id));
    };
    
    // method for open photos of user or loved

    $scope.openPhoto = function (photo, loved) {
      InterPageHelper.forNextPage.resolveIntendedModelThenSave(function () {
        return PhotosAggregate.findOne(photo.id);
      });
      $location.path('/photographer/' + $scope.userData.id + (loved ? '/photo/loves/' : '/photo/') + photo.id);
    }

    // methods for follow or unfollow user

    $scope.follow = function () {
      $scope.processing2 = true;
      Following.save({user_id: $scope.userData.id}).then(function (response) {
        console.log('follow this person success', response);
        $scope.processing2 = false;
        $scope.userData.im_following = true;
        $rootScope.$broadcast('follow', $scope.userData.id, response);
      }, function (response) {
        console.log('follow this person failed', response);
        $scope.processing2 = false;
      });
    };

    $scope.unfollow = function () {
      $scope.processing2 = true;
      Following.delete({user_id: $scope.userData.id}).then(function (response) {
        console.log('unfollow this person success', response);
        $scope.processing2 = false;
        $scope.userData.im_following = false;
        $rootScope.$broadcast('unfollow', $scope.userData.id, response);
      }, function (response) {
        console.log('unfollow this person success', response);
        $scope.processing2 = false;
      });
    };

    // function for update profile avatar in profile page and in navbar
    var updateProfileAvatar = function (data) {
      $scope.userData.avatar = {};
      $rootScope.user.avatar = {};
      $scope.userData.avatar.avatar_big = data.hi_res_filename;
      $scope.userData.avatar.avatar_small = data.thumbnail_filename;
      $rootScope.user.avatar.avatar_big = data.hi_res_filename;
      $rootScope.user.avatar.avatar_small = data.thumbnail_filename;
    };
    // function for update profile cover in profile page
    var updateProfileBackground = function(data) {
      $scope.userData.background = {};
      $scope.userData.background.background_big = data.hi_res_filename;
      $scope.userData.background.background_small = data.thumbnail_filename;
    };

    $scope.onAvatarChange = function (files, type) {
      var file = files[0];

      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        var url = apiUrl + 'settings/' + type;
        var formData = new FormData();
        var headers = {
          transformRequest: angular.identity,
          headers: { 'Authorization': 'Bearer ' + Token.get(), "Content-Type": undefined }
        };

        if (type === 'avatar') $scope.avatarLoading = true;
        else $scope.bgLoading = true;

        formData.append('photo', file);
        $http.post(url, formData, headers).then(
          function (success) {
            console.log(type + ' post success: ', success.data);
            type === 'avatar' ? updateProfileAvatar(success.data.record) : updateProfileBackground(success.data.record);

            $scope.avatarLoading = false;
            $scope.bgLoading = false;
          }, function (error) {
            console.log(type + ' post error: ', error);

            $scope.avatarLoading = false;
            $scope.bgLoading = false;
          }
        )
      }
    };
  }]
);