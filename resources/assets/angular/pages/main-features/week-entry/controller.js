appSettings.app.controller('weekEntryController', [
    '$location', '$scope', '$stateParams', '$state', 'Photo', 'Variables', '$timeout', 'Comment', '$filter',
    '$rootScope', 'Helpers', 'PhotosAggregate', 'InterPageHelper', 'DataRelatedToCurrentUrlStorage', 'PromiseRequest',
    'PhotoViewNextPrevHelper', 'Analytics',
    function ($location, $scope, $stateParams, $state, Photo, Variables, $timeout, Comment, $filter,
              $rootScope, Helpers, PhotosAggregate, InterPageHelper, DataRelatedToCurrentUrlStorage, PromiseRequest,
              PhotoViewNextPrevHelper, Analytics) {

        $scope.analytics = {
            photoLoaded: function ($trackedItemId) {
                var okToCancel = true;
                var impression = $timeout(function () {
                    okToCancel = false;
                    Analytics.newEntry('photo_view_unique_3secs', $trackedItemId);
                }, 3000);
                $scope.$on('$destroy', function (event) {
                    if (!okToCancel) return;
                    $timeout.cancel(impression);
                });
            },
            photoLoadingError: function ($trackedItemId) {
                Analytics.errorLoadingAsset('photo_view_unique_3secs', $trackedItemId);
            }
        };

        var headersHeight = $('.header').eq(0).height();
        $('.photo_view_container').eq(0).css({
            "top" : headersHeight + 'px',
        });

        // Common

        $scope.variables = Variables;

        Variables.helpers.preparePopover();

        $scope.isProfilePage = $stateParams.userIdOrShorturl ? 1 : 0;

        $scope.helpers = Helpers;

        $scope.findCritiqueLevel = Variables.helpers.findCritiqueLevel;

        $scope.commentsHolder = {
            previous_count: 0,
            can_load_previous: 0,
            comments: [],
            total: 0
        };
        $scope.newComment = {message: ''};

        var mergeComments = function (commentsToAdd) {
            for (var i in $scope.commentsHolder.comments) {
                commentsToAdd.comments.push($scope.commentsHolder.comments[i]);
            }
            $scope.commentsHolder = commentsToAdd;
        };


        // HTML fixes

        var $body = $('body');
        $body.addClass('hide-scrollbar');
        $scope.$on('$destroy', function () {
            $body.removeClass('hide-scrollbar');
        });


        // Init Page Load

        var browsingBooster = {

            getIntendedModel: function () {
                var intendedPhoto = InterPageHelper.currentPage.getIntendedModel();
                var dataRelatedToUrl = DataRelatedToCurrentUrlStorage.getAllByKeys();
                if (!intendedPhoto && dataRelatedToUrl.photo) intendedPhoto = dataRelatedToUrl.photo;
                return {
                    intendedPhoto: intendedPhoto,
                    dataRelatedToUrl: dataRelatedToUrl
                };
            },

            savePhotoToUrlVars: function (photo, varKeyName) {
                if (!photo || !photo.id) return null;
                var photoSummary = PhotosAggregate.findOne(photo.id);
                DataRelatedToCurrentUrlStorage.saveOrUpdate(photoSummary, varKeyName);
                return photoSummary;
            },

            applyPhotoSummariesToScope: function (summaries) {
                $scope.photoSummary = summaries.photoSummary;
                $scope.nextPhotoSummary = summaries.nextPhotoSummary;
                $scope.previousPhotoSummary = summaries.previousPhotoSummary;
            }

        };


        // Attempts to look for previous variables utilized by this url, earlier.

        (function () {

            var intent = browsingBooster.getIntendedModel();
            if (!intent.intendedPhoto) return;

            browsingBooster.applyPhotoSummariesToScope({
                photoSummary: PhotosAggregate.findOne(intent.intendedPhoto.id),
                nextPhotoSummary: intent.dataRelatedToUrl.nextPhoto,
                previousPhotoSummary: intent.dataRelatedToUrl.previousPhoto
            });

        })();


        // Get Photo, for display.

        var initPhotoRequestCallback = function () {

            return Photo.get({
                year: $stateParams.pathYear,
                week_path: $stateParams.pathWeekTheme,
                id: $stateParams.userIdOrShorturl || $stateParams.pathUser,
                is_profile_page: $scope.isProfilePage,
                filter_by: $location.search().filter_by
            }).then(function (photo) {
                console.log('get photo success: ', photo);

                $scope.photo = photo;
                $rootScope.$broadcast('fromPhotoView:get:photo.show', photo);

                browsingBooster.applyPhotoSummariesToScope({
                    photoSummary: browsingBooster.savePhotoToUrlVars(photo, 'photo'),
                    nextPhotoSummary: browsingBooster.savePhotoToUrlVars(photo.nextPhoto, 'nextPhoto'),
                    previousPhotoSummary: browsingBooster.savePhotoToUrlVars(photo.previousPhoto, 'previousPhoto')
                });

                $scope.newComment.photo_id = photo.id;
                mergeComments(photo.commentsData);

                $rootScope.$broadcast('photoPageLoaded');
            }, function (response) {
                console.log('get photo failed: ', response);
                var targetUrl;
                if (response.redirect) {
                    if ($stateParams.pathUser) {
                        targetUrl = Helpers.formWeekThemeUrl(
                            $stateParams.pathYear,
                            $stateParams.pathWeekTheme,
                            response.shorturl,
                            $stateParams.pathUser
                        );
                        $location.path(targetUrl);
                    } else {
                        targetUrl = Helpers.formProfileThemeUrl(
                            $stateParams.userIdOrShorturl,
                            $stateParams.pathYear,
                            $stateParams.pathWeekTheme,
                            response.shorturl
                        );
                        $location.path(targetUrl);
                    }
                } else if (response.show_challenge_page) {
                    targetUrl = Helpers.formWeekThemeUrl(
                        $stateParams.pathYear,
                        $stateParams.pathWeekTheme
                    );
                    $location.path(targetUrl + '/challenge');
                } else if (response.photo_not_found) {
                    window.location = '/404';
                } else if (response.user_not_found) {
                    window.location = '/404';
                } else if (response.no_such_album) {
                    window.location = '/404';
                }
            });

        };

        PhotoViewNextPrevHelper.setup(initPhotoRequestCallback)
            .onInitOnTempPage(angular.noop)
            .onInitOnRealPage(angular.noop);

        $scope.heartPhoto = function (photo) {
            photo.hearted = true;
            Photo.heartPhoto(photo).then(function (response) {
                console.log('hearPhoto success: ', response);
                photo.num_loves = response.photo_num_loves;
                $scope.$broadcast('update:photoNumLoves', photo);
            }, function (response) {
                if (!response.already_loved) photo.hearted = false;
                console.log('hearPhoto failed: ', response);
            });
        };

        $scope.unheartPhoto = function (photo) {
            photo.hearted = false;
            Photo.unheartPhoto(photo).then(function (response) {
                console.log('unheartPhoto success: ', response);
                photo.num_loves = response.photo_num_loves;
                $scope.$broadcast('update:photoNumLoves', photo);
            }, function (response) {
                photo.hearted = true;
                console.log('unheartPhoto failed: ', response);
            });
        };

        var hasModalOpened = false;
        $scope.gotoAlbum = function () {
            if (hasModalOpened) return;
            if ($stateParams.pathUser) {
                $location.path('/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme);
            } else if ($stateParams.userIdOrShorturl) {
                $location.path('/photographer/' + $stateParams.userIdOrShorturl);
            }
        };

        $scope.queryString = function () {
            return $location.search();
        };

        $scope.showPhoto = function () {
            $scope.photo.show = true;
            $scope.photoSummary.show = true;
            $rootScope.$broadcast('fromPhotoView:set:photo.show', $scope.photo);
        };

        $scope.editPhoto = function () {
            hasModalOpened = true;
            var original = angular.copy($scope.photo);
            $rootScope.$broadcast('fromPhotoView:requesting:showEditPhotoModal', $scope.photo, function () {
                hasModalOpened = false;
            }, function (edited) {
                edited.previousPhoto = original.previousPhoto;
                edited.commentsData = original.commentsData;
                edited.nextPhoto = original.nextPhoto;
                edited.album = original.album;
                edited.owner = original.owner;
                angular.copy(edited, $scope.photo);
                $rootScope.$broadcast('fromPhotoView:updateThumbnail', original.id, edited);
            });
        };

        $scope.deletePhoto = function () {
            if (!$scope.photo) return;
            var confirmed = confirm(Variables.deletePhotoConfirmation);
            if (!confirmed) return;
            $scope.processing = true;
            Photo.delete({
                id: $scope.photo.id
            }).then(function (response) {
                console.log('delete successful: ', response);
                $scope.processing = false;
                $rootScope.$broadcast('fromPhotoView:photoDeleted', $scope.photo.id);
                $scope.gotoAlbum();
            }, function (response) {
                console.log('delete failed: ', response);
                $scope.processing = false;
                if (response.message) alert(response.message);
            });
        };

        $scope.gotoPreviousPhoto = function () {
            if (!$scope.previousPhotoSummary) return;
            InterPageHelper.forNextPage.setIntendedModel($scope.previousPhotoSummary);
            if ($scope.isProfilePage) {
                var album = $scope.previousPhotoSummary.album;
                $state.go('photographer.photo', {
                    pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
                    pathYear: album.year
                });
            } else {
                var handleOrId = $rootScope.$navbar.getSelfHandleOrId($scope.previousPhotoSummary.owner);
                $state.go('weekTheme.photo', {pathUser: handleOrId});
            }
        };

        $scope.gotoNextPhoto = function () {
            if (!$scope.nextPhotoSummary) return;
            InterPageHelper.forNextPage.setIntendedModel($scope.nextPhotoSummary);
            if ($scope.isProfilePage) {
                var album = $scope.nextPhotoSummary.album;
                $state.go('photographer.photo', {
                    pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
                    pathYear: album.year
                });
            } else {
                var userIdOrName = $rootScope.$navbar.getSelfHandleOrId($scope.nextPhotoSummary.owner);
                $state.go('weekTheme.photo', {pathUser: userIdOrName});
            }
        };


        // Caption section

        $scope.captionEllipsesCb = function (hasEllipses, id) {
            $scope.captionHasEllipses = hasEllipses;
            id && $timeout(function () {
                $('#' + id).click($scope.expandCaption);
            });
        };

        $scope.expandCaption = function () {
            $scope.captionExpanded = true;
        };

        $scope.collapseCaption = function () {
            $scope.captionExpanded = false;
        };


        // Comments section

        $scope.fetchingMoreComments = false;

        $scope.loadMoreComments = function () {
            $scope.fetchingMoreComments = true;
            var prevOfCommentId;
            if ($scope.commentsHolder.comments.length)
                prevOfCommentId = $scope.commentsHolder.comments[0].id;
            else prevOfCommentId = $scope.commentsHolder.last_comment_id_deleted;
            Comment.get({
                photo_id: $scope.photo.id,
                previous_of: prevOfCommentId
            }).then(function (response) {
                console.log('get comments success: ', response);
                mergeComments(response);
                $scope.fetchingMoreComments = false;
            }, function (response) {
                $scope.fetchingMoreComments = false;
                console.log('get comments failed: ', response);
            });
        };

        $scope.newCommentAdded = function (comment, numPhotoComments, commentsLevelTotal) {
            console.log('new comment added: ', comment);
            $scope.commentsHolder.comments.push(comment);
            $scope.commentsHolder.total = commentsLevelTotal;
            $scope.newComment.message = '';
            $scope.updatePhotoCommentsCount(comment, numPhotoComments);
        };

        $scope.updatePhotoCommentsCount = function (comment, numPhotoComments) {
            if (comment.photo_id === $scope.photo.id) {
                $scope.photo.num_comments = numPhotoComments;
                $rootScope.$broadcast('update:photoNumComments', $scope.photo);
            }
        };

        $scope.commentRemoved = function (comment, numPhotoComments, commentsLevelTotal) {
            console.log('comment deleted: ', comment);
            var commentDeleted = $filter('filter')($scope.commentsHolder.comments, function (value, index) {
                return value.id === comment.id;
            });
            if (commentDeleted[0]) {
                $scope.commentsHolder.total = commentsLevelTotal;
                $scope.commentsHolder.last_comment_id_deleted = comment.id;
                $scope.commentsHolder.comments = $filter('filter')($scope.commentsHolder.comments, function (value, index) {
                    return value.id !== comment.id;
                });
            }
            if (!$scope.commentsHolder.comments.length) $scope.loadMoreComments();
            if (comment.photo_id === $scope.photo.id) {
                $scope.photo.num_comments = numPhotoComments;
                $rootScope.$broadcast('update:photoNumComments', $scope.photo);
            }
        };

    }
]);