appSettings.app.controller('EditPhotoController', [
    '$scope', '$rootScope', 'Photo', 'ExifItem', 'Variables', '$timeout', 'TinymceHelper', 'Helpers', 'HelpsUploadProgress', 'SmoothScroll',
    function ($scope, $rootScope, Photo, ExifItem, Variables, $timeout, TinymceHelper, Helpers, HelpsUploadProgress, SmoothScroll) {

        $scope.maxKbAllowed = appSettings.photo.upload.validMaxKb;

        var modalId = $scope.modalId || 'editPhotoModal';
        var $editPhotoModal = $('#' + modalId);

        $editPhotoModal.on('hidden.bs.modal', function (e) {
            // Note: this EVENT is fired right after the Modal has been completely animated and not visible on the page!
            if ($scope.modalClosedCallback) $scope.modalClosedCallback();
        });

        var $form = $editPhotoModal.find('form');

        $scope.helpers = Helpers;

        $scope.albumTagsList = $scope.album.tags ? $scope.album.tags.split(',') : [];

        $scope.photoDetails = angular.copy($scope.photo);

        $scope.photoDetails.tags = [];
        var i, existingTags = $scope.photo.tags ? $scope.photo.tags.split(',') : [];
        for (i = 0; i < existingTags.length; i++) {
            $scope.photoDetails.tags.push(existingTags[i]);
        }

        var $photoPreviewImg = $('#editPhoto-photo-preview')[0];
        $photoPreviewImg.src = $scope.photoDetails.hi_res_filename;
        $scope.showSelectedPhoto = true;
        $scope.photoFromServer = true;

        $scope.photoDetails.is_graphic = !!$scope.photoDetails.is_graphic;
        $scope.photoDetails.ownership_confirmation = !!$scope.photoDetails.ownership_confirmation;
        $scope.photoDetails.qualifies_extra_credit = !!$scope.photoDetails.qualifies_extra_credit;
        $scope.photoDetails.screencast_critique = !!$scope.photoDetails.screencast_critique;

        $scope.photoDetails.about_photo_answers = {};
        $scope.photoDetails.camera_settings_answers = {};

        var exifSectionMap = {
            about_photo_questions: {
                submitKey: 'about_photo_answers',
                pageParentId: '#editPhoto-tell-more'
            },
            camera_settings_questions: {
                submitKey: 'camera_settings_answers',
                pageParentId: '#editPhoto-share-settings'
            }
        };
        var openPageSection = function (section) {
            var input = $(exifSectionMap[section].pageParentId);
            if (!input.is(':checked')) input.click();
        };
        var processExifAnswers = function (exifAnswer) {
            var sectionSource = exifAnswer.exif_question.parent || exifAnswer.exif_question;
            var sectionData = exifSectionMap[sectionSource.group_name];
            if (sectionData) {
                var section = sectionData.submitKey;
                var question = exifAnswer.exif_question;
                if (question.type === 'boolean') {
                    $scope.photoDetails[section][question.id] = exifAnswer.value === '1';
                } else if (question.type === 'text') {
                    $scope.photoDetails[section][question.id] = exifAnswer.value;
                } else if (question.type === 'array') {
                    $scope.photoDetails[section][question.id] = exifAnswer.value * 1;
                }
                openPageSection(sectionSource.group_name);
            }
        };
        $timeout(function () {
            var j, exifAnswers = $scope.photoDetails.exif_answers;
            if (exifAnswers) {
                for (j = 0; j < exifAnswers.length; j++) {
                    processExifAnswers(exifAnswers[j]);
                }
            }
        });

        $scope.photoDetails.model_consent = !!$scope.photoDetails.model_consent;
        $timeout(function () {
            if (!!$scope.photoDetails.has_nudity) {
                $('#editPhoto-has-nudity').click();
            }
        });


        $scope.errors = {};

        $scope.isOwnPhoto = $scope.photo.user_id === $rootScope.user.id;

        $scope.canReplacePhoto = function () {
            return $scope.isFromChallengePage || $scope.photo.photo_replaceable;
        };


        $timeout(function () {
            if (!window.tinymce) return;
            var captionBoxName = 'editPhoto-caption';
            if (tinymce.get(captionBoxName)) {
                tinymce.EditorManager.execCommand('mceRemoveEditor', true, captionBoxName);
            }
            tinymce.init({
                selector: '#' + captionBoxName,
                max_chars: Variables.captionLimit.total,
                height: 250,
                menubar: false,
                plugins: TinymceHelper.plugins_caption,
                toolbar: TinymceHelper.toolbar_short,
                content_css: TinymceHelper.content_css,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, $scope.photoDetails.caption);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.caption = {};
                    });
                }
            });
        });


        var droppedFiles = false;

        var $fileInput = $form.find('#editPhoto-photo').on('click', function () {
            delete $scope.errors.photo;
        }).on('change', function () {
            droppedFiles = false;
            checkFileSelected();
        });

        var isAdvancedUpload = function () {
            var label = document.createElement('label');
            return (('draggable' in label) || ('ondragstart' in label && 'ondrop' in label)) && window.FormData && window.FileReader;
        }();

        if (isAdvancedUpload) {
            $form.addClass('has-advanced-upload');
        }

        if (isAdvancedUpload) {

            var droppedInsideTarget = false;
            $scope.isDraggedInsideBox = false;

            var $body = $('body');
            $body.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('dragover dragenter', function () {
                if (!$scope.canReplacePhoto()) return;
                droppedInsideTarget = false;
                $body.addClass('is-dragover');
            }).on('dragleave dragend', function (e) {
                if (e.originalEvent.pageX != 0 || e.originalEvent.pageY != 0) return;
                if (!$scope.canReplacePhoto()) return;
                $body.removeClass('is-dragover');
            }).on('drop', function (e) {
                if (!$scope.canReplacePhoto()) return;
                $body.removeClass('is-dragover');
                if (!droppedInsideTarget) $scope.removePhoto();
            });

            $form.find('.box__droptarget').on('dragover dragenter', function () {
                if (!$scope.canReplacePhoto()) return;
                delete $scope.errors.photo;
                $body.addClass('is-insidebox');
                $scope.isDraggedInsideBox = true;
            });

            $form.find('.box__insidetarget').on('dragleave dragend drop', function (e) {
                if (!$scope.canReplacePhoto()) return;
                $body.removeClass('is-insidebox');
                $scope.isDraggedInsideBox = false;
            }).on('drop', function (e) {
                if (!$scope.canReplacePhoto()) return;
                droppedInsideTarget = true;
                droppedFiles = e.originalEvent.dataTransfer.files;
                $fileInput[0].value = '';
                checkFileSelected();
            });

        }

        var getFileChosen = function () {
            var files = droppedFiles || $fileInput[0].files;
            return files && files[0];
        };

        var checkPhotoFilesize = function (file) {
            var chosenFileKb = file.size ? file.size / 1024 : 0;
            var validSize = chosenFileKb <= $scope.maxKbAllowed;
            if (validSize) return;
            $scope.errors.photo = [
                'The photo may not be greater than ' + $scope.maxKbAllowed + ' kilobytes.'
            ];
        };

        var checkFileSelected = function () {
            $scope.showSelectedPhoto = false;
            $scope.photoFromServer = false;
            var file = getFileChosen();
            if (!file) return;
            if (!file.name.match(/\.jpg|png/i)) {
                $scope.removePhoto();
                alert('Only image files are allowed! :)');
            } else if (window.File && window.FileReader && window.FileList && window.Blob) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $photoPreviewImg.src = reader.result;
                    $scope.showSelectedPhoto = true;
                    checkPhotoFilesize(file);
                };
                reader.readAsDataURL(file);
            }
        };

        $scope.removePhoto = function () {
            if ($scope.processing) return;
            $scope.showSelectedPhoto = false;
            delete $scope.errors.photo;
            $scope.photoFromServer = false;
            if ($fileInput[0]) $fileInput[0].value = '';
            droppedFiles = false;
        };


        $scope.critiqueLevels = Variables.critiqueLevels;


        var trimText = function (text) {
            return text.replace(/^\s+/, '').replace(/\s+$/, '');
        };

        var checkDuplicates = function (word, inputArr) {
            for (var i = 0; i < inputArr.length; i++) {
                if (inputArr[i] === word) return true;
            }
            return false;
        };

        $scope.focusTagsInput = function () {
            $('#editPhoto-tags').focus();
        };

        $scope.addToTags = function (word) {
            var allTags = [].concat($scope.albumTagsList, $scope.photoDetails.tags);
            if (!checkDuplicates(word, allTags)) {
                $scope.photoDetails.tags.push(word);
            }
        };

        $scope.checkTagInput = function () {
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/^\s+/, '').replace(/^#/, '');
            var hasNewWord = cleaned.match(/[, #]/);
            if (hasNewWord && hasNewWord.length) {
                var words = cleaned.split(hasNewWord[0]);
                var previousWord = trimText(words[0]);
                $scope.tagInput = trimText(words[1]);
                if (previousWord) {
                    $scope.addToTags(previousWord);
                }
            }
        };

        $scope.tagInputBlur = function () {
            if (!$scope.tagInput) return;
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/[# ,]/g, '');
            var lastInput = trimText(cleaned);
            if (lastInput) {
                $scope.addToTags(lastInput);
                $scope.tagInput = '';
            }
            return lastInput;
        };

        $scope.onEnterKeyInsideTagInput = function () {
            var lastInput = $scope.tagInputBlur();
            if (!lastInput) return true;
        };

        $scope.onBackspaceInsideTagInput = function () {
            if (!$scope.tagInput) {
                $scope.photoDetails.tags.pop();
            }
        };

        $scope.removeTag = function (index) {
            if ($scope.processing) return;
            $scope.photoDetails.tags.splice(index, 1);
        };


        $scope.exifItems = {
            camera_settings_questions: [],
            about_photo_questions: []
        };
        ExifItem.get().then(function (response) {
            for (var i = 0; i < response.length; i++) {
                var responseItem = response[i];
                $scope.exifItems[responseItem.group_name].push(responseItem);
            }
        }, function (response) {
            console.log('get list of exif items failed: ', response);
        });


        HelpsUploadProgress.registerTextHolder($scope, 'uploadProgress');
        $scope.savePhotoEdits = function () {
            $scope.errors = {};
            var photoDetails = angular.copy($scope.photoDetails);
            photoDetails.id = $scope.photo.id;
            photoDetails.photo = getFileChosen();
            photoDetails.caption = $('#editPhoto-caption').val();
            delete photoDetails.exif_answers;
            delete photoDetails.album;
            delete photoDetails.owner;
            if (!$scope.photoFromServer && !photoDetails.photo) {
                $scope.errors.photo = ["The photo field is required."];
                SmoothScroll.toFirstError($scope.errors);
                return;
            }
            $scope.processing = true;
            console.log('photoDetails: ', photoDetails);
            HelpsUploadProgress.uploadStarted();
            Photo.update(photoDetails).then(function (response) {
                console.log('saving photo edits success', response);
                HelpsUploadProgress.uploadCompleted(true);
                $scope.successCallback({photo: response.record});
                $scope.processing = false;
                $editPhotoModal.modal('hide');
                if ($scope.isFromChallengePage) setTimeout(function () {
                    $('#uploadPhotoSuccessModal').modal('show');
                }, 400);
            }, function (response) {
                console.log('saving photo edits failed', response);
                HelpsUploadProgress.uploadCompleted(false);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else if (response.message) {
                    alert(response.message);
                    if (response.reload_page)
                        Helpers.reloadPage();
                }
            }, HelpsUploadProgress.uploadProgress);
        };


        $scope.clearModelConsent = function () {
            delete $scope.photoDetails.model_consent;
            delete $scope.errors['model_consent'];
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            delete $scope.errors[fieldname];
        };
    }
]);