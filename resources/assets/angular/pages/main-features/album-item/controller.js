appSettings.app.controller('albumItemController', [
  'Loading', '$location', '$scope', '$stateParams', 'Album', '$state', 'Photo', 'Variables',
  '$timeout', 'Comment', '$filter', '$rootScope', 'Following', '$http', 'Token', 'Analytics',
  function (Loading, $location, $scope, $stateParams, $state, Album, Photo,
            Variables, $timeout, Comment, $filter, $rootScope, Following, $http, Token, Analytics) {
    var pathArray = $location.$$path.split('/');
    $scope.pathArray = pathArray;
    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';

    $scope.analytics = {
      photoLoaded: function ($trackedItemId) {
        var okToCancel = true;
        var impression = $timeout(function () {
          okToCancel = false;
          Analytics.newEntry('photo_view_unique_3secs', $trackedItemId);
        }, 3000);
        $scope.$on('$destroy', function (event) {
          if (!okToCancel) return;
          $timeout.cancel(impression);
        });
      },
      photoLoadingError: function ($trackedItemId) {
        Analytics.errorLoadingAsset('photo_view_unique_3secs', $trackedItemId);
      }
    };

    $scope.openLogin = function () {
      $rootScope.$emit('CallLogInMethod', {});
    };

    document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth'
        });
      });
    });

    // function for getting coords for message about not allowing download photos
    function getPositionOfMessage(elem, event) {
      elem.css({
        top: event.clientY + 15 + 'px',
        left: event.clientX + 15 + 'px',
      });
    }

    // function for show message about not allowing to download photo
    function dontDownload(event) {
      $scope.dontDownload = true;
      var dontDownloadMessage = $('#dontDownloadMessage');
      getPositionOfMessage(dontDownloadMessage, event);
      $('.photo_view__slider').eq(0).mousemove(function (event) {
        getPositionOfMessage(dontDownloadMessage, event);
      });
      setTimeout(function () {
        $scope.dontDownload = false;
      }, 2000);
    }

    // here we disable standard context menu by clicking to right button of mouse to picture
    document.getElementsByClassName('photo_view__slider')[0].oncontextmenu = function (event) {
      if ($rootScope.user) {
        if ($rootScope.user.max_admin_points>=90000) {
          return;
        }
      }
      dontDownload(event);
      return false;
    };

    // event listener for going to prev or next photo by clicking to right-left keys.
    $(window).keyup(function (event) {
      if (event.keyCode === 39 && $scope.photo.nextPhoto) {
        $scope.goNext();
      }
      if (event.keyCode === 37 && $scope.photo.previousPhoto) {
        $scope.goPrev();
      }
    });

    $scope.openSignUp = function () {
      $rootScope.$emit('CallSignUpMethod', {});
    };

    Loading.pageLoaded($scope);
    $scope.slickCount = 0;

    $scope.goBack = function () {
      pathArray.forEach(function (item, index) {
        if (item === 'photo') pathArray.splice(index);
      });
      $location.path(pathArray.join('/'));
    };

    $scope.open = false;
    $scope.openInfo = function() {
      $scope.open = !$scope.open;
    };

    var getShortCaption = function (caption) {
      if (caption.length > 575) {
        $scope.photo.caption += ' <a onclick="angular.element(this).scope().openInfo()" href="#">Hide</a>';
        return caption.split('').splice(0, 565).join('') + '... <a onclick="angular.element(this).scope().openInfo()" href="#">Show more</a>';
      }
      $scope.open = true;
      return null;
    };

    var getCurrentPhoto = function () {
      if (!$stateParams.pathUserId) {
        $scope.loading = true;
        Photo.get({
          year: $stateParams.pathYear,
          week_path: $stateParams.pathWeekTheme,
          id: $stateParams.pathHandle,
          is_profile_page: 0, //$scope.isProfilePage
          filter_by: $location.search().filter_by
        }).then(function (photo) {
          console.log('Single photo success: ', photo);
          $scope.photo = photo;
          $scope.photo.short_caption = getShortCaption($scope.photo.caption);
          $scope.targetUserId = photo.owner.id;
          $scope.imFollowing = photo.owner.im_following;
          $scope.newComment.photo_id = photo.id;
          $scope.editPhotoInAlbumItem = false;
          $scope.loading = false;

          mergeComments(photo.commentsData);
          setValuesToExifAnswers(photo);
        }, function (error) {
          console.log('Single photo error: ', error);
          $scope.loading = false;
        });
      } else {
        var url = apiUrl + 'photo/' + $stateParams.pathUserId
          + (pathArray[4] === 'loves' ? '?is_loves_page=1&photo_id=' : '?is_profile_page=1&photo_id=')
          + $stateParams.pathPhotoId;
        $http({
          url: url,
          method: 'GET',
          headers: {'Authorization': 'Bearer ' + Token.get(),},
        }).then(function (success) {
          console.log('Single photo success: ', success.data);
          var photo = success.data;
          $scope.photo = photo;
          $scope.photo.short_caption = getShortCaption($scope.photo.caption);
          $scope.targetUserId = photo.owner.id;
          $scope.imFollowing = photo.owner.im_following;
          $scope.newComment.photo_id = photo.id;
          $scope.editPhotoInAlbumItem = false;
          $scope.loading = false;

          mergeComments(photo.commentsData);
          setValuesToExifAnswers(photo);
        }, function (error) {
          console.log('Single photo error: ', error);
          $scope.loading = false;
        });
      }
    };
    getCurrentPhoto();

    $rootScope.$on('CallFunctionForCurrentPhoto', function () {
      if ($scope.editPhotoInAlbumItem) getCurrentPhoto();
    });

    $scope.heartPhoto = function (photo, $event) {
      $event.preventDefault();
      if (!photo.hearted) {
        photo.hearted = true;
        Photo.heartPhoto(photo).then(function (response) {
          console.log('hearPhoto success: ', response);
          photo.num_loves = response.photo_num_loves;
          $scope.$broadcast('update:photoNumLoves', photo);
        }, function (response) {
          if (!response.already_loved) photo.hearted = false;
          console.log('hearPhoto failed: ', response);
        });
      } else {
        photo.hearted = false;
        Photo.unheartPhoto(photo).then(function (response) {
          console.log('unheartPhoto success: ', response);
          photo.num_loves = response.photo_num_loves;
          $scope.$broadcast('update:photoNumLoves', photo);
        }, function (response) {
          photo.hearted = true;
          console.log('unheartPhoto failed: ', response);
        });
      }
    };

    var mergeComments = function (commentsToAdd) {
      for (var i in $scope.commentsHolder.comments) {
        commentsToAdd.comments.push($scope.commentsHolder.comments[i]);
      }
      $scope.commentsHolder = commentsToAdd;
    };

    $scope.getTagsAsArray = function (tags) {
      if (tags) {
        return tags.split(',');
      }
    };

    // function for getting search and sort values for request for next or prev photo
    var getSearchParams = function () {
      if ($stateParams.pathHandle) {
        var searchParams = $stateParams.pathHandle.split('?');
        if (searchParams.length > 1) {
          return '?' + searchParams[1];
        }
      }
      return '';
    };

    $scope.goPrev = function () {
      if (!$stateParams.pathUserId) {
        $location.path('/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme + '/photo/' + ($scope.photo.previousPhoto.owner.handle || $scope.photo.previousPhoto.owner.id) + getSearchParams());
      } else $location.path('/photographer/' + $scope.photo.profile_owner_id
        + (pathArray[4] === 'loves' ? '/photo/loves/' : '/photo/') + $scope.photo.previousPhoto.id + getSearchParams());
    };

    $scope.goNext = function () {
      if (!$stateParams.pathUserId) {
        $location.path('/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme + '/photo/' + ($scope.photo.nextPhoto.owner.handle || $scope.photo.nextPhoto.owner.id) + getSearchParams());
      } else $location.path('/photographer/' + $scope.photo.profile_owner_id
        + (pathArray[4] === 'loves' ? '/photo/loves/' : '/photo/') + $scope.photo.nextPhoto.id + getSearchParams());
    };

    $scope.newComment = {message: ''};
    $scope.commentsHolder = {
      previous_count: 0,
      can_load_previous: 0,
      comments: [],
      total: 0
    };

    $scope.updatePhotoCommentsCount = function (comment, numPhotoComments) {
      if (comment.photo_id === $scope.photo.id) {
        $scope.photo.num_comments = numPhotoComments;
        $rootScope.$broadcast('update:photoNumComments', $scope.photo);
      }
    };

    $scope.commentRemoved = function (comment, numPhotoComments, commentsLevelTotal) {
      console.log('comment deleted: ', comment);
      var commentDeleted = $filter('filter')($scope.commentsHolder.comments, function (value, index) {
        return value.id === comment.id;
      });
      if (commentDeleted[0]) {
        $scope.commentsHolder.total = commentsLevelTotal;
        $scope.commentsHolder.last_comment_id_deleted = comment.id;
        $scope.commentsHolder.comments = $filter('filter')($scope.commentsHolder.comments, function (value, index) {
          return value.id !== comment.id;
        });
      }
      if (!$scope.commentsHolder.comments.length) $scope.loadMoreComments();
      if (comment.photo_id === $scope.photo.id) {
        $scope.photo.num_comments = numPhotoComments;
        $rootScope.$broadcast('update:photoNumComments', $scope.photo);
      }
    };

    $scope.newCommentAdded = function (comment, numPhotoComments, commentsLevelTotal) {
      console.log('new comment added: ', comment);
      $scope.commentsHolder.comments.push(comment);
      $scope.commentsHolder.total = commentsLevelTotal;
      $scope.newComment.message = '';
      $scope.updatePhotoCommentsCount(comment, numPhotoComments);
    };

    function setValuesToExifAnswers(photo) {
      var thisAnswers = photo.exif_answers;
      if (thisAnswers.length) {
        $scope.exif_answers = {};
        thisAnswers.forEach(function (answer) {
          if (answer.exif_member) {
            $scope.exif_answers[answer.exif_item_id] = answer.exif_member.name;
          }
        });
      }
    }

    $scope.editPhoto = function () {
      $scope.editPhotoInAlbumItem = true;
      $rootScope.$emit('CallSubmitPhotoMethod', $scope.photo);
    };

    // method for delete photo

    $scope.deletePhoto = function () {
      if (!$scope.photo) return;
      var confirmed = confirm(Variables.deletePhotoConfirmation);
      if (!confirmed) return;
      $scope.processing = true;
      Photo.delete({
        id: $scope.photo.id
      }).then(function (response) {
        console.log('delete successful: ', response);
        $scope.processing = false;
        $rootScope.$broadcast('fromPhotoView:photoDeleted', $scope.photo.id);
        if (!$stateParams.pathUserId) {
          $location.path('/albums/' + $stateParams.pathYear + '/' + $stateParams.pathWeekTheme);
        } else {
          $scope.goBack();
        }
      }, function (response) {
        console.log('delete failed: ', response);
        $scope.processing = false;
        if (response.message) alert(response.message);
      });
    };

    // methods for follow or unfollow some user

    $scope.variables = Variables;

    Variables.helpers.preparePopover();

    $scope.follow = function () {
      $scope.processing2 = true;
      Following.save({user_id: $scope.targetUserId}).then(function (response) {
        console.log('follow this person success', response);
        $scope.processing2 = false;
        $scope.imFollowing = true;
        $rootScope.$broadcast('follow', $scope.targetUserId, response);
      }, function (response) {
        console.log('follow this person failed', response);
        $scope.processing2 = false;
      });
    };

    $scope.unfollow = function () {
      $scope.processing2 = true;
      Following.delete({user_id: $scope.targetUserId}).then(function (response) {
        console.log('unfollow this person success', response);
        $scope.processing2 = false;
        $scope.imFollowing = false;
        $rootScope.$broadcast('unfollow', $scope.targetUserId, response);
      }, function (response) {
        console.log('unfollow this person success', response);
        $scope.processing2 = false;
      });
    };

    $scope.showPhoto = function () {
      $scope.photo.show = true;
      $rootScope.$broadcast('fromPhotoView:set:photo.show', $scope.photo);
    };

  }]);