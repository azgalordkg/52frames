appSettings.app.controller('SignManifestoConfirmController', [
  '$scope', '$timeout', '$rootScope', 'Manifesto', 'Auth',
  function ($scope, $timeout, $rootScope, Manifesto, Auth) {

    $(function () {
      $timeout(function () {
        $scope.showPage = true;
      }, 500);
    });

    var signManifestoConfirmModal = $('#signManifestoConfirmModal');

    signManifestoConfirmModal.on('show.bs.modal', function() {
      $scope.currentAlbum = $rootScope.currentAlbum;
      $scope.canonicalUrl = window.appSettings.canonicalUrl;
      $scope.canonicalUrlDisplay = window.appSettings.canonicalUrl.replace(/^https?\:\/\//, '');
    });
  }
]);