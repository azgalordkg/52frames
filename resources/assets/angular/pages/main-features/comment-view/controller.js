appSettings.app.controller('CommentViewController', [
    '$scope', 'Comment', '$filter', '$timeout', '$rootScope', 'Variables',
    function ($scope, Comment, $filter, $timeout, $rootScope, Variables) {

        $scope.variables = Variables;

        $scope.errors = {};

        $scope.comment.newReply = {
            reply_to_comment_id: $scope.comment.id,
            photo_id: $scope.comment.photo_id,
            message: ''
        };

        $scope.comment.repliesData = {
            can_load_previous: 0,
            previous_count: 0,
            comments: [],
            total: 0
        };

        $scope.comment.dontShowReplies = $scope.dontShowReplies || $scope.comment.dontShowReplies;

        var callbacksHelper = {
            newCommentAdded: function (comment, numPhotoComments, commentsLevelTotal) {
                if ($scope.newCommentCallback && comment) {
                    $scope.newCommentCallback({
                        numPhotoComments: numPhotoComments,
                        commentsLevelTotal: commentsLevelTotal,
                        comment: comment
                    });
                }
            },
            commentDeleted: function (comment, numPhotoComments, commentsLevelTotal) {
                if ($scope.commentDeletedCallback) {
                    $scope.commentDeletedCallback({
                        numPhotoComments: numPhotoComments,
                        commentsLevelTotal: commentsLevelTotal,
                        comment: comment
                    });
                }
            }
        };

        var loginRequired = function () {
            alert('You may need to Login or Signup to continue.');
        };

        var mergeComments = function (commentsToAdd) {
            for (var i in $scope.comment.repliesData.comments) {
                commentsToAdd.comments.push($scope.comment.repliesData.comments[i]);
            }
            $scope.comment.repliesData = commentsToAdd;
        };

        var attachAllOtherData = function (oldComment, newComment) {
            for (var method in oldComment) {
                if (!newComment[method]) newComment[method] = angular.copy(oldComment[method]);
            }
        };

        $scope.isPhotographer = function () {
            return $scope.owner && ($scope.owner.signed_manifesto || $scope.owner.manifesto);
        };

        $scope.inputSubmitted = function () {
            if ($scope.comment.editing) {
                $scope.submitEditedComment();
            } else {
                $scope.addNewComment();
            }
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) $scope.errors = {};
            else delete $scope.errors[fieldname];
        };

        $scope.registerMessageElement = function ($element) {
            $scope.comment.message_element = $element;
        };

        $scope.addNewComment = function () {
            console.log('submitting new comment: ', $scope.comment);
            $scope.hideErrors();
            $scope.processing = true;
            // $scope.comment.photo_id = $scope.comment.photo_id;
            Comment.save($scope.comment).then(function (response) {
                console.log('saving comment success: ', response);
                $scope.processing = false;
                callbacksHelper.newCommentAdded(
                    response.comment,
                    response.photo_num_comments,
                    response.comments_level_total
                );
            }, function (response) {
                $scope.processing = false;
                console.log('saving comment failed: ', response);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                }
            });
            $timeout(function () {
                $scope.comment.message_element.blur();
            });
        };

        $scope.commentKbPress = function ($event) {
            var allowedKeys = [8, 37, 38, 39, 40, 46]; // backspace, delete and cursor keys
            var max_chars = Variables.commentLimit.total;
            var message = $($event.target).text();
            if (allowedKeys.indexOf($event.keyCode) !== -1) return true;
            if (max_chars && message.length + 1 > max_chars) {
                $event.preventDefault();
                $event.stopPropagation();
                return false;
            }
            return true;
        };

        $scope.commentPasted = function ($event) {
            var max_chars = Variables.commentLimit.total;
            var element = $($event.currentTarget);
            var origMessageHtml = element.html();
            $timeout(function () {
                var message = element.text();
                if (max_chars && message.length + 1 > max_chars) {
                    $timeout(function () {
                        element.html(origMessageHtml);
                    });
                    alert('Pasting this exceeds the maximum allowed number of ' + max_chars + ' characters.');
                }
            });
        };

        $scope.commentEllipsesCb = function (hasEllipses, id) {
            $scope.comment.hasEllipses = hasEllipses;
            id && $timeout(function () {
                $('#' + id).click($scope.expandEllipses);
            });
        };

        $scope.expandEllipses = function () {
            $scope.comment.expandEllipses = true;
        };

        $scope.collapseEllipses = function () {
            $scope.comment.expandEllipses = false;
        };

        $scope.showEditForm = function () {
            console.log('switch to editing mode: ', $scope.comment);
            $scope.hideErrors('message');
            $scope.comment.message_backup = $scope.comment.message;
            $scope.comment.message = $scope.comment.message.replace(/<br>/gi, "\n");
            $scope.comment.editing = true;
            $scope.showForm = true;
            $timeout(function () {
                $scope.comment.message_element.focus();
            });
        };

        $scope.cancelCommenting = function () {
            if ($scope.cancelCommentCallback) $scope.cancelCommentCallback();
            if ($scope.comment.editing) $scope.cancelEditing();
        };

        $scope.cancelEditing = function () {
            console.log('cancelled edit!', $scope.comment);
            $scope.comment.message = $scope.comment.message_backup;
            $scope.comment.editing = false;
            $scope.showForm = false;
        };

        $scope.submitEditedComment = function () {
            console.log('submitting edited comment: ', $scope.comment);
            $scope.hideErrors();
            $scope.processing = true;
            Comment.update($scope.comment).then(function (response) {
                console.log('editing comment success: ', response);
                attachAllOtherData($scope.comment, response.comment);
                angular.copy(response.comment, $scope.comment);
                $scope.comment.editing = false;
                $scope.processing = false;
                $scope.showForm = false;
            }, function (response) {
                $scope.processing = false;
                console.log('editing comment failed: ', response);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                }
            });
        };

        $scope.deleteComment = function (comment) {
            var userOkay = confirm('This action cannot be undone.\n\n' +
                'You will end-up permanently deleting this comment.\n\n' +
                'Is that okay with you?');
            if (userOkay) {
                comment.processing = true;
                Comment.delete($scope.comment).then(function (response) {
                    console.log('deleting comment success: ', response);
                    $scope.comment.last_comment_id_deleted = $scope.comment.id;
                    comment.processing = false;
                    callbacksHelper.commentDeleted(
                        $scope.comment,
                        response.photo_num_comments,
                        response.comments_level_total
                    );
                }, function (response) {
                    comment.processing = false;
                    console.log('deleting comment failed: ', response);
                });
            }
        };

        $scope.showReplyBox = function (skipCheckRootUser) {
            if (!skipCheckRootUser && !$rootScope.user) return loginRequired();
            $scope.comment.add_reply = true;
            $scope.comment.newReply.message = '';
            $timeout(function () {
                $scope.comment.newReply.message_element.focus();
            });
        };

        $scope.revertReplyBox = function () {
            $scope.comment.add_reply = false;
            $scope.comment.showReplyLink = true;
            $scope.comment.newReply.message = '';
        };

        $scope.newReplyAdded = function (comment, numPhotoComments, commentsLevelTotal) {
            console.log('newReplyAdded: ', comment, numPhotoComments);
            $scope.comment.repliesData.comments.push(comment);
            $scope.comment.replies_count = commentsLevelTotal;
            $scope.revertReplyBox();
            callbacksHelper.newCommentAdded(comment, numPhotoComments, commentsLevelTotal);
        };

        $scope.getReplies = function () {
            $scope.comment.replies_loading = true;
            Comment.get({
                reply_to_comment_id: $scope.comment.id,
                photo_id: $scope.comment.photo_id
            }).then(function (response) {
                console.log('get comments success: ', response);
                mergeComments(response);
                $scope.comment.replies_loading = false;
                $scope.showReplyBox(true);
                if (!$rootScope.user) $scope.revertReplyBox();
            }, function (response) {
                console.log('get comments failed: ', response);
                $scope.comment.replies_loading = false;
            });
        };

        $scope.fetchingMoreReplies = false;
        $scope.loadMoreReplies = function () {
            $scope.fetchingMoreReplies = true;
            var prevOfCommentId;
            if ($scope.comment.repliesData.comments.length)
                prevOfCommentId = $scope.comment.repliesData.comments[0].id;
            else prevOfCommentId = $scope.comment.last_comment_id_deleted;
            Comment.get({
                photo_id: $scope.comment.photo_id,
                reply_to_comment_id: $scope.comment.id,
                previous_of: prevOfCommentId
            }).then(function (response) {
                console.log('get comments success: ', response);
                mergeComments(response);
                $scope.fetchingMoreReplies = false;
            }, function (response) {
                $scope.fetchingMoreReplies = false;
                console.log('get comments failed: ', response);
            });
        };

        $scope.replyDeleted = function (comment, numPhotoComments, commentsLevelTotal) {
            console.log('comment deleted: ', comment);
            var commentDeleted = $filter('filter')($scope.comment.repliesData.comments, function (value, index) {
                return value.id === comment.id;
            });
            if (commentDeleted[0]) {
                $scope.comment.replies_count = commentsLevelTotal;
                $scope.comment.repliesData.last_comment_id_deleted = comment.id;
                $scope.comment.repliesData.comments = $filter('filter')($scope.comment.repliesData.comments, function (value, index) {
                    return value.id !== comment.id;
                });
            }
            if (!$scope.comment.repliesData.comments.length) $scope.loadMoreReplies();
            callbacksHelper.commentDeleted(comment, numPhotoComments, commentsLevelTotal);
        };

    }
]);