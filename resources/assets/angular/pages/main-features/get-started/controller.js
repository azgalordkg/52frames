appSettings.app.controller('getStartedController', ['Loading', '$scope', '$rootScope', function (Loading, $scope, $rootScope) {

    Loading.pageLoaded($scope);

    $rootScope.isGetStartedPage = true;
    $scope.$on('$destroy', function () {
        delete $rootScope.isGetStartedPage;
    });

}]);