appSettings.app.controller('LoginController', [
  '$rootScope', '$scope', '$location', 'Auth', 'FacebookAuth', '$q', '$cookies', '$timeout', 'InstagramAuth',
  function ($rootScope, $scope, $location, Auth, FacebookAuth, $q, $cookies, $timeout, InstagramAuth) {

    window.getIntendedPage = function () {
      var intendedPage = window.localStorage && localStorage.getItem('intendedPage');
      window.localStorage && localStorage.removeItem('intendedPage');
      return intendedPage;
    };

    window.getAlbumChallengePage = function (album) {
      return [
        '',
        'albums',
        album.year,
        [
          'week',
          album.week_number,
          album.shorturl
        ].join('-'),
        'challenge',
        album.id
      ].join('/');
    };

    window.redirectToIntendedPageOrCurrentAlbumOrRefresh = function (backendResponse, alternativePath) {
      window.location.reload(false); // new code for only reload
      // var user = backendResponse.user; // old code
      // var intendedPage = getIntendedPage();
      // if (intendedPage) {
      //     window.location.href = intendedPage;
      // } else if (user && user.signed_manifesto) {
      //     var album = backendResponse.albums_recent.album_current_week;
      //     if (album) {
      //         window.location.href = getAlbumChallengePage(album);
      //     } else {
      //         if (alternativePath) {
      //             window.location.href = alternativePath;
      //         } else {
      //             window.location.reload(false);
      //         }
      //     }
      // } else {
      //     window.location.reload(false);
      // }
    };

    window.fbRenderButtons();

    $('#loginModal').on('show.bs.modal', function (e) {

      $scope.loginProviders = null;
      $scope.errors = {};

    });

    $scope.shouldShowSignup = function () {
      return !!$cookies.get('cookie-consent');
    };

    window.instagramLoginSuccess = function (access_token, promise, fromWhere) {
      if ($scope.processing) return;

      $scope.hideErrors();

      if (!access_token) return;

      var deferred = promise && $q.defer();
      var instAccessToken = access_token;

      $scope.processing = true;
      InstagramAuth.login({
        access_token: instAccessToken
      }).then(function (response) {
        $scope.processing = false;
        promise && deferred.resolve(response);
        if (response.access_token) {
          redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
        } else if (response.needsSignup) {
          console.log('needs signup!', response);
          // if (!$cookies.get('cookie-consent'))
          //     return $rootScope.$navbar.cookieRemiderMayNotSignup();
          InstagramAuth.userData = response.needsSignup;
          InstagramAuth.userData.access_token = instAccessToken;
          InstagramAuth.fromWhere = fromWhere || 'loginModal';
          $('#loginModal').modal('hide');
          $timeout(function () {
            $('#signupConfirmModal').modal('show');
          }, 400);
        } else if (response.emailExists) {
          console.log('email exists!', response);
          InstagramAuth.userData = response.emailExists;
          InstagramAuth.userData.access_token = instAccessToken;
          $('#loginModal').modal('hide');
          $timeout(function () {
            $('#fbLoginExistingEmailModal').modal('show');
          }, 400);
        }
      }, function (response) {
        $scope.processing = false;
        console.log('login with instagram failed', response);
        if (response.message) alert(response.message);
        promise && deferred.reject(response);
      });

      return promise && deferred.promise;
    };

    window.facebookLoginSuccess = function (response, promise, fromWhere) {
      if ($scope.processing) return;

      $scope.hideErrors();

      if (!response.authResponse) return;

      var deferred = promise && $q.defer();
      var fbAccessToken = response.authResponse.accessToken;

      $scope.processing = true;
      FacebookAuth.login({
        access_token: fbAccessToken
      }).then(function (response) {
        $scope.processing = false;
        promise && deferred.resolve(response);
        if (response.access_token) {
          redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
        } else if (response.needsSignup) {
          console.log('needs signup!', response);
          // if (!$cookies.get('cookie-consent'))
          //     return $rootScope.$navbar.cookieRemiderMayNotSignup();
          FacebookAuth.userData = response.needsSignup;
          FacebookAuth.userData.access_token = fbAccessToken;
          FacebookAuth.fromWhere = fromWhere || 'loginModal';
          $('#loginModal').modal('hide');
          $timeout(function () {
            $('#signupConfirmModal').modal('show');
          }, 400);
        } else if (response.emailExists) {
          console.log('email exists!', response);
          FacebookAuth.userData = response.emailExists;
          FacebookAuth.userData.access_token = fbAccessToken;
          $('#loginModal').modal('hide');
          $timeout(function () {
            $('#fbLoginExistingEmailModal').modal('show');
          }, 400);
        }
      }, function (response) {
        $scope.processing = false;
        console.log('login with facebook failed', response);
        if (response.message) alert(response.message);
        promise && deferred.reject(response);
      });

      return promise && deferred.promise;
    };

    $scope.login = function () {
      if ($scope.processing) return;

      $scope.hideErrors();
      console.log($scope.email, $scope.password);

      var userData = {
        email: $scope.email,
        password: $scope.password
      };

      $scope.processing = true;
      Auth.login(userData).then(function (response) {
        console.log(response);
        $scope.processing = false;
        redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
      }, function (response) {
        console.log('login failed', response);
        $scope.processing = false;
        if (response.validation_errors) {
          $scope.errors = response.validation_errors;
        } else if (response.use_social_login) {
          $scope.loginProviders = response.providers;
        } else if (response.needsSignup) {
          $scope.noAccount = true;
        } else if (response.message) {
          $scope.errors.password = [response.message];
        }
      });
    };

    $scope.hasProvider = function (name) {
      return $.inArray(name, $scope.loginProviders || []) > -1;
    };

    $scope.facebookLogin = function () {
      if ($scope.processing) return;
      FB.login(facebookLoginSuccess, FacebookAuth.apiOptions);
    };

    var instaId = $('meta[property="inst:app_id"]').attr('content');
    var instaRedirect = location.origin;

    $scope.instaLink = 'https://api.instagram.com/oauth/authorize/?client_id=' + instaId + '&redirect_uri=' + instaRedirect + '&response_type=token';

    $scope.attachPassword = function () {
      if ($scope.processing) return;
      FB.login(function (response) {
        if (!response.authResponse) return;
        var fbAccessToken = response.authResponse.accessToken;
        if (fbAccessToken) {
          FacebookAuth.userData = {
            access_token: fbAccessToken,
            email: $scope.email
          };
          $('#loginModal').modal('hide');
          $timeout(function () {
            $('#fbAttachPasswordModal').modal('show');
          }, 400);
        }
      }, FacebookAuth.apiOptions);
    };

    $scope.forgotPassword = function () {
      if ($scope.processing) return;
      Auth.userData = {email: $scope.email};
      $('#loginModal').modal('hide');
      $timeout(function () {
        $('#forgotPasswordModal').modal('show');
      }, 400);
    };

    $scope.getStarted = function () {
      if ($scope.processing) return;
      if (!$rootScope.isGetStartedPage)
        $location.path('/get-started');
      else $timeout(function () {
        $('#signupModal').modal('show');
      }, 400);
    };

    $scope.hideErrors = function () {
      delete $scope.noAccount;
      delete $scope.useFblogin;
      $scope.errors = {};
    };

  }
]);