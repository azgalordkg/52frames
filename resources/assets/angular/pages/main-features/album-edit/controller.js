appSettings.app.controller('EditAlbumController', [
    '$scope', '$timeout', 'Album', 'TinymceHelper', '$location', 'HelpsUploadProgress', 'SmoothScroll',
    function ($scope, $timeout, Album, TinymceHelper, $location, HelpsUploadProgress, SmoothScroll) {

        var albumCopy = angular.copy($scope.album);

        $scope.albumEdit = {
            upload_custom_msg: albumCopy.upload_custom_msg,
            id: albumCopy.id,
            year: albumCopy.year,
            week_number: albumCopy.week_number,
            theme_title: albumCopy.theme_title,
            shorturl: albumCopy.shorturl,
            extra_credit_title: albumCopy.extra_credit_title || undefined,
            temp_cover_photo: {
                replace_file: 1
            },
            sample_photo: {
                replace_file: 1
            }
        };
        if (albumCopy.temp_cover_photo) {
            $scope.albumEdit.temp_cover_photo = {
                file: undefined,
                replace_file: 0,
                owner_name: albumCopy.temp_cover_photo.extra_data.owner_name,
                album_theme: albumCopy.temp_cover_photo.extra_data.album_theme,
                album_week_number: albumCopy.temp_cover_photo.extra_data.album_week_number,
                url: albumCopy.temp_cover_photo.extra_data.url || undefined
            };
            $scope.temp_cover_photo_existing = albumCopy.temp_cover_photo.photo;
        }
        if (albumCopy.sample_photo) {
            $scope.albumEdit.sample_photo = {
                file: undefined,
                replace_file: 0,
                owner_name: albumCopy.sample_photo.extra_data.owner_name,
                album_theme: albumCopy.sample_photo.extra_data.album_theme,
                album_week_number: albumCopy.sample_photo.extra_data.album_week_number,
                url: albumCopy.sample_photo.extra_data.url
            };
            $scope.sample_photo_file_exiting = albumCopy.sample_photo.photo;
        }

        $scope.tagsList = albumCopy.tags ? albumCopy.tags.split(',') : [];

        var calendarDateChange = function (dateText, inputFormat) {
            inputFormat = inputFormat || 'MM/DD/YYYY';
            var isSunday = moment(dateText, inputFormat).day() === 0;
            var displayFormat = 'ddd MMM D YYYY';
            var dateFormat = 'YYYY-MM-DD';
            var startDate, endDate;
            if (isSunday) {
                startDate = moment(dateText, inputFormat).add(-1, 'week').add(1, 'days');
                endDate = moment(dateText, inputFormat).startOf('week');
            } else {
                startDate = moment(dateText, inputFormat).startOf('week').add(1, 'days');
                endDate = moment(dateText, inputFormat).add(1, 'week').startOf('week');
            }
            $scope.albumEdit.start_date = startDate.format(dateFormat);
            $scope.albumEdit.display_start_date = startDate.format(displayFormat);
            $scope.albumEdit.end_date = endDate.format(dateFormat);
            $scope.albumEdit.display_end_date = endDate.format(displayFormat);
        };

        calendarDateChange(albumCopy.start_date, 'YYYY-MM-DD');
        calendarDateChange(albumCopy.end_date, 'YYYY-MM-DD');

        $timeout(function () {

            var makeCalendar = function (textfield) {
                var $calendarIcon = $(textfield).next('.calendar-icon').weekpicker({
                    onSelect: function (dateText, startDateText, startDateInput, endDateInput, inst) {
                        calendarDateChange(dateText);
                        $calendarBox.hide();
                    }
                });
                var $calendarBox = $calendarIcon.find('.ui-datepicker-inline').hide();
                $('body').click(function (event) {
                    if (event.target == $calendarIcon[0]) {
                        $calendarBox.toggle();
                        if ($calendarBox.is(':visible') && $scope.errors) {
                            delete $scope.errors.start_date;
                            delete $scope.errors.end_date;
                        }
                    } else {
                        if (!$calendarBox.is(':visible')) return;
                        if ($(event.target).hasClass('ui-icon')) return;
                        if ($(event.target).hasClass('ui-corner-all')) return;
                        if (!$.contains($calendarIcon[0], event.target)) {
                            $calendarBox.hide();
                        }
                    }
                });
            };

            makeCalendar('#albumEdit-start-date');

            $('#albumEdit-start-date, #albumEdit-end-date').click(function () {
                $timeout(function () {
                    $('#albumEdit-start-date').next().click();
                });
            });

            tinymce.init({
                selector: '#albumEdit-blurb',
                height: 350,
                menubar: false,
                plugins: TinymceHelper.plugins,
                toolbar: TinymceHelper.toolbar,
                content_css: TinymceHelper.content_css,
                file_browser_callback: TinymceHelper.file_browser_callback,
                relative_urls: false,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, albumCopy.blurb);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.blurb = {};
                    });
                }
            });

            tinymce.init({
                selector: '#albumEdit-short-blurb',
                height: 200,
                menubar: false,
                plugins: TinymceHelper.plugins_shortblur,
                toolbar: TinymceHelper.toolbar_short,
                content_css: TinymceHelper.content_css,
                relative_urls: false,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, albumCopy.short_blurb);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.short_blurb = {};
                    });
                }
            });

            tinymce.init({
                selector: '#albumEdit-extra-credit-body',
                height: 300,
                menubar: false,
                plugins: TinymceHelper.plugins,
                toolbar: TinymceHelper.toolbar,
                content_css: TinymceHelper.content_css,
                file_browser_callback: TinymceHelper.file_browser_callback,
                relative_urls: false,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, albumCopy.extra_credit_body);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.extra_credit_body = {};
                    });
                }
            });

            tinymce.init({
                selector: '#albumEdit-tips-body',
                height: 300,
                menubar: false,
                plugins: TinymceHelper.plugins,
                toolbar: TinymceHelper.toolbar,
                content_css: TinymceHelper.content_css,
                file_browser_callback: TinymceHelper.file_browser_callback,
                relative_urls: false,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, albumCopy.tips_body);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.tips_body = {};
                    });
                }
            });

            tinymce.init({
                selector: '#custom_message',
                height: 300,
                menubar: false,
                plugins: TinymceHelper.plugins,
                toolbar: TinymceHelper.toolbar,
                content_css: TinymceHelper.content_css,
                file_browser_callback: TinymceHelper.file_browser_callback,
                relative_urls: false,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, albumCopy.upload_custom_msg);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.upload_custom_msg = {};
                    });
                }
            });

            $('#albumEdit-temp-cover-photo-file').on('click', function () {
                delete $scope.albumEdit.temp_cover_photo.file;
            }).on('change', function () {
                $scope.albumEdit.temp_cover_photo.file = this.files[0];
            });

            $('#albumEdit-sample-photo-file').on('click', function () {
                delete $scope.albumEdit.sample_photo.file;
            }).on('change', function () {
                $scope.albumEdit.sample_photo.file = this.files[0];
            });

        });

        $scope.baseurl = window.appSettings.canonicalUrl;

        $scope.updateShorturl = function () {
            $scope.shorturlChanged($scope.albumEdit.theme_title);
        };

        $scope.albumEdit.shorturlLoadingOk = true;
        $scope.shorturlChanged = function (text) {
            $scope.albumEdit.shorturlLoadingFail = false;
            $scope.albumEdit.shorturlLoadingOk = false;
            $scope.albumEdit.shorturlLoading = false;

            text = text || $scope.albumEdit.shorturl;
            $scope.albumEdit.shorturl = text && text.toLowerCase().replace(/^[^a-z0-9]/, '').replace(/\s|[^-a-z.0-9_~]/g, '-');

            if (!$scope.albumEdit.week_number || !$scope.albumEdit.year) return;

            if ($scope.albumEdit.shorturl) {
                $scope.albumEdit.shorturlLoadingFail = false;
                $scope.albumEdit.shorturlLoadingOk = false;
                $scope.albumEdit.shorturlLoading = true;

                Album.checkShorturl({
                    year: $scope.albumEdit.year,
                    week_number: $scope.albumEdit.week_number,
                    shorturl: $scope.albumEdit.shorturl,
                    album_id: $scope.albumEdit.id
                }).then(function (response) {
                    console.log('success', response);
                    $scope.albumEdit.shorturlLoadingFail = false;
                    $scope.albumEdit.shorturlLoadingOk = true;
                    $scope.albumEdit.shorturlLoading = false;
                    if ($scope.errors) delete $scope.errors.shorturl;
                }, function (response) {
                    console.log('failed', response);
                    $scope.albumEdit.shorturlLoadingFail = true;
                    $scope.albumEdit.shorturlLoadingOk = false;
                    $scope.albumEdit.shorturlLoading = false;
                    if (response.validation_errors && response.validation_errors.shorturl) {
                        if (!$scope.errors) $scope.errors = {};
                        $scope.errors.shorturl = response.validation_errors.shorturl;
                    }
                });

            }
        };

        $scope.focusShorturl = function () {
            $('#albumEdit-shorturl').focus();
        };

        $scope.focusTagsInput = function () {
            $('#albumEdit-tags').focus();
        };

        var trimText = function (text) {
            return text.replace(/^\s+/, '').replace(/\s+$/, '');
        };

        var checkDuplicates = function (word, inputArr) {
            for (var i = 0; i < inputArr.length; i++) {
                if (inputArr[i] == word) return true;
            }
            return false;
        };

        $scope.replacePhoto = function (item) {
            $scope.albumEdit[item].replace_file = 1;
        };

        $scope.addToTags = function (word) {
            if (!checkDuplicates(word, $scope.tagsList)) {
                $scope.tagsList.push(word);
            }
        };

        $scope.checkTagInput = function () {
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/^\s+/, '').replace(/^#/, '');
            var hasNewWord = cleaned.match(/[, #]/);
            if (hasNewWord && hasNewWord.length) {
                var words = cleaned.split(hasNewWord[0]);
                var previousWord = trimText(words[0]);
                $scope.tagInput = trimText(words[1]);
                if (previousWord) {
                    $scope.addToTags(previousWord);
                }
            }
        };

        $scope.tagInputBlur = function () {
            if (!$scope.tagInput) return;
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/[# ,]/g, '');
            var lastInput = trimText(cleaned);
            if (lastInput) {
                $scope.addToTags(lastInput);
                $scope.tagInput = '';
            }
            return lastInput;
        };

        $scope.onEnterKeyInsideTagInput = function () {
            var lastInput = $scope.tagInputBlur();
            if (!lastInput) return true;
        };

        $scope.onBackspaceInsideTagInput = function () {
            if (!$scope.tagInput) {
                $scope.tagsList.pop();
            }
        };

        $scope.removeTag = function (index) {
            if ($scope.processing) return;
            $scope.tagsList.splice(index, 1);
        };

        HelpsUploadProgress.registerTextHolder($scope, 'uploadProgress');
        $scope.saveAlbumEdit = function () {
            $scope.processing = true;
            $scope.errors = {};
            var albumEditInput = $scope.albumEdit;
            albumEditInput.blurb = $('#albumEdit-blurb').val();
            albumEditInput.short_blurb = $('#albumEdit-short-blurb').val();
            albumEditInput.extra_credit_body = $('#albumEdit-extra-credit-body').val();
            albumEditInput.tips_body = $('#albumEdit-tips-body').val();
            albumEditInput.upload_custom_msg = $('#custom_message').val();
            albumEditInput.tags = $scope.tagsList;
            HelpsUploadProgress.uploadStarted();
            Album.update(albumEditInput).then(function (response) {
                $scope.processing = false;
                console.log('save album edit success: ', response);
                HelpsUploadProgress.uploadCompleted(true);
                var extraAlbumPath = $scope.extraAlbumPath || '';
                window.location.href = '/albums/' + albumEditInput.year + '/week-' + albumEditInput.week_number + '-' + albumEditInput.shorturl + extraAlbumPath;
            }, function (response) {
                $scope.processing = false;
                console.log('save album edit failed: ', response);
                HelpsUploadProgress.uploadCompleted(false);
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else if (response.message) {
                    alert(response.message);
                }
            }, HelpsUploadProgress.uploadProgress);
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            delete $scope.errors[fieldname];
        };

    }
]);