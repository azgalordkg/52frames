appSettings.app.controller('signAcceptController', [
    '$scope', '$timeout', 'Manifesto', 'Country', 'State', 'City', '$rootScope', '$q', 'Variables', '$location', '$http',
    function ($scope, $timeout, Manifesto, Country, State, City, $rootScope, $q, Variables, $location, $http) {

        var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';
        $('#signAccept').on('show.bs.modal', function (e) {
            $scope.manifestoInput = Manifesto.manifestoInput || $scope.manifestoInput || {};
            delete Manifesto.manifestoInput;
            $scope.errors = [];
        });

        $scope.steps = [false, false, false, false, false];
        $scope.activeStep = 0;

        function changeActiveStep() {
          if ($scope.steps[$scope.activeStep] && $scope.activeStep < 5) {
            $scope.activeStep++;
            changeActiveStep();
          }
        }

        $scope.getActiveStep = function (index) {
          $scope.activeStep = 0;
          $scope.steps[index] = !$scope.steps[index];
          changeActiveStep();
        };

        $scope.submit = function () {
            $scope.errors = [];

            var manifestoInput = angular.copy($scope.manifestoInput);
            manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
            manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;
            manifestoInput.manifesto_questions = 1;
            manifestoInput.non_perfection_understanding = true;
            manifestoInput.vulnerability_understanding = true;

            $scope.processing = true;

            $http({
                method: 'POST',
                data: manifestoInput,
                url: apiUrl + 'manifesto/validate',
            }).then(function (success) {
                console.log('validation success: ', success.data);
                $scope.processing = false;
                $scope.manifestoInput.non_perfection_understanding = true;
                $scope.manifestoInput.vulnerability_understanding = true;
                Manifesto.manifestoInput = $scope.manifestoInput;
                $('#signAccept').modal('hide');
                setTimeout(function () {
                    $rootScope.activeStepOfManifesto = 4;
                    $('#createAccountPhotographer').modal('show');
                }, 400);
            }, function (error) {
                console.log('validation failed: ', error.data);
                $scope.processing = false;
                if (error.data.validation_errors) {
                    $scope.errors = error.data.validation_errors;
                    console.log($scope.errors);
                } else if (error.data.message) {
                    alert(error.data.message);
                }
            });

            // Manifesto.validate(manifestoInput).then(function (response) {
            //     console.log('success: ', response);
            //     $scope.processing = false;
            //     Manifesto.manifestoInput = $scope.manifestoInput;
            //     $('#signAccept').modal('hide');
            //     setTimeout(function () {
            //         $('#createAccountPhotographer').modal('show');
            //     }, 400);
            // }, function (response) {
            //     console.log('failed: ', response);
            //     $scope.processing = false;
            //     if (response.validation_errors) {
            //         $scope.errors = response.validation_errors;
            //     } else if (response.message) {
            //         alert(response.message);
            //     }
            // });
        };

        $scope.back = function () {
            Manifesto.manifestoInput = $scope.manifestoInput;
            setTimeout(function () {
                $('#signFramer').modal('show');
            }, 400);
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };

    }
]);