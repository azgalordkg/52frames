appSettings.app.controller('PhotoController', ['$scope', 'Photo', 'Variables', '$rootScope', '$location', function ($scope, Photo, Variables, $rootScope, $location) {

  var getWidthOfItems = function () {
    var albumItemGallery = $('.album_item__gallery_list .item .img_wrapper');
    var galleryListWidth = $('.album_item__gallery_list').width();
    if (galleryListWidth !== 0) {
      albumItemGallery.css('height', albumItemGallery.width() + 'px');
    } else {
      // getWidthOfItems();
    }
  };

  $scope.$$postDigest(getWidthOfItems);

  $scope.variables = Variables;
  $scope.mainUrl = $location.$$path;

  Variables.helpers.preparePopover();

  var skipClickPropagation = false;

  $scope.stopPropagation = function ($event) {
    if (!skipClickPropagation) $event.stopPropagation();
    skipClickPropagation = false;
  };

  $scope.allowClickPropagation = function () {
    skipClickPropagation = true;
  };

  $scope.showPhoto = function () {
    $scope.photo.show = true;
    $rootScope.$broadcast('fromPhotoView:set:photo.show', $scope.photo);
  };

  $scope.heartPhoto = function (photo, $event) {
    $event.preventDefault();
    if (!photo.hearted) {
      photo.hearted = true;
      Photo.heartPhoto(photo).then(function (response) {
        console.log('hearPhoto success: ', response);
        photo.num_loves = response.photo_num_loves;
        $scope.$broadcast('update:photoNumLoves', photo);
      }, function (response) {
        if (!response.already_loved) photo.hearted = false;
        console.log('hearPhoto failed: ', response);
      });
    } else {
      photo.hearted = false;
      Photo.unheartPhoto(photo).then(function (response) {
        console.log('unheartPhoto success: ', response);
        photo.num_loves = response.photo_num_loves;
        $scope.$broadcast('update:photoNumLoves', photo);
      }, function (response) {
        photo.hearted = true;
        console.log('unheartPhoto failed: ', response);
      });
    }
  };

  $scope.openLogin = function () {
    $rootScope.$emit('CallLogInMethod', {});
  };

  $scope.openSignUp = function () {
    $rootScope.$emit('CallSignUpMethod', {});
  };

}]);