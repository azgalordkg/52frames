appSettings.app.controller('homeController', [
    'Loading', '$scope', '$location', '$rootScope', 'Helpers', '$cookies', 'InstagramAuth', '$http', 'Token',
    function (Loading, $scope, $location, $rootScope, Helpers, $cookies, InstagramAuth, $http, Token) {

        var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';

        if ($location.path() === '/password/reset') {
            $rootScope.resetPasswordToken = $location.search().token;
        }

        if ($location.path() === '/remove-cookie-consent') {
            $location.path('/');
            $cookies.remove('cookie-consent');
            console.log('here home');
            $rootScope.$navbar.logout();
        }

        if ($location.hash().split('=')[0] === 'access_token') {
            let access_token = ($location.hash().split('=')[1]);

            $location.hash('instagram-redirect');
            $location.replace();

            InstagramAuth.login({
                "access_token": access_token
            }).then(function () {
                instagramLoginSuccess(access_token, true, 'loginModal');
            });
        }

        $scope.helpers = Helpers;

        Loading.pageLoaded($scope);

        $scope.onSubmitClicked = function () {
            $rootScope.$emit('CallSubmitPhotoMethod', null);
        };

        $scope.signManifesto = function() {
            $rootScope.$emit('CallSignManifestoMethod', {});
        };

        $scope.openLogin = function () {
            $rootScope.$emit('CallLogInMethod', {});
        };

        $scope.openSignUp = function () {
            $rootScope.$emit('CallSignUpMethod', {});
        };

    }
]);