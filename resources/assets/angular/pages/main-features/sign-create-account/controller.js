appSettings.app.controller('signCreateAccountController', [
  '$scope', '$timeout', 'Manifesto', 'Auth', 'Boot', '$http', '$location', '$rootScope', 'Token',
  function ($scope, $timeout, Manifesto, Auth, Boot, $http, $location, $rootScope, Token) {
    var signedUp = localStorage.getItem('signedUp');
    if (signedUp === 'true') {
      $('#signManifestoConfirmModal').modal('show');
      localStorage.removeItem('signedUp')
    }

    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';
    $('#createAccountPhotographer').on('show.bs.modal', function (e) {
      $scope.manifestoInput = Manifesto.manifestoInput || $scope.manifestoInput || {};
      delete Manifesto.manifestoInput;

      $scope.errors = {};
    });

    $scope.finish = function () {
      $scope.errors = {};

      var manifestoInput = angular.copy($scope.manifestoInput);
      manifestoInput.manifesto_intro = null;
      manifestoInput.manifesto_question = null;
      manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
      manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;
      manifestoInput.is_fan = false;
      manifestoInput.dont_save = false;

      $scope.processing = true;

      let headers = {};
      if ($rootScope.user) {
        headers = {
          'Authorization': 'Bearer ' + Token.get(),
        }
      }
      $http({
        method: 'POST',
        data: manifestoInput,
        url: apiUrl + `${$rootScope.user ? 'manifesto' : 'signup'}`,
        headers: headers,
      }).then(function (response) {
        console.log('success: ', response.data);
        Manifesto.manifestoInput = manifestoInput;
        $scope.processing = false;
        $('#createAccountPhotographer').modal('hide');

        if (!$rootScope.user) {
          localStorage.setItem('signedUp', 'true');
          var userData = {
            email: manifestoInput.email,
            password: manifestoInput.password
          };

          Auth.login(userData).then(function (response) {
            console.log(response);
            $scope.processing = false;
            redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
          }, function (response) {
            console.log('login failed', response);
            $scope.processing = false;
          });
        } else {
          Auth.me();
          $('#createAccountPhotographer').modal('hide');
          setTimeout(function () {
            $('#signManifestoConfirmModal').modal('show');
          }, 400);
        }

      }, function (response) {
        console.log('failed: ', response.data);
        $scope.processing = false;
        if (response.data.validation_errors) {
          $scope.errors = response.data.validation_errors;
        } else if (response.data.message) {
          alert(response.data.message);
        }
      });
      // Manifesto.save(manifestoInput).then(function (response) {
      //     console.log('success: ', response);
      //     if (!response.success) {
      //         $scope.processing = false;
      //         alert(response);
      //     } else Auth.me().then(function (response) {
      //         Boot.copyAlbumCurrentWeekFromLoginToRootScope(response);
      //         $scope.processing = false;
      //         $('#createAccountPhotographer').modal('hide');
      //         setTimeout(function () {
      //             $('#signManifestoConfirmModal').modal('show');
      //         }, 400);
      //     }, function () {
      //         $scope.processing = false;
      //     });
      // }, function (response) {
      //     console.log('failed: ', response);
      //     $scope.processing = false;
      //     if (response.validation_errors) {
      //         $scope.errors = response.validation_errors;
      //     } else if (response.message) {
      //         alert(response.message);
      //     }
      // });
    };

    $scope.back = function () {
      Manifesto.manifestoInput = $scope.manifestoInput;
      $('#createAccountPhotographer').modal('hide');
      setTimeout(function () {
        $('#signAccept').modal('show');
      }, 400);
    };

    $scope.hideErrors = function (fieldname) {
      if (!$scope.errors) return;
      $scope.errors[fieldname] = '';
    };

  }
]);