appSettings.app.controller('EditManifestoController', [
    '$scope', '$timeout', 'Manifesto', 'Country', 'State', 'City', '$rootScope', '$q',
    function ($scope, $timeout, Manifesto, Country, State, City, $rootScope, $q) {

        $scope.manifestoInput = {
            id: $scope.userData.id,
            firstname: $scope.userData.firstname,
            lastname: $scope.userData.lastname,
            nickname: $scope.userData.handle,
            display_as_handle: !!$scope.userData.display_as_handle,
            photography_level: $scope.userData.profile.photography_level,
            camera_type: $scope.userData.profile.camera_type,
            dont_share_location: !!$scope.userData.dont_share_location,
            photowalks: $scope.userData.profile.photowalks_level,
            shortbio: $scope.userData.profile.shortbio,
        };

        var findCurrentCountry = function (countries) {
            for (var i = 0; i < countries.length; i++) {
                var country = countries[i];
                if (country.id == $scope.userData.city.country.id) {
                    return country;
                }
            }
        };

        var findCurrentState = function (states) {
            if (!$scope.userData.city.state) return;
            for (var i = 0; i < states.length; i++) {
                var state = states[i];
                if (state.id == $scope.userData.city.state.id) {
                    return state;
                }
            }
        };

        var setCityInput = function () {
            $scope.manifestoInput.city = $scope.userData.city.name;
        };

        Country.get().then(function (countries) {
            $scope.startup = true;
            $scope.countries = countries;
            $scope.manifestoInput.country = findCurrentCountry(countries);
            $scope.changeCountry();
        });

        for (var i = 0; i < $scope.userData.external_pages.length; i++) {
            var websiteData = $scope.userData.external_pages[i];
            $scope.manifestoInput[websiteData.provider + '_handle'] = websiteData.handle;
        }

        $scope.userData.manifestoInput = $scope.manifestoInput;

        $scope.errors = {};

        $scope.shortUrlBase = window.appSettings.shortUrlBase;

        $scope.nicknameLoading = {};
        if ($scope.manifestoInput.nickname) {
            $scope.nicknameLoading.ok = true;
        }

        var nicknameRequestCanceler = null;
        $scope.nicknameChanged = function () {
            $scope.nicknameLoading.indicator = false;
            $scope.nicknameLoading.ok = false;
            var nickname = $scope.manifestoInput.nickname;
            nickname = nickname && nickname.toLowerCase().replace(/^[^a-z]/, '').replace(/\s|[^-a-z.0-9_~]/g, '-');
            $scope.manifestoInput.nickname = nickname;
            if (nickname) {
                $scope.nicknameLoading.indicator = true;
                $scope.nicknameLoading.ok = false;
                if (nicknameRequestCanceler) nicknameRequestCanceler();
                var canceler = $q.defer();
                nicknameRequestCanceler = canceler.resolve;
                Manifesto.checkNickname({
                    user_id: $scope.userData.id,
                    nickname: nickname
                }, {timeout: canceler.promise}).then(function (response) {
                    console.log('nickname check success: ', response);
                    $scope.nicknameLoading.indicator = false;
                    $scope.nicknameLoading.ok = true;
                    delete $scope.errors.nickname;
                    nicknameRequestCanceler = null;
                }, function (response) {
                    console.log('nickname check failed: ', response);
                    if (!response) return;
                    $scope.nicknameLoading.indicator = false;
                    $scope.nicknameLoading.ok = false;
                    if (response.validation_errors) {
                        $scope.errors.nickname = response.validation_errors.nickname;
                    } else {
                        $scope.nicknameLoading.fail = true;
                    }
                    $scope.manifestoInput.display_as_handle = false;
                    nicknameRequestCanceler = null;
                });
            } else {
                delete $scope.errors.nickname;
                $scope.manifestoInput.display_as_handle = false;
            }
        };

        $scope.focusNickname = function () {
            $('#editmanifesto-nickname').focus();
        };

        $scope.changeCountry = function () {
            $scope.states = {};
            $scope.manifestoInput.city = '';
            var country = $scope.manifestoInput.country;
            if (country && country.state_member_term) {
                State.get({
                    country_id: country.id
                }).then(function (states) {
                    $scope.states = states;
                    if ($scope.startup) {
                        $scope.startup = false;
                        $scope.manifestoInput.state = findCurrentState(states);
                        setCityInput();
                    }
                }, function () {
                });
            } else if ($scope.startup) {
                $scope.startup = false;
                setCityInput();
            }
        };

        $scope.changeState = function () {
            $scope.manifestoInput.city = '';
        };

        $scope.suggestCity = function () {
            if (!$scope.manifestoInput) return;
            var country = $scope.manifestoInput.country;
            var state = $scope.manifestoInput.state;
            var cityInput = $scope.manifestoInput.city;
            if (country && cityInput) {
                var requestInput = {
                    country_id: country.id,
                    query: cityInput
                };
                if (country.state_member_term && state) {
                    requestInput.state_id = state.id;
                }
                City.get(requestInput).then(function (cities) {
                    if ($scope.dontSuggestCity) {
                        $scope.dontSuggestCity = false;
                        return;
                    }
                    console.log('city autosuggest: ', cities);
                    var $cityInputField = $('.edit-manifesto input[name=city]');
                    $cityInputField.data('typeahead').source = cities;
                    $cityInputField.typeahead('lookup').focus();
                }, function () {
                });
            }
        };

        $scope.citySelected = function () {
            $scope.dontSuggestCity = true;
        };

        $scope.saveChanges = function () {

            $scope.errors = {};

            var manifestoInput = angular.copy($scope.manifestoInput);
            manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
            manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;

            $scope.processing = true;
            Manifesto.update(manifestoInput).then(function (response) {
                console.log('update success: ', response);
                $scope.processing = false;
                if (response.nickname_changed) {
                    var handleOrId = $rootScope.$navbar.getSelfHandleOrId(response.user);
                    window.location.href = '/photographer/' + handleOrId;
                } else {
                    $scope.successCallback({userData: response.user});
                    $('#editManifestoModal').modal('hide');
                }
            }, function (response) {
                console.log('update failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.message) {
                    alert(response.message);
                }
            });
        };

        $scope.deleteAccount = function () {
            $rootScope.$broadcast('deleteUser:requested', $scope.userData);
            $timeout(function () {
                $('#deleteAccountModal').modal('show');
            }, 400);
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };

    }
]);