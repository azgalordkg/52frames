appSettings.app.controller('ForgotPasswordController', [
    '$rootScope', '$scope', '$timeout', '$location', 'Auth', 'FacebookAuth', 'Variables',
    function ($rootScope, $scope, $timeout, $location, Auth, FacebookAuth, Variables) {

        var forgotPasswordModal = $('#forgotPasswordModal');

        forgotPasswordModal.on('show.bs.modal', function (e) {

            var email = Auth.userData && Auth.userData.email;
            delete Auth.userData;

            $scope.userData = {
                email: email || ''
            };

            $scope.errors = {};

        });

        forgotPasswordModal.on('hide.bs.modal', function (e) {
            $scope.noAccount = false;
            $scope.loginProviders = [];
        });

        $scope.emailPasswordLink = function () {

            $scope.errors = {};

            var userData = {
                email: $scope.userData.email
            };

            $scope.processing = true;
            Auth.forgotPassword(userData).then(function (response) {
                console.log('password reset success', response);
                Variables.localStorage('ns:resetPassword:emailSent', 1);
                $scope.processing = false;
                $('#forgotPasswordModal').modal('hide');
                setTimeout(function () {
                    $('#forgotPassEmailSentModal').modal('show');
                }, 400);
            }, function (response) {
                console.log('password reset failed', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.use_social_login) {
                    $scope.loginProviders = response.providers;
                } else if (response.needsSignup) {
                    $scope.noAccount = true;
                }
            });

        };

        $scope.hasProvider = function (name) {
            return $.inArray(name, $scope.loginProviders || []) > -1;
        };

        $scope.loginScreen = function () {
            setTimeout(function () {
                $('#loginModal').modal('show');
            }, 400);
        };

        $scope.getStarted = function () {
            if ($rootScope.isGetStartedPage)
                setTimeout(function () {
                    $('#signupModal').modal('show');
                }, 400);
            else $location.path('/get-started');
        };

        $scope.facebookLogin = function () {
            FB.login(window.facebookLoginSuccess, FacebookAuth.apiOptions);
        };

        $scope.attachPassword = function () {
            FB.login(function (response) {
                if (!response.authResponse) return;
                var fbAccessToken = response.authResponse.accessToken;
                if (fbAccessToken) {
                    FacebookAuth.userData = {
                        access_token: fbAccessToken,
                        email: $scope.userData.email
                    };
                    forgotPasswordModal.modal('hide');
                    setTimeout(function () {
                        $('#fbAttachPasswordModal').modal('show');
                    }, 400);
                }
            }, FacebookAuth.apiOptions);
        };

        $scope.hideErrors = function () {
            delete $scope.noAccount;
            delete $scope.loginProviders;
            $scope.errors = {};
        };

    }
]);