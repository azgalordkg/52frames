appSettings.app.controller('albumWeekController', [
  'Loading', '$scope', '$rootScope', '$stateParams', 'Album', '$location', 'Photo', '$filter', '$state', 'PhotosAggregate', 'InterPageHelper', '$http', 'Token',
  function (Loading, $scope, $rootScope, $stateParams, Album, $location, Photo, $filter, $state, PhotosAggregate, InterPageHelper, $http, Token) {

    // init values for this controller
    var apiUrl = $location.$$protocol + '://' + $location.$$host + ($location.$$port ? (':' + $location.$$port + '/') : '/') + 'api/';
    var pageLoaded = false;
    var folderPath = '/albums/' + $stateParams.pathYear + '/';
    var pagination = {};

    var resetPagination = function () {
      pagination = {
        ready: false,
        currentPage: 0,
        isFetching: false,
        tryNextPage: true,
      };
      $scope.total_photos = 0;
      $scope.photos = [];
    };

    // function for only push to array
    var pushPhotosToArray = function (photosResponse) {
      $scope.total_photos = photosResponse.total;
      if (!photosResponse.next_page_url) pagination.tryNextPage = false;
      for (var i = 0; i < photosResponse.data.length; i++) {
        var photo = photosResponse.data[i];
        $scope.photos.push(photo);
      }
    };

    // function for preparing photos for render in array
    var getPhotosAndPushToArray = function (successCb, failCb) {
      if (pagination.isFetching) return;
      if (!pagination.tryNextPage) return;
      pagination.isFetching = true;
      pagination.currentPage++;
      Photo.get({
        album_id: $scope.album.id,
        page: pagination.currentPage,
        filter_by: $location.search().filter_by
      }).then(function (response) {
        console.log('get list of photos success: ', response);
        pagination.isFetching = false;
        pushPhotosToArray(response);
        if (successCb) successCb();
      }, function (response) {
        console.log('get list of photos failed: ', response);
        pagination.isFetching = false;
        if (failCb) failCb();
      });
    };

    resetPagination();

    $rootScope.$on('CallInitPageFunction', function () {
      $scope.photos = [];
      initPage();
    });

    // function for first initialisation of page, first ajax-request for getting photos data.
    var initPage = function () {
      Album.get({
        id: $stateParams.pathWeekTheme,
        year: $stateParams.pathYear,
        filter_by: $location.search().filter_by,
        is_challenge_page: false
      }).then(function (response) {
        console.log('get album entry success: ', response);

        if (!response.publicly_visible) Loading.pageRequiresAdminRole($scope);

        $scope.album = response;
        $scope.album.pagination = [];
        for (var i = 1; i <= $scope.album.photosData.last_page; i++) {
          $scope.album.pagination.push(i);
        }
        $scope.lessText = response.short_blurd;
        $scope.fullPath = folderPath + 'week-' + $scope.album.week_number + '-' + $scope.album.shorturl;

        pagination.currentPage++;
        pushPhotosToArray(response.photosData);
        pagination.ready = true;

        pageLoaded = Loading.pageLoaded($scope);
      }, function (response) {
        console.log('get album entry failed: ', response);
        if (response.redirect) {
          $location.path(folderPath + $stateParams.pathWeekTheme + '-' + response.shorturl);
        } else if (response.not_live) {
          $location.path('/not-admin');
        } else if (response.show_challenge_page) {
          $location.path(folderPath + $stateParams.pathWeekTheme + '/challenge');
        } else if (response.no_such_album) {
          $location.path('/404');
        } else {
          $location.path('/not-admin');
        }
      });
    };

    $scope.queryString = function () {
      return $location.search();
    };

    $scope.applyFilter = function (filterBy) {
      $location.search('filter_by', filterBy);
    };

    if (!$stateParams.pathUser) initPage();
    else $scope.$on('photoPageLoaded', function (event, args) {
      if (!pageLoaded) initPage();
    });

    // function for open modal for edit Album
    $scope.editAlbum = function () {
      $('#editAlbumModal').modal('show');
    };

    $scope.$on('update:photoNumLoves', function (event, photo) {
      var targetPhoto = $filter('filter')($scope.photos, function (value, index) {
        return value.id === photo.id;
      });
      if (targetPhoto.length) {
        targetPhoto[0].num_loves = photo.num_loves;
        targetPhoto[0].hearted = photo.hearted;
      }
    });

    $scope.$on('update:photoNumComments', function (event, photo) {
      var targetPhoto = $filter('filter')($scope.photos, function (value, index) {
        return value.id === photo.id;
      });
      if (targetPhoto.length) targetPhoto[0].num_comments = photo.num_comments;
    });

    $scope.$on('follow', function (event, userId) {
      if ($location.search().filter_by === 'following') {
        var originalPageCount = pagination.currentPage;
        resetPagination();
        var getNextPage = function () {
          getPhotosAndPushToArray(function () {
            if (pagination.tryNextPage && pagination.currentPage < originalPageCount)
              getNextPage();
          });
        };
        getNextPage();
      }
    });

    $scope.$on('unfollow', function (event, userId) {
      if ($location.search().filter_by === 'following') {
        var found = false;
        $scope.photos = $filter('filter')($scope.photos, function (photo, index) {
          found = photo.user_id === userId;
          return photo.user_id !== userId;
        });
        if ($scope.total_photos) $scope.total_photos--;
      }
    });

    $scope.loadMorePhotos = function () {
      if (!pagination.ready) return;
      getPhotosAndPushToArray();
    };

    $scope.viewPhoto = function (photo) {
      var mainUrl = $location.$$path;
      var searchParamsArray = null;
      if ($scope.url) {
        searchParamsArray = [];
        $scope.url.split('&').map(function(el, index) {
          if (index > 1) searchParamsArray.push(el);
        });
        searchParamsArray = searchParamsArray.join('&');
      }

      InterPageHelper.forNextPage.resolveIntendedModelThenSave(function () {
        return PhotosAggregate.findOne(photo.id);
      });
      $location.path(mainUrl + '/photo/' + (photo.owner.handle ? photo.owner.handle : photo.owner.id) + (searchParamsArray ? ('?' + searchParamsArray) : ''));
    };

    $scope.showAlbumSetLiveModal = function () {
      $('#albumSetLive').modal('show');
    };

    // If Photo has Nudity flag, check if User has already clicked Show on this page
    $scope.$on('fromPhotoView:get:photo.show', function (event, photo) {
      var localPhoto = $filter('filter')($scope.photos, function (value, index) {
        return value.id === photo.id;
      });
      photo.show = localPhoto.length ? !!localPhoto[0].show : false;
    });

    $scope.$on('fromPhotoView:set:photo.show', function (event, photo) {
      var localPhoto = $filter('filter')($scope.photos, function (value, index) {
        return value.id === photo.id;
      });
      if (localPhoto.length) localPhoto[0].show = !!photo.show;
    });


    // When Photo is Deleted from the Photo view

    $scope.$on('fromPhotoView:photoDeleted', function (event, photoId, dontReloadPage) {
      $scope.photos = $filter('filter')($scope.photos, function (value, index) {
        return value.id !== photoId;
      });
      if ($scope.total_photos) $scope.total_photos--;
      if (!dontReloadPage) location.reload(true);
    });


    // Related to Editing a Photo

    var interPageCallbacks = {};
    var resetCallbacks = function () {
      interPageCallbacks = {
        modalClosedCb: angular.noop,
        editSuccessCb: angular.noop
      };
    };
    resetCallbacks();

    $scope.$on('fromPhotoView:updateThumbnail', function (event, targetPhotoId, editedVersion) {
      var photo = $filter('filter')($scope.photos, function (value, index) {
        return value.id === targetPhotoId;
      });
      if (photo && photo[0]) angular.copy(editedVersion, photo[0]);
    });

    $scope.$on('fromPhotoView:requesting:showEditPhotoModal', function (event, photo, modalClosedCb, editSuccessCb) {
      resetCallbacks();
      $scope.editPhoto(photo);
      if (angular.isFunction(modalClosedCb)) interPageCallbacks.modalClosedCb = modalClosedCb;
      if (angular.isFunction(editSuccessCb)) interPageCallbacks.editSuccessCb = editSuccessCb;
    });

    $scope.editPhoto = function (photo) {
      $rootScope.$emit('CallSubmitPhotoMethod', photo);
    };

    $scope.editPhotoModalClosed = function () {
      delete $scope.photoIsForEditing;
      interPageCallbacks.modalClosedCb();
      resetCallbacks();
    };

    // $scope.editPhotoSuccess = function (editedPhoto) {
    //   if (!$scope.photoIsForEditing) return;
    //   if ($scope.photoIsForEditing.id !== editedPhoto.id)
    //     location.reload(true);
    //   else {
    //     editedPhoto.show = $scope.photoIsForEditing.show;
    //     angular.copy(editedPhoto, $scope.photoIsForEditing);
    //     interPageCallbacks.editSuccessCb(editedPhoto);
    //   }
    // };

    $scope.openBypassUploadModal = function () {
      $('#allowSomeoneToUploadModal').modal('show');
    };

    $scope.makeAlbumPublic = function () {
      $http({
        url: apiUrl + 'album/' + $scope.album.id + '/make_public',
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + Token.get()
        },
        data: {
          "publicly_visible": !$scope.album.publicly_visible
        }
      }).then(function (success) {
        console.log('The visibility of album has successfully changed ', success);
        $scope.album.publicly_visible = success.data.publicly_visible;
      }, function (error) {
        console.log('The visibility of album not changed, error: ', error)
      });
    };

    // default values for Search:

    $scope.searchParam = 'username';
    $scope.levelParam = 'beginner';

    // data for filter-buttons

    $scope.viewData = [
      {iconClass: 'fa-images', value: 'All', active: true, filter: 'all'},
      {iconClass: 'fa-certificate', value: 'Extra Credit', active: false, filter: 'extra_credit'},
      // {iconClass: 'fa-trophy', value: 'Staff Picks', active: false, filter: 'staff_picks'},
      {iconClass: 'fa-star', value: 'Following', active: false, filter: 'following'},
      {iconClass: 'fa-child', value: 'My Photo!', active: false, filter: 'my_photo'},
      {iconClass: 'fa-certificate new', value: 'New', active: false, filter: 'new'},
      {iconClass: 'fa-certificate', value: 'Shred Away', active: false, filter: 'shred_away'},
    ];

    // function for change active value in filter-buttons

    function toggleActiveFilterButton(index) {
      $scope.viewData = $scope.viewData.map(function (dataItem) {
        dataItem.active = false;
        return dataItem;
      });
      $scope.viewData[index].active = true;
    }

    // Method for filter photos by view

    $scope.viewFilter = function ($index, filterParams, searchParams) {
      // var url = apiUrl + 'photo?album_id=' + $scope.album.id + '&year=' + $stateParams.pathYear;
      if (filterParams) $scope.filterParams = filterParams;
      if (filterParams === 'all') {
        $scope.filterParams = '';
        $scope.sort_by = '';
        $scope.searchParam = 'username';
        $scope.inputSearchParam = '';
        $scope.tags = [];
      }
      if (searchParams) $scope.searchParams = searchParams;

      $http({
        url: prepareCorrectUrl(),
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + Token.get()
        },
      }).then(function (success) {
        console.log('get filtered album success: ', success.data);
        $scope.photos = success.data.data;
        if ($index !== null && $index !== undefined) {
          console.log($index);
          toggleActiveFilterButton($index);
        }
      }, function (error) {
        console.log('get filtered album error: ', error);
      });
    };

    function getUserNamesFromRequest(data) {
      if (data.length) {
        return data.map(function (item) {
          return item.name;
        });
      }
      return data;
    }

    // method for life search by name
    $scope.nameLifeSearch = function ($event) {
      if ($scope.searchParam === 'username') {
        if ($event.target.value) {
          $http.get(apiUrl + 'album/' + $scope.album.id + '/photographers?input=' + $event.target.value).then(
            function (success) {
              console.log('Life search by ' + $scope.searchParam + ' success: ', success.data);
              $scope.lifeSearchVariants = getUserNamesFromRequest(success.data);
            }, function (error) {
              console.log('Life search by ' + $scope.searchParam + ' error: ', error);
            }
          )
        }
      }
    };

    // method for edit album

    $scope.editAlbum = function () {
      $('#editAlbumModal').modal('show');
    };

    // method for delete challenge

    $scope.deleteAlbum = function () {
      // confirmation for access deleting album
      var ifDeleteAlbum = confirm('This will completely delete the ALBUM and all its related information, photos and comments. \n Note: This action cannot be undone!');

      if (ifDeleteAlbum) {
        $http({
          url: apiUrl + 'album/' + $scope.album.id,
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + Token.get()
          },
          data: {
            _method: 'delete',
          },
        }).then(function (success) {
          console.log('Album has been deleted: ', success);
          $location.path('/albums');
        }, function (error) {
          console.log('Album delete error: ', error);
        })
      }
    };

    // method for open modal with form for setting album Live
    $scope.showAlbumSetLiveModal = function () {
      $('#albumSetLive').modal('show');
    };

    $scope.hiddenBlock = false;
    $scope.hiddenBtn = true;
    $scope.rendered = false;

    $scope.showMore = function () {
      $scope.hiddenBlock = !$scope.hiddenBlock;
    };

    $scope.checkHeightOfBlurbBlock = function () {
      if ($scope.album) {
        if ($('#shortBlurb').height() > 60) {
          $scope.hiddenBlock = true;
          $scope.hiddenBtn = false;
          $scope.rendered = true;
        }
      }
    };

    this.$doCheck = function () {
      if ($scope.album && !$scope.rendered) {
        $scope.checkHeightOfBlurbBlock();
      }
    };

    // methods and functions for tags
    $scope.tags = [];
    $scope.tagsSearchFocus = false;

    // function with ajax-request for getting tags-variants values for tags-input
    function getTagsVariants() {
      var url = apiUrl + 'album/' + $scope.album.id + '/tags?input=' + $scope.inputSearchParam;
      $http({
        url: url,
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + Token.get()
        }
      }).then(function (success) {
        console.log('Tags variants success: ', success.data);
        $scope.lifeSearchVariants = success.data;
      }, function (error) {
        console.log('Tags variants error: ', error);
      });
    }

    // method for adding tag
    $scope.addNewTag = function () {
      if ($scope.inputSearchParam) {
        if ($scope.inputSearchParam[0] === '#') {
          $scope.inputSearchParam = $scope.inputSearchParam.split('');
          $scope.inputSearchParam.splice(0, 1);
          $scope.inputSearchParam = $scope.inputSearchParam.join('');
        }
        var ifHasSimilar = false;
        var regexp = new RegExp("^[a-zA-z0-9]+$");
        var ifRegexpTrue = regexp.test($scope.inputSearchParam);
        $scope.inputSearchParam = '#' + $scope.inputSearchParam;

        if ($scope.tags.length) { // checking if new tag is exists it tags array
          $scope.tags.forEach(function (tag) {
            if ($scope.inputSearchParam === tag) {
              ifHasSimilar = true;
            }
          });
        }

        if (!ifHasSimilar && $scope.inputSearchParam && ifRegexpTrue) { // if tag is not exists in tags array, we push it there
          $scope.tags.push($scope.inputSearchParam);
        }
        $scope.inputSearchParam = ''; // making binding data for new tag empty
        $scope.lifeSearchVariants = []; // and making tags variants array empty
      }
    };

    $('#tagsTextarea').keydown(function (e) {
      if (e.which === 9) {
        e.preventDefault();
        $scope.addNewTag();
      }
    });

    $scope.onTagsSearchFocus = function (boolean) {
      setTimeout(function () {
        $scope.tagsSearchFocus = boolean;
      }, 500);
    };

    $(window).keydown(function (event) {
      if (event.keyCode === 13) {
        $scope.viewFilter();
        document.getElementById('tagsInput').blur();
      }
    });

    this.$onDestroy = function () {
      $(window).keydown(function () {});
    };

    // method for check if user clicked space button for add new tag
    $scope.keyupFunction = function ($event) {
      $event.preventDefault();
      if ($event.keyCode === 32 || $event.keyCode === 13) {
        $scope.addNewTag();
      } else {
        getTagsVariants();
      }
    };

    // if user clicked button for delete tag
    $scope.deleteTag = function ($index, $event) {
      $event.preventDefault();
      $scope.tags.splice($index, 1);
    };

    // method for choose one on the variants of tags
    $scope.chooseTagVariant = function (item) {
      $scope.inputSearchParam = item;

      if ($scope.searchParam === 'tags') {
        $scope.addNewTag();
      }
    };

    // function for preparing correct url for request for sort_by select method

    function prepareCorrectUrl() {
      var sort_by_array = $scope.sort_by.split(', ');
      var url = apiUrl + 'photo?album_id=' + $scope.album.id + '&year=' + $stateParams.pathYear;
      // here if we have only one param of sort_by, new_random for example, we add only it to our url for request
      if (sort_by_array[0]) url += '&sort_by=' + sort_by_array[0];
      // here if we have one more param, like desc or asc, we add sort_dir for request
      if (sort_by_array[1]) url += '&sort_dir=' + sort_by_array[1];

      if (sort_by_array[0] === 'new_random') { // if first param is new_random, we reset all searching and filtering values
        $scope.filterParams = '';
        $scope.searchParam = 'username';
        $scope.inputSearchParam = '';
        $scope.tags = [];
      } else { // else we add all searching and filtering values to our request
        if ($scope.filterParams) url += '&filter_by=' + $scope.filterParams;
        if ($scope.searchParam === 'username' && $scope.inputSearchParam) url += '&search_by=' + $scope.searchParam + '&search_query=' + $scope.inputSearchParam;

        if ($scope.searchParam === 'tags' && $scope.tags) {
          var search_query = $scope.tags.map(function (tag) {
            return tag[0] === '#' ? tag.split('').map(function (char) {
              return char === '#' ? '' : char;
            }).join('') : tag;
          }).join(',');
          url += '&search_by=' + $scope.searchParam + '&search_query=' + search_query;
        }
      }

      $scope.url = url;
      return url;
    }

    // method for send request for sort_by select
    $scope.sort_by = '';
    $scope.onSortByChange = function () {
      $http({
        url: prepareCorrectUrl(),
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + Token.get()
        },
      }).then(function (success) {
        console.log('get filtered album success: ', success.data);
        $scope.photos = success.data.data;
      }, function (error) {
        console.log('get filtered album error: ', error);
      });
    };

    var updatePhotosData = function(url) {
      url += '&is_challenge_page=false&year=' + $scope.album.year;
      $scope.loadingMore = true;
      $http.get(url).then(function (response) {
        console.log('More photos success: ', response.data);
        var newPhotosData = response.data.photosData;
        pushPhotosToArray(newPhotosData);
        $scope.loadingMore = false;
        $scope.album.photosData.current_page = newPhotosData.current_page;
        $scope.album.photosData.next_page_url = newPhotosData.next_page_url;
      }, function (error) {
        $scope.loadingMore = false;
        console.log('More photos success: ', error);
      });
    };

    // METHODS for Pagination

    // method for getting next page of pagination
    $scope.showMorePhotos = function () {
      if ($scope.album.photosData.next_page_url) {
        updatePhotosData($scope.album.photosData.next_page_url);
      }
    };
  }
]);
































