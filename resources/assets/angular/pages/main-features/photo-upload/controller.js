appSettings.app.controller('UploadPhotoController', [
    '$scope', '$rootScope', 'Photo', 'ExifItem', 'Variables', '$timeout', 'TinymceHelper', 'Helpers', 'HelpsUploadProgress', 'SmoothScroll',
    function ($scope, $rootScope, Photo, ExifItem, Variables, $timeout, TinymceHelper, Helpers, HelpsUploadProgress, SmoothScroll) {

        $scope.maxKbAllowed = appSettings.photo.upload.validMaxKb;

        $scope.albumTagsList = $scope.album.tags ? $scope.album.tags.split(',') : [];

        $scope.photoDetails = {
            screencast_critique: true,
            critique_level: '52f-cc_regular',
            camera_settings_answers: {},
            about_photo_answers: {},
            tags: []
        };

        $scope.errors = {};

        var $uploadPhotoModal = $('#uploadPhotoModal');

        $timeout(function () {
            if (!window.tinymce) return;
            var captionBoxName = 'uploadPhoto-caption';
            if (tinymce.get(captionBoxName)) {
                tinymce.EditorManager.execCommand('mceRemoveEditor', true, captionBoxName);
            }
            tinymce.init({
                selector: '#' + captionBoxName,
                max_chars: Variables.captionLimit.total,
                height: 250,
                menubar: false,
                plugins: TinymceHelper.plugins_caption,
                toolbar: TinymceHelper.toolbar_short,
                content_css: TinymceHelper.content_css,
                init_instance_callback: function (inst) {
                    TinymceHelper.defaultValue(inst, $scope.photoDetails.caption);
                },
                setup: function (editor) {
                    editor.on('change', function () {
                        TinymceHelper.setup(editor);
                        if ($scope.errors) $scope.errors.caption = {};
                    });
                }
            });
        });

        var $form = $uploadPhotoModal.find('form');

        var droppedFiles = false;

        var $fileInput = $form.find('#uploadPhoto-photo').on('click', function () {
            delete $scope.errors.photo;
        }).on('change', function () {
            droppedFiles = false;
            checkFileSelected();
        });

        var isAdvancedUpload = function () {
            var label = document.createElement('label');
            return (('draggable' in label) || ('ondragstart' in label && 'ondrop' in label)) && window.FormData && window.FileReader;
        }();

        if (isAdvancedUpload) {
            $form.addClass('has-advanced-upload');
        }

        if (isAdvancedUpload) {

            var droppedInsideTarget = false;
            $scope.isDraggedInsideBox = false;

            var $body = $('body');
            $body.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
            }).on('dragover dragenter', function () {
                droppedInsideTarget = false;
                $body.addClass('is-dragover');
            }).on('dragleave dragend', function (e) {
                if (e.originalEvent.pageX != 0 || e.originalEvent.pageY != 0) return;
                $body.removeClass('is-dragover');
            }).on('drop', function (e) {
                $body.removeClass('is-dragover');
                if (!droppedInsideTarget) $scope.removePhoto();
            });

            $form.find('.box__droptarget').on('dragover dragenter', function () {
                delete $scope.errors.photo;
                $body.addClass('is-insidebox');
                $scope.isDraggedInsideBox = true;
            });

            $form.find('.box__insidetarget').on('dragleave dragend drop', function (e) {
                $body.removeClass('is-insidebox');
                $scope.isDraggedInsideBox = false;
            }).on('drop', function (e) {
                droppedInsideTarget = true;
                droppedFiles = e.originalEvent.dataTransfer.files;
                $fileInput[0].value = '';
                checkFileSelected();
            });

        }

        var getFileChosen = function () {
            var files = droppedFiles || $fileInput[0].files;
            return files && files[0];
        };

        var checkPhotoFilesize = function (file) {
            var chosenFileKb = file.size ? file.size / 1024 : 0;
            var validSize = chosenFileKb <= $scope.maxKbAllowed;
            if (validSize) return;
            $scope.errors.photo = [
                'The photo may not be greater than ' + $scope.maxKbAllowed + ' kilobytes.'
            ];
        };

        var checkFileSelected = function () {
            $scope.showSelectedPhoto = false;
            var file = getFileChosen();
            if (!file) return;
            if (!file.name.match(/\.jpg|png/i)) {
                $scope.removePhoto();
                alert('Only image files are allowed! :)');
            } else if (window.File && window.FileReader && window.FileList && window.Blob) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $('#uploadPhoto-photo-preview')[0];
                    img.src = reader.result;
                    $scope.showSelectedPhoto = true;
                    checkPhotoFilesize(file);
                };
                reader.readAsDataURL(file);
            }
        };


        $scope.critiqueLevels = Variables.critiqueLevels;

        $scope.exifItems = {
            camera_settings_questions: [],
            about_photo_questions: []
        };
        ExifItem.get().then(function (response) {
            console.log('get list of exif items success: ', response);
            for (var i = 0; i < response.length; i++) {
                var responseItem = response[i];
                $scope.exifItems[responseItem.group_name].push(responseItem);
            }
        }, function (response) {
            console.log('get list of exif items failed: ', response);
        });


        var trimText = function (text) {
            return text.replace(/^\s+/, '').replace(/\s+$/, '');
        };

        var checkDuplicates = function (word, inputArr) {
            for (var i = 0; i < inputArr.length; i++) {
                if (inputArr[i] === word) return true;
            }
            return false;
        };

        $scope.focusTagsInput = function () {
            $('#uploadPhoto-tags').focus();
        };

        $scope.addToTags = function (word) {
            var allTags = [].concat($scope.albumTagsList, $scope.photoDetails.tags);
            if (!checkDuplicates(word, allTags)) {
                $scope.photoDetails.tags.push(word);
            }
        };

        $scope.checkTagInput = function () {
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/^\s+/, '').replace(/^#/, '');
            var hasNewWord = cleaned.match(/[, #]/);
            if (hasNewWord && hasNewWord.length) {
                var words = cleaned.split(hasNewWord[0]);
                var previousWord = trimText(words[0]);
                $scope.tagInput = trimText(words[1]);
                if (previousWord) {
                    $scope.addToTags(previousWord);
                }
            }
        };

        $scope.tagInputBlur = function () {
            if (!$scope.tagInput) return;
            $scope.tagInput = $scope.tagInput.replace(/[^a-z A-z,0-9#]/g, '');
            var cleaned = $scope.tagInput.replace(/[# ,]/g, '');
            var lastInput = trimText(cleaned);
            if (lastInput) {
                $scope.addToTags(lastInput);
                $scope.tagInput = '';
            }
            return lastInput;
        };

        $scope.onEnterKeyInsideTagInput = function () {
            var lastInput = $scope.tagInputBlur();
            if (!lastInput) return true;
        };

        $scope.onBackspaceInsideTagInput = function () {
            if (!$scope.tagInput) {
                $scope.photoDetails.tags.pop();
            }
        };

        $scope.removeTag = function (index) {
            if ($scope.processing) return;
            $scope.photoDetails.tags.splice(index, 1);
        };


        $scope.removePhoto = function () {
            if ($scope.processing) return;
            $scope.showSelectedPhoto = false;
            delete $scope.errors.photo;
            $fileInput[0].value = '';
            droppedFiles = false;
        };

        HelpsUploadProgress.registerTextHolder($scope, 'uploadProgress');
        $scope.saveNewPhoto = function () {
            $scope.errors = {};
            var photoDetails = angular.copy($scope.photoDetails);
            photoDetails.album_id = $scope.album.id;
            photoDetails.photo = getFileChosen();
            photoDetails.caption = $('#uploadPhoto-caption').val();
            $scope.processing = true;
            console.log('photoDetails: ', photoDetails);
            HelpsUploadProgress.uploadStarted();
            Photo.save(photoDetails).then(function (response) {
                console.log('saving photo success', response);
                HelpsUploadProgress.uploadCompleted(true);
                $scope.successCallback({photo: response.record});
                $scope.processing = false;
                $uploadPhotoModal.modal('hide');
                setTimeout(function () {
                    $('#uploadPhotoSuccessModal').modal('show');
                }, 400);
            }, function (response) {
                console.log('saving photo failed', response);
                HelpsUploadProgress.uploadCompleted(false);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                    SmoothScroll.toFirstError($scope.errors);
                } else if (response.message) {
                    alert(response.message);
                    if (response.reload_page)
                        Helpers.reloadPage();
                }
            }, HelpsUploadProgress.uploadProgress);
        };

        $scope.clearModelConsent = function () {
            delete $scope.photoDetails.model_consent;
            delete $scope.errors['model_consent'];
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            delete $scope.errors[fieldname];
        };

    }
]);