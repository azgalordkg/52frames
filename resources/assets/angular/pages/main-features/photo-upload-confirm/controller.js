appSettings.app.controller('UploadPhotoConfirmController', [
    '$scope', 'Variables', '$timeout', 'Photo', function ($scope, Variables, $timeout, Photo) {

        $scope.variables = Variables;
        $scope.critiqueLevels = Variables.critiqueLevels;
        $scope.findCritiqueLevel = Variables.helpers.findCritiqueLevel;

        $scope.exifData = {
            shutter: '',
            aperture: '',
            iso: '',
            lens: ''
        };
        $scope.hasExifData = function (photo) {
            var shouldShow = false;
            if (!photo || !photo.exif_answers) return shouldShow;
            for (var column in $scope.exifData) {
                for (var i = 0; i < photo.exif_answers.length; i++) {
                    var answer = photo.exif_answers[i];
                    if (answer.exif_member && answer.exif_member.group_name == column) {
                        $scope.exifData[column] = answer.exif_member.name;
                        shouldShow = true;
                    }
                }
            }
            return shouldShow;
        };

        $scope.showHideExifData = function () {
            $scope.showExifData = !$scope.showExifData;
        };

        $scope.showHideCheckboxes = function () {
            $scope.showCheckboxes = !$scope.showCheckboxes;
        };


        $scope.captionEllipsesCb = function (hasEllipses, id) {
            $scope.captionHasEllipses = hasEllipses;
            id && $timeout(function () {
                $('#' + id).click($scope.expandCaption);
            });
        };

        $scope.expandCaption = function () {
            $scope.captionExpanded = true;
        };

        $scope.collapseCaption = function () {
            $scope.captionExpanded = false;
        };


        $scope.getTags = function (album) {
            var albumTags = album.tags ? album.tags : '';
            var photoTags = (album.user_has_uploaded && album.user_has_uploaded.tags) || '';
            var allTags = albumTags + (albumTags && photoTags ? ',' : '') + photoTags;
            return allTags.replace(/,\s*/g, ', ');
        };

        $scope.getShootingAndProcessing = function (photo) {
            $scope.showShootAndProc = false;
            var result = [];
            if (!photo || !photo.exif_answers) return;
            for (var i = 0; i < photo.exif_answers.length; i++) {
                var answer = photo.exif_answers[i];
                if (answer.exif_question && answer.exif_question.group_name == 'shoot_and_proc') {
                    result.push(answer.exif_question.name);
                    $scope.showShootAndProc = true;
                }
            }
            return result.join(', ');
        };

        $scope.getSubjectMatters = function (photo) {
            $scope.showSubjectMatter = false;
            var result = [];
            if (!photo || !photo.exif_answers) return;
            for (var i = 0; i < photo.exif_answers.length; i++) {
                var answer = photo.exif_answers[i];
                if (answer.exif_question && answer.exif_question.group_name == 'subject_matter') {
                    result.push(answer.exif_question.name);
                    $scope.showSubjectMatter = true;
                }
            }
            return result.join(', ');
        };

        $scope.editPhoto = function () {
            $('#uploadPhotoSuccessModal').modal('hide');
            setTimeout(function () {
                $('#editPhotoModal').modal('show');
            }, 400);
        };

        $scope.deleteEntry = function () {
            if (!$scope.album.user_has_uploaded) return;
            $scope.processing = true;
            Photo.delete({
                id: $scope.album.user_has_uploaded.id
            }).then(function (response) {
                console.log('delete successful: ', response);
                $scope.deleteSuccessCallback(response);
                $scope.processing = false;
                $('#uploadPhotoSuccessModal').modal('hide');
                setTimeout(function () {
                    delete $scope.album.user_has_uploaded;
                }, 400);
            }, function (response) {
                console.log('delete failed: ', response);
                $scope.processing = false;
                if (response.message) alert(response.message);
            });
        };

    }
]);