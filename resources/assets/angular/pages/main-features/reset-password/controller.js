appSettings.app.controller('ResetPasswordController', ['$rootScope', '$scope', '$timeout', '$location', 'Auth', 'Variables', function ($rootScope, $scope, $timeout, $location, Auth, Variables) {

    $(function () {
        $timeout(function () {
            if ($rootScope.resetPasswordToken)
                $('#resetPasswordModal').modal('show');
        }, 500);
    });

    $('#resetPasswordModal').on('show.bs.modal', function (e) {
        $timeout(function () {

            $scope.userData = {
                token: $rootScope.resetPasswordToken
            };

            $scope.errors = {};

        }, 0);

    });

    $scope.changePassword = function () {

        $scope.errors = {};

        $scope.processing = true;
        Auth.resetPassword($scope.userData).then(function (response) {
            console.log('password reset success', response);
            $scope.processing = false;
            Variables.localStorage('ns:resetPassword:emailSent', null);
            if (!Variables.helpers.hasIntendedPage())
                Variables.helpers.rememberPage();
            $('#resetPasswordModal').modal('hide');
            setTimeout(function () {
                $('#loginModal').modal('show');
            }, 400);
        }, function (response) {
            console.log('password reset failed', response);
            $scope.processing = false;
            if (response.validation_errors) {
                $scope.errors = response.validation_errors;
            } else if (response.error) {
                $scope.errors = {token: [response.message]};
            }
        });

    };

    $scope.logout = function () {
        $scope.processing = true;
        $rootScope.$navbar.logout().then(function (response) {
            $scope.processing = false;
        }, function (response) {
            $scope.processing = false;
        });
    };

    $scope.hideErrors = function () {
        $scope.errors = {};
    };

}]);