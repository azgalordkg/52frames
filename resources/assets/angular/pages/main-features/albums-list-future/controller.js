appSettings.app.controller('futureAlbumsListController', ['Loading', '$scope', '$rootScope', 'Album', '$state', function (Loading, $scope, $rootScope, Album, $state) {

    var albumPage = '/albums/{pathYear}/{pathWeekTheme}';

    Album.future().then(function (response) {
        console.log('get list of albums, success: ', response);
        $scope.albums = response;
        Loading.pageLoaded($scope);
    }, function (response) {
        console.log('get list of albums, failed: ', response);
    });

    $scope.showAlbumOrChallengePage = function (album) {
        var maxAdminPoints = $rootScope.user ? $rootScope.user.max_admin_points : 0;
        var showAlbumPage = album.live || maxAdminPoints >= 100000;
        $state.go(showAlbumPage ? 'weekTheme' : 'weekChallenge', {
            pathWeekTheme: 'week-' + album.week_number + '-' + album.shorturl,
            pathYear: album.year
        });
    };

}]);