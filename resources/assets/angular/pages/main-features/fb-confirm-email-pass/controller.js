appSettings.app.controller('FbLoginFoundExistingEmailModalController', ['$scope', 'Auth', 'FacebookAuth', function ($scope, Auth, FacebookAuth) {

    $('#fbLoginExistingEmailModal').on('show.bs.modal', function (e) {

        $scope.userData = FacebookAuth.userData || {};
        delete FacebookAuth.userData;

        $scope.errors = {};
    });

    $scope.linkAccount = function () {

        $scope.errors = {};

        $scope.processing = true;
        FacebookAuth.linkAccount($scope.userData).then(function (response) {
            console.log('linking success: ', response);
            $scope.processing = false;
            redirectToIntendedPageOrCurrentAlbumOrRefresh(response);
        }, function (response) {
            console.log('linking failed: ', response);
            $scope.processing = false;
            if (response.validation_errors) {
                $scope.errors = response.validation_errors;
            } else if (response.message) {
                alert(response.message);
            }
        });

    };

    $scope.forgotPassword = function () {
        Auth.userData = {email: $scope.userData.email};
        setTimeout(function () {
            $('#forgotPasswordModal').modal('show');
        }, 400);
    };

    $scope.hideErrors = function () {
        $scope.errors = {};
    };

}]);