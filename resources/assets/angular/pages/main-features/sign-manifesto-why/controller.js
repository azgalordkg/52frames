appSettings.app.controller('SignManifestoWhyController', [
    '$scope', '$timeout', 'Manifesto', 'Auth', 'Boot',
    function ($scope, $timeout, Manifesto, Auth, Boot) {

        $('#signManifestoWhyModal').on('show.bs.modal', function (e) {

            $scope.manifestoInput = Manifesto.manifestoInput || {};
            delete Manifesto.manifestoInput;

            $scope.errors = {};

        });

        $scope.finish = function () {
            $scope.errors = {};

            var manifestoInput = angular.copy($scope.manifestoInput);
            manifestoInput.country = manifestoInput.country ? manifestoInput.country.id : null;
            manifestoInput.state = manifestoInput.state ? manifestoInput.state.id : null;

            $scope.processing = true;
            Manifesto.save(manifestoInput).then(function (response) {
                console.log('success: ', response);
                if (!response.success) {
                    $scope.processing = false;
                    alert(response);
                } else Auth.me().then(function (response) {
                    Boot.copyAlbumCurrentWeekFromLoginToRootScope(response);
                    $scope.processing = false;
                    $('#signManifestoWhyModal').modal('hide');
                    setTimeout(function () {
                        $('#signManifestoConfirmModal').modal('show');
                    }, 400);
                }, function () {
                    $scope.processing = false;
                });
            }, function (response) {
                console.log('failed: ', response);
                $scope.processing = false;
                if (response.validation_errors) {
                    $scope.errors = response.validation_errors;
                } else if (response.message) {
                    alert(response.message);
                }
            });
        };

        $scope.back = function () {
            Manifesto.manifestoInput = $scope.manifestoInput;
            setTimeout(function () {
                $('#signManifestoCheckboxesModal').modal('show');
            }, 400);
        };

        $scope.hideErrors = function (fieldname) {
            if (!$scope.errors) return;
            $scope.errors[fieldname] = '';
        };

    }
]);