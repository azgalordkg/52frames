appSettings.app.factory('FileHelper', ['$q', function ($q) {

    var methods = {

        readContents: function (file, successCb, failedCb) {
            if (!file) return failedCb && failedCb('no_file_input');
            if (!window.FileReader) return failedCb && failedCb('browser_unsupported');
            var reader = new FileReader();
            reader.onload = function (e) {
                var contents = reader.result;
                if (contents) return successCb && successCb($.trim(reader.result));
                failedCb && failedCb('file_empty');
            };
            reader.readAsText(file, 'ISO-8859-1');
        },

        validateFile: function (file, fileExtArr) {
            fileExtArr = fileExtArr || ['text/csv'];
            var error, deferred = $q.defer();
            var rejectCb = function (errString) {
                deferred.reject({file: [errString]});
            };
            if (!file || file.type && $.inArray(file.type, fileExtArr) < 0)
                error = 'Please choose a valid CSV file.';
            if (error) rejectCb(error);
            else methods.readContents(file, deferred.resolve, function (error_code) {
                switch (error_code) {
                    case 'file_empty':
                        error = 'The file seems to be empty!';
                        break;
                    case 'browser_unsupported':
                        error = 'You may need to upgrade your browser? :)';
                        break;
                    default:
                        error = 'Something went wrong. Please try again?';
                }
                rejectCb(error);
            });
            return deferred.promise;
        }

    };

    return methods;

}]);