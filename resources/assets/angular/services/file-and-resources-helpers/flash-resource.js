appSettings.app.factory('FlashResource', ['$q', function ($q) {

    var data = {};

    return {

        set: function (key, value) {
            return data[key] = value;
        },

        get: function (key) {
            return data[key];
        }

    };

}]);