appSettings.app.service('HelpsUploadProgress', ['$timeout', 'RandomLoadingTexts', function ($timeout, RandomLoadingTexts) {

    var uploadStarted = false;
    var progressTextHolder = {
        progress: null,
        percentage: null,
        value: null
    };
    var targetScope = null;
    var scopeKeyName = null;

    var randomText = {
        keyword: '{randomText}',
        isKeyword: function (text) {
            return text === randomText.keyword;
        },
        generate: function () {
            var randomText = RandomLoadingTexts.generate();
            return randomText.join(' ');
        }
    };

    var helpers = {
        resetTextHolder: function () {
            if (targetScope && scopeKeyName)
                progressTextHolder = targetScope[scopeKeyName] = {};
        },
        chooseText: function () {
            if (uploadStarted && typeof(progressTextHolder.percentage) === 'number') {
                if (progressTextHolder.percentage >= 100) helpers.extraText.init();
                else progressTextHolder.value = 'Upload progress: ' + progressTextHolder.percentage + '%';
            }
        },
        extraText: {
            count: 0,
            timer: null,
            messages: [
                'Validating Inputs...',
                'Compressing Photo...',
                'Compressing Photo...',
                'Creating thumbnail...',
                'Waiting for server...',
                randomText.keyword,
                randomText.keyword,
                randomText.keyword,
                'Max wait time is ' + appSettings.photo.upload.awaitSecs +' seconds...'
            ],
            init: function () {
                helpers.extraText.count = 0;
                $timeout.cancel(helpers.extraText.timer);
                helpers.extraText.timer = $timeout(helpers.extraText.chooseMessage, 1250);
            },
            chooseMessage: function () {
                if (!uploadStarted) return;
                var count = helpers.extraText.count;
                var message = helpers.extraText.messages[count];
                var shouldRandomText = randomText.isKeyword(message);
                if (!message || shouldRandomText) message = randomText.generate();
                progressTextHolder.value = message;
                helpers.extraText.count++;
                helpers.extraText.timer = $timeout(helpers.extraText.chooseMessage, 1750);
            }
        }
    };

    this.registerTextHolder = function (scope, keyName) {
        targetScope = scope;
        scopeKeyName = keyName;
        helpers.resetTextHolder();
    };

    this.uploadStarted = function () {
        uploadStarted = true;
    };

    this.uploadProgress = function (notify) {
        if (uploadStarted && notify) {
            progressTextHolder.progress = notify.loaded ? notify.loaded / notify.total : 0;
            progressTextHolder.percentage = Math.round(progressTextHolder.progress * 100);
            helpers.chooseText();
        } else helpers.resetTextHolder();
    };

    this.uploadCompleted = function (success) {
        uploadStarted = false;
        helpers.resetTextHolder();
    };

}]);