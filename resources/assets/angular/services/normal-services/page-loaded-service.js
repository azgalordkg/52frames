appSettings.app.service('Loading', ['$timeout', '$rootScope', function ($timeout, $rootScope) {

    this.pageLoaded = function ($scope) {
        $timeout(function () {
            $rootScope.pageLoaded = true;
            $scope.$on('$destroy', function () {
                delete $rootScope.pageLoaded;
            });
        });
        return true;
    };

    this.pageRequiresAdminRole = function ($scope) {
        $timeout(function () {
            $rootScope.pageRequiresAdminRole = true;
            $scope.$on('$destroy', function () {
                delete $rootScope.pageRequiresAdminRole;
            });
        });
        return true;
    };

}]);