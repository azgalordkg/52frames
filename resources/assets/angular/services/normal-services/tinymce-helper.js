appSettings.app.factory('TinymceHelper', ['$timeout', function ($timeout) {

    return {

        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu paste directionality',
            "emoticons textcolor colorpicker textpattern"
        ],

        plugins_shortblur: [
            'link charmap hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars',
            'insertdatetime nonbreaking save contextmenu paste directionality',
            "emoticons textcolor colorpicker textpattern"
        ],

        plugins_caption: [
            'link charmap hr',
            'searchreplace charactercount visualblocks visualchars',
            'insertdatetime nonbreaking contextmenu paste directionality',
            "emoticons"
        ],

        toolbar: 'undo redo | emoticons insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | code',

        toolbar_short: 'undo redo | emoticons insert | bold italic | link',

        toolbar_notes_from_yosef: 'emoticons insert | bold italic | link',

        content_css: [
            '//fonts.googleapis.com/css?family=Dosis',
            '/vendor/tinymce/css/styles.css'
        ],

        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
            var cmsURL = '/laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'File Manager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no"
            });
        },

        defaultValue: function (editor, value) {
            editor.setContent(value || '');
            editor.save();
        },

        setup: function (editor) {
            editor.save();
        },

        init_instance_callback: function (editor) {

        }

    };

}]);