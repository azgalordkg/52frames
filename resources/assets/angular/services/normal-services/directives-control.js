appSettings.app.factory('DirectivesControl', ['$rootScope', function ($rootScope) {
    return {
        disable: function () {
            $rootScope.disableDirectives = true;
        },
        enable: function () {
            delete $rootScope.disableDirectives;
        }
    };
}]);