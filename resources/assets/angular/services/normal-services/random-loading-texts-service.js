appSettings.app.factory('RandomLoadingTexts', [function () {

    var messages = [
        [
            'Initializing',
            'Regulating',
            'Preparing',
            'Encoding',
            'Buffering',
            'Formulating',
            'Charging',
            'Activating'
        ],
        [
            'Spectrometer',
            'Flux Capacitor',
            'Light Field',
            'Strobe',
            'Light Particle',
            'Gamma Wave',
            'Fun-Time',
            'Magical'
        ],
        [
            'Capacity',
            'Spectrum',
            'Analysis',
            'Index',
            'Encoder',
            'Beans',
            'Watermelons',
            'Frames'
        ]
    ];

    var generateRandomNumber = function () {
        var minimum = 0;
        var maximum = messages[0].length - 1;
        return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
    };

    return {
        generate: function () {
            return [
                messages[0][generateRandomNumber()],
                messages[1][generateRandomNumber()],
                messages[2][generateRandomNumber()]
            ];
        }
    }

}]);