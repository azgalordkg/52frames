appSettings.app.factory('PhotosAggregate', ['$filter', '$rootScope', function ($filter, $rootScope) {

    var photosList = [];

    var helpers = {

        /**
         * Validates the input photo and returns a copy of it with only the essential keys for caching.
         * @param photo
         * @returns {boolean|object}
         */
        validatePhoto: function (photo) {
            if (!photo) return false;
            if (!photo.id) return false;
            if (!photo.hi_res_filename) return false;
            if (!photo.thumbnail_filename) return false;
            if (typeof(photo.show) !== 'boolean') photo.show = !!photo.show;
            if (typeof(photo.is_graphic) !== 'number' || photo.is_graphic < 0 || photo.is_graphic > 1) return false;
            if (typeof(photo.has_nudity) !== 'number' || photo.has_nudity < 0 || photo.has_nudity > 1) return false;
            if (!photo.owner || typeof(photo.owner) !== 'object') return false;
            if (!photo.owner.id || photo.owner.handle && typeof(photo.owner.handle) !== 'string') return false;
            if (photo.album) {
                if (typeof(photo.album) !== 'object') return false;
                if (!photo.album.id || !photo.album.week_number || !photo.album.year || photo.album.shorturl && typeof(photo.album.shorturl) !== 'string') return false;
            }
            var output = {
                id: photo.id,
                show: photo.show,
                is_graphic: photo.is_graphic,
                has_nudity: photo.has_nudity,
                hi_res_filename: photo.hi_res_filename,
                thumbnail_filename: photo.thumbnail_filename,
                owner: {id: photo.owner.id, handle: photo.owner.handle}
            };
            if (photo.album) output.album = {
                id: photo.album.id,
                year: photo.album.year,
                week_number: photo.album.week_number,
                shorturl: photo.album.shorturl
            };
            return output;
        },

        /**
         * Filters the Photos list using the input id.
         * @param id
         * @returns {null|object}
         */
        filterPhotosById: function (id) {
            if (!id) return null;
            var result = $filter('filter')(photosList, function (value) {
                return value.id === id;
            });
            return result.length ? result[0] : null;
        }

    };

    /**
     * Listens to the event, so we can update the variable later when the user decides to click.
     */
    $rootScope.$on('fromPhotoView:set:photo.show', function (event, photo) {
        var targetPhoto = helpers.filterPhotosById(photo.id);
        if (targetPhoto) targetPhoto.show = !!photo.show;
    });

    var methods = {

        /**
         * Gets 1 Photo object from the list.
         * @param id
         * @returns {null|object}
         */
        findOne: function (id) {
            return helpers.filterPhotosById(id);
        },

        /**
         * Saves the photo to the list or updates the object if it already exists.
         * @param photo
         * @returns {null|object}
         */
        saveOrUpdate: function (photo) {
            if (!photo || !photo.id) return null;
            var inputPhoto = angular.copy(photo);
            $rootScope.$broadcast('fromPhotoView:get:photo.show', inputPhoto);
            var validated = helpers.validatePhoto(inputPhoto);
            if (!validated) return null;
            var existing = helpers.filterPhotosById(inputPhoto.id);
            if (existing) angular.copy(validated, existing);
            else photosList.push(validated);
            if (photo.nextPhoto) methods.saveOrUpdate(photo.nextPhoto);
            if (photo.previousPhoto) methods.saveOrUpdate(photo.previousPhoto);
            return validated;
        },

        /**
         * Saves a batch of Photos.
         * @param photos
         * @returns {null|boolean}
         */
        saveOrUpdatePhotoArray: function (photos) {
            if (!angular.isArray(photos)) return null;
            for (var i = 0; i < photos.length; i++) {
                methods.saveOrUpdate(photos[i]);
            }
            return true;
        }

    };

    return methods;

}]);