(function () {
    var serviceName = 'PromiseRequest';
    appSettings.app.factory(serviceName, ['$q', 'Common', '$location', function ($q, Common, $location) {

        var keywords = {
            urlTempIdKeyword: '_page_loading_[replace]',
            replaceTerm: '[replace]'
        };

        var promisesList = {
            // parentResourceUrl: [entry, entry]
        };

        var needsRequestThen = [];

        var prioRequestsByGroup = {
            // group: index
        };

        var resolvers = {

            replaceKwWithRealId: function (string, instance) {
                var kwToReplace = instance.urlTempIdKeyword;
                var modelId = instance.referenceModel.cbToGetId(instance);
                return string.replace(kwToReplace, modelId);
            },

            replaceKwWithTempId: function (string, instance) {
                var kwToReplace = instance.urlTempIdKeyword;
                return keywords.urlTempIdKeyword.replace(kwToReplace, instance.index);
            },

            saveResponseData: function (response, instance) { // todo: many use the resource service instead?
                instance.resources.queryResult = response;
            },

            retrieveResponseData: function (instance) {
                return instance.resources.queryResult;
            }

        };

        var awaitProcesses = {

            routeToTemporaryUrl: function (instance, deferred) {
                console.info('routeToTemporaryUrl');
                var parentResourceUrl = instance.parentResourceUrl;
                var currentResourceUrlPattern = instance.currentResourceUrlPattern;
                var replacedCurrentResourceURL = resolvers.replaceKwWithTempId(currentResourceUrlPattern, instance);
                $location.path(parentResourceUrl + replacedCurrentResourceURL);
            },

            routeToFinalUrl: function (instance, deferred, response) {
                console.info('routeToFinalUrl', response);
                awaitProcesses.startClaimingForResult();
                var parentResourceUrl = instance.parentResourceUrl;
                var currentResourceUrlPattern = instance.currentResourceUrlPattern;
                var replacedCurrentResourceURL = resolvers.replaceKwWithRealId(currentResourceUrlPattern, instance);
                $location.path(parentResourceUrl + replacedCurrentResourceURL);
            },

            startClaimingForResult: function (instance) {
                if (instance.statuses.claimed) throw new Error('Data already claimed!');
                instance.statuses.claiming = true;
            },

            initBackendCallback: function (instance, argsArray) {
                var callback = instance.backendCallback;
                return Common.execCbWithThisBinding(callback, argsArray);
            }

        };

        var processFlows = {

            forMethods: {

                await: {

                    init: function (instance, deferred) {
                        console.info('processFlows.forMethods.await.startWatch()');
                        if (instance.backendCallback) {
                            if (instance.statuses.claiming) {
                                var response = resolvers.retrieveResponseData(instance);
                                deferred.resolve(response);
                            } else {
                                awaitProcesses.initBackendCallback(instance, []).then(function (response) {
                                    console.log('initBackendCallback done: ', response);
                                    resolvers.saveResponseData(response, instance);


                                    // todo: could have finished but now the user is on another page


                                    awaitProcesses.routeToFinalUrl(instance, deferred, response);
                                }, function (response) {
                                    console.log('initBackendCallback failed: ', response);
                                    deferred.reject(response);
                                });
                            }
                        }


                        // todo: the page does not know the next page's url


                        awaitProcesses.routeToTemporaryUrl(instance, deferred);

                    }

                }

            }

        };

        var helpers = {

            buildKeyword: function (modelId) {
                modelId = modelId || '';
                var tempIdPattern = keywords.urlTempIdKeyword;
                var defaultReplaceKw = keywords.replaceTerm;
                return tempIdPattern.replace(defaultReplaceKw, modelId);
            },

            attachReplaceTerm: function (input) {
                var inputRegEx = new RegExp(input);
                var contains = inputRegEx.test(keywords.replaceTerm);
                if (!contains) input = input + keywords.replaceTerm;
                return input;
            },

            findPromiseHolder: function (parentResourceUrl) {
                console.log('findPromiseHolder');
                if (!promisesList[parentResourceUrl]) promisesList[parentResourceUrl] = [];
                return promisesList[parentResourceUrl];
            },

            getDistanceFromRef: function (entry) {
                var referenceModel = entry.referenceModel;
                var modelId = referenceModel.cbToGetId(entry);
                var numPagesAwayWithAxis = entry.referenceModel.numPagesAwayWithAxis;
                return {modelId: modelId, numPagesAwayWithAxis: numPagesAwayWithAxis};
            },

            findSameEntry: function (parentResourceUrl, modelId, numPagesAwayWithAxis) {
                var i, holder = helpers.findPromiseHolder(parentResourceUrl);
                if (!holder) return;
                for (i = 0; i < holder.length; i++) {
                    var entry = holder[i];
                    var entryDistanceFromRef = helpers.getDistanceFromRef(entry);
                    var entryModelId = entryDistanceFromRef.modelId;
                    var entryNumPagesAwayWithAxis = entryDistanceFromRef.numPagesAwayWithAxis;
                    if (entryModelId === modelId && entryNumPagesAwayWithAxis === numPagesAwayWithAxis) return entry;
                }
            },

            getIfHasExistEntry: function (entry) {
                var entryDistanceFromRef = helpers.getDistanceFromRef(entry);
                var modelId = entryDistanceFromRef.modelId;
                var numPagesAwayWithAxis = entryDistanceFromRef.numPagesAwayWithAxis;
                var parentResourceUrl = entry.parentResourceUrl;
                return helpers.findSameEntry(parentResourceUrl, modelId, numPagesAwayWithAxis);
            },

            attachEntryToPromises: function (entry) {
                var holder = helpers.findPromiseHolder(entry.parentResourceUrl);
                holder.push(entry);
                entry.index = holder.length - 1;
                deferred.resolve(entry);
            },

            cantAttachHasExisting: function (existing, deffered) {
                console.log('existing entry found: ', existing, 'in promisesList: ', promisesList);
                deffered.reject(existing);

                // todo: maybe we shouldn't be rejected here, maybe just ignore it?

                // todo: is the user on the phtographer's url now? same id/nickname? is he claiming for the data?

                // todo: note, this may not be needed, refer to usage on the ctrl side
                // todo: may we should resolve from here and run the callback

                // todo: wait for the response of that existing, instead! (than rejecting this request)

            },

            attachSuccessful: function (entry) {
                console.log('added entry: ', entry, 'to promisesList: ', promisesList);

            }

        };

        var validators = {

            forMethods: {
                await: {
                    initial: function (options) {
                        return options.scope &&
                            options.referenceModel &&

                            options.parentResourceUrl &&
                            options.currentResourceUrlPattern &&

                            options.referenceModel.instance &&
                            options.referenceModel.cbToGetId &&
                            options.referenceModel.numPagesAwayWithAxis;
                    },
                    complete: function (options) {
                        var okOnInit = validators.forMethods.await.initial(options);
                        return !okOnInit ? okOnInit : options.urlTempIdKeyword && options.backendCallback;
                    }
                }
            }

        };

        var transformers = {

            forMethods: {

                await: function (options, deferred) {
                    return {
                        parentResourceUrl: Common.getUrlPathname(options.parentResourceUrl),
                        currentResourceUrlPattern: Common.getUrlPathname(helpers.attachReplaceTerm(options.currentResourceUrlPattern)),
                        urlTempIdKeyword: helpers.attachReplaceTerm(options.urlTempIdKeyword) || keywords.urlTempIdKeyword,
                        backendCallback: options.backendCallback || null,
                        referenceModel: {
                            instance: options.referenceModel.instance,
                            cbToGetId: Common.makeFunction(options.referenceModel.cbToGetId),
                            numPagesAwayWithAxis: options.referenceModel.numPagesAwayWithAxis
                        },
                        resources: {},
                        queue: deferred,
                        scope: options.scope,
                        statuses: {
                            claiming: false,
                            claimed: false
                        }
                    };
                }

            }

        };

        var errorsHelper = {

            commonMethods: {
                genErrMsgHeader: function (methodName) {
                    var msg = serviceName;
                    msg += methodName ? '::' + methodName + '()' : '';
                    return msg + ' -- ';
                },

                throwErrorNow: function (groupMsg, methodName, msg) {
                    groupMsg = groupMsg + ': ' || '';
                    var msgHeader = errorsHelper.commonMethods.genErrMsgHeader(methodName);
                    throw new Error(msgHeader + groupMsg + msg);
                }
            },

            validationError: function (methodName, msg) {
                methodName = methodName || '';
                msg = msg || 'Please make sure your inputs are correct';
                errorsHelper.commonMethods.throwErrorNow('Validation error', methodName, msg);
            }

        };

        var methods = {

            await: function (options) {
                var methodName = 'await';
                console.log(serviceName + '.' + methodName + '() detected', options);
                if (!validators.forMethods.await.initial(options)) {
                    console.log('validation failed'); // todo: how to handle return process, maybe the caller is expecting a specific response format
                    return errorsHelper.validationError(methodName);
                }
                var deferred = $q.defer();
                var reBuiltOptions = transformers.forMethods.await(options, deferred);
                if (!validators.forMethods.await.complete(reBuiltOptions)) {
                    console.log('validation failed'); // todo: how to handle return process, maybe the caller is expecting a specific response format
                    return errorsHelper.validationError(methodName);
                }
                var existing = helpers.getIfHasExistEntry(reBuiltOptions);
                if (!existing || existing.statuses.claimed) {
                    var instance = helpers.attachEntryToPromises(reBuiltOptions);
                    processFlows.forMethods.await.init(instance, deferred);
                } else helpers.cantAttachHasExisting(existing, deferred);
                return deferred.promise;
            },

            isTempPage: function (currentResourceUrlPattern) {
                var tempIdPattern = helpers.buildKeyword();
                var regExKeyword = new RegExp(tempIdPattern);
                return currentResourceUrlPattern.match(regExKeyword);
            }

        };

        var events = {

            onReset: function (group) {                                     // todo: will be executed by album controller, onInit of the page
                return prioRequestsByGroup[group] = null;
            }

        };

        return methods;

    }]);
})();