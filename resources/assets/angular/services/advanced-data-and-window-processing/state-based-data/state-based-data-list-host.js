appSettings.app.factory('StateBasedDataListHost', ['$timeout', function ($timeout) {

    function StateBasedDataListHost($scope, repos) {

        this.repos = {};
        this.$scope = $scope;
        this.csvFile = $scope.csvFile;

        this.setStateResetOthers = function (repo, state, args) {
            if (!state || !this.repos[repo] || !this.repos[repo].statesList[state]) return;
            for (var i in this.repos) {
                var savedRepo = this.repos[i];
                if (savedRepo.resets) savedRepo.current = null;
            }
            var _this = this;
            var stateObj = this.repos[repo].statesList[state];
            if (stateObj.initCallback) stateObj.initCallback.call(this, stateObj, args);
            _this.repos[repo].current = state;
        };

        this.getCurrentState = function (repo) {
            if (!this.repos[repo] || !this.repos[repo].current) return;
            var current = this.repos[repo].current;
            return current ? this.repos[repo].statesList[current] : null;
        };

        for (var i in repos) {
            var repo = repos[i];
            repo.setHost(this);
            this.repos[repo.name] = repo;
        }

    }

    return {
        init: function ($scope, repos) {
            return new StateBasedDataListHost($scope, repos);
        }
    }

}]);