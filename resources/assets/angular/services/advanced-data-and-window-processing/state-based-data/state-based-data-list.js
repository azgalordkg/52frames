appSettings.app.factory('StateBasedDataList', ['$timeout', function ($timeout) {

    function StateBasedDataList(name, states, resets, extra) {

        this.name = name;
        this.extra = extra;
        this.current = null;
        this.statesList = [];
        this.resets = resets;

        this.setHost = function (host) {
            this.host = host;
        };

        this.addState = function (keyword, state) {
            this.statesList[keyword] = state;
        };

        this.setStatusUsingKw = function (keyword) {
            if (!keyword || !this[keyword]) return;
            this.current = keyword;
            return this.statesList[keyword];
        };

        for (var i in states) {
            var record = states[i];
            this.addState(record.keyword, record.state);
        }

    }

    return {
        init: function (name, states, resets, extra) {
            return new StateBasedDataList(name, states, resets, extra);
        }
    }

}]);