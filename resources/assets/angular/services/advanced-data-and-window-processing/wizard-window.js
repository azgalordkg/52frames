appSettings.app.service('WizardWindow', ['$timeout', 'Common', function ($timeout, Common) {

    this.samplePageInfo = {

        csvFileUploadPage: {
            prevPageKw: 'introPage --> keyName in this same json object with similar information (optional)',
            guideTextIndex: '0 --> the array index of the guide that should be highlighted --> this should be integer though',
            initScopeMethod: 'initMethodNameHere --> must exist in $scope',
            submitText: 'Next --> the text that you want to show on the submit button to go to the next page',
            submitScopeMethod: 'csvFilePageCompleted --> must be an existing method in $scope',
            nextPageKw: 'validationPage (or something similar)'
        },

        validationPage: {
            similar: ['json', 'formatting', 'here', 'as', 'csvFileUploadPage']
        },

        introPage: {
            another: 'example'
        }
    };

    /**
     * inputJson must have guideTexts and pages key-value pairs.
     * guideTexts must have a list of 'title pages' in an array format.
     * pages must have a json keyword as the key and the page's details as the value, also in json.
     * see: WizardWindowHelper.samplePageInfo (property) for a sample of the pageInfo json format we are expecting.
     *
     * @param $scope
     * @param inputJson
     * @returns {WizardWindowConfig}
     */
    this.setupPages = function ($scope, inputJson) {
        var guideTexts = $.isArray(inputJson.guideTexts) ? inputJson.guideTexts : null;
        var pages = Common.isObjLiteral(inputJson.pages) ? inputJson.pages : null;
        if (guideTexts && pages) return new WizardWindowConfig($scope, guideTexts, pages, this);
    };

    function WizardWindowConfig($scope, guideTexts, pages, service) {

        this.$scope = $scope;

        this.pages = pages;
        this.currentPage = '';
        this.guideTexts = guideTexts;

        this.pages_Example_Format = angular.copy(service.samplePageInfo);
    }

    WizardWindowConfig.prototype.getPageInfoByKw = function (pageKw) {
        return this.pages[pageKw];
    };

    WizardWindowConfig.prototype.getCurrentPageInfo = function () {
        return this.getPageInfoByKw(this.currentPage);
    };

    WizardWindowConfig.prototype.getFirstPageInfoByGuideTextIndex = function ($index) {
        var guideText = this.guideTexts[$index];
        if (!guideText) return;
        for (var pageKw in this.pages) {
            var pageInfo = this.pages[pageKw];
            if (pageInfo.guideTextIndex == $index)
                return pageKw;
        }
    };

    WizardWindowConfig.prototype.initPage = function (pageKw, successCb, failedCb) {
        var targPageInfo = this.getPageInfoByKw(pageKw);
        if (!targPageInfo) return successCb && successCb();
        var methodName = targPageInfo.initScopeMethod;
        if (!methodName || !this.$scope[methodName]) return successCb && successCb();
        this.$scope[methodName](function () {
            this.currentPage = pageKw;
            successCb && $timeout(successCb);
        }.bind(this), function () {
            failedCb && failedCb('next_page_aborted');
        });
    };

    WizardWindowConfig.prototype.startOver = function () {
        var pageKw = this.getFirstPageInfoByGuideTextIndex(0);
        this.initPage(pageKw);
    };

    WizardWindowConfig.prototype.goToNextPage = function (successCb, failedCb, dontInit) {
        var currPageInfo = this.getCurrentPageInfo();
        var nextPageInfo = this.getPageInfoByKw(currPageInfo.nextPageKw);
        if (!nextPageInfo) return failedCb && failedCb('next_page_kw_404');
        if (!dontInit) this.initPage(currPageInfo.nextPageKw, successCb, failedCb);
        else return {keyword: currPageInfo.nextPageKw, pageInfo: nextPageInfo};
    };

    WizardWindowConfig.prototype.goToNextPageWithoutInit = function (successCb, failedCb) {
        var nextPageInfo = this.goToNextPage(successCb, failedCb, true);
        if (!nextPageInfo.keyword) return nextPageInfo;
        this.currentPage = nextPageInfo.keyword;
    };

    WizardWindowConfig.prototype.goToPrevPageByKw = function (pageKw) {
        var prevPageInfo = this.getPageInfoByKw(pageKw);
        if (!prevPageInfo) return;
        this.currentPage = pageKw;
    };

    WizardWindowConfig.prototype.goToPrevPage = function () {
        var currPageInfo = this.getCurrentPageInfo();
        this.goToPrevPageByKw(currPageInfo.prevPageKw);
    };

    WizardWindowConfig.prototype.guideClick = function ($index) {
        var pageKw = this.getFirstPageInfoByGuideTextIndex($index);
        var currPageInfo = this.getCurrentPageInfo();
        if (!pageKw || currPageInfo.guideTextIndex <= $index) return;
        this.goToPrevPageByKw(pageKw);
    };

}]);