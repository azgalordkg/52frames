appSettings.app.factory('DataRelatedToCurrentUrlStorage', ['$location', function ($location) {

    var config = {

        defaultRowSetup: {
            default: {},
            withKeys: {}
        }

    };

    var combiStorage = {

        urlsList: {

            // actual url here (as the Key): then a copy of defaultRowSetup (onInit) stays here as the value
        }

    };

    var helpers = {

        getCurrentUrl: function () {
            return $location.$$absUrl;
        },

        getOrInitUrlRow: function (create) {
            var currUrl = helpers.getCurrentUrl();
            var row = combiStorage.urlsList[currUrl];
            var templ = config.defaultRowSetup;
            if (!row && create) row = combiStorage.urlsList[currUrl] = angular.copy(templ);
            return row;
        }
    };

    return {

        get: function (key) {
            var row = helpers.getOrInitUrlRow();
            if (!row) return null;
            var data = key ? row.withKeys[key] : row.default;
            return angular.copy(data);
        },

        getAllByKeys: function () {
            var row = helpers.getOrInitUrlRow() || {};
            return angular.copy(row.withKeys || {});
        },

        saveOrUpdate: function (data, key) {
            var row = helpers.getOrInitUrlRow(true);
            if (key) row.withKeys[key] = data;
            else row.default = data;
            return angular.copy(data);
        }

    };

}]);