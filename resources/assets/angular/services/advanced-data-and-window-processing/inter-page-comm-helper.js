appSettings.app.factory('InterPageHelper', [function () {

    var config = {
        numTransitsDataValidity: 1
    };

    var helpers = {

        resetVariables: function () {
            nextPageData = {};
            nextPageDataPristine = true;
            transitionsPassed = 0;
        }

    };

    var transitionsPassed, nextPageData, nextPageDataPristine;
    helpers.resetVariables();

    var methods = {

        checks: {

            isVarsResetRequired: function (autoReset) {
                var transitsExceeded = transitionsPassed > config.numTransitsDataValidity;
                if (transitsExceeded && autoReset) helpers.resetVariables();
                return transitsExceeded;
            }

        },

        forNextPage: {

            setIntendedModel: function (model) {
                helpers.resetVariables();
                nextPageData = model;
                nextPageDataPristine = false;
                return true;
            },

            resolveIntendedModelThenSave: function (modelResolverCb) {
                if (typeof(modelResolverCb) !== 'function') return;
                var model = modelResolverCb();
                if (!model) return;
                return methods.forNextPage.setIntendedModel(model);
            }

        },

        transitionMethods: {

            doResetIfNeeded: function () {
                if (!nextPageDataPristine) transitionsPassed++;
                return methods.checks.isVarsResetRequired(true);
            }

        },

        currentPage: {

            getIntendedModel: function () {
                if (nextPageDataPristine) return null;
                var model = nextPageData;
                helpers.resetVariables();
                return model;
            }

        }

    };

    return methods;

}]);