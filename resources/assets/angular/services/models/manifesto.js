(function () {

    function Manifesto($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'manifesto';

        this.checkNickname = function (input, config) {
            var _this = this;
            var cleaned = this.cleanInput(input, this.endpoint + '/check/nickname');
            return this.$http({
                timeout: config && config.timeout,
                params: cleaned.params,
                url: cleaned.endpoint,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return _this.$q.reject(response.data);
            });
        }

    }

    Manifesto.prototype = new appSettings.BaseApi();
    appSettings.app.service('Manifesto', ['$q', '$http', Manifesto]);

})();