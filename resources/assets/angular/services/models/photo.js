(function () {

    function Photo($q, $http, Upload, PhotosAggregate) {

        this.$q = $q;
        this.$http = $http;
        this.parent = this.__proto__;
        this.endpoint = appSettings.apiUrl + 'photo';

        this.get = function (input, config) {
            return this.parent.get.call(this, input, config).then(function (response) {
                if (response.per_page) PhotosAggregate.saveOrUpdatePhotoArray(response.data);
                else PhotosAggregate.saveOrUpdate(response);
                return response;
            }, function (response) {
                return $q.reject(response);
            });
        };

        this.save = function (input) {
            return Upload.upload({
                url: this.endpoint,
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.update = function (input) {
            input._method = 'patch';
            return Upload.upload({
                url: this.endpoint + '/' + (input.id || ''),
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.suggestFilename = function (input) {
            var cleaned = this.cleanInput(input, this.endpoint);
            return this.$http({
                url: cleaned.endpoint + '/suggest_filename',
                params: cleaned.params,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.heartPhoto = function (photo) {
            var cleaned = this.cleanInput(photo, this.endpoint);
            return this.$http({
                url: cleaned.endpoint + '/love',
                data: cleaned.params,
                method: 'POST'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.unheartPhoto = function (photo) {
            var cleaned = this.cleanInput(photo, this.endpoint);
            cleaned.params._method = 'delete';
            return this.$http({
                url: cleaned.endpoint + '/love',
                data: cleaned.params,
                method: 'POST'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

    }

    Photo.prototype = new appSettings.BaseApi();
    appSettings.app.service('Photo', ['$q', '$http', 'Upload', 'PhotosAggregate', Photo]);

})();