(function () {

    function Following($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'following';

    }

    Following.prototype = new appSettings.BaseApi();
    appSettings.app.service('Following', ['$q', '$http', Following]);

})();