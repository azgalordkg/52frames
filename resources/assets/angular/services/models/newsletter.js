(function () {

    function Newsletter($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'newsletter';

        this.subscribe = this.save;

    }

    Newsletter.prototype = new appSettings.BaseApi();
    appSettings.app.service('Newsletter', ['$q', '$http', Newsletter]);

})();