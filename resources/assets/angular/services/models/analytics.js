(function () {

    function Analytics($q, $http, $location) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'analytics';

        var _this = this, saveToApi = function (term, trackedId, extra_json, config, extraUrl) {
            term = term || '';
            extraUrl = extraUrl || '';
            extraUrl = extraUrl + term;
            var params = {tracked_item_id: trackedId, relative_page_path: $location.path()};
            var jsonCount = Object.keys(extra_json || {}).length;
            if (jsonCount) params.extra_json = extra_json;
            return _this.crudMethodWithExtraUrl('save', _this, params, config, extraUrl);
        };

        this.newEntry = function (term, trackedId, extra_json, config) {
            return saveToApi(term, trackedId, extra_json, config, '/entry/');
        };

        this.errorLoadingAsset = function (term, trackedId, extra_json, config) {
            return saveToApi(term, trackedId, extra_json, config, '/asset/didnt_load/');
        };

    }

    Analytics.prototype = new appSettings.BaseApi();
    appSettings.app.service('Analytics', ['$q', '$http', '$location', Analytics]);

})();