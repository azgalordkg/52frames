(function () {

    function Photographer($q, $http, PhotosAggregate) {

        this.$q = $q;
        this.$http = $http;
        this.parent = this.__proto__;
        this.endpoint = appSettings.apiUrl + 'photographer';

        this.get = function (input, config) {
            return this.parent.get.call(this, input, config).then(function (response) {
                if (response.photosData) PhotosAggregate.saveOrUpdatePhotoArray(response.photosData.data);
                return response;
            }, function (response) {
                return $q.reject(response);
            });
        };

        this.getPhotos = function (input) {
            var cleaned = this.cleanInput(input, this.endpoint);
            return this.$http({
                url: cleaned.endpoint + '/photos',
                params: cleaned.params,
                method: 'GET'
            }).then(function (response) {
                console.log('response from getPhotos: ', response);
                PhotosAggregate.saveOrUpdatePhotoArray(response);
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        }

    }

    Photographer.prototype = new appSettings.BaseApi();
    appSettings.app.service('Photographer', ['$q', '$http', 'PhotosAggregate', Photographer]);

})();