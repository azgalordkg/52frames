(function () {

    function Country($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'country';

    }

    Country.prototype = new appSettings.BaseApi();
    appSettings.app.service('Country', ['$q', '$http', Country]);

})();