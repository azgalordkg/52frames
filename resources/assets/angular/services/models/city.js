(function () {

    function City($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'city';

    }

    City.prototype = new appSettings.BaseApi();
    appSettings.app.service('City', ['$q', '$http', City]);

})();