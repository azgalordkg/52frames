(function () {

    function State($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'state';

    }

    State.prototype = new appSettings.BaseApi();
    appSettings.app.service('State', ['$q', '$http', State]);

})();