(function () {

    function Role($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'role';

    }

    Role.prototype = new appSettings.BaseApi();
    appSettings.app.service('Role', ['$q', '$http', Role]);

})();