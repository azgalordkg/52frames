(function () {

    function UserRole($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'user-role';

    }

    UserRole.prototype = new appSettings.BaseApi();
    appSettings.app.service('UserRole', ['$q', '$http', UserRole]);

})();