(function () {

    function User($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'user';

        this.suggest = function (input, config, extraPath) {
            extraPath = extraPath || '';
            var _this = this;
            var cleaned = this.cleanInput(input, this.endpoint + '/suggest' + extraPath);
            return this.$http({
                timeout: config && config.timeout,
                params: cleaned.params,
                url: cleaned.endpoint,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return _this.$q.reject(response.data);
            });
        };

        this.suggestPhotographer = function (input, config) {
            return this.suggest(input, config, '/photographer');
        };

    }

    User.prototype = new appSettings.BaseApi();
    appSettings.app.service('User', ['$q', '$http', User]);

})();