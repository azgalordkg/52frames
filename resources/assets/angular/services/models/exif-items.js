(function () {

    function ExifItem($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'exif-item';

    }

    ExifItem.prototype = new appSettings.BaseApi();
    appSettings.app.service('ExifItem', ['$q', '$http', ExifItem]);

})();