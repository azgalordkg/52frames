(function () {

    function Comment($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'comment';

    }

    Comment.prototype = new appSettings.BaseApi();
    appSettings.app.service('Comment', ['$q', '$http', Comment]);

})();