(function () {

    function BypassUpload($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'bypass-upload';

    }

    BypassUpload.prototype = new appSettings.BaseApi();
    appSettings.app.service('BypassUpload', ['$q', '$http', BypassUpload]);

})();