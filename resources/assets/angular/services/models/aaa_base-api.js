appSettings.BaseApi = function () {

    this.parent = this;

    this.cleanInput = function (input, endpoint, config) {
        input = input || {};
        config = config || {};
        var extraUrl = config.extraUrl || '';
        endpoint = endpoint + extraUrl;
        var params = angular.copy(input);
        if (params.id) {
            endpoint += '/' + params.id;
            delete params.id;
        }
        return {endpoint: endpoint, params: params};
    };

    this.crudMethodWithExtraUrl = function (crudTerm, thisRef, input, config, extraUrl) {
        var _this = this;
        config = config || {};
        thisRef = thisRef || _this;
        if (extraUrl) config.extraUrl = extraUrl;
        return _this[crudTerm].call(thisRef, input, config).then(function (response) {
            return response;
        }, function (response) {
            return _this.$q.reject(response);
        });//asd
    };

    this.get = function (input, config) {
        var _this = this;
        var cleaned = this.cleanInput(input, this.endpoint, config);
        return this.$http({
            timeout: config && config.timeout,
            params: cleaned.params,
            url: cleaned.endpoint,
            method: 'GET'
        }).then(function (response) {
            return response.data;
        }, function (response) {
            return _this.$q.reject(response.data);
        });
    };

    this.save = function (input, config) {
        var _this = this;
        var cleaned = this.cleanInput(input, this.endpoint, config);
        return this.$http({
            timeout: config && config.timeout,
            url: cleaned.endpoint,
            data: cleaned.params,
            method: 'POST'
        }).then(function (response) {
            return response.data;
        }, function (response) {
            return _this.$q.reject(response.data);
        });
    };

    this.validate = function (input, config) {
        var clonedInput = angular.copy(input);
        clonedInput.dont_save = 1;
        return this.save(clonedInput, config);
    };

    this.update = function (input, config) {
        var _this = this;
        var cleaned = this.cleanInput(input, this.endpoint, config);
        cleaned.params._method = 'patch';
        return this.$http({
            timeout: config && config.timeout,
            url: cleaned.endpoint,
            data: cleaned.params,
            method: 'POST'
        }).then(function (response) {
            return response.data;
        }, function (response) {
            return _this.$q.reject(response.data);
        });
    };

    this.delete = function (input, config) {
        var _this = this;
        var cleaned = this.cleanInput(input, this.endpoint, config);
        cleaned.params._method = 'delete';
        return this.$http({
            timeout: config && config.timeout,
            url: cleaned.endpoint,
            data: cleaned.params,
            method: 'POST'
        }).then(function (response) {
            return response.data;
        }, function (response) {
            return _this.$q.reject(response.data);
        });
    };

};