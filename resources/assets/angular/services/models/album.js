(function () {

    function Album($q, $http, Upload, PhotosAggregate) {

        this.$q = $q;
        this.$http = $http;
        this.parent = this.__proto__;
        this.endpoint = appSettings.apiUrl + 'album';

        this.get = function (input, config) {
            return this.parent.get.call(this, input, config).then(function (response) {
                if (response.photosData) PhotosAggregate.saveOrUpdatePhotoArray(response.photosData.data);
                return response;
            }, function (response) {
                return $q.reject(response);
            });
        };

        this.recentAlbums = function (input) {
            var cleaned = this.cleanInput(input || {}, this.endpoint + '/recent');
            return this.$http({
                url: cleaned.endpoint,
                params: cleaned.params,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.future = function (input) {
            var cleaned = this.cleanInput(input || {}, this.endpoint + '/future');
            return this.$http({
                url: cleaned.endpoint,
                params: cleaned.params,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.checkShorturl = function (input) {
            var cleaned = this.cleanInput(input, this.endpoint + '/check/shorturl');
            return this.$http({
                url: cleaned.endpoint,
                params: cleaned.params,
                method: 'GET'
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.makeLive = function (input) {
            return Upload.upload({
                url: this.endpoint + '/' + input.id + '/make_live',
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.save = function (input) {
            return Upload.upload({
                url: this.endpoint,
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

        this.update = function (input) {
            var albumId = input.id;
            input._method = 'patch';
            return Upload.upload({
                url: this.endpoint + '/' + albumId,
                data: input
            }).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        };

    }

    Album.prototype = new appSettings.BaseApi();
    appSettings.app.service('Album', ['$q', '$http', 'Upload', 'PhotosAggregate', Album]);

})();