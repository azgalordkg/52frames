(function () {

    function UserSettings($q, $http) {

        this.$q = $q;
        this.$http = $http;
        this.endpoint = appSettings.apiUrl + 'settings';

    }

    UserSettings.prototype = new appSettings.BaseApi();
    appSettings.app.service('UserSettings', ['$q', '$http', UserSettings]);

})();