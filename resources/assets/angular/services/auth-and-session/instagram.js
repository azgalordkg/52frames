appSettings.app.factory('InstagramAuth', ['$http', '$q', 'Auth', function ($http, $q, Auth) {

    if (!appSettings.apiUrl) return {};

    return {

        apiOptions: {scope: 'email'},

        login: function (userInput) {
            return Auth.login(userInput, '/instagram');
        },

        linkAccount: function (userInput) {
            return Auth.login(userInput, '/instagram/link');
        },

        addPassword: function (userInput) {
            return Auth.login(userInput, '/instagram/password');
        },

        registerUser: function (userInput) {
            return Auth.registerUser(userInput, '/instagram');
        }

    };

}]);