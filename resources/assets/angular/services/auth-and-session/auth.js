appSettings.app.factory('Auth', ['$rootScope', '$http', '$q', 'Token', function ($rootScope, $http, $q, Token) {

    if (!appSettings.apiUrl) return {};
    var apiUrl = appSettings.apiUrl;

    var methods = {

        login: function (userInput, extraPath) {
            extraPath = extraPath || '';
            return $http.post(apiUrl + 'login' + extraPath, userInput).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        forgotPassword: function (userInput) {
            return $http.post(apiUrl + 'password/email', userInput).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        resetPassword: function (userInput) {
            return $http.post(apiUrl + 'password/reset', userInput).then(function (response) {
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        registerUser: function (userInput, extraPath) {
            extraPath = extraPath || '';
            return $http.post(apiUrl + 'signup' + extraPath, userInput).then(function (response) {
                if (response.data.access_token) Token.update(response.data);
				console.log("TCL: response.data", response.data)
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        validate: function (userInput, extraPath) {
            var clonedUserInput = angular.copy(userInput);
            if (clonedUserInput.dont_save === undefined) {
                clonedUserInput.dont_save = 1;
            }
            return this.registerUser(clonedUserInput, extraPath);
        },

        me: function () {
            return $http.get(apiUrl + 'user').then(function (response) {
                $rootScope.user = response.data;
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        logout: function () {
            return $http.post(apiUrl + 'logout', {
                _method: 'delete'
            }).then(function (response) {
                methods.logoutSuccess();
                return response.data;
            }, function (response) {
                return $q.reject(response.data);
            });
        },

        logoutSuccess: function () {
            Token.destroy();
            delete $rootScope.user;
        }

    };

    return methods;

}]);