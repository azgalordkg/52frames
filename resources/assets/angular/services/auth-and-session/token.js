appSettings.app.factory('Token', ['$rootScope', function ($rootScope) {

    var tokenBase = appSettings.canonicalUrl + 'oauth :: ';
    var tokenExpiryName = tokenBase + 'tokenExpiry';
    var tokenName = tokenBase + 'token';

    var timeoutHolder;

    return {
        sessionExpired: function () {
            console.log('session expired!');
            if (timeoutHolder) clearTimeout(timeoutHolder);
            $rootScope.$broadcast('session:expired');
            delete $rootScope.user;
        },
        checkIfValid: function () {
            var _this = this;
            var isValid = false;
            var token = window.localStorage && localStorage.getItem(tokenName);
            var expiry = window.localStorage && localStorage.getItem(tokenExpiryName);
            if (expiry) {
                var withinValidity = expiry && (new Date()).getTime() < expiry;
                isValid = !!token && withinValidity;
            }
            else isValid = !!token;
            if (isValid) {
                if (timeoutHolder) clearTimeout(timeoutHolder);
                var remainingTime = expiry - (new Date()).getTime();
                if (remainingTime <= 0) {
                    this.destroy(true);
                }
                else timeoutHolder = setTimeout(function () {
                    _this.destroy(true);
                }, remainingTime);
            } else {
                this.destroy(!!token);
            }
            return isValid;
        },
        update: function (response) {
            this.destroy();
            window.localStorage && localStorage.setItem(tokenName, response.access_token);
            if (response.expires_in) {
                var currentTime = (new Date()).getTime();
                var tokenDuration = response.expires_in * 1000;
                window.localStorage && localStorage.setItem(tokenExpiryName, currentTime + tokenDuration);
                this.checkIfValid();
            }
        },
        get: function () {
            this.checkIfValid();
            return window.localStorage && localStorage.getItem(tokenName);
        },
        destroy: function (broadcastExpiry) {
            broadcastExpiry = broadcastExpiry || false;
            window.localStorage && localStorage.removeItem(tokenName);
            window.localStorage && localStorage.removeItem(tokenExpiryName);
            if (broadcastExpiry) this.sessionExpired();
        }
    };

}]);