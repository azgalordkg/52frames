appSettings.app.factory('FacebookAuth', ['$http', '$q', 'Auth', function ($http, $q, Auth) {

    if (!appSettings.apiUrl) return {};

    return {

        apiOptions: {scope: 'email'},

        login: function (userInput) {
            return Auth.login(userInput, '/facebook');
        },

        linkAccount: function (userInput) {
            return Auth.login(userInput, '/facebook/link');
        },

        addPassword: function (userInput) {
            return Auth.login(userInput, '/facebook/password');
        },

        registerUser: function (userInput) {
            return Auth.registerUser(userInput, '/facebook');
        }

    };

}]);