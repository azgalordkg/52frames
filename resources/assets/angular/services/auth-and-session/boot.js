appSettings.app.factory('Boot', ['Token', '$rootScope', 'BootHelper', function (Token, $rootScope, BootHelper) {

    return {

        copyAlbumCurrentWeekFromLoginToRootScope: BootHelper.copyAlbumCurrentWeekFromLoginToRootScope,

        backendCheck: function () {
            if (Token.checkIfValid()) {
                BootHelper.checkUserLogin(function (success, response) {
                    if (success) BootHelper.copyAlbumCurrentWeekFromLoginToRootScope(response);
                    else BootHelper.userNotValidGetAlbum();
                });
            } else BootHelper.userNotValidGetAlbum();
        },

        removeFlags: function () {
            delete $rootScope.albums_recent;
            $rootScope.backendChecked = false;
        }

    };

}]);