appSettings.app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(['$q', 'Token', function ($q, Token) {
        return {
            request: function (config) {
                config.headers['Accept'] = 'application/json';
                var apiPattern = new RegExp(appSettings.apiUrl);
                var isApi = apiPattern.test(config.url);
                var token = Token.get();
                if (isApi && token) config.headers['Authorization'] = 'Bearer ' + token;
                return config;
            },
            response: function (response) {
                var authServerPattern = new RegExp(appSettings.apiUrl + 'login');
                var isAuthServer = authServerPattern.test(response.config.url);
                if (isAuthServer && response.data.access_token) Token.update(response.data);
                return response;
            },
            responseError: function (rejection) {
                if (rejection.data && rejection.data.error) {
                    var broadcast = rejection.data.error !== 'invalid_credentials';
                    Token.destroy(broadcast);
                }
                return $q.reject(rejection);
            }
        };
    }]);
}]);