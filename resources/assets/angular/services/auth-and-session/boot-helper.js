appSettings.app.factory('BootHelper', ['Auth', 'Album', '$rootScope', function (Auth, Album, $rootScope) {

    var broadcastBackendChecked = function (success, response) {
        $rootScope.$broadcast('backendChecked', success, response);
    };

    var methods = {

        checkUserLogin: function (callback) {
            Auth.me().then(function (response) {
                callback(true, response);
            }, function (response) {
                callback(false, response);
            });
        },

        getAlbumCurrentWeek: function (callback) {
            Album.recentAlbums().then(function (response) {
                callback(true, response);
            }, function (response) {
                callback(false, response);
            });
        },

        copyAlbumCurrentWeekFromLoginToRootScope: function (response) {
            $rootScope.albums_recent = response.albums_recent;
            delete response.albums_recent;
            $rootScope.backendChecked = true;
            broadcastBackendChecked(true, response);
        },

        userNotValidGetAlbum: function () {
            methods.getAlbumCurrentWeek(function (success, response) {
                if (!success) return broadcastBackendChecked(false, response);
                delete response.success;
                methods.copyAlbumCurrentWeekFromLoginToRootScope({
                    albums_recent: response
                });
            });
        }

    };

    return methods;

}]);