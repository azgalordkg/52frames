(function () {

    appSettings.app.service('CustomizeErrorMessages', ['$q', function ($q) {

        var service = null;

        var inputFixers = {

            serviceName: function () {

            }

        };

        var helpers = {

            parseMessage: function (error) {
                return [
                    service.name,
                    error.group ? error.group + ' ' : '',
                    error.method ? '::' + error.method + '()' : '',
                    error.message ? ' -- ' + error.message : ''
                ].join('');
            }

        };

        function CustomError(error) {
            if (!error) return;
            error = error || {message: error};
            var message = helpers.parseMessage(error);
            console.log('message: ', message);
            return message;
        }

        return {

            setClass: function (callerObj, serviceName) {
                return;
                service = callerObj || {};
                return CustomError;
            }

        };

    }]);

})();