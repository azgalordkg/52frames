appSettings.app.factory('MethodsHelper', ['Common', function (Common) {

    var errorsList = {

        onInitOnTempPage: {
            noInstance: 'please run setup() before subscribing to this event'
        },

        onInitOnRealPage: {
            noInstance: 'please run setup() before subscribing to this event'
        }

    };

    var features = {

        updateErrorsList: function (methodName) {
            return errorsList[methodName] || {};
        },

        addMessageHeader: function(methodName, message){
            return methodName + ' -- ' + message;
        },

        updateMethodIssues: function (methodName, errorsList) {
            var issues = {};
            for (var errorCode in errorsList) {
                var message = errorsList[errorCode];
                issues[errorCode] = features.addMessageHeader(methodName, message);
            }
            return issues;
        },

        monitorHandler: function (methodName, handler) {
            return function () {
                var args = Common.makeArray(arguments);
                var errorsList = features.updateErrorsList(methodName);
                this.methodIssues = features.updateMethodIssues(methodName, errorsList);
                return handler.apply(this, args);
            };
        }

    };

    return {

        addNew: function (instance, methodName, handler) {
            instance[methodName] = features.monitorHandler(methodName, handler);
        }

    };

}]);