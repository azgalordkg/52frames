(function () {

    var serviceName = 'PhotoViewNextPrevHelper';

    appSettings.app.service(serviceName, [
        '$q', 'PromiseRequest', '$stateParams', 'Common', 'CustomizeErrorMessages', 'MethodsHelper',
        function ($q, PromiseRequest, $stateParams, Common, CustomizeErrorMessages, MethodsHelper) {

            CustomizeErrorMessages.setClass(this, serviceName);

            var process = {

                init: function (initPhotoRequestCallback) {


                    // note:temporarily postponed for next version
                    // executing the callback right away, for now

                    return initPhotoRequestCallback();


                    // todo: finish this on v1.1


                    var parentResourceUrl;
                    var currentResourceUrlPattern;

                    if ($scope.isProfilePage) {
                        parentResourceUrl = Helpers.formProfileThemeUrl($stateParams.userIdOrShorturl);
                        currentResourceUrlPattern = '/photo/' + $stateParams.pathYear + '/';
                    } else {
                        parentResourceUrl = Helpers.formWeekThemeUrl($stateParams.pathYear, $stateParams.pathWeekTheme);
                        currentResourceUrlPattern = '/photo/';
                    }

                    console.log('parentResourceUrl: ', parentResourceUrl);
                    console.log('currentResourceUrlPattern: ', currentResourceUrlPattern);

                    PromiseRequest.await({
                        parentResourceUrl: parentResourceUrl,
                        currentResourceUrlPattern: currentResourceUrlPattern,
                        urlTempIdKeyword: '_photo_loading_',
                        backendCallback: initPhotoRequestCallback,
                        referenceModel: {
                            instance: {},
                            cbToGetId: function (instance) {
                                console.log('callback on the controller side: ', instance);
                            },
                            numPagesAwayWithAxis: 1
                        },
                        scope: $scope
                    }).then(function (response) {

                    });

                    /*

            $scope.init
            - if no expected #album #queue for this #promise, go to 404


            on close window:
            reset queue list


            on press Next
            save to flash: direction taken


            make way for: is claiming process --re-claim the data, not go to the backend again
            try to access if the photoResponse is now in the cache

                    */

                }

            };

            var instance = null;

            var eventListeners = {
                onInitOnTempPage: null,
                onInitOnRealPage: null
            };

            MethodsHelper.addNew(this, 'setup', function (initPhotoRequestCallback) {
                instance = this;
                var promiseRequestInstance = process.init(initPhotoRequestCallback);
                var usernameOrId = $stateParams.userIdOrShorturl || $stateParams.pathUser;
                var isTempPage = PromiseRequest.isTempPage(usernameOrId);
                var methodName = isTempPage ? 'onInitOnTempPage' : 'onInitOnRealPage';
                var callback = eventListeners[methodName];
                Common.execCbWithThisBinding(callback);
                return this;
            });

            MethodsHelper.addNew(this, 'onInitOnTempPage', function (callback) {
                if (!instance) throw new Error(this.methodIssues.noInstance);
                eventListeners[this.methodName] = Common.makeFunction(callback);
                return this;
            });

            MethodsHelper.addNew(this, 'onInitOnRealPage', function (callback) {
                if (!instance) throw new Error(this.methodIssues.noInstance);
                eventListeners[this.methodName] = Common.makeFunction(callback);
                return this;
            });

        }
    ]);

})();