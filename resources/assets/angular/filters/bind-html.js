window.appSettings.app.filter('showAsHtml', ['$sce', function ($sce) {
    return $sce.trustAsHtml;
}]);