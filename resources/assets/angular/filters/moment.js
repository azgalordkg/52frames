window.appSettings.app.filter('moment', function () {
    if (!window.moment) angular.element('head').append('<scr' + 'ipt src="/vendor/momentjs/2.10.6/moment.min.js"></scr' + 'ipt>');
    var filter = function (input, momentFn /*, param1, param2, ...param n */) {
        var args = Array.prototype.slice.call(arguments, 2), momentObj = moment(input);
        return momentObj[momentFn].apply(momentObj, args);
    };
    filter.$stateful = true;
    return filter;
});