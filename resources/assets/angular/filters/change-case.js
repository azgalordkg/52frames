window.appSettings.app.filter('changeCase', function () {
    var filter = function (text, reqCase) {
        switch (reqCase) {
            case 'titleCase':
            default:
                text = $.map(text.split('_'), function (word) {
                    return word.charAt(0).toUpperCase() + word.slice(1);
                }).join(' ');
                break;
        }
        return text;
    };
    filter.$stateful = true;
    return filter;
});