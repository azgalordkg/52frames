window.appSettings.app.filter('ellipsesAfter', function () {

    var attachEllipses = function (value, tailIsLink, tail, callback) {
        var div = document.createElement('div');
        div.innerHTML = value;
        value = div.innerHTML;
        value = value.replace(/<\/p>$/i, '');
        if (tailIsLink) {
            var id = Math.random() + '';
            id = id.replace('.', '');
            value += '… <a href="#" class="ellipses-more" id="' + id + '">' + tail + '</a>';
            if (callback) callback(true, id);
        } else value += tail || '…';
        return value;
    };

    var checkNumBreaks = function (value, maxNumOfBreaks) {
        if (maxNumOfBreaks) {
            var symbol = '<br>';
            var valueArr = value.split(symbol);
            if (valueArr.length > maxNumOfBreaks) {
                valueArr.length = maxNumOfBreaks;
                value = valueArr.join(symbol);
                return {
                    exceedsLimit: true,
                    value: value
                };
            }
        }
        return {
            exceedsLimit: false,
            value: value
        };
    };

    return function (value, max, wordwise, tailIsLink, tail, tailCb, maxNumOfBreaks) {
        if (!value) return '';

        var callback = tailCb && angular.isFunction(tailCb) ? tailCb : null;

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) {
            response = checkNumBreaks(value, maxNumOfBreaks);
            if (tailIsLink && callback) {
                if (response.exceedsLimit) {
                    value = attachEllipses(response.value, tailIsLink, tail, callback);
                } else callback(false);
            }
            return value;
        }

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace;
            lastspace = value.lastIndexOf('<');
            if (lastspace !== -1) value = value.substr(0, lastspace);
            lastspace = value.lastIndexOf(' ');
            if (lastspace !== -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }

        response = checkNumBreaks(value, maxNumOfBreaks);
        value = attachEllipses(response.value, tailIsLink, tail, callback);

        return value;
    };
});