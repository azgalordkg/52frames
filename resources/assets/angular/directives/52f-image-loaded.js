appSettings.app.directive('52fImageLoaded', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var fn = $parse(attrs['52fImageLoaded']);
            if (!fn) return;
            element.bind('load', function ($event) {
                fn(scope, {$event: $event, $element: element});
            });
        }
    };
}]);
appSettings.app.directive('52fImageLoadingError', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var fn = $parse(attrs['52fImageLoadingError']);
            if (!fn) return;
            element.bind('error', function ($event) {
                fn(scope, { $event: $event, $element: element});
            });
        }
    };
}]);
