appSettings.app.directive('clearFloats', function () {
    return {
        restrict: 'E',
        transclude: true,
        template: '<div ng-transclude></div>'
    };
});