appSettings.app.directive('52fOnChange', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        compile: function (element, attrs) {
            var fn = $parse(attrs['52fOnChange']);
            return function ($scope, element) {
                element.on('change', function () {
                    fn($scope, {$element: element});
                });
                element.on('$destroy', function () {
                    element.off();
                });
            };
        }
    };
}]);