appSettings.app.directive('52fSetUnsetAttr', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        compile: function (element, attrs) {
            var fn = $parse(attrs['52fSetUnsetAttr']); //TODO: check if needed
            return function ($scope, element) {
                $scope.$watch(attrs['52fSetUnsetAttr'], function () {
                    var attrsAndValueRefs = $parse(attrs['52fSetUnsetAttr'])($scope) || {};
                    Object
                        .keys(attrsAndValueRefs)
                        .filter(isAttrValid)
                        .forEach(function (attr) {
                            var value = attrsAndValueRefs[attr];
                            if (value) element.attr(attr, value);
                            else element.removeAttr(attr);
                        });
                    function isAttrValid(attr) {
                        return typeof attrsAndValueRefs[attr] === 'string' &&
                            attrsAndValueRefs[attr] !== element.attr(attr);
                    }
                }, true);
            };
        }
    };
}]);