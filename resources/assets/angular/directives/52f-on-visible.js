appSettings.app.directive('52fOnVisible', ['$parse', '$timeout', function ($parse, $timeout) {
    return {
        restrict: 'A',
        compile: function (element, attrs) {
            var fn = $parse(attrs['52fOnVisible']);
            return function ($scope, element) {
                $scope.$watch(function () {
                    return element.is(':visible');
                }, function () {
                    $timeout(function () {
                        if (!element.is(':visible')) return;
                        fn($scope, {$element: element});
                    });
                }, true);
            };
        }
    };
}]);