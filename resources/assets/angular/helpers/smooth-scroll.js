appSettings.app.service('SmoothScroll', function () {

    var pageOnLoadFired = false;

    if (document.readyState === 'complete') {
        pageOnLoadFired = true;
    } else $(window).on('load', function () {
        pageOnLoadFired = true;
    });

    function startScroll($element, callback, $options) {
        callback = callback || $.noop;
        var scrollBase = $options && $options.scrollBase ? $options.scrollBase : $('html');
        var $calcReference = $options && $options.scrollCalcRef ? $options.scrollCalcRef : scrollBase;
        var minTopMargin = $options && $options.minTopMargin ? $options.minTopMargin : 0;
        var scrollTop = -(($calcReference.offset().top - $element.offset().top) - (minTopMargin));
        scrollBase.stop().animate({scrollTop: scrollTop}, 'slow', callback);
    }

    function waitElemVisible($element, callback) {
        if ($element.is(':visible')) callback($element);
        else var interval = setInterval(function () {
            if (!$element.is(':visible')) return;
            clearInterval(interval);
            callback($element);
        }, 10);
    }

    function scrollToIfAvail($element, waitTilVisible, callback, options) {
        if (!$element || !$element.length) return;
        if (!waitTilVisible) startScroll($element, callback, options);
        else waitElemVisible($element, function () {
            startScroll($element, callback, options);
        });
    }

    $(window).on('hashchange', function (event) {
        var hashTag = location.hash ? location.hash.replace(/^#/, '') : '';
        var element = hashTag ? $('a[smooth-scroll-name=' + hashTag + ']')[0] : null;
        element && scrollToIfAvail($(element), true);
    });

    this.scrollToHash = function () {
        var triggerHashChange = function () {
            $(window).trigger('hashchange');
        };
        if (pageOnLoadFired) triggerHashChange();
        else $(window).on('load', triggerHashChange);
    };

    this.scrollTo = function (inputElem, callback, options) {
        scrollToIfAvail($(inputElem), true, callback, options);
    };

    this.scrollToElementWhenPageisLoaded = function (inputElem, callback) {
        var todo = function () {
            setTimeout(function () {
                scrollToIfAvail($(inputElem), true, callback);
            }, 500);
        };
        if (pageOnLoadFired) todo();
        else $(window).on('load', todo);
    };

    this.toFirstError = function (errors) {
        var elemHint = Object.keys(errors).shift();
        var selectors = [
            "form:visible [error-scroll='" + elemHint + "']",
            "form:visible label[for*='-" + elemHint + "']",
            "form:visible label[ng-click*='" + elemHint + "']",
            "form:visible input[ng-model$='" + elemHint + "']"
        ];
        var $elem = $(selectors.join(','));
        this.scrollTo($elem, $.noop, {
            scrollBase: $elem.parents('.modal'),
            scrollCalcRef: $elem.parents('form'),
            minTopMargin: 20
        });
    };

});