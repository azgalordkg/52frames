appSettings.app.factory('Common', ['$timeout', function ($timeout) {

    var methods = {

        getUrlPathname: function (href) {
            var anchor = document.createElement("a");
            anchor.href = href || '';
            var pathname = anchor.pathname;
            if (pathname.length <= 1) return pathname;
            return methods.rTrim(pathname, '/');
        },

        lTrim: function (string, delimiter) {
            delimiter = new RegExp('^' + delimiter + '+');
            return string.replace(delimiter, '');
        },

        rTrim: function (string, delimiter) {
            delimiter = new RegExp(delimiter + '+$');
            return string.replace(delimiter, '');
        },

        trim: function (string, delimiter) {
            if (!delimiter) return string.replace(/^\s+/, '').replace(/\s+$/, '');
            return methods.lTrim(methods.rTrim(string, delimiter), delimiter);
        },

        getObjType: function (input) {
            return Object.prototype.toString.call(input);
        },

        isObjLiteral: function (input) {
            return methods.getObjType(input) === '[object Object]';
        },

        isNullOrUndefined: function (value) {
            return value === undefined || value === null;
        },

        makeArray: function (input) {
            if (!input) return [];
            if (input.length && typeof input !== 'object') return [input];
            return [].map.call(input, function () {
                return arguments[0];
            });
        },

        makeFunction: function (callback) {
            if (!callback) return angular.noop;
            return angular.isFunction(callback) ? callback : function () {
                return callback;
            };
        },

        callMethod: function (object, method, args) {
            return object[method].apply(object, methods.makeArray(args));
        },

        callMethodWithTimeout: function (object, method, args, duration, onCompleteCb) {
            $timeout(function () {
                var result = methods.callMethod(object, method, args);
                onCompleteCb && onCompleteCb(result);
            }, duration);
        },

        execCbWithThisBinding: function (callback, argsArray, specifyThis) {
            return methods.bindCbThisReturnCallable(callback, argsArray)(specifyThis);
        },

        bindCbThisReturnCallable: function (callback, argsArray) {
            var args = methods.makeArray(argsArray);
            var cbFunction = methods.makeFunction(callback);
            return function (specifyThis) {
                var thisObj = specifyThis || cbFunction;
                return cbFunction.apply(thisObj, args);
            };
        }

    };

    return methods;

}]);