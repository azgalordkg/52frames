appSettings.app.factory('Helpers', ['$rootScope', '$stateParams', '$location', function ($rootScope, $stateParams, $location) {

    var helpers = {

        trimText: function (text) {
            return text.replace(/^\s+/, '').replace(/\s+$/, '');
        },

        reloadPage: function (forcedReload) {
            return window.location.reload(!!forcedReload);
        },

        getWeekShorturl: function (album) {
            return [
                'week',
                album.week_number,
                album.shorturl
            ].join('-');
        },

        getWeekThemeUrl: function (album) {
            if (!album) return;
            return [
                '',
                'albums',
                album.year,
                helpers.getWeekShorturl(album)
            ].join('/');
        },

        getChallengePageUrl: function (album) {
            if (!album) return;
            return helpers.getWeekThemeUrl(album) + '/challenge';
        },

        getPhotographerUrl: function (user) {
            if (!user) return;
            return '/photographer/' + $rootScope.$navbar.getSelfHandleOrId(user);
        },

        getPhotographerHandleOrName: function (user) {
            if (!user) return;
            return $rootScope.$navbar.getSelfHandleOrName(user, true);
        },

        isProfilePage: function () {
            return !!$stateParams.userIdOrShorturl;
        },

        gotoPath: function (path) {
            $location.path(path || '/');
        },

        formWeekThemeUrl: function (year, weekNumber, albumShortUrl, optionalUsername) {
            if (!/^week-/.test(weekNumber)) weekNumber = 'week-' + weekNumber;
            var albumPath = [
                '',
                'albums',
                year,
                weekNumber
            ];
            if (albumShortUrl) albumPath[3] += '-' + albumShortUrl;
            if (optionalUsername) {
                albumPath.push('photo');
                albumPath.push(optionalUsername);
            }
            return albumPath.join('/');
        },

        formProfileThemeUrl: function (username, optionalYear, optionalWeekNumber, optionalAlbumShortUrl) {
            var albumPath = [
                '',
                'photographer',
                username
            ];
            if (optionalYear && optionalWeekNumber && optionalAlbumShortUrl) {
                if (!/^week-/.test(optionalWeekNumber)) optionalWeekNumber = 'week-' + optionalWeekNumber;
                albumPath.push('photo');
                albumPath.push(optionalYear);
                albumPath.push(optionalWeekNumber + optionalAlbumShortUrl ? ('-' + optionalAlbumShortUrl) : '');
            }
            return albumPath.join('/');
        }

    };

    return helpers;

}]);