appSettings.app.factory('Variables', ['$rootScope', '$interval', '$timeout', function ($rootScope, $interval, $timeout) {

    var variables = {

        critiqueLevels: [
            {
                'key': '52f-cc_regular',
                'name': '52F-CC Regular'
            },
            {
                'key': 'shred_away',
                'name': 'SHRED AWAY! I feel no pain.'
            },
            {
                'key': 'extra_sensitive',
                'name': 'Please be extra sensitive with your critique.'
            },
            {
                'key': 'no_critique',
                'name': 'Not interested in a critique this week, but feel free to comment! :)'
            }
        ],

        captionLimit: {
            collapsed: 170,
            total: 800
        },

        commentLimit: {
            collapsed: 170,
            maxBreaks: 4,
            total: 10000
        },

        notSignedInFollowerHtml: "<div>\n" +
          "<p><a class='link' onclick='angular.element(this).scope().openLogin()' data-toggle='modal' data-target='#loginModal' href='javascript:;'>Sign into your account</a> in order to follow Framers!</p>\n" +
          "<p>Don't have an account? <a class='link' onclick='angular.element(this).scope().openSignUp()' data-toggle='modal' data-target='#loginModal' href='javascript:;'>Register here</a>, it only takes a few minutes!</p>\n" +
          "<p class='font-size-14'><em>(and you can signup as a &quot;fan&quot; if you don't wish to submit photos and only want to follow along!)</em></p>\n" +
          "</div>",

        notSignedInPhotoSubmitHtml: "<div>\n" +
        "<p><a class='link' onclick='popoverLogin()'>Sign into your account</a> in order to submit your Masterpiece!</p>\n" +
        "<p>Don't have an account? <a class='link' onclick='popoverSignup()'>Register here</a>, it only takes a few minutes!</p>\n" +
        "</div>",

        noManifestoPhotoSubmitHtml: "<div>\n" +
        "<p>Sign the <a class='link' onclick='signManifesto()'>Manifesto</a> to become a Framer and Submit your Masterpiece today! It only takes a few minutes!</p>\n" +
        "</div>",

        notLoggedInCommenter: "<a class='link' onclick='loginToComment()'>Login</a> or <a class='link' onclick='signupToComment()'>Signup</a> to be able to participate in the Comments!",

        hideCommentsIfAlbumNotLive: "The comments section will be available when the Album goes Live!",

        notSignedInLovePhoto: "<div>\n" +
        "<p><a class='link' onclick='popoverLogin()'>Sign into your account</a> to love a photo!</p>\n" +
        "<p>Don't have an account? <a class='link' onclick='popoverSignup()'>Register here</a>, it only takes a few minutes!</p>\n" +
        "</div>",

        deletePhotoConfirmation: "This will completely delete the Photo and all its related information.\nNote: This action cannot be undone!",

        localStorage: function (key, value) {
            if (!window.localStorage) return;
            if (!key) throw new Error('key name is required');
            if (value === null) return localStorage.removeItem(key);
            if (value) return localStorage.setItem(key, value);
            return localStorage.getItem(key);
        },

        helpers: {

            findCritiqueLevel: function (key) {
                var levels = variables.critiqueLevels;
                for (var i = 0; i < levels.length; i++) {
                    if (levels[i].key == key) return levels[i];
                }
            },

            rememberPage: function () {
                var currentPage = window.location.href;
                var passwordResetPagePattern = new RegExp('password/reset', 'i');
                var origIntendedPage = variables.localStorage('intendedPage') || '/';
                var targetPage = passwordResetPagePattern.test(currentPage) ? origIntendedPage : currentPage;
                variables.localStorage('intendedPage', targetPage);
            },

            hasIntendedPage: function () {
                return variables.localStorage('intendedPage');
            },

            removeIntendedPage: function () {
                return variables.localStorage('intendedPage', null);
            },

            preparePopover: function () {
                $(function () {
                    var loginModal = $('#loginModal');
                    var showLoginModal = function () {
                        loginModal.modal('show');
                    };
                    var clickSignupButton = function () {
                        return loginModal.find('.signup-link').click();
                    };
                    window.popoverLogin = function () {
                        variables.helpers.rememberPage();
                        showLoginModal();
                    };
                    window.popoverSignup = function () {
                        variables.helpers.rememberPage();
                        clickSignupButton();
                    };
                    window.signManifesto = function () {
                        variables.helpers.leaveManifestoMarker();
                        window.location.href = '/get-started';
                    };
                    window.loginToComment = function () {
                        variables.helpers.rememberPage();
                        showLoginModal();
                    };
                    window.signupToComment = function () {
                        variables.helpers.rememberPage();
                        clickSignupButton();
                    };
                    $('[data-toggle="popover"]').popover();
                });
            },

            leaveManifestoMarker: function (value) {
                value = value || 1;
                window.localStorage && localStorage.setItem('willSignManifesto', value);
            },

            removeManifestoMarker: function () {
                delete $rootScope.willSignManifesto;
                window.localStorage && localStorage.removeItem('willSignManifesto');
            },

            onManifestoPagePreInit: function (callback) {
                $timeout(function () {
                    var willSignManifesto = window.localStorage && localStorage.getItem('willSignManifesto');
                    if (willSignManifesto) {
                        $rootScope.willSignManifesto = true;
                        var $wait = $interval(function () {
                            if ($rootScope.backendChecked) {
                                $interval.cancel($wait);
                                if ($rootScope.user) return callback && callback(true);
                                variables.helpers.removeManifestoMarker();
                                return callback && callback(false);
                            }
                        }, 100);
                    }
                });
            }

        }

    };

    variables.helpers.onManifestoPagePreInit(); // just starting an observer, just to make sure it's not forgotten.

    return variables;

}]);