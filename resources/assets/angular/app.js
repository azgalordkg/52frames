(function (appSettings) {

  if (!appSettings || !appSettings.apiUrl) return;

  appSettings.app = angular.module('app', ['ui.router', 'ngCookies', 'ngFileUpload', 'bootstrap3-typeahead', 'infinite-scroll', 'ngAnimate'])

    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
      $locationProvider.html5Mode(true);

      $stateProvider
        .state({
          name: 'home',
          url: '/',
          component: 'homeComponent'
        })
        .state({
          name: 'home.resetPassword',
          url: 'password/reset'
        })
        .state({
          name: 'home.cookiesConsent',
          url: 'remove-cookie-consent'
        })
        .state({
          name: 'currentChallenge',
          url: '/challenge',
          component: 'challengeComponent'
        })
        .state({
          name: 'submitToChallenge',
          url: '/submit',
          component: 'submitToChallengeComponent'
        })
        .state({
          name: 'getStarted',
          url: '/get-started',
          component: 'getStartedComponent'
        })
        .state({
          name: 'albumsList',
          url: '/albums',
          component: 'albumsListComponent'
        })
        .state({
          name: 'challengeList',
          url: '/challenges',
          component: 'albumsListComponent'
        })
        .state({
          name: 'challengeTest',
          url: '/challenge',
          component: 'challengeTestComponent'
        })
        .state({
          name: 'futureAlbumsList',
          url: '/albums/future',
          component: 'futureAlbumsListComponent'
        })
        .state({
          name: 'weekChallenge',
          url: '/albums/{pathYear}/{pathWeekTheme}/challenge',
          component: 'weekChallengeComponent'
        })
        .state({
          name: 'albumItem',
          url: '/albums/{pathYear}/{pathWeekTheme}/photo/{pathHandle}',
          component: 'albumItemComponent'
        })
        .state({
          name: 'profilePhoto',
          url: '/photographer/{pathUserId}/photo/{pathPhotoId}',
          component: 'albumItemComponent'
        })
        .state({
          name: 'lovedPhoto',
          url: '/photographer/{pathUserId}/photo/loves/{pathPhotoId}',
          component: 'albumItemComponent'
        })
        .state({
          name: 'weekTheme',
          url: '/albums/{pathYear}/{pathWeekTheme}?filter_by',
          component: 'albumWeekComponent' // weekThemeComponent - old component
        })
        .state({
          name: 'weekTheme.photo',
          url: '/photo/{pathUser}',
          component: 'weekEntryComponent'
        })
        .state({
          name: 'photographer',
          url: '/photographer/{userIdOrShorturl}',
          component: 'photographerComponent'
        })
        .state({
          name: 'photographer.photo',
          url: '/photo/{pathYear}/{pathWeekTheme}',
          component: 'weekEntryComponent'
        })
        .state({
          name: 'about',
          url: '/about',
          component: 'aboutComponent'
        })
        .state({
          name: 'profileTest',
          url: '/profileTest',
          component: 'profileTestComponent'
        })
        .state({
          name: 'profileAboutTest',
          url: '/profileAboutTest',
          component: 'profileAboutTestComponent'
        })
        .state({
          name: 'profileLoveTest',
          url: '/profileLoveTest',
          component: 'profileLoveTestComponent'
        })
        .state({
          name: 'contact',
          url: '/contact',
          component: 'contactComponent'
        })
        .state({
          name: 'faq',
          url: '/faq',
          component: 'faqComponent'
        })
        .state({
          name: 'community-guidelines',
          url: '/community-guidelines',
          component: 'communityGuidelinesComponent'
        })
        .state({
          name: 'terms',
          url: '/terms',
          component: 'termsComponent'
        })
        .state({
          name: 'privacy',
          url: '/privacy',
          component: 'privacyComponent'
        })
        .state({
          name: 'emailLinks',
          url: '/email-links?tokenId',
          component: 'emailLinksComponent'
        })
        .state({
          name: 'emailLinks.dailyEmailNotif',
          url: '/daily-email-notifs',
          component: 'dailyEmailNotifComponent'
        })
        .state({
          name: 'emailLinks.dailyEmailNotif.unsub',
          url: '/unsubscribe',
          component: 'unsubDailyNotifComponent'
        })
        .state({
          name: 'migrationTool',
          url: '/tools/migration',
          component: 'migrationToolComponent'
        })
        .state({
          name: 'error404',
          url: '/404',
          component: 'error404Component'
        })
        .state({
            name: 'digitalcamera',
            url: '/digital-camera',
            component: 'digitalCameraComponent'
        })
        .state({
          name: 'notAdminPage',
          url: '/not-admin',
          component: 'notAdminComponent'
        })
        .state({
          name: 'albumWeek',
          url: '/albumWeek',
          component: 'albumWeekComponent'
        });
      $urlRouterProvider.otherwise('/404');
    }])

    .run(['Boot', '$window', '$transitions', '$location', '$rootScope', 'InterPageHelper',
      function (Boot, $window, $transitions, $location, $rootScope, InterPageHelper) {
        Boot.backendCheck();
        if ($window.unsupportedBrowser) $rootScope.unsupportedBrowser = true;
        var googleAnalyticsID = appSettings.googleAnalyticsID;
        if (googleAnalyticsID) $window.ga('create', googleAnalyticsID, 'auto');
        $transitions.onSuccess({}, function () {
          if (googleAnalyticsID) $window.ga('send', 'pageview', $location.path());
          InterPageHelper.transitionMethods.doResetIfNeeded();
        });
      }
    ])
  ;

})(window.appSettings);