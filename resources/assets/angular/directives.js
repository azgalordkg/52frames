(function (appSettings) {
  if (!appSettings || !appSettings.app) return;

  appSettings.app
    .directive('navbar', function () {
      return {
        restrict: 'E',
        controller: 'navController',
        controllerAs: '$navbar',
        templateUrl: 'angular/pages/common/navbar/template.html'
      };
    })
    .directive('challengeExpired', function () {
      return {
        restrict: 'E',
        controller: 'ChallengeExpiredController',
        templateUrl: 'angular/pages/common/navbar/challenge-expired/template.html'
      };
    })

    .directive('footer', function () {
      return {
        restrict: 'E',
        controller: 'footerController',
        templateUrl: 'angular/pages/common/footer/template.html'
      };
    })

    .directive('loading', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'loadingController',
        templateUrl: 'angular/pages/common/loading/template.html'
      };
    })
    .directive('loadingHolder', function () {
      return {
        restrict: 'A',
        link: function (scope, element) {
          scope.loadingHolder = element;
          element.scope = scope;
        }
      };
    })
    .directive('loadingSpinner', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'LoadingSpinnerController',
        templateUrl: 'angular/pages/common/loading-spinner/template.html'
      };
    })

    .directive('onBackspace', function () {
      return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
          if (event.which === 8) {
            scope.$apply(function () {
              scope.$eval(attrs.onBackspace);
            });
          }
        });
      };
    })
    .directive('onDelKey', function () {
      return function (scope, element, attrs) {
        var onKeyUp = function (event) {
          if (event.which === 46) {
            scope.$apply(function () {
              scope.$eval(attrs.onDelKey);
            });
          }
        };
        angular.element(document).on('keydown keypress', onKeyUp);
        scope.$on('$destroy', function () {
          angular.element(document).off('keydown keypress', onKeyUp);
        });
      };
    })
    .directive('onEscKey', ['$rootScope', function ($rootScope) {
      return function (scope, element, attrs) {
        var onKeyUp = function (event) {
          if ($rootScope.disableDirectives) return;
          if (event.which === 27) {
            event.preventDefault();
            scope.$apply(function () {
              scope.$eval(attrs.onEscKey);
            });
          }
        };
        angular.element(document).on('keydown keypress', onKeyUp);
        scope.$on('$destroy', function () {
          angular.element(document).off('keydown keypress', onKeyUp);
        });
      };
    }])
    .directive('onEnterKey', ['$parse', function ($parse) {
      return function (scope, element, attrs) {
        var onKeyUp = function (event) {
          if (event.shiftKey) return;
          if (event.target !== element[0]) return;
          if (event.which === 13) {
            scope.$apply(function () {
              var fn = $parse(attrs.onEnterKey);
              var dontStopEvent = fn(scope, {$event: event, $element: element[0]});
              if (!dontStopEvent) event.preventDefault();
            });
          }
        };
        angular.element(document).on('keydown keypress', onKeyUp);
        scope.$on('$destroy', function () {
          angular.element(document).off('keydown keypress', onKeyUp);
        });
      };
    }])

    .directive('onRightArrowKey', ['$rootScope', function ($rootScope) {
      return function (scope, element, attrs) {
        var onKeyUp = function (event) {
          if ($rootScope.disableDirectives) return;
          var body = angular.element(document.body)[0];
          if (event.target !== body) return;
          if (event.which === 39) {
            scope.$apply(function () {
              scope.$eval(attrs.onRightArrowKey);
            });
          }
        };
        angular.element(document).on('keydown keypress', onKeyUp);
        scope.$on('$destroy', function () {
          angular.element(document).off('keydown keypress', onKeyUp);
        });
      };
    }])
    .directive('onLeftArrowKey', ['$rootScope', function ($rootScope) {
      return function (scope, element, attrs) {
        var onKeyUp = function (event) {
          if ($rootScope.disableDirectives) return;
          var body = angular.element(document.body)[0];
          if (event.target !== body) return;
          if (event.which === 37) {
            scope.$apply(function () {
              scope.$eval(attrs.onLeftArrowKey);
            });
          }
        };
        angular.element(document).on('keydown keypress', onKeyUp);
        scope.$on('$destroy', function () {
          angular.element(document).off('keydown keypress', onKeyUp);
        });
      };
    }])

    .directive("contenteditable", function () {
      return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {

          function read() {
            var html = element.html();
            html = html.replace(/<br>/gi, "\n");
            html = html.replace(/&lt;/gi, '<');
            html = html.replace(/&gt;/gi, '>');
            html = html.replace(/&nbsp;/gi, ' ');
            ngModel.$setViewValue(html);
          }

          ngModel.$render = function () {
            var value = ngModel.$viewValue;
            value = value.replace(/>/gi, '&gt;');
            value = value.replace(/</gi, '&lt;');
            value = value.replace(/\n/gi, '<br>');
            element.html(value || '');
          };

          element.bind("blur keyup change", function () {
            scope.$apply(read);
          });
        }
      };
    })

    .directive('onElemReady', ['$parse', function ($parse) {
      return {
        restrict: 'A',
        compile: function (element, attrs) {
          var fn = $parse(attrs.onElemReady);
          return function (scope, element) {
            fn(scope, {$element: element});
          };
        }
      };
    }])

    .directive('loginPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'LoginController',
        templateUrl: 'angular/pages/main-features/login/template.html'
      };
    })
    .directive('fbAttachPassword', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'FbAttachPasswordModalController',
        templateUrl: 'angular/pages/main-features/fb-attach-password/template.html'
      };
    })
    .directive('fbConfirmEmailPassPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'FbLoginFoundExistingEmailModalController',
        templateUrl: 'angular/pages/main-features/fb-confirm-email-pass/template.html'
      };
    })

    .directive('forgotPasswordPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'ForgotPasswordController',
        templateUrl: 'angular/pages/main-features/forgot-password/template.html'
      };
    })
    .directive('forgotPassEmailSentPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'ForgotPassEmailSentController',
        templateUrl: 'angular/pages/main-features/forgot-pass-email-sent/template.html'
      };
    })
    .directive('resetPasswordPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'ResetPasswordController',
        templateUrl: 'angular/pages/main-features/reset-password/template.html'
      };
    })

    .directive('signupPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignupController',
        templateUrl: 'angular/pages/main-features/signup/template.html'
      };
    })
    .directive('signupConfirmPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignupConfirmController',
        templateUrl: 'angular/pages/main-features/signup-confirm/template.html'
      };
    })

    .directive('userChoicePage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'userChoiceController',
        templateUrl: 'angular/pages/main-features/user-choice/template.html'
      };
    })

    .directive('signCreateAccount', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'signCreateAccountController',
        templateUrl: 'angular/pages/main-features/sign-create-account/template.html'
      };
    })

    .directive('userChoiceTypePage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'userChoiceTypeController',
        templateUrl: 'angular/pages/main-features/user-choice-type/template.html'
      };
    })

    .directive('signFramerPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'signFramerController',
        templateUrl: 'angular/pages/main-features/sign-framer/template.html'
      };
    })

    .directive('signAcceptPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'signAcceptController',
        templateUrl: 'angular/pages/main-features/sign-accept/template.html'
      };
    })

    .directive('sessionExpired', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SessionExpiredController',
        templateUrl: 'angular/pages/common/session-expired/template.html'
      };
    })

    .directive('signManifestoPage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignManifestoController',
        templateUrl: 'angular/pages/main-features/sign-manifesto/template.html'
      };
    })
    .directive('signManifestoCheckboxes', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignManifestoCheckboxesController',
        templateUrl: 'angular/pages/main-features/sign-manifesto-checkboxes/template.html'
      };
    })
    .directive('signManifestoWhy', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignManifestoWhyController',
        templateUrl: 'angular/pages/main-features/sign-manifesto-why/template.html'
      };
    })
    .directive('signManifestoConfirm', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'SignManifestoConfirmController',
        templateUrl: 'angular/pages/main-features/sign-manifesto-confirm/template.html'
      };
    })

    .directive('cookieReminderMessage', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'CookieReminderMessageController',
        templateUrl: 'angular/pages/common/cookie-reminder-message/template.html'
      };
    })

    .directive('deleteAccount', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'DeleteAccountController',
        templateUrl: 'angular/pages/common/delete-account/template.html'
      };
    })

    .directive('followUnfollow', function () {
      return {
        scope: {
          followCallback: '&',
          unfollowCallback: '&',
          popoverPlacement: '@',
          imFollowing: '=',
          targetUserId: '<'
        },
        restrict: 'E',
        controller: 'FollowUnfollowController',
        templateUrl: 'angular/pages/common/follow-unfollow/template.html'
      };
    })
    .directive('userCity', function () {
      return {
        scope: {
          user: '='
        },
        restrict: 'E',
        controller: 'UserCityController',
        templateUrl: 'angular/pages/common/user-city/template.html'
      };
    })
    .directive('commentView', function () {
      return {
        scope: {
          owner: '<',
          comment: '<',
          isReply: '<',
          showForm: '<',
          isNewComment: '<',
          showCommentHint: '<',
          dontShowReplies: '<',
          dontShowCancelButton: '<',
          commentDeletedCallback: '&',
          cancelCommentCallback: '&',
          newCommentCallback: '&'
        },
        restrict: 'E',
        controller: 'CommentViewController',
        templateUrl: 'angular/pages/main-features/comment-view/template.html'
      };
    })

    .directive('editManifesto', function () {
      return {
        scope: {
          userData: '=',
          successCallback: '&'
        },
        restrict: 'E',
        controller: 'EditManifestoController',
        templateUrl: 'angular/pages/main-features/edit-manifesto/template.html'
      };
    })
    .directive('assignRoles', function () {
      return {
        scope: {
          userData: '='
        },
        restrict: 'E',
        controller: 'AssignRolesController',
        templateUrl: 'angular/pages/main-features/assign-roles/template.html'
      };
    })

    .directive('newAlbum', function () {
      return {
        scope: {
          closeModalCallback: '&'
        },
        restrict: 'E',
        controller: 'NewAlbumController',
        templateUrl: 'angular/pages/main-features/album-new/template.html'
      };
    })

    .directive('countdownTimer', function () {
      return {
        scope: {
          utcTargetTime: '<',
          timeArrivedDisplay: '@',
          timeArrivedCallback: '&',
          saveTimerDataHere: '=?',
          dayOrHrsOnly: '<',
          makeInline: '<',
          black: '='
        },
        transclude: true,
        link: function (scope, elem, attrs, ctrl, $transclude) {
          scope.transcluded = !!elem.find('.transcluded-content').html();
          if (attrs.makeInline) elem.addClass('make-any-elem-inline');
        },
        restrict: 'E',
        controller: 'CountdownTimerController',
        templateUrl: 'angular/pages/common/countdown-timer/template.html'
      };
    })

    .directive('editAlbum', function () {
      return {
        scope: {
          album: '=',
          extraAlbumPath: '@'
        },
        restrict: 'E',
        controller: 'EditAlbumController',
        templateUrl: 'angular/pages/main-features/album-edit/template.html'
      };
    })

    .directive('albumMakeLive', function () {
      return {
        scope: {
          album: '='
        },
        restrict: 'E',
        controller: 'MakeAlbumLive',
        templateUrl: 'angular/pages/main-features/album-make-live/template.html'
      };
    })

    .directive('allowUploadBypass', function () {
      return {
        scope: {
          album: '='
        },
        restrict: 'E',
        controller: 'AllowUploadBypassController',
        templateUrl: 'angular/pages/main-features/album-allow-upload-bypass/template.html'
      };
    })

    .directive('uploadPhoto', function () {
      return {
        scope: {
          album: '=',
          successCallback: '&'
        },
        restrict: 'E',
        controller: 'UploadPhotoController',
        templateUrl: 'angular/pages/main-features/photo-upload/template.html'
      };
    })
    .directive('editPhoto', function () {
      return {
        scope: {
          album: '=',
          photo: '=',
          successCallback: '&',
          isFromProfilePage: '=',
          isFromChallengePage: '=',
          modalClosedCallback: '&',
          modalId: '@'
        },
        restrict: 'E',
        controller: 'EditPhotoController',
        templateUrl: 'angular/pages/main-features/photo-edit/template.html'
      };
    })
    .directive('randomUploadTexts', function () {
      return {
        scope: true,
        restrict: 'E',
        controller: 'randomUploadTextsController',
        templateUrl: 'angular/pages/common/random-upload-texts/template.html'
      };
    })
    .directive('uploadTimerEnded', function () {
      return {
        scope: {
          callback: '&'
        },
        restrict: 'E',
        controller: 'UploadTimerEndedController',
        templateUrl: 'angular/pages/common/upload-timer-ended/template.html'
      };
    })
    .directive('uploadPhotoConfirm', function () {
      return {
        scope: {
          album: '=',
          deleteSuccessCallback: '&'
        },
        restrict: 'E',
        controller: 'UploadPhotoConfirmController',
        templateUrl: 'angular/pages/main-features/photo-upload-confirm/template.html'
      };
    })

    .directive('photo', function () {
      return {
        scope: {
          album: '=',
          photo: '=',
          clickHandler: '&',
          editPhotoCallback: '&',
          isFromProfilePage: '='
        },
        replace: true,
        restrict: 'E',
        controller: 'PhotoController',
        templateUrl: 'angular/pages/main-features/photo-thumbnail/template.html'
      };
    })

    .directive('editPhotoModal', function () {
      return {
        scope: {
          photo: '=',
          sendingPhotoForm: '=',
          uploadme: '=',
          exifItems: '=',
        },
        restrict: 'E',
        controller: 'EditPhotoModalController',
        templateUrl: 'angular/pages/common/edit-photo-modal/template.html'
      }
    })

    .directive("modal", ['$q', function ($q) {
      return {
        restrict: 'E',
        compile: function (element, attrs) {
          return function ($scope, $element) {
            var modalVar = attrs.id || '$modal';
            var $modal = {
              $element: $element,
              hide: function () {
                $element.modal('hide');
              },
              show: function () {
                $element.modal('show');
              }
            };
            if (!$scope[modalVar]) $scope[modalVar] = $modal;
            if (attrs.id) $scope[modalVar].id = attrs.id;
            $scope.$on('$destroy', function () {
              $scope[modalVar].hide();
            });
            $scope[modalVar].show();
          };
        }
      };
    }])
    .directive("modalOnInit", ['$timeout', function ($timeout) {
      return {
        restrict: 'A',
        compile: function (element, attrs) {
          return function ($scope, $element) {
            $timeout(function () {
              if (!$scope.$modal) return;
              $scope.$modal.$element.on('show.bs.modal', function (e) {
                $scope.$eval(attrs.modalOnInit);
              });
            });
          };
        }
      };
    }])
    .directive("modalOnDestroy", ['$timeout', function ($timeout) {
      return {
        restrict: 'A',
        compile: function (element, attrs) {
          return function ($scope, $element) {
            $timeout(function () {
              if (!$scope.$modal) return;
              $scope.$modal.$element.on('hide.bs.modal', function (e) {
                $scope.$eval(attrs.modalOnDestroy);
              });
            });
          };
        }
      };
    }])
    .directive("modalOnBackdropClick", ['$timeout', function ($timeout) {
      return {
        restrict: 'A',
        compile: function (element, attrs) {
          return function ($scope, $element) {
            $timeout(function () {
              if (!$scope.$modal) return;
              $(document).click(function (e) {
                if (e.target === $scope.$modal.$element[0] && $('body').hasClass('modal-open')) {
                  $scope.$eval(attrs.modalOnBackdropClick);
                }
              });
            });
          };
        }
      };
    }])
    .directive('onFinishRender', ['$timeout', '$parse', function ($timeout, $parse) {
      return {
        restrict: 'A',
        link: function (scope, element, attr) {
          if (scope.$last === true) {
            $timeout(function () {
              scope.$emit('ngRepeatFinished');
              if (!!attr.onFinishRender) {
                $parse(attr.onFinishRender)(scope);
              }
            });
          }
          if (!!attr.onStartRender) {
            if (scope.$first === true) {
              $timeout(function () {
                scope.$emit('ngRepeatStarted');
                if (!!attr.onStartRender) {
                  $parse(attr.onStartRender)(scope);
                }
              });
            }
          }
        }
      }
    }])
    .directive('submitPhoto2', function () {
      return {
        restrict: 'E',
        scope: {
          uploadme: '=',
          currentAlbum: '=',
          file: '=',
          sendingPhotoForm: '=',
          shouldFileBeHere: '=',
          isFileChanging: '=',
          ifFileEditing: '=',
        },
        controller: 'SubmitPhoto2Controller',
        templateUrl: 'angular/pages/common/submit-photo-2/template.html'
      };
    })
    .directive('submitPhoto3', function () {
      return {
        restrict: 'E',
        scope: {
          currentAlbum: '=',
        },
        controller: 'SubmitPhoto3Controller',
        templateUrl: 'angular/pages/common/submit-photo-3/template.html'
      };
    })
    .directive('allowUploadPhoto', function () {
      return {
        restrict: 'E',
        scope: {
          currentAlbum: '=',
        },
        controller: 'AllowUploadPhotoController',
        templateUrl: 'angular/pages/common/allow-upload-photo/template.html'
      };
    })
    .directive('allowUploadPhoto2', function () {
      return {
        restrict: 'E',
        scope: {
          currentAlbum: '=',
          file: '=',
          uploadme: '='
        },
        controller: 'AllowUploadPhoto2Controller',
        templateUrl: 'angular/pages/common/allow-upload-photo-2/template.html'
      };
    })
    .directive('allowUploadPhoto3', function () {
      return {
        restrict: 'E',
        scope: {
          currentAlbum: '='
        },
        controller: 'AllowUploadPhoto3Controller',
        templateUrl: 'angular/pages/common/allow-upload-photo-3/template.html'
      };
    })
})(window.appSettings);