(function (appSettings) {
    if (!appSettings || !appSettings.app) return;

    appSettings.app
        .component('homeComponent', {
            controller: 'homeController',
            templateUrl: 'angular/pages/main-features/home/template.html'
        })
        .component('challengeComponent', {
            controller: 'challengeController',
            templateUrl: 'angular/pages/common/challenge/template.html'
        })
        .component('submitToChallengeComponent', {
            controller: 'submitToChallengeController',
            templateUrl: 'angular/pages/common/submit/template.html'
        })
        .component('getStartedComponent', {
            controller: 'getStartedController',
            templateUrl: 'angular/pages/main-features/get-started/template.html'
        })
        .component('albumsListComponent', {
            controller: 'albumsListController',
            templateUrl: 'angular/pages/main-features/albums-list/template.html'
        })

        .component('challengeTestComponent', {
            controller: 'challengeTestController',
            templateUrl: 'angular/pages/main-features/challenge-test/template.html'
        })

        .component('futureAlbumsListComponent', {
            controller: 'futureAlbumsListController',
            templateUrl: 'angular/pages/main-features/albums-list-future/template.html'
        })
        .component('weekThemeComponent', {
            controller: 'weekThemeController',
            templateUrl: 'angular/pages/main-features/week-theme/template.html'
        })
        .component('weekChallengeComponent', {
            controller: 'weekChallengeController',
            templateUrl: 'angular/pages/main-features/week-challenge/template.html'
        })
        .component('weekEntryComponent', {
            controller: 'weekEntryController',
            templateUrl: 'angular/pages/main-features/week-entry/template.html'
        })
        .component('photographerComponent', {
            controller: 'photographerController',
            templateUrl: 'angular/pages/main-features/photographer/template.html'
        })
        .component('aboutComponent', {
            controller: 'aboutController',
            templateUrl: 'angular/pages/main-features/about/template.html'
        })
        .component('profileTestComponent', {
            controller: 'profileTestController',
            templateUrl: 'angular/pages/main-features/profile-test/template.html'
        })
        .component('profileAboutTestComponent', {
            controller: 'profileAboutTestController',
            templateUrl: 'angular/pages/main-features/profile-about-test/template.html'
        })
        .component('profileLoveTestComponent', {
            controller: 'profileLoveTestController',
            templateUrl: 'angular/pages/main-features/profile-love-test/template.html'
        })
        .component('contactComponent', {
            controller: 'contactController',
            templateUrl: 'angular/pages/main-features/contact/template.html'
        })
        .component('faqComponent', {
            controller: 'faqController',
            templateUrl: 'angular/pages/common/faq/template.html'
        })
        .component('termsComponent', {
            controller: 'termsController',
            templateUrl: 'angular/pages/common/terms/template.html'
        })
        .component('communityGuidelinesComponent', {
            controller: 'communityGuidelinesController',
            templateUrl: 'angular/pages/common/community-guidelines/template.html'
        })
        .component('privacyComponent', {
            controller: 'privacyController',
            templateUrl: 'angular/pages/common/privacy/template.html'
        })
        .component('emailLinksComponent', {
            controller: 'emailLinksController',
            templateUrl: 'angular/pages/common/email-links/template.html'
        })
        .component('dailyEmailNotifComponent', {
            controller: 'dailyEmailNotifController',
            templateUrl: 'angular/pages/common/email-links/daily-email-notifs/template.html'
        })
        .component('unsubDailyNotifComponent', {
            controller: 'unsubDailyNotifController',
            templateUrl: 'angular/pages/common/email-links/daily-email-notifs/unsubscribe/template.html'
        })
        .component('migrationToolComponent', {
            controller: 'MigrationToolController',
            templateUrl: 'angular/pages/system-tools/migration-tool/template.html'
        })
        .component('notAdminComponent', {
          controller: 'notAdminController',
          templateUrl: 'angular/pages/common/not-admin/template.html'
        })
        .component('digitalCameraComponent', {
            controller: 'digitalCameraController',
            templateUrl: 'angular/pages/common/digital-camera/template.html'
        })
        .component('error404Component', {
            controller: 'error404Controller',
            templateUrl: 'angular/pages/common/error-404/template.html'
        })
        .component('albumWeekComponent', {
            controller: 'albumWeekController',
            templateUrl: 'angular/pages/main-features/album-week/template.html'
        })
        .component('albumItemComponent', {
            controller: 'albumItemController',
            templateUrl: 'angular/pages/main-features/album-item/template.html'
        })
    ;

})(window.appSettings);