

// Facebook Login Button

(function (fbSdkLoaded, fbClientId) {
    if (!fbClientId) return;
    window.fbAsyncInit = function () {
        fbSdkLoaded = true;
        window.fbRenderButtons();
        FB.AppEvents.logPageView();
    };
    window.fbRenderButtons = function () {
        if (!fbSdkLoaded) return;
        FB.init({
            appId: fbClientId,
            cookie: true,
            xfbml: true,
            version: 'v3.0'
        });
        FB.Event.subscribe('xfbml.render', function () {
            $('.facebookSpinner').each(function () {
                $(this).removeClass('facebookSpinner');
                this.removeChild(this.childNodes[0]);
            });
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
})(false, $('meta[property="fb:app_id"]').attr('content'));


// Google Analytics Scripts

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    if(!window.appSettings ||! appSettings.googleAnalyticsID) return;
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');