(function (brandingPrefix, configElemSelector) {
    var attrSelector = brandingPrefix + configElemSelector;
    var configHolderElem = $("script[" + attrSelector + "]");
    var appSettings = window.appSettings = configHolderElem.length ? JSON.parse(configHolderElem.attr(attrSelector)) : {};
    appSettings.branding = {prefix: brandingPrefix};
})('52f', '-config');