@extends('layouts.emails')


@section('body')

<p>Problem uploading Photo to {{ $data['source'] }}</p>

<p>Error:<br>{{ $data['error'] }}</p>

<p>Photo Information:<br><pre>{{ json_encode($data['photo'], JSON_PRETTY_PRINT) }}</pre></p>

@endsection