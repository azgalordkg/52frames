@extends('layouts.emails')


@section('body')

<p>Hi <b>{{ $data['fullName'] }}</b>! </p>

<p>You should now be able to upload to the ​album: Week <b>{{ $data['weekNumber'] }}</b>: <b>{{ $data['albumTheme'] }}</b> (20<b>{{ $data['yearLastDigits'] }}</b>)</p>

<p><a href="{{ env('APP_URL') . $data['challengePageUrl'] }}" title="{{ env('APP_URL') . $data['challengePageUrl'] }}" target="_blank">​CLICK HERE TO GO TO THE CHALLENGE PAGE</a>, and hit the submit button ​to upload your entry :)</p>

<p>If you ​have ​already uploaded​ a photo in this album​, <b>​​your entry may ​be removed</b>, ​along with any comments and likes it had.</p>

<p>After your upload, ​you will not be able to upload again.</p>

<p>Please <a href="{{ $data['contactAsLink'] }}" title="{{ $data['contactAsLink'] }}">contact</a> any Admins for assistance if you run into any troubles while trying to upload your photo.</p>

<p>-52Frames Help Squad</p>

@endsection