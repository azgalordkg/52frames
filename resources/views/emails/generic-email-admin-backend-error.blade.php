@extends('layouts.emails')


@section('body')

<p>Backend problem detected:</p>

<pre>{{ json_encode($errors, JSON_PRETTY_PRINT) }}</pre>

@endsection