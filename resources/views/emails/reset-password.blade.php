@extends('layouts.emails')


@section('body')

<p>Don't worry about it, <strong>I forget things all the time</strong>.</p>

<p>Not as bad as my old roommate, though. One time, on his way out to town, <strong>he grabbed a big bag of garbage to take out</strong>. He then walked to the local bus stop to wait for the bus. When the bus pulled up, the driver asked him what was in the bag?</p>

<p><span style="font-style: italic">He looked down and realized he never threw it in the bin.</span></p>

<p>"Garbage," he sheepishly replied.</p>

<p><strong>He sat on that bus for 20 minutes with a big bag of garbage on his lap.</strong></p>

<p>But wait, what was I talking about? Oh yea, your password!</p>

<a href="{{ env('APP_URL') . '/password/reset?token=' . $data['token'] }}">CLICK HERE TO RESET YOUR PASSWORD</a>

<p>And don't forget to wear sunscreen!</p>

@endsection