@extends('layouts.emails')


@section('body')

    <p><strong>The following asset didn't load:</strong></p>

    <table>
        <tr>
            <td>
                <strong>Site:</strong>
            </td>
            <td>
                {{ env('APP_URL') }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Trigger Term:</strong>
            </td>
            <td>
                <pre>{{ json_encode($triggerTerm, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) }}</pre>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Reported Data:</strong>
            </td>
            <td>
                <pre>{{ json_encode($inputs, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) }}</pre>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Target Item:</strong>
            </td>
            <td>
                <pre>{{ json_encode($targetItem, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) }}</pre>
            </td>
        </tr>
    </table>

@endsection