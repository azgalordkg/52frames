@extends('layouts.emails')


@section('body')
    <style>
        .padding-10 {
            padding: 10px;
        }

        .margin-right-10 {
            margin-right: 10px;
        }

        .padding-bottom-5 {
            padding-bottom: 5px;
        }

        .margin-0 {
            margin: 0;
        }
    </style>

    <table cellpadding="5px" width="800px">
        <tr>
            <td>
                <h2>Your Notifications for today:</h2>
            </td>
        </tr>
        @if(isset($organizedNotifs['total_followers']) && $organizedNotifs['for_user']['signed_manifesto'])
            <tr>
                <td>
                    <h2 class="padding-bottom-5 margin-0">
                        <strong>Followers: <strong><a
                                        href="{{ $organizedNotifs['base_url'] }}/photographer/{{ $organizedNotifs['for_user']['handleOrId'] }}">{{ $organizedNotifs['total_followers'] }}</a></strong></strong>
                    </h2>
                    @if(isset($organizedNotifs['grouped']['profile_follow']))
                        @foreach($organizedNotifs['grouped']['profile_follow'] as $follower)
                            <div>
                                <strong>{{ $follower['by_user']['displayName'] }}</strong> has followed you
                            </div>
                        @endforeach
                    @endif
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        @endif
        @if(isset($organizedNotifs['photo_regroup']))
            @foreach($organizedNotifs['photo_regroup'] as $photoData)
                <tr>
                    <td>
                        <a href="{{ $organizedNotifs['base_url'] }}/albums/{{ $photoData['photo']['album']['year'] }}/week-{{ $photoData['photo']['album']['week_number'] }}-{{ $photoData['photo']['album']['shorturl'] }}/photo/{{ $organizedNotifs['for_user']['handleOrId'] }}">
                            <img src="{{ $photoData['photo']['thumbnail_filename'] }}" border="0" height="250">
                        </a>
                        <div class="padding-10">
                            <span class="margin-right-10">{{ count(@$photoData['photo_love'] ?? []) }}
                                new {{ str_plural('like', count(@$photoData['photo_love'] ?? [])) }},</span>
                            <span class="margin-right-10">{{ count(@$photoData['photo_comment'] ?? []) }}
                                new {{ str_plural('comment', count(@$photoData['photo_comment'] ?? [])) }}</span>
                        </div>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td>&nbsp;</td>
            </tr>
        @endif
        @if(isset($organizedNotifs['grouped']['comment_reply']))
            @foreach($organizedNotifs['grouped']['comment_reply'] as $commentReply)
                <tr>
                    <td>
                        <a href="{{ $organizedNotifs['base_url'] }}/albums/{{ $commentReply['involved_model']['album']['year'] }}/week-{{ $commentReply['involved_model']['album']['week_number'] }}-{{ $commentReply['involved_model']['album']['shorturl'] }}/photo/{{ $commentReply['involved_model']['user']['handleOrId'] }}">
                            <img src="{{ $commentReply['involved_model']['thumbnail_filename'] }}" border="0"
                                 height="75" align="left" class="margin-right-10"></img>
                        </a>
                        <div>
                            <strong>{{ $commentReply['by_user']['displayName']  }}</strong> responded to your comment on
                            @if($commentReply['by_user']['id']==$commentReply['involved_model']['user']['id'])
                                their
                            @elseif($commentReply['source_model']['user_id']==$commentReply['involved_model']['user_id'])
                                your
                            @else
                                <strong>{{ $commentReply['involved_model']['user']['displayName'] }}'s</strong>
                            @endif
                            photo.
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>

    <small>By the way, if these emails are too annoying for you, you can always <a
                href="{{ $organizedNotifs['unsubscribeUrl'] }}">Unsubscribe</a> from them.<br>
        If you want to Subscribe again, please contact an admin.</small>

@endsection