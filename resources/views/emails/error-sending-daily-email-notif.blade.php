@extends('layouts.emails')


@section('body')

<p>Problem sending a Daily Notification Email to the following Users:</p>

<pre>{{ json_encode($notifWithError, JSON_PRETTY_PRINT) }}</pre>

@endsection