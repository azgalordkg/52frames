@extends('layouts.master')

@section('content')
    <div class="older-browsers container">
        <div>
            <div>Whoa there! It looks like you are using an "unsupported browser"​!</div>
            <div>What this means is that your <b><strong>"{{ $browser['fullName'] }}"</strong></b> is out-of-date.</div>
            <div>
                <a href="{{ $browser['installLink'] }}" target="_blank">CLICK HERE</a> {{ $browser['installMessage'] }}
            </div>
        </div>
        <script>
            window.unsupportedBrowser = true;
        </script>
    </div>
@endsection
