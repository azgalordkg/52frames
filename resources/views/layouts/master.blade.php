<?php

$debugMode = @$config['debugMode'];
$configJson = json_encode($config,  JSON_UNESCAPED_SLASHES | ($debugMode? JSON_PRETTY_PRINT : 0));
$configJson = $debugMode? preg_replace('/^\s|(})/m', "\t$1", $configJson) : $configJson;

$page = isset($page) ? $page : [];
$page['title'] = isset($page['title']) ? $page['title'] . ' | ' . env('APP_NAME') : 'Welcome to ' . env('APP_NAME');


?><!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="app">
@if(env('FB_CLIENT_ID'))
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
@else
<head>
@endif
    <meta charset="utf-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
@if(env('FB_CLIENT_ID'))
    <meta property="fb:app_id" content="{{ env('FB_CLIENT_ID') }}">
    <meta property="inst:app_id" content="{{ env('INSTAGRAM_CLIENT_ID') }}">
    <meta property="inst:app_redirect" content="{{ env('INSTAGRAM_CLIENT_REDIRECT') }}">
@endif
@isset($page['description'])
    <meta name="description" content="{{ htmlentities($page['description']) }}">
@endisset
    <title>{{ htmlentities($page['title'])  }}</title>
    <link rel="shortcut icon" type="image/png" href="/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Sue+Ellen+Francisco" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    <link rel="preconnect" href="//{{ env('AWS_CLOUDFRONT_CNAME')  }}">
@if(env('GOOGLE_ANALYTICS_ID'))
    <link rel="preconnect" href="//www.google-analytics.com">
@endif
    <link rel="preconnect" href="//fonts.googleapis.com">
    <link rel="preconnect" href="//fonts.gstatic.com">
@if(env('NOCAPTCHA_SITEKEY'))
    <link rel="preconnect" href="//www.gstatic.com">
    <link rel="preconnect" href="//www.google.com">
    <meta property="nocaptcha:sitekey" content="{{ env('NOCAPTCHA_SITEKEY') }}">
@endif
@if(env('FB_CLIENT_ID'))
    <link rel="preconnect" href="//staticxx.facebook.com">
    <link rel="preconnect" href="//connect.facebook.net">
    <link rel="preconnect" href="//static.xx.fbcdn.net">
    <link rel="preconnect" href="//www.facebook.com">
    <link rel="preconnect" href="//fbcdn.net">
    <link rel="preconnect" href="{{ env('APP_URL')  }}">
@if($config['canonicalUrl'])
    <link rel="canonical" href="{{ $config['canonicalUrl'] }}">
@endif
    <base href="/">
@endif
</head>
<body ng-strict-di>


    <div id="wrapper" class="wrapper">
        <div class="overlay_navbar" ng-click="openNavbar()"></div>
        <article 52f-container>
            <navbar></navbar>
            @yield('content')
            <footer></footer>
        </article>
    </div>
    <script src="{{ elixir('js/app.js') }}" 52f-config='<?= $configJson ?>'></script>
</body>
</html>
