# Installation
**1. Docker**
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
```

**2. Docker-compose:**
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo usermod -aG docker $USER
sudo systemctl enable docker
```

**3. Reboot system**

**4. Make project dir**

**5. Clone & run project**
```
git clone --single-branch --branch staging git@git.sibers.com:sibers/52frames.git .
docker-compose up --build
```

Waiting for:
> Starting 52frames-mysql     ... done

> Starting 52frames-webserver ... done

> Starting 52frames-php-fpm   ... done

**6. Open docker console (in new tab)**
```
docker-compose exec php-fpm bash
su dev
```

**7. Run init script (first build only)**
```
./docker_init.sh
```

**8. Check http://127.0.0.1:8080**

**DONE!**

# Regular work with project
Open project dir and run:
```
git pull
docker-compose up --build
```

Open docker console in new tab  
```
docker-compose exec php-fpm bash
su dev
```

**(!) Run all framework commands in docker console**

Migrations:
```
php artisan migrate
```
Build frontend:
```
gulp
```

# Test users:

| email / password      | password               | role         |
|-----------------------|------------------------|--------------|
|test.user@52frames.com | test.user@52frames.com | user         |
|info@52frames.com      | $root!                 | admin        |
