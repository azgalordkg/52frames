#!/usr/bin/env bash

cp .env.docker .env

composer install
php artisan key:generate
php artisan migrate

mysql -hmysql -uhomestead -phomestead homestead < db_from_staging.sql

php artisan passport:keys --force
php artisan passport:install --force

npm install uglify-js
npm i

#gulp watch
#php production-script

chmod -R 777 /application/storage
#php artisan serve
