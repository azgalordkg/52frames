<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/newsletter', 'NewsletterController@store');

Route::post('/signup', 'Auth\RegisterController@apiSignup');
Route::post('/signup/facebook', 'Auth\FacebookAuthController@apiSignup');
Route::post('/signup/instagram', 'Auth\InstagramAuthController@apiSignup');

Route::post('/login', 'Auth\LoginController@apiLogin');
Route::post('/login/facebook', 'Auth\FacebookAuthController@apiLogin');
Route::post('/login/facebook/link', 'Auth\FacebookAuthController@apiLinkAccountToEmailPass');
Route::post('/login/facebook/password', 'Auth\FacebookAuthController@apiAddPasswordToAccount');
Route::post('/login/instagram', 'Auth\InstagramAuthController@apiLogin');
Route::post('/login/instagram/link', 'Auth\InstagramAuthController@apiLinkAccountToEmailPass');
Route::post('/login/instagram/password', 'Auth\InstagramAuthController@apiAddPasswordToAccount');

Route::delete('/logout', 'Auth\LoginController@apiLogout');

Route::post('/password/email', 'Auth\ForgotPasswordController@apiForgotPassword');
Route::post('/password/reset', 'Auth\ResetPasswordController@apiResetPassword');

Route::get('/user', 'UserController@apiMe');
Route::get('/user/suggest', 'UserController@suggestUsers');
Route::get('/user/suggest/photographer', 'UserController@suggestPhotographers');
Route::delete('/user/{id}', 'UserController@destroy');

Route::get('/country', 'CountryController@index');
Route::get('/state', 'StateController@index');
Route::get('/city', 'CityController@index');

Route::post('/manifesto/validate', 'ManifestoController@validateInput');
Route::post('/manifesto', 'ManifestoController@store');
Route::get('/manifesto/check/nickname', 'ManifestoController@checkNickname');
Route::patch('/manifesto/{id}', 'ManifestoController@update');

Route::post('/gears', 'UserGearsController@store');
Route::patch('/gears/{id}', 'UserGearsController@update');
Route::delete('/gears/{id}', 'UserGearsController@destroy');

Route::post('/software', 'UserSoftwareController@store');
Route::patch('/software/{id}', 'UserSoftwareController@update');
Route::delete('/software/{id}', 'UserSoftwareController@destroy');

Route::get('/photographer/{idOrShorturl}', 'PhotographerController@show');
Route::get('/photographer/{idOrShorturl}/photos', 'PhotographerController@getPhotos');
Route::get('/photographer/{user}/loves', 'PhotographerController@getLoves');
Route::get('/photographer/{user}/followers', 'PhotographerController@getFollowers');
Route::get('/photographer/{user}/following', 'PhotographerController@getFollowing');

Route::post('/following', 'FollowingController@store');
Route::delete('/following', 'FollowingController@destroy');

Route::get('/role', 'RoleController@index');

Route::post('/user-role', 'UserRoleController@store');
Route::delete('/user-role', 'UserRoleController@destroy');

Route::get('/album', 'AlbumController@index');
Route::get('/album/future', 'AlbumController@futureAlbums');
Route::get('/album/recent', 'AlbumController@getRecentAlbums');
Route::get('/album/{idOrWeekUrl}', 'AlbumController@show');
Route::post('/album', 'AlbumController@store');
Route::patch('/album/{id}', 'AlbumController@update');
Route::delete('/album/{id}', 'AlbumController@destroy');
Route::post('/album/{id}/make_live', 'AlbumController@makeLive');
Route::post('/album/{id}/make_public', 'AlbumController@makePublic');
Route::get('/album/check/shorturl', 'AlbumController@checkShorturl');
Route::get('/album/{album}/tags', 'AlbumController@tags');
Route::get('/album/{album}/photographers', 'AlbumController@photographers');

Route::get('/bypass-upload', 'BypassUploadController@index');
Route::post('/bypass-upload', 'BypassUploadController@store');
Route::delete('/bypass-upload', 'BypassUploadController@destroy');

Route::get('/exif-item', 'ExifController@index');

Route::get('/photo', 'PhotoController@index');
Route::post('/photo', 'PhotoController@store');
Route::post('/photo/compression-result', 'PhotoController@compressionFeedback');
Route::patch('/photo/{id}', 'PhotoController@update');
Route::get('/photo/suggest_filename', 'PhotoController@suggestFilename');
Route::get('/photo/{idOrUserHandle}', 'PhotoController@show');
Route::post('/photo/{id}/love', 'PhotoController@heartPhoto');
Route::delete('/photo/{id}/love', 'PhotoController@unHeartPhoto');
Route::delete('/photo/{id}', 'PhotoController@destroy');

Route::get('/comment', 'CommentController@index');
Route::post('/comment', 'CommentController@store');
Route::patch('/comment/{id}', 'CommentController@update');
Route::delete('/comment/{id}', 'CommentController@destroy');

Route::patch('/settings', 'UserSettingsController@update');
Route::post('/settings/avatar', 'UserSettingsController@uploadAvatar');
Route::post('/settings/background', 'UserSettingsController@uploadBackground');

Route::prefix('integrations')->group(function () {
    Route::prefix('mailchimp')->group(function () {
        Route::get('/', 'Integrations\MailchimpController@getRequest');
        Route::post('/', 'Integrations\MailchimpController@postRequest');
    });
});

Route::prefix('analytics')->group(function () {
    Route::post('/entry/{term}', 'Analytics\MainAnalyticsController@saveEntry');

    Route::prefix('asset')->group(function () {
        Route::post('/didnt_load/{term}', 'Analytics\StaticAssetController@assetFailedToLoad');
    });
});

Route::prefix('tools')->group(function () {
    Route::prefix('migration')->group(function () {
        Route::prefix('csv')->group(function () {
            Route::get('/', 'Tools\MigrationTool\CsvFileController@index');
            Route::post('/', 'Tools\MigrationTool\CsvFileController@store');
            Route::get('/{id}', 'Tools\MigrationTool\CsvFileController@show');
            Route::delete('/{id}', 'Tools\MigrationTool\CsvFileController@destroy');

            Route::post('/columns', 'Tools\MigrationTool\ColumnSelectionController@store');

            Route::patch('/validation/summary', 'Tools\MigrationTool\LongPollingValidationSummary@index');

            Route::get('/rows', 'Tools\MigrationTool\CsvRowController@index');
            Route::post('/migrate/row', 'Tools\MigrationTool\CsvMigrateController@migrateRow');
            Route::post('/migrate/all-valid', 'Tools\MigrationTool\CsvMigrateController@migrateAllValid');

            Route::post('/utils/city_db_search/submit_spreadsheet', 'Tools\MigrationTool\Utilities\CityNamesOnlyDbScanner@readSpreadsheetFile');
            Route::post('/utils/city_db_search/chosen_worksheet', 'Tools\MigrationTool\Utilities\CityNamesOnlyDbScanner@chosenWorksheet');
            Route::post('/utils/city_db_search/get_city_matches', 'Tools\MigrationTool\Utilities\CityNamesOnlyDbScanner@startCityMatching');
        });
    });
    Route::prefix('php')->group(function () {
        Route::get('info', 'Tools\Php\Info@show');
    });
});
