<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Carbon::setWeekStartsAt(Carbon::SUNDAY);
Carbon::setWeekEndsAt(Carbon::SATURDAY);

//Route::get('phpinfo', function(){
//    echo phpinfo();
//});

//Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

Route::group(['prefix' => 'laravel-filemanager'], function () {
    Route::any('/');
    Route::any('/crop');
    Route::any('/cropimage');
    Route::any('/cropnewimage');
    Route::any('/delete');
    Route::any('/deletefolder');
    Route::any('/demo');
    Route::any('/doresize');
    Route::any('/download');
    Route::any('/errors');
    Route::any('/files/{base_path}/{file_name}');
    Route::any('/folders');
    Route::any('/jsonitems');
    Route::any('/newfolder');
    Route::any('/photos/{base_path}/{image_name}');
    Route::any('/rename');
    Route::any('/resize');
    Route::any('/upload');
});

Route::group(['prefix' => '_debugbar'], function () {
    Route::any('/open');
    Route::any('/clockwork/{id}');
    Route::any('/assets/stylesheets');
    Route::any('/assets/javascript');
});

Route::get('{all}', 'MainController@allPages')->where('all', '.*');
