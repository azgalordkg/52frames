FROM phpdockerio/php72-fpm:latest
WORKDIR "/application"

# Fix debconf warnings upon build
ARG DEBIAN_FRONTEND=noninteractive

# Install selected extensions and other stuff
RUN apt-get update

RUN apt-get -y --no-install-recommends install \
    php-memcached php7.2-mysql php-xdebug php7.2-bcmath php7.2-bz2 php7.2-gd php-imagick php7.2-imap php7.2-intl php7.2-phpdbg php7.2-xsl php-yaml \
    libpng-dev

# Install git
RUN apt-get -y install git

# Install node and npm
RUN apt-get -y install nodejs npm

# Install mysql client
RUN apt-get -y install mysql-client

# Install gulp
RUN npm install --global gulp-cli
RUN cd /application && \
    npm install gulp

# clean apt cache
RUN apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*


# Install composer
RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls && \
    mv composer.phar /usr/local/bin/composer

# Create user
RUN groupadd dev -g 999
RUN useradd dev -g dev -d /home/dev -m
RUN usermod -s /bin/bash dev
RUN passwd -d dev
RUN echo "dev ALL=(ALL) ALL" > /etc/sudoer
