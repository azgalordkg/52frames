const elixir = require('laravel-elixir');

elixir(function (mix) {

    mix.sass('app.scss');

    elixir.config.sourcemaps = false;

    let customScripts = [
        'resources/assets/js/vendor/angular-bootstrap3-typeahead.js',
        'resources/assets/js/pick-up-config-json.js',
        'resources/assets/js/clean-frontend-console.js',
        'resources/assets/js/third-party-scripts.js'
    ];

    if (elixir.config.production) {

        mix.scripts(customScripts, 'public/js/app.js');

    } else {

        mix.copy('resources/assets/angular/**/*.html', 'public/angular');

        mix.scriptsIn('resources/assets/angular');

        let vendorScripts = [
            'resources/assets/js/vendor/jquery-3.2.1.min.js',
            'resources/assets/js/vendor/bootstrap-3.3.7.min.js',
            'resources/assets/js/vendor/slick.min.js',
            'resources/assets/js/vendor/bootstrap3-typeahead.min.js',
            'resources/assets/js/vendor/angularjs-1.6.10.min.js',
            'resources/assets/js/vendor/angular-ui-router-1.0.3.min.js',
            'resources/assets/js/vendor/angular-cookies.min.js',
            'resources/assets/js/vendor/ng-infinite-scroll.min.js',
            'resources/assets/js/vendor/ng-file-upload/ng-file-upload-all.min.js',
            'resources/assets/js/vendor/angular-google-analytics.min.js',
            './node_modules/angular-animate/angular-animate.min.js',
            'resources/assets/js/vendor/jquery.time-to.min.js',
            'resources/assets/js/vendor/jquery.fancybox.min.js'
        ];

        mix.scripts(vendorScripts, 'public/js/vendors.js');

        mix.scripts([
            './public/js/vendors.js'
        ].concat(customScripts).concat([
            './public/js/all.js'
        ]), 'public/js/app.js');

    }

    mix.version([
        'css/app.css',
        'js/app.js'
    ]);

});