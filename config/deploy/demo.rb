# The server-based syntax can be used to override options:
# ------------------------------------
server '10.1.1.57',
  user: 'jenkins',
  roles: %w{web app},
  ssh_options: {
    forward_agent: false,
    auth_methods: %w(publickey password)
  }
  set :deploy_to, '/var/www/html/52frames/demo'
  set :branch, "staging"
  set :file_permissions_groups, ["phpteam"]


after   "laravel:migrate", "deploy:load_fixtures"