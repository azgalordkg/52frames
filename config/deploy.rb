# config valid for current version and patch releases of Capistrano
lock "~> 3.10.0"

set :application, "52frames"
set :repo_url, "git@git.sibers.com:sibers/52frames.git"

# The version of laravel being deployed
set :laravel_version, 5.4

# Default value for keep_releases is 5
set :keep_releases, 3

# The user that the server is running under (used for ACLs)
set :laravel_server_user, 'apache'

# laravel-standard edition directories
append :linked_dirs,
    'storage/app',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs'

# Remove app_dev.php during deployment, other files in web/ can be specified here
set :controllers_to_clear, []

# asset management
set :assets_install_path, "public"
set :assets_install_flags, '--symlink'

# Default value for :linked_files is []
append :linked_files, '.env'

# Whether to upload the dotenv file on deploy
set :laravel_upload_dotenv_file_on_deploy, false

# Set correct permissions between releases, this is turned off by default
set :laravel_5_acl_paths, [
  "bootstrap/cache",
  "storage",
  "storage/app",
  "storage/app/public",
  "storage/framework",
  "storage/framework/cache",
  "storage/framework/sessions",
  "storage/framework/views",
  "storage/logs"
]

set :composer_install_flags, '--no-interaction --quiet --optimize-autoloader'

set :deploy_public_dir, "web"

namespace :deploy do
    task :touch_params do
        on roles(:app) do
            linked_files(shared_path).each do |file|
                unless test "[ -f #{file} ]"
                    execute(:touch, "#{file}")
                end
            end
        end
    end
    task :rewrite_params do
        stage = fetch(:stage)
        on roles(:app) do
            within "#{release_path}" do
                execute(:cp, ".env.#{stage}",".env")
            end
        end
    end
    task :create_symlink_to_web do
        on roles(:app) do
            execute(:ln, "-sf", "#{current_path}/public", "#{deploy_to}/#{fetch(:deploy_public_dir)}")
        end
    end
    task :load_fixtures do
        on roles(:app) do
             within "#{release_path}" do
                 execute(:php, "artisan db:seed")
             end
        end
    end
end

namespace :front do
    desc 'Install Dependencies'
    task :install_npm do
        on roles(:app) do
            execute("cd '#{release_path}'; npm install --non-interactive --no-progress")
        end
    end

    task :install_gulp do
        on roles(:app) do
            execute("cd '#{release_path}'; npm install gulp")
        end
    end

    desc 'Build App'
    task :build do
        on roles(:app) do
            execute("cd '#{release_path}'; node_modules/.bin/gulp")
        end
    end
end

before 'deploy:check:linked_files',    'deploy:touch_params'
before 'composer:run',                 'deploy:rewrite_params'
before 'deploy:starting',              'laravel:resolve_linked_dirs'
before 'deploy:starting',              'laravel:resolve_acl_paths'
after  'deploy:starting',              'laravel:ensure_linked_dirs_exist'
before 'deploy:updating',              'laravel:ensure_acl_paths_exist'
before 'deploy:updated',               'deploy:set_permissions:acl'
after  'composer:run',                 'laravel:storage_link'
after  'composer:run',                 'laravel:optimize'
after  'composer:run',                 'laravel:migrate'
after  'laravel:migrate',              'deploy:create_symlink_to_web'
after  'deploy:create_symlink_to_web', 'front:install_npm'
after  'front:install_npm',            'front:install_gulp'
after  'front:install_gulp',           'front:build'