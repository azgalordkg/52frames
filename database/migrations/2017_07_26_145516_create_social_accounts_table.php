<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_accounts', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->string('provider_user_id');
            $table->string('provider');
            $table->string('avatar_small')->nullable();
            $table->string('avatar_big')->nullable();
            $table->string('profile_url')->nullable();
            $table->string('gender')->nullable();
            $table->string('locale')->nullable();
            $table->integer('timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_accounts');
    }
}
