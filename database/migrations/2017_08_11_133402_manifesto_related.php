<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ManifestoRelated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifesto_answers', function (Blueprint $table) {
            $table->bigInteger('user_id');
            $table->boolean('commitment_understanding')->default(false);
            $table->boolean('being_constant_understanding')->default(false);
            $table->boolean('creative_process_understanding')->default(false);
            $table->boolean('vulnerability_understanding')->default(false);
            $table->boolean('non_perfection_understanding')->default(false);
            $table->boolean('understanding_to_release_the_notion_of_perfection')->default(false);
            $table->boolean('understanding_for_the_sake_of_consistency')->default(false);
            $table->text('why')->nullable();
            $table->timestamps();
        });
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('current_streak')->default(0);
            $table->string('photowalks_level')->nullable();
            $table->string('photography_level')->nullable();
            $table->string('camera_type')->nullable();
            $table->string('profile_photo')->nullable();
            $table->text('shortbio')->nullable();
            $table->timestamps();
        });
        Schema::create('user_gears', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('type');
            $table->string('value');
            $table->timestamps();
        });
        Schema::create('user_software', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('value');
            $table->timestamps();
        });
        Schema::create('user_external_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('provider');
            $table->string('handle')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_external_pages');
        Schema::dropIfExists('user_software');
        Schema::dropIfExists('user_gears');
        Schema::dropIfExists('user_profiles');
        Schema::dropIfExists('manifesto_answers');
    }
}
