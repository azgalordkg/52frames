<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('city_id')->nullable();
            $table->boolean('dont_share_location')->default(false);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('handle')->unique()->nullable();
            $table->boolean('display_as_handle')->default(false);
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
//            $table->date('birthday')->nullable();
            $table->text('how_did_hear')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        DB::update("ALTER TABLE users AUTO_INCREMENT = 1000;");

        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id');
            $table->bigInteger('state_id')->nullable();
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('state_member_term')->nullable();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('users');
    }
}
