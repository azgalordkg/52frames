<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationsRelated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->boolean('photo_censor_show_always')->default(false);
            $table->boolean('notif_emails_daily_dont_receive')->default(false);
            $table->timestamps();
        });
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('sent')->default(false);
            $table->string('keyword');
            $table->bigInteger('for_user_id');
            $table->string('source_model_type');
            $table->bigInteger('source_model_id');
            $table->bigInteger('by_user_id');
            $table->string('related_model_type');
            $table->bigInteger('related_model_id');
            $table->string('involved_model_type')->nullable();
            $table->bigInteger('involved_model_id')->nullable();
            $table->text('target_action_json')->nullable();
            $table->timestamps();
            $table->index(['sent', 'keyword']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('user_settings');
    }
}
