<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotoRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('album_id');
            $table->boolean('uploaded_to_dropbox')->default(false);
            $table->boolean('uploaded_to_cdn')->default(false);
            $table->boolean('staff_pick')->default(false);
            $table->bigInteger('num_views')->default(0);
            $table->bigInteger('num_loves')->default(0);
            $table->bigInteger('num_comments')->default(0);
            $table->string('title')->nullable();
            $table->text('notes_from_yosef')->nullable();
            $table->text('caption')->nullable();
            $table->boolean('ownership_confirmation');
            $table->boolean('is_graphic')->default(false);
            $table->boolean('has_nudity')->default(false);
            $table->boolean('model_consent')->default(false);
            $table->boolean('qualifies_extra_credit')->default(false);
            $table->boolean('screencast_critique')->default(true);
            $table->string('critique_level')->nullable();
            $table->string('location')->nullable();
            $table->string('original_res_filename');
            $table->string('thumbnail_filename');
            $table->string('hi_res_filename');
            $table->text('tags')->nullable();
            $table->timestamps();
        });
        Schema::create('exif_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->default(0);
            $table->string('type');
            $table->string('group_name')->nullable();
            $table->string('name');
            $table->string('icon')->nullable();
            $table->timestamps();
        });
        Schema::create('photo_exifs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('photo_id');
            $table->bigInteger('exif_item_id');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_exifs');
        Schema::dropIfExists('exif_items');
        Schema::dropIfExists('photos');
    }
}
