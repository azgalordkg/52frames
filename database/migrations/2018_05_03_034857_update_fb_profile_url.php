<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFbProfileUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->text('avatar_small')->nullable()->change();
            $table->text('avatar_big')->nullable()->change();
            $table->text('profile_url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->string('avatar_small')->nullable()->change();
            $table->string('avatar_big')->nullable()->change();
            $table->string('profile_url')->nullable()->change();
        });
    }
}
