<?php

use App\User;
use App\CsvUploadedFile;
use App\Database\Schema;
use App\Database\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportPrevYearsAndRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_uploaded_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->makeForeignKey(User::class);
            $table->boolean('processing')->default(false);
            $table->string('category');
            $table->string('filename');
            $table->text('header_columns');
            $table->text('header_columns_with_samples')->nullable();
            $table->bigInteger('records_found')->default(0);
            $table->text('columns_selected')->nullable();
            $table->text('validation_summary')->nullable();
            $table->timestamps();
        });
        Schema::create('csv_uploaded_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('file_id')->makeForeignKey(CsvUploadedFile::class);
            $table->boolean('cancelled')->default(false);
            $table->boolean('migrated')->default(false);
            $table->unsignedBigInteger('migrated_to_user_id')->nullable();
            $table->unsignedBigInteger('album_id')->nullable();
            $table->unsignedBigInteger('photo_id')->nullable();
            $table->text('row_data');
            $table->text('validation_report')->nullable();
            $table->timestamps();
        });
        Schema::table('photos', function (Blueprint $table) {
            $table->unsignedBigInteger('imported_from_row_id')->after('id')->nullable();
            $table->text('imported_info')->after('imported_from_row_id')->nullable();
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->unsignedBigInteger('imported_from_row_id')->after('id')->nullable();
            $table->text('imported_info')->after('imported_from_row_id')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('imported_from_row_id')->after('id')->nullable();
            $table->text('imported_info')->after('imported_from_row_id')->nullable();
            $table->boolean('import_claimed')->after('imported_info')->default(false);
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->string('keyword')->after('access_level_points')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTableColumns('roles', ['keyword']);
        Schema::dropTableColumns('users', ['imported_from_row_id', 'imported_info', 'import_claimed']);
        Schema::dropTableColumns('albums', ['imported_from_row_id', 'imported_info']);
        Schema::dropTableColumns('photos', ['imported_from_row_id', 'imported_info']);
        Schema::dropIfExists('csv_uploaded_rows');
        Schema::dropIfExists('csv_uploaded_files');
    }
}
