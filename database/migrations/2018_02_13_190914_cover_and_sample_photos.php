<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoverAndSamplePhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->bigInteger('cover_photo_id')->after('id')->nullable();
            $table->text('cover_photo_data')->after('cover_photo_id')->nullable();
            $table->bigInteger('sample_photo_id')->after('cover_photo_data')->nullable();
            $table->text('sample_photo_data')->after('sample_photo_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn([
                'cover_photo_id',
                'cover_photo_data',
                'sample_photo_id',
                'sample_photo_data'
            ]);
        });
    }
}
