<?php

use App\Database\Schema;
use App\Database\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_photos', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->enum('type', ['avatar', 'background']);
            $table->unsignedBigInteger('imported_from_row_id')->nullable();
            $table->text('imported_info')->nullable();
            $table->string('original_res_filename');
            $table->string('thumbnail_filename');
            $table->string('hi_res_filename');
            $table->boolean('uploaded_to_dropbox')->default(false);
            $table->boolean('uploaded_to_cdn')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_photos');
    }
}
