<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WufooDumpCompressPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wufoo_dump_compress_photos_config', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key');
            $table->text('value')->nullable();
            $table->timestamps();
        });
        Schema::create('wufoo_dump_compress_photos_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('processing')->default(false);
            $table->text('processing_error')->nullable();
            $table->boolean('completed')->default(false);
            $table->string('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wufoo_dump_compress_photos_list');
        Schema::dropIfExists('wufoo_dump_compress_photos_config');
    }
}
