<?php

use App\Database\Schema;
use App\Database\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlbumScoreField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->unsignedTinyInteger('album_score')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->dropColumn('album_score');
        });
    }
}
