<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlbumRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('top1_photo_id')->nullable();
            $table->bigInteger('top2_photo_id')->nullable();
            $table->bigInteger('top3_photo_id')->nullable();
            $table->bigInteger('top4_photo_id')->nullable();
            $table->bigInteger('top5_photo_id')->nullable();
            $table->bigInteger('top6_photo_id')->nullable();
            $table->bigInteger('patreon_photo_id')->nullable();
            $table->bigInteger('flier_photo_id')->nullable();
            $table->boolean('publicly_visible')->default(false);
            $table->boolean('live')->default(false);
            $table->integer('year');
            $table->integer('week_number');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('theme_title');
            $table->string('shorturl');
            $table->text('blurb')->nullable();
            $table->text('short_blurb')->nullable();
            $table->string('extra_credit_title')->nullable();
            $table->text('extra_credit_body')->nullable();
            $table->text('tips_body')->nullable();
            $table->text('tags')->nullable();
            $table->text('upload_custom_msg')->nullable();
            $table->timestamps();
        });
        Schema::create('bypass_album_uploads', function (Blueprint $table) {
            $table->bigInteger('album_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bypass_album_uploads');
        Schema::dropIfExists('albums');
    }
}
