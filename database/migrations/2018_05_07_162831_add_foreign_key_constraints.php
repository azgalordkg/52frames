<?php

use App\Album;
use App\AlbumSort;
use App\BypassAlbumUpload;
use App\City;
use App\Comment;
use App\Country;
use App\Database\Blueprint;
use App\Database\Schema;
use App\ExifItem;
use App\Follower;
use App\Love;
use App\ManifestoAnswer;
use App\Notification;
use App\Photo;
use App\PhotoExif;
use App\Role;
use App\Session;
use App\SocialAccount;
use App\State;
use App\User;
use App\UserExternalPage;
use App\UserGear;
use App\UserProfile;
use App\UserRole;
use App\UserSetting;
use App\UserSoftware;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('album_sorts', function (Blueprint $table) {
            $table->setTableModel(AlbumSort::class);
            $table->makeForeignKeyWithPreClean('album_id', Album::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('bypass_album_uploads', function (Blueprint $table) {
            $table->setTableModel(BypassAlbumUpload::class);
            $table->makeForeignKeyWithPreClean('album_id', Album::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->setTableModel(City::class);
            $table->makeForeignKeyWithPreClean('country_id', Country::class);
            $table->makeForeignKeyWithPreClean('state_id', State::class);
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->setTableModel(Comment::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
            $table->makeForeignKeyWithPreClean('photo_id', Photo::class);
            $table->makeForeignKeyWithPreClean('album_id', Album::class);
            $table->makeForeignKeyWithPreClean('reply_to_comment_id', Comment::class);
        });
        Schema::table('exif_items', function (Blueprint $table) {
            $table->setTableModel(ExifItem::class);
            $table->makeForeignKeyWithPreClean('parent_id', ExifItem::class);
        });
        Schema::table('followers', function (Blueprint $table) {
            $table->setTableModel(Follower::class);
            $table->makeForeignKeyWithPreClean('follower_user_id', User::class);
            $table->makeForeignKeyWithPreClean('following_user_id', User::class);
        });
        Schema::table('loves', function (Blueprint $table) {
            $table->setTableModel(Love::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
            $table->makeForeignKeyWithPreClean('photo_id', Photo::class);
            $table->makeForeignKeyWithPreClean('album_id', Album::class);
        });
        Schema::table('manifesto_answers', function (Blueprint $table) {
            $table->setTableModel(ManifestoAnswer::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->setTableModel(Notification::class);
            $table->makeForeignKeyWithPreClean('for_user_id', User::class);
            $table->makeForeignKeyWithPreClean('by_user_id', User::class);
        });
        Schema::table('photo_exifs', function (Blueprint $table) {
            $table->setTableModel(PhotoExif::class);
            $table->makeForeignKeyWithPreClean('photo_id', Photo::class);
            $table->makeForeignKeyWithPreClean('exif_item_id', ExifItem::class);
        });
        Schema::table('photos', function (Blueprint $table) {
            $table->setTableModel(Photo::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
            $table->makeForeignKeyWithPreClean('album_id', Album::class);
        });
        Schema::table('sessions', function (Blueprint $table) {
            $table->setTableModel(Session::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->setTableModel(SocialAccount::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('states', function (Blueprint $table) {
            $table->setTableModel(State::class);
            $table->makeForeignKeyWithPreClean('country_id', Country::class);
        });
        Schema::table('user_external_pages', function (Blueprint $table) {
            $table->setTableModel(UserExternalPage::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('user_gears', function (Blueprint $table) {
            $table->setTableModel(UserGear::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->setTableModel(UserProfile::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('user_roles', function (Blueprint $table) {
            $table->setTableModel(UserRole::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
            $table->makeForeignKeyWithPreClean('role_id', Role::class);
        });
        Schema::table('user_settings', function (Blueprint $table) {
            $table->setTableModel(UserSetting::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
        Schema::table('user_software', function (Blueprint $table) {
            $table->setTableModel(UserSoftware::class);
            $table->makeForeignKeyWithPreClean('user_id', User::class);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeignKeys('album_sorts', [
            'album_id',
            'user_id',
        ]);
        Schema::dropForeignKeys('bypass_album_uploads', [
            'album_id',
            'user_id'
        ]);
        Schema::dropForeignKeys('cities', [
            'country_id',
            'state_id'
        ]);
        Schema::dropForeignKeys('comments', [
            'user_id',
            'photo_id',
            'album_id',
            'reply_to_comment_id',
        ]);
        Schema::dropForeignKeys('exif_items', [
            'parent_id'
        ]);
        Schema::dropForeignKeys('followers', [
            'follower_user_id',
            'following_user_id'
        ]);
        Schema::dropForeignKeys('loves', [
            'user_id',
            'photo_id',
            'album_id',
        ]);
        Schema::dropForeignKeys('manifesto_answers', [
            'user_id'
        ]);
        Schema::dropForeignKeys('notifications', [
            'for_user_id',
            'by_user_id'
        ]);
        Schema::dropForeignKeys('photo_exifs', [
            'photo_id',
            'exif_item_id',
        ]);
        Schema::dropForeignKeys('photos', [
            'user_id',
            'album_id'
        ]);
        Schema::dropForeignKeys('sessions', [
            'user_id'
        ]);
        Schema::dropForeignKeys('social_accounts', [
            'user_id'
        ]);
        Schema::dropForeignKeys('states', [
            'country_id'
        ]);
        Schema::dropForeignKeys('user_external_pages', [
            'user_id'
        ]);
        Schema::dropForeignKeys('user_gears', [
            'user_id',
        ]);
        Schema::dropForeignKeys('user_profiles', [
            'user_id',
        ]);
        Schema::dropForeignKeys('user_roles', [
            'user_id',
            'role_id'
        ]);
        Schema::dropForeignKeys('user_settings', [
            'user_id'
        ]);
        Schema::dropForeignKeys('user_software', [
            'user_id'
        ]);
    }
}