<?php

use App\AlbumSort;
use App\Comment;
use App\ExifItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeysToUnsignedBigint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // based on previous migration files
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('city_id')->nullable()->change();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('country_id')->change();
            $table->unsignedBigInteger('state_id')->nullable()->change();
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        Schema::table('states', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('country_id')->change();
        });
//
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
        });
//
        Schema::table('manifesto_answers', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
        });
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('current_streak')->default(0)->change();
        });
        Schema::table('user_gears', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
        });
        Schema::table('user_software', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
        });
        Schema::table('user_external_pages', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
        });
//
        Schema::table('followers', function (Blueprint $table) {
            $table->unsignedBigInteger('follower_user_id')->change();
            $table->unsignedBigInteger('following_user_id')->change();
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('access_level_points')->change();
        });
        Schema::table('user_roles', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('role_id')->change();
        });
//
        Schema::table('albums', function (Blueprint $table) {
            $table->unsignedBigInteger('top1_photo_id')->nullable()->change();
            $table->unsignedBigInteger('top2_photo_id')->nullable()->change();
            $table->unsignedBigInteger('top3_photo_id')->nullable()->change();
            $table->unsignedBigInteger('top4_photo_id')->nullable()->change();
            $table->unsignedBigInteger('top5_photo_id')->nullable()->change();
            $table->unsignedBigInteger('top6_photo_id')->nullable()->change();
            $table->unsignedBigInteger('patreon_photo_id')->nullable()->change();
            $table->unsignedBigInteger('flier_photo_id')->nullable()->change();
        });
        Schema::table('bypass_album_uploads', function (Blueprint $table) {
            $table->unsignedBigInteger('album_id')->change();
            $table->unsignedBigInteger('user_id')->change();
        });
        Schema::table('photos', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('album_id')->change();
            $table->unsignedBigInteger('num_views')->default(0)->change();
            $table->unsignedBigInteger('num_loves')->default(0)->change();
            $table->unsignedBigInteger('num_comments')->default(0)->change();
        });
        Schema::table('exif_items', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('parent_id')->nullable()->default(NULL)->change();
        });
        $this->updateExifItemsTable();
        Schema::table('photo_exifs', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('photo_id')->change();
            $table->unsignedBigInteger('exif_item_id')->change();
        });
//
        Schema::table('sessions', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->change();
        });
        Schema::table('loves', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('photo_id')->nullable()->change();
            $table->unsignedBigInteger('album_id')->nullable()->change();
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('photo_id')->nullable()->change();
            $table->unsignedBigInteger('album_id')->nullable()->change();
            $table->unsignedBigInteger('reply_to_comment_id')->nullable()->default(NULL)->change();
        });
        $this->updateCommentsTable();
//
        Schema::table('album_sorts', function (Blueprint $table) {
            $table->bigIncrements('id')->first('album_id');
            $table->unsignedBigInteger('album_id')->change();
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL)->change();
        });
        $this->updateAlbumSortsTable();
        Schema::table('user_settings', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->change();
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->unsignedBigInteger('for_user_id')->change();
            $table->unsignedBigInteger('source_model_id')->change();
            $table->unsignedBigInteger('by_user_id')->change();
            $table->unsignedBigInteger('related_model_id')->change();
            $table->unsignedBigInteger('involved_model_id')->nullable()->change();
        });
//
        Schema::table('albums', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('cover_photo_id')->nullable()->change();
            $table->unsignedBigInteger('sample_photo_id')->nullable()->change();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('main_social_account_id')->nullable()->change();
        });
    }

    public function updateExifItemsTable()
    {
        $exifItems = ExifItem::whereParentId(0)->get();
        foreach ($exifItems as $exifItem) {
            $exifItem->parent_id = null;
            $exifItem->save();
        }
    }

    public function updateAlbumSortsTable()
    {
        $sorts = AlbumSort::whereUserId(0)->get();
        foreach ($sorts as $sort) {
            $sort->user_id = null;
            $sort->save();
        }
    }

    public function updateCommentsTable()
    {
        $albums = Comment::whereAlbumId(0)->get();
        foreach ($albums as $album) {
            $album->album_id = null;
            $album->save();
        }
        $replies = Comment::whereReplyToCommentId(0)->get();
        foreach ($replies as $reply) {
            $reply->reply_to_comment_id = null;
            $reply->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('album_sorts', function (Blueprint $table) {
            $table->dropColumn('id');
        });
    }
}
