<?php

use App\Database\Schema;
use App\Database\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Analytics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->timestamps();
        });
        Schema::create('analytics_trigger_terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('creator_id');
            $table->bigInteger('type_id');
            $table->string('term');
            $table->string('db_model');
            $table->string('description')->nullable;
            $table->boolean('requires_login')->default(false);
            $table->boolean('requires_unique_counting')->default(false);
            $table->timestamps();
        });
        Schema::create('analytics_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trigger_term_id');
            $table->bigInteger('user_id');
            $table->string('relative_page_path');
            $table->bigInteger('tracked_item_id');
            $table->text('extra_json')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics_logs');
        Schema::dropIfExists('analytics_trigger_terms');
        Schema::dropIfExists('analytics_types');
    }
}
