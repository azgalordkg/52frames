<?php

use App\Photo;
use App\Traits\HelpsFileUploads;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class RenameAllS3UrlsInDbToUseCloudFront extends Seeder
{

    use HelpsFileUploads;

    protected $s3BaseUrl;
    protected $cloudFrontBaseUrl;

    protected $columnNames = [
        'thumbnail_filename',
        'hi_res_filename'
    ];

    public function __construct()
    {
        $this->s3BaseUrl = $this->generateS3Url();
        $this->cloudFrontBaseUrl = $this->generateCloudFrontUrl();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photos = $this->getPhotosWithS3Urls();

        if($photos->count()) echo "number of photos to fix: " . $photos->count() . "\n";

        $this->replaceUrls($photos);
    }

    protected function getPhotosWithS3Urls()
    {
        return Photo::orWhereColumnsArray($this->columnNames, $this->s3BaseUrl . '%', 'like')->get();
    }

    protected function replaceUrls(Collection $photos)
    {
        foreach ($photos as $photo) {
            echo "  photo id: " . $photo->id . "\n";

            foreach ($this->columnNames as $columnName) {
                $originalUrl = $photo->$columnName;
                $rowContainsS3Url = strpos($originalUrl, $this->s3BaseUrl) !== false;

                if ($rowContainsS3Url) {
                    $newUrl = str_replace($this->s3BaseUrl, $this->cloudFrontBaseUrl, $originalUrl);
                    $photo->$columnName = $newUrl;
                    $photo->save();

                    echo "    columnName: $columnName --> fixed!\n";
                    echo "      original: $originalUrl\n";
                    echo "      current: $newUrl\n";
                }
            }
        }

    }

}