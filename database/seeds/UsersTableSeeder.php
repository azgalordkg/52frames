<?php

use App\Role;
use App\User;
use App\UserRole;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testUserEmail = 'test.user@52frames.com';
        $testUser = User::whereEmail($testUserEmail)->first();
        if (!$testUser) {
            User::create([
                'firstname' => 'Test',
                'lastname' => 'User',
                'email' => $testUserEmail,
                'password' => $testUserEmail
            ]);
        }

        $rootEmail = 'info@52frames.com';
        $admin = User::whereEmail($rootEmail)->first();
        if (!$admin) {
            $admin = User::create([
                'firstname' => '52Frames',
                'lastname' => '',
                'email' => $rootEmail,
                'password' => '$root!'
            ]);
        } else {
            echo "  super admin already exists\n";
        }

        $superAdminRole = Role::whereAccessLevelPoints(1000000)->first();
        $rootIsSuperAdmin = UserRole::whereUserId($admin->id)->whereRoleId($superAdminRole->id)->first();

        if (!$rootIsSuperAdmin) {
            UserRole::create([
                'user_id' => $admin->id,
                'role_id' => $superAdminRole->id,
            ]);
        }
    }
}
