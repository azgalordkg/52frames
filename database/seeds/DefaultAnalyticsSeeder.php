<?php

use App\User;
use App\AnalyticsType;
use App\AnalyticsTriggerTerm;
use Illuminate\Database\Seeder;

class DefaultAnalyticsSeeder extends Seeder
{
    protected $creatorEmail = 'info@52frames.com';

    protected $typesWithTerms = [
        'impression' => [
            [
                'term' => 'photo_view_unique_3secs',
                'db_model' => \App\Photo::class,
                'description' => 'Records the number of Photo views. Tracks logged-in users only, once per photo.',
                'requires_login' => 1,
                'requires_unique_counting' => 1,
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $creator = User::whereEmail($this->creatorEmail)->first();
        foreach ($this->typesWithTerms as $type => $terms) {
            $type = AnalyticsType::firstOrCreate([
                'type' => $type
            ]);
            foreach ($terms as $term) {
                $data = array_merge($term, [
                    'creator_id' => $creator->id,
                    'type_id' => $type->id,
                ]);
                AnalyticsTriggerTerm::firstOrCreate($data);
            }
        }
    }
}
