<?php

use App\ExifItem;
use Illuminate\Database\Seeder;

class ExifItemsSeeder extends Seeder
{
    protected $exifItems = [
        [
            'group_name' => 'about_photo_questions',
            'type' => 'group',
            'name' => 'Shooting and Processing',
            'members' => [
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'Used External Flash'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => '1" Shutter and Longer'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'f1.8 Aperture and Wider'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'Composite Edit'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'HDR'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'Shot with a Phone'
                ],
                [
                    'group_name' => 'shoot_and_proc',
                    'type' => 'boolean',
                    'name' => 'Black & White'
                ]
            ]
        ],
        [
            'group_name' => 'about_photo_questions',
            'type' => 'group',
            'name' => 'Subject Matter',
            'members' => [
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Architecture'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Fine Art'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Street Photography'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Travel'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Action'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Humor'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Nature'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Macro'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Abstract'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Portrait/Self-Portrait'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Landscape'
                ],
                [
                    'group_name' => 'subject_matter',
                    'type' => 'boolean',
                    'name' => 'Still Life'
                ]
            ]
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'array',
            'name' => 'Shutter Speed (Choose the closest option)',
            'members' => [
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/2000th of a second or faster'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/500th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/250th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/125th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/60th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/30th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/15th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/8th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/4th of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1/2nd of a second'
                ],
                [
                    'group_name' => 'shutter',
                    'type' => 'item',
                    'name' => '1 second or slower'
                ]
            ]
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'array',
            'name' => 'Aperture (Choose the closest option)',
            'members' => [
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/1.4'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/1.8'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/2.8'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/3.5'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/4'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/5.6'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/8'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/11'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/16'
                ],
                [
                    'group_name' => 'aperture',
                    'type' => 'item',
                    'name' => 'f/22 and higher'
                ]
            ]
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'array',
            'name' => 'ISO (Choose the closest option)',
            'members' => [
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '100 and below'
                ],
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '200'
                ],
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '400'
                ],
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '800'
                ],
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '1600'
                ],
                [
                    'group_name' => 'iso',
                    'type' => 'item',
                    'name' => '3200 and above'
                ]
            ]
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'text',
            'name' => 'Focal Length'
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'text',
            'name' => 'Camera Manufacturer'
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'text',
            'name' => 'Camera Model'
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'array',
            'name' => 'Lens',
            'members' => [
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Apple Lensbaby'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 10-18mm f/4.5-5.6 IS STM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 10-22mm f/3.5-4.5 USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 11-16mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 135mm f/2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 14mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 15mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 16-35mm f/2.8L'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 16-35mm f/4L'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 17-40mm f/4 L'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 17-55mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 17-85mm f4-5.6 IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 18-55mm f/3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 18-135mm f/3.5-5.6 IS'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 18-200mm f/3.5-5.6 IS'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon EF-M 18-55mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon EF 28-300mm 6L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon EF24-105 f/4L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24-105mm f/3.5-5.6 IS STM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24-70mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24-70mm f/4 IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24-105mm f/4.0L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24mm EF-S f/2.8 STM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24mm f/1.4L'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 24mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 300mm 2.8 L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 35-105mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 35mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 35mm f/2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 400mm Prime'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 40mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 50mm f/1.2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 50mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 50mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 50mm f/2.5 Compact Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 55-200mm f/4.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 55-250mm f/4-5.6 IS'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 60mm f/2.8 Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 70-200mm f/2.8L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 70-200mm f/2.8L USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 70-200mm f/4L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 70-200mm f/4L USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 70-300 f/4-5.6L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 75-300mm f/4-5.6 L'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 80-200mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 85mm f/1.2L USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 85mm f/1.8 USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 100mm f/2.8 Macro USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon 100mm f/2.8L Macro IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon EF 100-400mm f/4.5-5.6 L IS USM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon Lensbaby Velvet 56'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon Petzval 85 Art'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Canon Vintage Vivitar 28-85mm Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fixed / Non interchangable / Cell Phone'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 10-24mm f/4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 12mm Rokinon'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 16-55 f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 16mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 18-135mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 18-55mm f/2.8-4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 23mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 35mm  f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 35mm f/2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 56mm f/1.2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm 90mm f/2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm Rokinon 8mm Fisheye'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm Soligor 75-205 f3.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Fujifilm XF 18mm-135mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Jupiter-9 85mm f/2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Leica Elmarit-M 21mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Leica Vario-summilux'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Lensbaby 56 Velvet'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Lumix G20/F1.7 II'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Minolta 50mm f/1.7'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 10-24mm f/3.5-4.5G ED DX'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 105mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 14-24mm f/2.8G ED'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 16 mm f/2.8 (fisheye)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 16-300 mm f/3.5-6.3'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 16-35mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 17-55mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-105mm f/3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-140mm f/3.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-200 f/3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-300mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-35 f/1.8 Sigma Art'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-55mm f/3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 18-70mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 20-300mm f/3.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 24-120mm f/4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 24-70mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 24-85mm f/3.5-4.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 24mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 28-300mm f/3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 300mm f/4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 35mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 35mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 35mm f/1.8G'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 40mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 5.1-25.5mm, f/1.8-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 50mm 1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 50mm f/1.4g'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 50mm f/1.8d'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 50mm f/1.8g'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 55-200mm f/4-5.6G ED VR II'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 55-300mm f4.5-5.6 DX'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 60mm f/2.8 Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 70-200mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 70-300mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 80-200mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 85mm f/1.4d'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 85mm f/1.4g'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 85mm f/1.8d'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 85mm f/1.8g'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon 85mm f/3.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon Micro 60mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Nikon18.0-55.0mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 12-40 Pro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 12-40mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 14-42mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 25mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 40-150mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 45mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus 50-200mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus Lumix G20/ f/1.7 II'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus M.75-300mm F4.8-6.7 ||'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus Zuiko 12mm f2'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus Zuiko 17mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Olympus Zuiko 45mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Leica DC-Vario-Summilux'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Leica DG SUMMILUX 25mm ASPH f1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic LUMIX G 20/F1.7'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Lumix G Vario 14-140mm ASPH HD f3.5-5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Lumix G Vario 14-42'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Lumix G Vario 14-42mm f3.5-f5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Lumix G Vario 45-200mm f4.0-f5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Lumix G Vario 7-14mm f4.0'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Panasonic Olympus 40-150mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Pentax 18-55mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Pentax 43mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Pentax DA 18-255mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Pentax da 50-200mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Petzval 85 Art'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Quantaray 18-200mm f/3.5-6.3'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Ricoh 28mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Rokinon 14mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Rokinon 24mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Rokinon 8mm fisheye'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Samyang 85mm f/1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 10-20mm f 4-5.6 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 105 f/2.8 Macro (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 105mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 12-24mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 150-600mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 150mm f 2.8 Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 17-50mm f/2.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 17-50mm f/2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 17-55mm 2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 17-70mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 17-70mm f/2.8-4.0 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 18-125mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 18-200mm f/3.5-6.3 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 18-250mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 18-300mm  (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 18-300mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 24-70mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 24mm ART 1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 28-70 f/2.8 (SONY)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 30mm f/1.4 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 35mm f/1.4 Art (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 35mm f/1.4 Art (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 50mm 1.4 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 70-300mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 70-300mm f/4-5.6 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma 70-300mm f/4.5-6 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma APO 150-500mm f/5-6.3 DG OS HSM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma art 18-35mm f/1.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma art 18-35mm f/1.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sigma Art 24-105mm f/1.4 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 14mm 3.1 Samyang'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 16-50mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 18-200mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 18-50mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 18-55mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 24-240mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 24-70mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 28-80mm f/3.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 30mm f/3.5 Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 35 f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 50mm 1.4 Sigma'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 50mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 55-210mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 55mm ZA'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony 90mm Macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony DT 18-250mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony DT 50mm f/1.8 SAM'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony E 16mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony E 35mm f/1.8 OSS'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony E 55-210mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony e16-70mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony lens G'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Lensbaby Composer'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Minolta 28-80mm f/5.6'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Minolta 50mm Macro f/3.5'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Minolta 85 1.4'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Samyang 14mm 3.1 Cine'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Sigma 30mm 2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Sony Zeiss 55 f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 10 - 24mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 11-18mm (Sony)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 150-600mm (Sony)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 16-300mm f/3.5-6.3 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 17-35mm 2.8-4 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 17-50mm f/2.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 17-50mm f/2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 18-200mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 18-270mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 180mm Macro (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 24-70mm f/2.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 28-75mm 2.8 XR Di'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 35mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 55-200mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 60mm Macro (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 70-200 f/2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 70-300 F4/5.6 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 70-300mm (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 70-300mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 90mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron 90mm f/2.8 Macro (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron AF 18-200mm F/3.5-6.3 XR Di-II (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tamron sp 90mm (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina 100mm macro'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina 11-16mm f/2.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina 11-16mm f/2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina 16-50mm f/2.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina AT-X 24-70mm f/2.8 (Canon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Tokina AT-X 24-70mm f/2.8 (Nikon)'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Yongnuo 50mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Zeiss 50mm 1.4 Canon Fit'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Zeiss Batis 85mm'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Zeiss Touit 32mm f/1.8'
                ],
                [
                    'group_name' => 'lens',
                    'type' => 'item',
                    'name' => 'Other'
                ]
            ]
        ],
        [
            'group_name' => 'camera_settings_questions',
            'type' => 'array',
            'name' => 'Flash',
            'members' => [
                [
                    'group_name' => 'flash',
                    'type' => 'item',
                    'name' => 'The flash fired'
                ],
                [
                    'group_name' => 'flash',
                    'type' => 'item',
                    'name' => 'The flash did not fire'
                ]
            ]
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loopThroughExifArray($this->exifItems);
    }

    protected function loopThroughExifArray(array $exitArray, int $parentId = null)
    {
        foreach ($exitArray as $exifItem) {
            $exifItem['parent_id'] = $parentId;
            $exifEntry = $this->findOrAddToDb($exifItem);
            if (isset($exifItem['members'])) {
                $this->loopThroughExifArray($exifItem['members'], $exifEntry->id);
            }
        }
    }

    protected function findOrAddToDb(array $exifItem)
    {
        unset($exifItem['members']);
        $exists = ExifItem::whereName($exifItem['name'])
            ->whereGroupName(@$exifItem['group_name'] ?: null)
            ->whereType($exifItem['type'])
            ->first();
        return $exists ?: ExifItem::create($exifItem);
    }
}
