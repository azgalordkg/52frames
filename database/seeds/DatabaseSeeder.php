<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultRolesSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CoverAndSamplePhotosUploaderSeeder::class);
        $this->call(CardsUploadersSeeder::class);
        $this->call(ExifItemsSeeder::class);
        $this->call(CountriesStatesAndPopularCities::class);
        $this->call(AddUserSettingsRows::class);
        $this->call(CreateAvatarFromSocialAccounts::class);
        $this->call(RenameAllS3UrlsInDbToUseCloudFront::class);
        $this->call(ImportCsvRoles::class);
        $this->call(UpdateCitiesSeeder::class);
        $this->call(DefaultAnalyticsSeeder::class);
    }
}
