<?php

use App\Role;
use Illuminate\Database\Seeder;

class ImportCsvRoles extends Seeder
{
    protected $uploaderRoles = [
        [
            'dynamic' => false,
            'access_level_points' => 90000,
            'display_name' => 'CSV Uploader',
            'keyword' => 'csv_uploader'
        ],
        [
            'dynamic' => false,
            'access_level_points' => 900100,
            'display_name' => 'CSV Data Merger',
            'keyword' => 'csv_data_merger'
        ],
        [
            'dynamic' => false,
            'access_level_points' => 900200,
            'display_name' => 'Imported Data Manager',
            'keyword' => 'csv_data_manager'
        ]
    ];

    protected function getOlderRoles()
    {
        return (new DefaultRolesSeeder())->getRoles();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->processRoles(array_merge(
            $this->getOlderRoles(),
            $this->uploaderRoles
        ));
    }

    protected function processRoles(array $roles)
    {
        foreach ($roles as $roleInfo) {
            $row = $this->findOrAddRole($roleInfo);
            if ($row->keyword) continue;
            $row->keyword = $roleInfo['keyword'];
            $row->save();
        }
    }

    protected function findOrAddRole($role)
    {
        $exists = Role::whereDisplayName($role['display_name'])
            ->whereAccessLevelPoints($role['access_level_points'])
            ->first();

        if ($exists) return $exists;
        else return Role::create($role);
    }
}
