<?php

use App\Role;
use Illuminate\Database\Seeder;

class DefaultRolesSeeder extends Seeder
{
    protected $defaultRoles = [

        // Admin Roles

        [
            'dynamic' => false,
            'access_level_points' => 100000,
            'display_name' => 'Admin',
            'keyword' => 'admin'
        ],
        [
            'dynamic' => false,
            'access_level_points' => 200000,
            'display_name' => 'Committee',
            'keyword' => 'committee'
        ],
        [
            'dynamic' => false,
            'access_level_points' => 1000000,
            'display_name' => 'Super Admin',
            'keyword' => 'super_admin'
        ],


        // Dynamic Roles

        [
            'dynamic' => true,
            'access_level_points' => 1000,
            'display_name' => 'Streaker',
            'keyword' => 'streaker'
        ],
        [
            'dynamic' => true,
            'access_level_points' => 1010,
            'display_name' => 'Power User',
            'keyword' => 'power_user'
        ],
        [
            'dynamic' => true,
            'access_level_points' => 1020,
            'display_name' => 'Senior Critic',
            'keyword' => 'senior_critic'
        ]
    ];

    public function getRoles(): array
    {
        return $this->defaultRoles;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->defaultRoles as $role) {
            $this->findOrAddRole($role);
        }
    }

    protected function findOrAddRole($role)
    {
        $exists = Role::whereDisplayName($role['display_name'])
            ->whereAccessLevelPoints($role['access_level_points'])
            ->first();

        if ($exists) return $exists;
        else return Role::create($role);
    }
}
