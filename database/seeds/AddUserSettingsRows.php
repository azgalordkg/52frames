<?php

use App\User;
use App\UserSetting;
use Illuminate\Database\Seeder;

class AddUserSettingsRows extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::get();
        foreach ($users as $user) {
            UserSetting::findOrCreateRow($user);
        }
    }

}
