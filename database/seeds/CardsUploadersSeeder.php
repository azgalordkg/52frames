<?php

use App\User;
use Illuminate\Database\Seeder;

class CardsUploadersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['fliers', 'patreon'] as $handle) {
            $this->createUser($handle);
        }
    }

    protected function createUser($handle)
    {
        $uploaderEmail = $handle . '@52frames.com';

        $user = User::whereEmail($uploaderEmail)->first();

        if (!$user) {
            User::create([
                'firstname' => '52Frames',
                'lastname' => '',
                'email' => $uploaderEmail,
                'handle' => $handle
            ]);
        } else {
            echo "  $handle uploader user already exists\n";
        }
    }
}
