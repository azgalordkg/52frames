<?php

use App\SocialAccount;
use Illuminate\Database\Seeder;

class CreateAvatarFromSocialAccounts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $socialAccounts = SocialAccount::get();
        foreach ($socialAccounts as $account) {
            if ($account->user && !$account->user->main_social_account_id) {
                $account->user->main_social_account_id = $account->id;
                $account->user->save();
            }
        }
    }

}
