<?php

class UpdateCitiesSeeder extends CountriesStatesAndPopularCities
{
    protected $countries = [
        "American Samoa" => null,
        "Anguilla" => null,
        "Antarctica" => null,
        "Armenia" => null,
        "Aruba" => null,
        "Bermuda" => null,
        "Bouvet Island" => null,
        "British Indian Ocean Territory" => null,
        "Canada" => [
            "states" => [
                "Canada" => null,
                "Jurisdiction" => null,
            ]
        ],
        "Cape Verde" => null,
        "Cayman Islands" => null,
        "Christmas Island" => null,
        "Cocos (keeling) Islands" => null,
        "Congo, The Democratic Republic Of The" => null,
        "Cook Islands" => null,
        "East Timor" => null,
        "Falkland Islands (Malvinas)" => null,
        "Faroe Islands" => null,
        "French Guiana" => null,
        "French Polynesia" => null,
        "French Southern Territories" => null,
        "Gibraltar" => null,
        "Greenland" => null,
        "Guadeloupe" => null,
        "Guam" => null,
        "Heard Island And Mcdonald Islands" => null,
        "Hong Kong" => null,
        "Iran, Islamic Republic Of" => null,
        "Kosovo" => null,
        "Libyan Arab Jamahiriya" => null,
        "Macau" => null,
        "Macedonia, The Former Yugoslav Republic Of" => null,
        "Martinique" => null,
        "Mayotte" => null,
        "Moldova, Republic Of" => null,
        "Montserrat" => null,
        "Netherlands Antilles" => null,
        "New Caledonia" => null,
        "Niue" => null,
        "Norfolk Island" => null,
        "Northern Mariana Islands" => null,
        "Pitcairn" => null,
        "Puerto Rico" => null,
        "Reunion" => null,
        "Saint Helena" => null,
        "Saint Pierre And Miquelon" => null,
        "Scotland" => null,
        "South Georgia And The South Sandwich Islands" => null,
        "Svalbard And Jan Mayen" => null,
        "Taiwan" => null,
        "Tanzania, United Republic Of" => null,
        "Tokelau" => null,
        "Turks And Caicos Islands" => null,
        "United States Minor Outlying Islands" => null,
        "Virgin Islands, British" => null,
        "Virgin Islands, U.S." => null,
        "Wallis And Futuna" => null,
        "Western Sahara" => null,
    ];
}
